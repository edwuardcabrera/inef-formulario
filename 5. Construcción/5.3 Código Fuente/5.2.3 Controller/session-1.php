<?php
	/********************************************************
	Name: session.php
    Version: 0.0.1
	Autor name: Edwuard H. Cabrera Rodríguez
	Modification autor name: Edwuard H. Cabrera Rodríguez
	Creation date: 27/10/2015
	Modification date: 16/04/2016
	Description: PHP file. It verifies session.
	********************************************************/
    
    session_start();
	if( ! isset($_SESSION["idUser"]) ){
	 	session_destroy();
        die(getSessionErrorMessage());
    }
    else if ( ((int) trim($_SESSION["idUser"]))==0 ){
        session_destroy();
		die(getSessionErrorMessage());	
    }
    
    function getSessionErrorMessage()
     {
        $vhtml='<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
		<meta name="description" content="Arsa - Acabados y Recubrimientos S.A de C.V.">
		<meta name="author" content="Bitzero Technologies S.A de C.V.">
		<!-- Title -->
		<title>Arsa (Dashboard)</title>
		<!-- Icon -->
		<link rel="shortcut icon" href="./tools/template-3.2/global/images/favicon.png" type="image/png">
		<!-- Template Styles -->
		<link href="./tools/template-3.2/global/css/style.css" rel="stylesheet">
		<link href="./tools/template-3.2/global/css/ui.css" rel="stylesheet">
		
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesnt work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sidebar-condensed error-page">
		<div class="row">
			<div class="col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
				<div class="error-container">
					<div class="error-main">
						<strong><span id="vmessage1" style="font-size:72px">&nbsp;</span></strong>
						<h3><span id="vmessage2">&nbsp;</span></h3>
					</div>
				</div>
			</div>
		</div>
          
		<div id="vfooterText" class="footer">&nbsp;</div>
		
		<!-- Template JavaScripts -->
		<script src="./tools/jquery-2.2.2/jquery-2.2.2.min.js"></script>
		<script src="./tools/template-3.2/global/plugins/jquery-ui/jquery-ui-1.11.2.min.js"></script>
		<script src="./tools/bootstrap-3.3.7/js/bootstrap.min.js"></script>
		<script src="./tools/template-3.2/global/plugins/typed/typed.js"></script>
		<!-- End Page Script -->
		<script src="./tools/template-3.2/admin/js/layout.js"></script>
		<!-- Customized -->
		<script src="./view/js/menu.js" charset="iso-8859-1"></script>
		<script>
			showErrorFooter();
    
		$("#vmessage1").typed({
			strings: ["Acceso Denegado!"],
			typeSpeed: 200,
			backDelay: 500,
			loop: false,
			contentType: "html",
			loopCount: false,
			callback: function() {
				$(".typed-cursor").css("-webkit-animation", "none").animate({opacity: 0}, 400);
				$("#vmessage2").typed({
					strings: [\'Para ingresar al sistema debe de hacer click en <a href="./frmlogin.php"><strong>Login</strong></a>\'],
					typeSpeed: 1,
					backDelay: 500,
					loop: false,
					contentType: "html", 
					loopCount: false
                });  
            }
        });
		</script>
	</body>
	</html>';
        return $vhtml;
     } 
?>