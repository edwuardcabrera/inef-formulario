<?php
/********************************************************
Name: users.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 18/08/2015
Modification date: 21/07/2017
Description: PHP file. It provides users service rest.
********************************************************/

require_once ('../tools/slim-3.3.0/autoload.php');
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBackendUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBackendUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLBackendUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLBackendUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLUserType.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLUserType.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLUserStatus.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLUserStatus.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLAccessPrivilege.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLAccessPrivilege.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLBackendUserAccessPrivilege.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLBackendUserAccessPrivilege.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLPasswordReset.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLPasswordReset.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLUserApplicant.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLUserApplicant.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLApplicant.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLAssignment.php");



$vapp = new Slim\App();
$vapp->get('/users', function(){
    //Purpose: It gets users data.
    //Limitations: The users data must be registered in database.  
    //Returns: Users data in JSON format.
    
    $vdataResponse=array();
    try{
        $vstatus=0;
        $vfilter ="WHERE c_user.id_user= c_user.id_user ";
        if ( (isset($_GET["txtname"])) && (strcmp(trim($_GET["txtname"]), "")!=0) ){
            $vfilter.="AND c_user.fldname LIKE '%" . trim($_GET["txtname"]) . "%' ";
        }
        if ( (isset($_GET["txtfirstName"])) && (strcmp(trim($_GET["txtfirstName"]), "")!=0) ){
            $vfilter.="AND c_user.fldfirstName LIKE '%" . trim($_GET["txtfirstName"]) . "%' ";
        } 
        if ( (isset($_GET["txtlastName"])) && (strcmp(trim($_GET["txtlastName"]), "")!=0) ){
            $vfilter.="AND c_user.fldlastName LIKE '%" . trim($_GET["txtlastName"]) . "%' ";
        }
        
        session_start();
        $vflUser= new clspFLUser();
		$vflUser->idUser=(int)($_SESSION["idUser"]);
        clspBLUser::queryByIdToDataBase($vflUser);
        if ( $vflUser->userType->idUserType==1 ){
            $vfilter.="AND c_user.id_userType IN(2, 3, 5) ";
            $vflUsers= new clscFLUser();
            $vstatus=clscBLUser::queryToDataBase($vflUsers, $vfilter);
            $vdataResponse["users"]=$vflUsers;
            
            unset($vflUsers);
        }
        else if ( $vflUser->userType->idUserType==2 || $vflUser->userType->idUserType==3 ){
            $vflBackendUser= new clspFLBackendUser();
            $vflBackendUser->idUser=$vflUser->idUser;
            clspBLBackendUser::queryByIdToDataBase($vflBackendUser);
            
            $vfilter.="AND c_backenduser.id_campus= " . $vflBackendUser->campus->idCampus . " ";
            if ( $vflUser->userType->idUserType==2 ){
                $vfilter.="AND c_user.id_userType IN(3, 5) ";     
            }
            else{
                $vfilter.="AND c_user.id_userType=5 ";
            }
            $vflBackendUsers= new clscFLBackendUser();
            $vstatus=clscBLBackendUser::queryToDataBase($vflBackendUsers, $vfilter);
            $vdataResponse["users"]=$vflBackendUsers;
            
            unset($vflBackendUser, $vflBackendUsers);
        }
        $vdataResponse["messageNumber"]=$vstatus;
                
        unset($vstatus, $vfilter, $vflUser);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->get('/userTypes', function(){
    //Purpose: It gets user types data.
    //Limitations: The user types data must be registered in database.  
    //Returns: User types data in JSON format.
    
    $vdataResponse=array();
    try{
        $vfilter="";
        
        session_start();
        $vflUser= new clspFLUser();
		$vflUser->idUser=(int)($_SESSION["idUser"]);
        clspBLUser::queryByIdToDataBase($vflUser);
        if ( $vflUser->userType->idUserType==1 ){
            $vfilter="WHERE id_userType IN(2, 3, 5)";
        }
        else if ( $vflUser->userType->idUserType==2 ){
            $vfilter="WHERE id_userType IN(3, 5)";
        }
        else if ( $vflUser->userType->idUserType==3 ){
            $vfilter="WHERE id_userType IN(5)";
        }
        $vflUserTypes= new clscFLUserType();
        $vstatus=clscBLUserType::queryToDataBase($vflUserTypes, $vfilter);
        if ( $vstatus==1 ){
            $vdataResponse["userTypes"]=$vflUserTypes;
        }
        $vdataResponse["messageNumber"]=$vstatus;
        
        unset($vfilter, $vflUser, $vflUserTypes, $vstatus);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->get('/userStatuses', function(){
    //Purpose: It gets user statuses data.
    //Limitations: The user statuses data must be registered in database.  
    //Returns: User statuses data in JSON format.
    
    $vdataResponse=array();
    try{
        $vflUserStatuses= new clscFLUserStatus();
        $vstatus=clscBLUserStatus::queryToDataBase($vflUserStatuses);
        if ( $vstatus==1 ){
            $vdataResponse["userStatuses"]=$vflUserStatuses;
        }
        $vdataResponse["messageNumber"]=$vstatus;
        
        unset($vfilter, $vflUser, $vflUserStatuses, $vstatus);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->get('/users/{vidUser}', function($vrequest){
    //Purpose: It gets user data.
    //Limitations: The user data must be registered in database.  
    //Returns: User data in JSON format.
    
    $vdataResponse=array();
    try{
        $vflUser= new clspFLUser();
		$vflUser->idUser=(int)(trim($vrequest->getAttribute("vidUser")));
        $vstatus=clspBLUser::queryByIdToDataBase($vflUser);
        if ( $vstatus==1 ){
            if ( $vflUser->userType->idUserType==1 ){
                $vdataResponse["user"]=$vflUser;
            }
            else if ( $vflUser->userType->idUserType==2 || $vflUser->userType->idUserType==3 || $vflUser->userType->idUserType==5 ){
                $vflBackendUser= new clspFLBackendUser();
                $vflBackendUser->idUser=$vflUser->idUser;
                clspBLBackendUser::queryByIdToDataBase($vflBackendUser);
                $vdataResponse["user"]=$vflBackendUser;
                
                unset($vflBackendUser);
            }
            else if ( $vflUser->userType->idUserType==4 ){
                //applicant user
            }
        }
        $vdataResponse["messageNumber"]=$vstatus;
        
        unset($vrequest, $vflUser, $vstatus);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->get('/users/{vidUser}/accessPrivileges', function($vrequest){
    //Purpose: It gets user access privileges data.
    //Limitations: The user access privileges data must be registered in database.  
    //Returns: User access privileges data in JSON format.
    
    $vdataResponse=array();
    try{
        $vflAccessPrivileges= new clscFLAccessPrivilege();
        $vstatus=clscBLAccessPrivilege::queryToDataBase($vflAccessPrivileges);
        if ( $vstatus==1 ){
            array_push($vdataResponse, $vflAccessPrivileges);
            $vidUser=(int)(trim($vrequest->getAttribute("vidUser")));
            if ( $vidUser==0 ){
                session_start();
                if ( isset($_SESSION["idUser"]) ){
                    $vidUser=(int)($_SESSION["idUser"]);
                }
            }
            $vflBackendUserAccessPrivileges= new clscFLBackendUserAccessPrivilege();
            clscBLBackendUserAccessPrivilege::getData($vflBackendUserAccessPrivileges, $vidUser);
            array_push($vdataResponse, $vflBackendUserAccessPrivileges);
            
            unset($vidUser, $vflBackendUserAccessPrivileges);
        }
        $vdataResponse["messageNumber"]=$vstatus;
        
        unset($vrequest, $vflAccessPrivileges);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->get('/users/{vemailAccount}/login', function($vrequest){
    //Purpose: It gets user login status and create a session if exist an user with login data.
    //Limitations: The user login data must be registered in database.
    //Returns: Message number in JSON format.
    
    $vdataResponse=array();
    try{
        session_start();
        $vflUser= new clspFLUser();
		$vflUser->emailAccount=trim($vrequest->getAttribute("vemailAccount"));
        $vstatus=clspBLUser::verifyPasswordByEmailAccountToDataBase($vflUser, trim($_GET["vpassword"]));
        if ( $vstatus==1 ){
            clspBLUser::queryByEmailAccountToDataBase($vflUser);
            if ( (int)(trim($_GET["vtype"]))==0 ){
                clspBLUser::activeInDataBase($vflUser);
                $_SESSION["idUser"]=$vflUser->idUser;
            }
            $vdataResponse["user"]=$vflUser;
            $_SESSION["attemptNumber"]=0;
        }
        else{
            if ( ! isset($_SESSION["attemptNumber"]) ){
                $_SESSION["attemptNumber"]=0;
            }
            if ( $vstatus==0 ){
                if ( clspBLUser::queryByEmailAccountToDataBase($vflUser)==1 ){
                    $_SESSION["attemptNumber"]+=1;
                    if ( $_SESSION["attemptNumber"]>=3 ){
                        clspBLUser::deactiveInDataBase($vflUser);
                        $vstatus=-2;
                        if ( (int)(trim($_GET["vtype"]))==0 ){                        
                            session_destroy();
                        }
                    }
                }
            }
        }
        $vdataResponse["messageNumber"]=$vstatus;
        
        unset($vrequest, $vflUser, $vstatus);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->get('/passwordReset/{vidPasswordReset}/users', function($vrequest){
    //Purpose: It gets password reset data.
    //Limitations: The password reset data must be registered in database.
    //Returns: Password reset data in JSON format.
    
    $vdataResponse=array();
    try{
        $vflPasswordReset= new clspFLPasswordReset();
        $vflPasswordReset->idPasswordReset=trim($vrequest->getAttribute("vidPasswordReset"));
		$vstatus=clspBLPasswordReset::verifyPasswordResetToDataBase($vflPasswordReset);
        if ( $vstatus==1 ){
            $vdataResponse["passwordReset"]=$vflPasswordReset;
        }
        $vdataResponse["messageNumber"]=$vstatus;
        
        unset($vrequest, $vflPasswordReset, $vstatus);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->post('/users', function($vrequest){
    //Purpose: It adds user data.
    //Limitations: The user data must not exist in database.  
    //Returns: Message number or user identifier in JSON format.
    
    $vdataResponse=array();
    try{
        $vbody=$vrequest->getBody();
        $vuser=json_decode($vbody); 
        
        if (isset($vuser->cmbeducationalLevel)) {
            $vfluserApplicant = new clspFLUserApplicant();
            $vfluserApplicant->name = $vuser->txtname;
            $vfluserApplicant->firstName = $vuser->txtfirstName;
            $vfluserApplicant->lastName = $vuser->txtlastName;
            $vfluserApplicant->emailAccount = $vuser->txtemailAccount;
            $vfluserApplicant->movilNumber = $vuser->txttelefono;
            $vfluserApplicant->userType->idUserType = 4;
            $vfluserApplicant->userStatus->idUserStatus = 2;
                       
            $vflApplicant = new clspFLApplicant();
            $vflApplicant->campus->idCampus = $vuser->vcampus;
            $vflApplicant->educationalOffer->assignment->educationalLevel->idEducationalLevel = $vuser->cmbeducationalLevel;
            $vflApplicant->educationalOffer->idEducationalOffer=$vuser->cmbeducationalOffer;
           
            $vflEducationalOffer = new clspFLEducationalOffer();
           
           $vstatus = clspBLUserApplicant::AddUserApplicantDatabase($vfluserApplicant, $vflApplicant, $vflEducationalOffer);
           if ($vstatus == 1) {
                $vdataResponse['educationalOffer'] = $vflEducationalOffer;
            }
        }
        else{
            $vflBackendUserNew= new clspFLBackendUser();
            $vflBackendUserNew->userType->idUserType=(int)($vuser->cmbuserType);
            $vflBackendUserNew->userStatus->idUserStatus=(int)($vuser->cmbuserStatus);
            $vflBackendUserNew->firstName=$vuser->txtfirstName;
            $vflBackendUserNew->lastName=$vuser->txtlastName;
            $vflBackendUserNew->name=$vuser->txtname;
            $vflBackendUserNew->emailAccount=$vuser->txtemailAccount;
            if ( isset($vuser->cmbcampus) ){
                $vflBackendUserNew->campus->idCampus=(int)($vuser->cmbcampus);    
            }
            else{
                session_start();
                $vflBackendUser= new clspFLBackendUser();
                $vflBackendUser->idUser=(int)($_SESSION["idUser"]);
                clspBLBackendUser::queryByIdToDataBase($vflBackendUser);
                $vflBackendUserNew->campus->idCampus=$vflBackendUser->campus->idCampus;
                
                unset($vflBackendUser);
            }
            $vstatus=clspBLBackendUser::addToDataBase($vflBackendUserNew);
            if ( $vstatus==1 ){
                $vdataResponse["idUser"]=$vflBackendUserNew->idUser;
            }
            
            unset($vflBackendUserNew);
        }
        $vdataResponse["messageNumber"]=$vstatus;
        
        unset($vrequest, $vbody, $vuser);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
        $vdataResponse["message"]=$vexception->getMessage();
	}
    echo json_encode($vdataResponse);
});

$vapp->post('/users/{vidUser}/accessPrivileges', function ($vrequest){
    //Purpose: It records user access privileges data.
    //Limitations: The user access privileges data must not be empty.  
    //Returns: Message number in JSON format.
    
    $vdataResponse=array();
    try{
        $vbody=$vrequest->getBody();
        $vuserAccessPrivileges=json_decode($vbody); 
        
        $vflBackendUserAccessPrivileges= new clscFLBackendUserAccessPrivilege();
        for ($vi=0; $vi<count($vuserAccessPrivileges); $vi++){
            $vflBackendUserAccessPrivilege= new clspFLBackendUserAccessPrivilege();
            $vflBackendUserAccessPrivilege->backendUser->idUser=(int)(trim($vrequest->getAttribute("vidUser")));
            $vflBackendUserAccessPrivilege->accessPrivilege->idAccessPrivilege=(int)($vuserAccessPrivileges[$vi]);
            clscBLBackendUserAccessPrivilege::add($vflBackendUserAccessPrivileges, $vflBackendUserAccessPrivilege);
            
            unset($vflBackendUserAccessPrivilege);
        }
        $vdataResponse["messageNumber"]=clscBLBackendUserAccessPrivilege::addToDataBase($vflBackendUserAccessPrivileges);
        
        unset($vrequest, $vbody, $vuserAccessPrivileges, $vflBackendUserAccessPrivileges);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->post('/users/{vemail}/passwordReset', function ($vrequest){
    //Purpose: It records a password reset request of user email account.
    //Limitations: The user email account must be recorded in database.  
    //Returns: Message number in JSON format.
    
    $vdataResponse=array();
    try{
        $vflUser= new clspFLUser();
		$vflUser->emailAccount=trim($vrequest->getAttribute("vemail"));
        $vdataResponse["messageNumber"]=clspBLUser::addPasswordResetRequestToDataBase($vflUser);
                
        unset($vflUser);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->put('/users/{vidUser}', function ($vrequest){
    //Purpose: It updates user data.
    //Limitations: The user data must be exist in database.  
    //Returns: Message number in JSON format.
    
    $vdataResponse=array();
    try{
        $vbody=$vrequest->getBody();
        $vuser=json_decode($vbody);
        
        $vflUser= new clspFLUser();
		$vflUser->idUser=(int)(trim($vrequest->getAttribute("vidUser")));
        $vstatus=clspBLUser::queryByIdToDataBase($vflUser);
        if ( $vstatus==1 ){
            $vflUser->firstName=$vuser->txtfirstName;
            $vflUser->lastName=$vuser->txtlastName;
            $vflUser->name=$vuser->txtname;
            if ( $vflUser->userType->idUserType==1 ){
                $vstatus=clspBLUser::updateInDataBase($vflUser);
            }
            else if ( $vflUser->userType->idUserType==2 || $vflUser->userType->idUserType==3 || $vflUser->userType->idUserType==5 ){
                $vflBackendUser= new clspFLBackendUser();
                $vflBackendUser->idUser=$vflUser->idUser;
                clspBLBackendUser::queryByIdToDataBase($vflBackendUser);
                if ( isset($vuser->cmbuserType) ){
                    $vflBackendUser->userType->idUserType=(int)($vuser->cmbuserType);    
                }
                if ( isset($vuser->cmbuserStatus) ){
                    $vflBackendUser->userStatus->idUserStatus=(int)($vuser->cmbuserStatus);    
                }
                $vflBackendUser->firstName=$vflUser->firstName;
                $vflBackendUser->lastName=$vflUser->lastName;
                $vflBackendUser->name=$vflUser->name;
                if ( isset($vuser->txtemailAccount) ){
                    $vflBackendUser->emailAccount=$vuser->txtemailAccount;
                }
                if ( isset($vuser->cmbcampus) ){
                    $vflBackendUser->campus->idCampus=(int)($vuser->cmbcampus);
                }
                $vstatus=clspBLBackendUser::updateInDataBase($vflBackendUser);
                
                unset($vflBackendUser);
            }
            else if ( $vflUser->userType->idUserType==4 ){
                //applicant user
            }
        }
        $vdataResponse["messageNumber"]=$vstatus;
        
        unset($vrequest, $vbody, $vuser, $vflUser, $vstatus);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->put('/users/{vidUser}/password', function ($vrequest){
    //Purpose: It updates user password datum.
    //Limitations: The user data must be exist in database.  
    //Returns: Message number in JSON format.
    
    $vdataResponse=array();
    try{
        $vbody=$vrequest->getBody();        
        $vpassword=json_decode($vbody); 
        
        $vflUser= new clspFLUser();
        $vflUser->idUser=(int)(trim($vrequest->getAttribute("vidUser")));
        $vdataResponse["messageNumber"]=clspBLUser::updatePasswordInDataBase($vflUser, $vpassword->txtpreviousPassword, $vpassword->txtnewPassword);
        
        unset($vrequest, $vbody, $vpassword, $vflUser);
    }
    catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
    }
    echo json_encode($vdataResponse);
});

$vapp->put('/passwordReset/{vidPasswordReset}/users', function ($vrequest){
    //Purpose: It updates user password datum.
    //Limitations: The user data must be exist in database.  
    //Returns: Message number in JSON format.
    
    $vdataResponse=array();
    try{
        $vbody=$vrequest->getBody();
        $vpasswordReset=json_decode($vbody);
        
        $vflPasswordReset= new clspFLPasswordReset();
        $vflPasswordReset->idPasswordReset=trim($vrequest->getAttribute("vidPasswordReset"));
        $vdataResponse["messageNumber"]=clspBLPasswordReset::updateUserPasswordInDataBase($vflPasswordReset, $vpasswordReset->txtpassword);
        
        unset($vrequest, $vbody, $vpasswordReset, $vflPasswordReset);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->put('/users/{vidUser}/campuses', function ($vrequest){
    //Purpose: It updates user campus datum.
    //Limitations: The user data must be exist in database.  
    //Returns: Message number in JSON format.
    
    $vdataResponse=array();
    try{
        $vbody=$vrequest->getBody();
        $vbackendUser=json_decode($vbody);
        
        $vflBackendUser= new clspFLBackendUser();
        $vflBackendUser->idUser=(int)($vrequest->getAttribute("vidUser"));
        clspBLBackendUser::queryByIdToDataBase($vflBackendUser);
        $vflBackendUser->campus->idCampus=(int)($vbackendUser->cmbcampus);
        $vdataResponse["messageNumber"]=clspBLBackendUser::updateInDataBase($vflBackendUser);
        
        unset($vrequest, $vbody, $vflBackendUser);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->delete('/users/{vidUser}', function ($vrequest){
    //Purpose: It deletes user data.
    //Limitations: The user data must be registered in database.
    //Returns: Message number in JSON format.
    
    $vdataResponse=array();
    try{
        $vflBackendUser= new clspFLBackendUser();
        $vflBackendUser->idUser=(int)($vrequest->getAttribute("vidUser"));
        $vdataResponse["messageNumber"]=clspBLBackendUser::deleteInDataBase($vflBackendUser);
        
        unset($vrequest, $vflBackendUser);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->run();
?>