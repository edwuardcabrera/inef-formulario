<?php
/********************************************************
Name: session-2.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 19/11/2015
Modification date: 19/04/2016
Description: PHP file. It provides session service rest.
********************************************************/

require_once ('../tools/slim-3.3.0/autoload.php');

$vapp = new Slim\App();
$vapp->delete('/sessions', function(){
    //Purpose: It deletes user session.
    //Limitations: None.
    //Returns: Message number in JSON format.
    
    $vdataResponse=array();
    try{
        session_start();
        $vdataResponse["messageNumber"]=1;
        session_destroy();
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->run();
?>