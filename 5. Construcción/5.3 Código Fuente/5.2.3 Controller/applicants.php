<?php
/********************************************************
Name: applicants.php
Version: 0.0.1
Autor name: Alejandro Hernández Guzmán
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 18/06/2017
Modification date: 25/07/2017
Description: PHP file. It provides applicants service rest.
********************************************************/

require_once ('../tools/slim-3.3.0/autoload.php');
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLAssignment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLApplicant.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLAssignment.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLApplicant.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLCampus.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLApplicant.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLUser.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLApplicant.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEducationalOffer.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEducationalOffer.php");


$vapp = new Slim\App();
$vapp->post("/applicants", function ($vrequest){
    //Purpose: It post applicants data.
    //Limitations: The users data must be registered in database.  
    //Returns: Assignments data in JSON format.
    
    $vemailAccount = $_POST["vemailAccount"];
    $vEducationLevel = $_POST['veducationalLevel'];
    $vcampus = $_POST['vcampus'];
    $vEducationalOffer=$_POST['veducationalOffer'];
    $vdataResponse = array();
    try{
        $vApplicant = new clspBLApplicant();
        $vflUser = new clspFLUser();
        $vflUser->emailAccount = $vemailAccount;
        if(clspBLUser::queryByEmailAccountToDataBase($vflUser)){
            $vflApplicant = new clspFLApplicant();
            $vflApplicant->user = $vflUser;
            $vflApplicant->campus->idCampus = $vcampus;
            $vflApplicant->educationalOffer->assignment->educationalLevel->idEducationalLevel = $vEducationLevel;
            $vflApplicant->educationalOffer->idEducationalOffer=$vEducationalOffer;
            $vApplicant->ApplicantGetToDatabase($vflApplicant);
            $vflEducationalOffer = new clspFLEducationalOffer();
            $vflEducationalOffer->assignment->campus->idCampus=$vflApplicant->campus->idCampus;
            $vflEducationalOffer->assignment->educationalLevel->idEducationalLevel=$vflApplicant->educationalOffer->assignment->educationalLevel->idEducationalLevel;
            $vflEducationalOffer->idEducationalOffer=$vflApplicant->educationalOffer->idEducationalOffer;
            $vstatus=clspBLEducationalOffer::queryToDatabase1($vflEducationalOffer,$vflApplicant);
            if($vstatus==1){
                $vdataResponse['educationalOffer'] = $vflEducationalOffer;
            }
        }
        else{
            $vstatus=0;
        }
       $vdataResponse["messageNumber"] = $vstatus;
        
       unset($vrequest, $vflEducationalOffer, $vflApplicant, $vstatus, $vemailAccount, $vEducationLevel, $vcampus);
    }
    catch (Exception $vexception){
        $vdataResponse["messageNumber"] = -100;
    }

    echo json_encode($vdataResponse);
});







$vapp->get('/applicants/{vidUserType}/{vidCampusApplicant}', function($vrequest){
    //Purpose: It gets applicants data.
    //Limitations: The applicants data must be registered in database.  
    //Returns: Applicants data in JSON format.

    $vdataResponse = array();
    try{
        $vstatus = 0;
        $vfilter = "";
        if ((int) ($vrequest->getAttribute("vidUserType")) == 1){
            if ((isset($_GET["txtinicial"])) && (strcmp(trim($_GET["txtinicial"]), "") != 0) && (isset($_GET["txtfinal"])) && (strcmp(trim($_GET["txtfinal"]), "") != 0)) {
                
                $vfilter.="AND p_applicant.fldrequestDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["txtinicial"]))) . " 00:00:00' " . "  AND  '" . date("Y-m-d", strtotime(trim($_GET["txtfinal"]))) . " 23:59:59' " . " ";
            }
            
            if ((isset($_GET["vcampus"])) && (strcmp(trim($_GET["vcampus"]), "") != 0 && $_GET["vcampus"] > 0)){
               
                $vfilter.="AND c_campus.id_campus LIKE '%" . trim($_GET["vcampus"]) . "%' ";
            }
            
            if ((isset($_GET["veducationalLevel"])) && (strcmp(trim($_GET["veducationalLevel"]), "") != 0 && $_GET["veducationalLevel"] > 0)){
               
                $vfilter.="AND c_educationallevel.id_educationalLevel LIKE '%" . trim($_GET["veducationalLevel"]) . "%' ";
            }
             if ((isset($_GET["veducationalOffer"])) && (strcmp(trim($_GET["veducationalOffer"]), "") != 0 && $_GET["veducationalOffer"] > 0)){
               
                $vfilter.="AND c_educationaloffer.id_educationalOffer LIKE '%" . trim($_GET["veducationalOffer"]) . "%' ";
            }
            
        } 
        else{
            if ((isset($_GET["txtinicial"])) && (strcmp(trim($_GET["txtinicial"]), "") != 0) && (isset($_GET["txtfinal"])) && (strcmp(trim($_GET["txtfinal"]), "") != 0)) {
                
                $vfilter.="AND p_applicant.fldrequestDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["txtinicial"]))) . " 00:00:00' " . "  AND  '" . date("Y-m-d", strtotime(trim($_GET["txtfinal"]))) . " 23:59:59' " . " ";
            }
            
            if ((isset($_GET["veducationalLevel"])) && (strcmp(trim($_GET["veducationalLevel"]), "") != 0 && $_GET["veducationalLevel"] > 0)) {
               
                $vfilter.="AND c_educationallevel.id_educationalLevel LIKE '%" . trim($_GET["veducationalLevel"]) . "%' ";
            }
             if ((isset($_GET["veducationalOffer"])) && (strcmp(trim($_GET["veducationalOffer"]), "") != 0 && $_GET["veducationalOffer"] > 0)){
               
                $vfilter.="AND c_educationaloffer.id_educationalOffer LIKE '%" . trim($_GET["veducationalOffer"]) . "%' ";
            }
            $vfilter.=" WHERE c_campus.id_campus=" . (int) ($vrequest->getAttribute("vidCampusApplicant"));
        }
            
        $vflApplicants = new clscFLApplicant();
        $vstatus = clscBLApplicant::queryToDataBase($vflApplicants, $vfilter);
        if ($vstatus == 1) {
            $vdataResponse["applicants"] = $vflApplicants;
        }
        $vdataResponse["messageNumber"] = $vstatus;
        
        unset($vstatus, $vfilter, $vflApplicants);
    }
    catch (Exception $vexception){
        $vdataResponse["messageNumber"] = -100;
    }
    echo json_encode($vdataResponse);
});

$vapp->get('/campus/{vidCampus}/applicants/{vidApplicant}', function($vrequest){
    //Purpose: It get applicants data.
    //Limitations: The applicants data must be registered in database.  
    //Returns: Applicants data in JSON format.

    $vdataResponse = array();
    try{
        $vflApplicant = new clspFLApplicant();
        $vflApplicant->campus->idCampus = (trim($vrequest->getAttribute("vidCampus")));
        $vflApplicant->idApplicant = (trim($vrequest->getAttribute("vidApplicant")));
        $vstatus = clspBLApplicant::queryToDataBase($vflApplicant);
        if ($vstatus == 1) {
            $vdataResponse["applicants"] = $vflApplicant;
        }
        $vdataResponse["messageNumber"] = $vstatus;

        unset($vrequest, $vflApplicant);
    }
    catch (Exception $vexception){
        $vdataResponse["messageNumber"] = -100;
    }
    echo json_encode($vdataResponse);
});

$vapp->run();
?>