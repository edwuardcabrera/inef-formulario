<?php
/********************************************************
Name: emails-scheduled.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 28/06/2017
Modification date: 29/06/201/
Description: PHP file. It sends emails scheduleds.
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLEmailMarketingScheduled.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLEmailMarketingScheduled.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEmailMarketing.php");


try{
    $verrorNumber=0;
    
    date_default_timezone_set('America/Mexico_City');
    $vfilter ="WHERE p_emailmarketingscheduled.fldscheduledDate='" . date("Y-m-d H:i") . ":00' ";
    $vfilter.="AND p_emailmarketing.id_emailMarketingStatus=2 ";
    $vflEmailMarketingScheduleds= new clscFLEmailMarketingScheduled();
    clscBLEmailMarketingScheduled::queryToDataBase($vflEmailMarketingScheduleds, $vfilter);
    $vemailMarketingScheduledsTotal=clscBLEmailMarketingScheduled::total($vflEmailMarketingScheduleds);
    for ($vi=0; $vi<$vemailMarketingScheduledsTotal; $vi++){
        if ( clspBLEmailMarketing::sendToUserApplicants($vflEmailMarketingScheduleds->emailMarketingScheduleds[$vi]->emailMarketing)<=0 ){
            $verrorNumber++;
        }    
    }
    if ( $vemailMarketingScheduledsTotal>0 ){
        if ( $verrorNumber==0 ){
            echo "Los correos electrónicos (" . $vemailMarketingScheduledsTotal . ") programados han sido enviados correctamente.";
        }
        else{
            echo "Ocurrió un error al tratar de enviar los correos electrónicos programados.";
        }
     } 
}
catch (Exception $vexception){
    echo "Ocurrió un error inesperado al tratar de enviar los correos electrónicos programados.";    
}
?>