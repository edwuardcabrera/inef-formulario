<?php
/********************************************************
Name: assignments.php
Version: 0.0.1
Autor name: Sandro Alan G�mez Aceituno.
Modification autor name: Edwuard H. Cabrera Rodr�guez
Creation date: 17/07/2017
Modification date: 25/07/2017
Description: PHP file. It provides assignments service rest.
********************************************************/
require_once ('../tools/slim-3.3.0/autoload.php');
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLEducationalOffer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLEducationalOffer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEducationalOffer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEducationalOffer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLEducationalLevel.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLEducationalLevel.php");

define("cfileUrl", "../others/career-plan/");
$vapp = new Slim\App();
$vapp->get('/educationalOffers/{vidCampusAssignments}', function($vrequest){
    //Purpose: It gets assignments data.
    //Limitations: The assignments must be registered in database.
    //Returns: assignments data in JSON format.
    
    $vdataResponse=array();
    try{
		$vfilter="WHERE c_educationaloffer.id_educationalOffer<>0 ";
		if( (int)($vrequest->getAttribute("vidCampusAssignments"))==0 ){
			if ( (isset($_GET["cmbcampus"])) && (strcmp(trim($_GET["cmbcampus"]), "0")!=0) ){
				$vfilter.=" AND c_campus.id_campus=".trim($_GET["cmbcampus"]);
			}
		}
		else{
			$vfilter.=" AND c_campus.id_campus=".(int)($vrequest->getAttribute("vidCampusAssignments"));
		}
		if ( (isset($_GET["cmbeducationalLeval"])) && (strcmp(trim($_GET["cmbeducationalLeval"]), "0")!=0) ){
			$vfilter.=" AND c_educationallevel.id_educationalLevel=".trim($_GET["cmbeducationalLeval"]);
        }
		
        $vflEducationalOfferes= new clscFLEducationalOffer();
        $vstatus=clscBLEducationalOffer::queryToDataBase($vflEducationalOfferes, $vfilter);
        if ( $vstatus==1 ){
            $vdataResponse["assignments"]=$vflEducationalOfferes;
        }
        $vdataResponse["messageNumber"]=$vstatus;
        
        unset($vflEducationalOfferes, $vstatus);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->get('/campuses/{vidCampus}/educationalLevel/{vidEducationalLevels}/educationalOffer/{vidEducationalOffer}', function ($vrequest){
    // Purpose: It gets educational offers marketing data.
    // Limitations: The educational offers data.
    // Returns: Message number in JSON format.
    
    $vdataResponse=array();
    try{
		session_start();
        $vflEducationalOffer= new clspFLEducationalOffer();
        $vflEducationalOffer->assignment->campus->idCampus=(int)($vrequest->getAttribute("vidCampus"));
        $vflEducationalOffer->assignment->educationalLevel->idEducationalLevel=(int)($vrequest->getAttribute("vidEducationalLevels"));
        $vflEducationalOffer->idEducationalOffer=(int)($vrequest->getAttribute("vidEducationalOffer"));
		
		$vstatus=clspBLEducationalOffer::queryToDataBase($vflEducationalOffer);
        if ( $vstatus==1 ){
            $vdataResponse["educationalOffer"]=$vflEducationalOffer;
        }
        $vdataResponse["messageNumber"]=$vstatus;
                
        unset($vrequest, $vflEducationalOffer);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
		$vdataResponse["message"]=$vexception->getMessage();
	}
    echo json_encode($vdataResponse);
});

$vapp->put('/educationalOffers', function ($vrequest){
    // Purpose: It update name file in assignments.
    // Limitations: The name file in assignment data must be registered in database.
    // Returns: Message number in JSON format.
    
    $vdataResponse=array();
    try {
		$vbody=$vrequest->getBody();
        $veducationalOffer=json_decode($vbody);
		
        $vflEducationalOffer= new clspFLEducationalOffer();        
		if( (int)($veducationalOffer->idCampus) != 0){
			$vflEducationalOffer->assignment->campus->idCampus=$veducationalOffer->idCampus;
		}
		else {
			$vflEducationalOffer->assignment->campus->idCampus=$veducationalOffer->cmbcampusRecord;
		}
		
		$vflEducationalOffer->assignment->educationalLevel->idEducationalLevel=$veducationalOffer->vidEducationalLevel;
		$vflEducationalOffer->idEducationalOffer=$veducationalOffer->idEducationalOffer;
		$vflEducationalOffer->educationalOffer=$veducationalOffer->txteducationalOffer;
		$vflEducationalOffer->fileName="";
		
        $vdataResponse["messageNumber"]=clspBLEducationalOffer::updateInDataBase($vflEducationalOffer);
                
        unset($vrequest, $vflEducationalOffer);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
        $vdataResponse["message"]=$vexception->getMessage();		
	}
    echo json_encode($vdataResponse);
});

$vapp->put('/educationalOffersFile', function ($vrequest){
    // Purpose: It update name file in assignments.
    // Limitations: The name file in assignment data must be registered in database.
    // Returns: Message number in JSON format.
    
    $vdataResponse=array();
    try{	
		$vbody=$vrequest->getBody();
        $veducationalOffer=json_decode($vbody);
			
		
        $vflEducationalOffer= new clspFLEducationalOffer();       
		if( (int)($veducationalOffer->idCampus) != 0){
			$vflEducationalOffer->assignment->campus->idCampus=$veducationalOffer->idCampus;
		}
		else {
			$vflEducationalOffer->assignment->campus->idCampus=$veducationalOffer->cmbcampusRecord;
		}
        $vflEducationalOffer->assignment->educationalLevel->idEducationalLevel=(int)($veducationalOffer->idEducationalLevel);
        $vflEducationalOffer->idEducationalOffer=(int)($veducationalOffer->idEducationalOffer);
        $vflEducationalOffer->fileName=trim($veducationalOffer->fileName);
		
        $vdataResponse["messageNumber"]=clspBLEducationalOffer::updateFileInDataBase($vflEducationalOffer);
                
        unset($vrequest, $vflEducationalOffer);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->post('/educationalOffers', function($vrequest){
    //Purpose: It adds educational Offers setting data.
    //Limitations: The  educational Offers setting data must not exist in database.  
    //Returns: Message number or educational Offers setting identifier in JSON format.
    
    $vdataResponse=array();
    try{
        $vbody=$vrequest->getBody();
        $veducationalOffer=json_decode($vbody);
		
        
        $vflEducationalOffer= new clspFLEducationalOffer();  
		if( (int)($veducationalOffer->idCampus) != 0){
			$vflEducationalOffer->assignment->campus->idCampus=(int)($veducationalOffer->idCampus);
		}
		else {
			$vflEducationalOffer->assignment->campus->idCampus=(int)($veducationalOffer->cmbcampusRecord);
		}		
		$vflEducationalOffer->assignment->educationalLevel->idEducationalLevel=$veducationalOffer->cmbeducationalLevelRecord;
		$vflEducationalOffer->educationalOffer=$veducationalOffer->txteducationalOffer;
		$vflEducationalOffer->fileName="";
		
        $vstatus=clspBLEducationalOffer::addToDataBase($vflEducationalOffer);
        if ( $vstatus==1 ){
            $vdataResponse["idEducationalOffer"]=$vflEducationalOffer->idEducationalOffer;
            $vdataResponse["idEducationalLevel"]=$vflEducationalOffer->assignment->educationalLevel->idEducationalLevel;
            $vdataResponse["idCampus"]=$vflEducationalOffer->assignment->campus->idCampus;
        }
        $vdataResponse["messageNumber"]=$vstatus;
        
        unset($vrequest, $vbody, $veducationalOffer, $vflEducationalOffer, $vstatus);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->get('/educationalLevels', function(){
    //Purpose: It gets educational Levels data.
    //Limitations: The educational Levels must be registered in database.
    //Returns: educational Levels data in JSON format.
    
    $vdataResponse=array();
    try{
        $vflEducationalLevels= new clscFLEducationalLevel();
        $vstatus=clscBLEducationalLevel::queryToDataBase($vflEducationalLevels, $vfilter="");
        if ( $vstatus==1 ){
            $vdataResponse["educationalLevels"]=$vflEducationalLevels;
        }
        $vdataResponse["messageNumber"]=$vstatus;
        
        unset($vflEducationalLevels, $vstatus);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->delete('/campuses/{vidCampus}/educationalLevel/{vidEducationalLevels}/educationalOffers/{vidEducationalOffer}', function ($vrequest){
    // Purpose: It deletes email marketing data.
    // Limitations: The email marketing  data must be registered in database.
    // Returns: Message number in JSON format.
    
    $vdataResponse=array();
    try{
		session_start();
        $vflEducationalOffer= new clspFLEducationalOffer();
        $vflEducationalOffer->assignment->campus->idCampus=(int)($vrequest->getAttribute("vidCampus"));
        $vflEducationalOffer->assignment->educationalLevel->idEducationalLevel=(int)($vrequest->getAttribute("vidEducationalLevels"));
        $vflEducationalOffer->idEducationalOffer=(int)($vrequest->getAttribute("vidEducationalOffer"));
		
        $vdataResponse["messageNumber"]=clspBLEducationalOffer::deleteInDataBase($vflEducationalOffer);
                
        unset($vrequest, $vflEducationalOffer);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
		$vdataResponse["message"]=$vexception->getMessage();
	}
    echo json_encode($vdataResponse);
});

$vapp->delete('/attachFiles/{vfileName}', function ($vrequest){
    //Purpose: It deletes attach file data.
    //Limitations: The attach file data must be registered in database.
    //Returns: Message number in JSON format.
    
    $vdataResponse=array();
    try{
        if ( is_file(cfileUrl . trim($vrequest->getAttribute("vfileName"))) ){
            if ( unlink(cfileUrl . trim($vrequest->getAttribute("vfileName"))) ){
                $vdataResponse["messageNumber"]=1;
            }
            else{
                $vdataResponse["messageNumber"]=0;
            }
		}
        else{
		  $vdataResponse["messageNumber"]=-1;
        }
        
        unset($vrequest);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->run();
?>