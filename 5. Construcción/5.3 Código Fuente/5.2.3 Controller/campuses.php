<?php
/********************************************************
Name: Campuses.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 26/05/2017
Modification date: 25/07/2017
Description: PHP file. It provides campuses service rest.
********************************************************/

require_once ('../tools/slim-3.3.0/autoload.php');
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCampus.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCampus.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLAssignment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLAssignment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLEducationalOffer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLEducationalOffer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEmailMarketing.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEmailMarketing.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLEmailMarketingFile.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLEmailMarketingFile.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEmailMarketingScheduled.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEmailMarketingScheduled.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEmailMarketingFile.php");

define("cfileUrl", "../others/career-plan/");
$vapp = new Slim\App();
$vapp->get('/campuses', function(){
    //Purpose: It gets campuses data.
    //Limitations: The campuses must be registered in database.
    //Returns: Campusess data in JSON format.
    
    $vdataResponse=array();
    try{
        $vflCampuses= new clscFLCampus();
        $vstatus=clscBLCampus::queryToDataBase($vflCampuses);
        if ( $vstatus==1 ){
            $vdataResponse["campuses"]=$vflCampuses;
        }
        $vdataResponse["messageNumber"]=$vstatus;
        
        unset($vflCampuses, $vstatus);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->get('/campuses/{vidCampus}/educationalLevels', function($vrequest){
    //Purpose: It gets assigments data.
    //Limitations: The assigments must be registered in database.
    //Returns: Assigments data in JSON format.
    
    $vdataResponse=array();
    try{
        $vfilter ="WHERE c_assignment.id_campus=" . trim($vrequest->getAttribute("vidCampus")) . " ";
        $vfilter.="AND c_educationallevel.id_educationalLevel<>0";
        $vflAssignments= new clscFLAssignment();
        $vstatus=clscBLAssignment::queryToDataBase($vflAssignments, $vfilter);
        if ( $vstatus==1 ){
            $vdataResponse["assignments"]=$vflAssignments;
        }
        $vdataResponse["messageNumber"]=$vstatus;
        
        unset($vfilter, $vflAssignments, $vstatus);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->get('/campuses/{vidCampus}/educationalLevels/{vidEducationalLevel}/educationalOffers', function($vrequest){
    //Purpose: It gets educational offers data.
    //Limitations: The educational offers must be registered in database.
    //Returns: Educational offers data in JSON format.
    
    $vdataResponse=array();
    try{
        $vfilter ="WHERE c_educationaloffer.id_campus=" . trim($vrequest->getAttribute("vidCampus")) . " ";
        $vfilter.="AND c_educationaloffer.id_educationalLevel=" . trim($vrequest->getAttribute("vidEducationalLevel")) . " ";
        $vfilter.="AND c_educationaloffer.id_educationalOffer<>0";
        $vflEducationalOffers= new clscFLEducationalOffer();
        $vstatus=clscBLEducationalOffer::queryToDataBase($vflEducationalOffers, $vfilter);
        if ( $vstatus==1 ){
            $vdataResponse["educationalOffers"]=$vflEducationalOffers;
        }
        $vdataResponse["messageNumber"]=$vstatus;
        
        unset($vfilter, $vflEducationalOffers, $vstatus);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->get('/campuses/{vidCampus}/emailMarketings/{vidEmailMarketing}', function($vrequest){
    //Purpose: It gets email marketing data.
    //Limitations: The email marketing data must be registered in database.  
    //Returns: Email marketing data in JSON format.
    
    $vdataResponse=array();
    try{
        $vflEmailMarketing= new clspFLEmailMarketing();
		$vflEmailMarketing->campus->idCampus=(int)(trim($vrequest->getAttribute("vidCampus")));
        $vflEmailMarketing->idEmailMarketing=trim($vrequest->getAttribute("vidEmailMarketing"));
        $vstatus=clspBLEmailMarketing::queryByIdsToDataBase($vflEmailMarketing);
        if ( $vstatus==1 ){
            $vdataResponse["emailMarketing"]=$vflEmailMarketing;
            
            $vfilter ="WHERE p_emailmarketingfile.id_campus=" . $vflEmailMarketing->campus->idCampus . " ";
            $vfilter.="AND p_emailmarketingfile.id_emailMarketing='" . $vflEmailMarketing->idEmailMarketing . "'";  
            $vflEmailMarketingFiles= new clscFLEmailMarketingFile();
            clscBLEmailMarketingFile::queryToDataBase($vflEmailMarketingFiles, $vfilter);
            $vdataResponse["emailMarketingFiles"]=$vflEmailMarketingFiles;
            
            $vflEmailMarketingScheduled= new clspFLEmailMarketingScheduled();
            if ( $vflEmailMarketing->emailMarketingType->idEmailMarketingType==2 ){
                $vflEmailMarketingScheduled->emailMarketing=$vflEmailMarketing;
                clspBLEmailMarketingScheduled::queryByIdsToDataBase($vflEmailMarketingScheduled);
            }
            $vdataResponse["emailMarketingScheduled"]=$vflEmailMarketingScheduled;
            
            unset($vfilter, $vflEmailMarketingFiles, $vflEmailMarketingScheduled);
        }
        $vdataResponse["messageNumber"]=$vstatus;
        
        unset($vrequest, $vflEmailMarketing, $vstatus);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->put('/campuses/{vidCampus}/emailMarketings/{vidEmailMarketing}', function ($vrequest){
    //Purpose: It updates email marketing data.
    //Limitations: The email marketing data must be exist in database.  
    //Returns: Message number in JSON format.
    
    $vdataResponse=array();
    try{
        $vbody=$vrequest->getBody();
        $vemailMarketing=json_decode($vbody);
        
        session_start();
        $vflEmailMarketing= new clspFLEmailMarketing();
        $vflEmailMarketing->campus->idCampus=(int)(trim($vrequest->getAttribute("vidCampus")));
        $vflEmailMarketing->idEmailMarketing=trim($vrequest->getAttribute("vidEmailMarketing"));
        $vflEmailMarketing->userRecord->idUser=(int)($_SESSION["idUser"]);
        $vflEmailMarketing->educationalOffer->assignment->campus->idCampus=$vflEmailMarketing->campus->idCampus;
        $vflEmailMarketing->educationalOffer->assignment->educationalLevel->idEducationalLevel=(int)($vemailMarketing->cmbeducationalLevel);
        $vflEmailMarketing->educationalOffer->idEducationalOffer=(int)($vemailMarketing->cmbeducationalOffer);
        $vflEmailMarketing->subject=trim($vemailMarketing->txtsubject);
        $vflEmailMarketing->message=trim($vemailMarketing->txtmessage);
        $vflEmailMarketingFiles= new clscFLEmailMarketingFile();
        for ($vi=0; $vi<count($vemailMarketing->attachedFiles); $vi++){
            $vflEmailMarketingFile= new clspFLEmailMarketingFile();
            $vflEmailMarketingFile->emailMarketing=$vflEmailMarketing;
            $vflEmailMarketingFile->realName=trim($vemailMarketing->attachedFiles[$vi][0]);
            $vflEmailMarketingFile->fisicName=trim($vemailMarketing->attachedFiles[$vi][1]);
                
            clscBLEmailMarketingFile::add($vflEmailMarketingFiles,  $vflEmailMarketingFile);
            unset($vflEmailMarketingFile);
        }
        $vflEmailMarketingScheduled= new clspFLEmailMarketingScheduled();
        if ( isset($vemailMarketing->txtdateScheduled) ){
            $vflEmailMarketingScheduled->emailMarketing=$vflEmailMarketing;
            $vflEmailMarketingScheduled->scheduledDate=$vemailMarketing->txtdateScheduled . " ". $vemailMarketing->txttimeScheduled;
        }
        $vdataResponse["messageNumber"]=clspBLEmailMarketing::updateInDataBase($vflEmailMarketing, $vflEmailMarketingFiles, $vflEmailMarketingScheduled);
        
        unset($vrequest, $vbody, $vemailMarketing, $vflEmailMarketing, $vflEmailMarketingFiles, $vflEmailMarketingScheduled);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->delete('/attachFiles/{vfileName}', function ($vrequest){
    //Purpose: It deletes attach file data.
    //Limitations: The attach file data must be registered in database.
    //Returns: Message number in JSON format.
    
    $vdataResponse=array();
    try{
        if ( is_file(cfileUrl . trim($vrequest->getAttribute("vfileName"))) ){
            if ( unlink(cfileUrl . trim($vrequest->getAttribute("vfileName"))) ){
                $vdataResponse["messageNumber"]=1;
            }
            else{
                $vdataResponse["messageNumber"]=0;
            }
		}
        else{
		  $vdataResponse["messageNumber"]=-1;
        }
        
        unset($vrequest);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->run();
?>