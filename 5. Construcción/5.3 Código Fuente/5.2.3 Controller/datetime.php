<?php 

	date_default_timezone_set('America/Mexico_City');
	
	$tiempo = getdate(time());
	$dia = $tiempo['wday'];
	$dia_mes=$tiempo['mday'];
	$mes = $tiempo['mon'];
	$year = $tiempo['year'];
	$hora= $tiempo['hours'];
	$minutos = $tiempo['minutes'];
	$segundos = $tiempo['seconds']; 
	
	if($mes <10){
		$mes="0".$mes;
	}

	if($dia_mes <10){
		$dia_mes="0".$dia_mes;
	}
	if($hora < 10){
		$hora="0".$hora;
	}

	if($minutos <10){
		$minutos="0".$minutos;
	}

	if($segundos<10){
		$segundos="0".$segundos;
	}

	$fecha= $mes."/".$dia_mes."/".$year. " ". $hora.":".$minutos.":".$segundos;
		
	echo json_encode($fecha);

	unset($tiempo,$dia,$dia_mes,$mes,$year,$hora,$minutos,$segundos);
	
?>