<?php
/********************************************************
Name: upload-file.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 15/08/2015
Modification date: 12/06/2017
Description: PHP file. It uploads file in server.
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/model/tools/clspString.php");

try{
    $vreturn=0;
    $vuploadDirectory = dirname(dirname(__FILE__)) .$_POST["vuploadDirectory"];
    $vuploadFileName = $vuploadDirectory . basename($_FILES['userfile']['name']);
    $vstring= new clspString($vuploadFileName);
    $vfileExtensionType=$vstring->getStringMatch(".", 2, 3);
    if (move_uploaded_file($_FILES['userfile']['tmp_name'], $vuploadFileName)){
        $vbytes=openssl_random_pseudo_bytes(5);
        $vfileName=bin2hex($vbytes) . "." . $vfileExtensionType;
        if ( rename($vuploadFileName, $vuploadDirectory . $vfileName) ){
            $vreturn=$vfileName;
        }
        else{
            unlink($vuploadFileName);
        }
    }
    echo $vreturn;
}
catch (Exception $vexception){
    echo -100;
}
?>