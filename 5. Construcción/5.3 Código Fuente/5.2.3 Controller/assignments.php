<?php
/********************************************************
Name: assignments.php
Version: 0.0.1
Autor name: Sandro Alan G�mez Aceituno.
Modification autor name: Edwuard H. Cabrera Rodr�guez
Creation date: 19/06/2017
Modification date: 27/06/2017
Description: PHP file. It provides assignments service rest.
********************************************************/

require_once ('../tools/slim-3.3.0/autoload.php');
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLAssignment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLAssignment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLAssignment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLAssignment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLEducationalLevel.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLEducationalLevel.php");


$vapp = new Slim\App();
$vapp->get('/assignments/{vidCampusAssignments}', function($vrequest){
    //Purpose: It gets assignments data.
    //Limitations: The assignments must be registered in database.
    //Returns: assignments data in JSON format.
    
    $vdataResponse=array();
    try {
		$vfilter="";
		$vwhereFilter=0;
		if( (int)($vrequest->getAttribute("vidCampusAssignments")) == 0){
			if ( (isset($_GET["cmbcampus"])) && (strcmp(trim($_GET["cmbcampus"]), "0")!=0) ){
				if ( $vwhereFilter == 0){
					$vfilter.=" WHERE c_campus.id_campus=".trim($_GET["cmbcampus"]);
					$vwhereFilter =1;
				}
				else {
					$vfilter.=" AND c_campus.id_campus=".trim($_GET["cmbcampus"]);
				}
			}		
		}
		else {
			$vfilter.=" WHERE c_campus.id_campus=".(int)($vrequest->getAttribute("vidCampusAssignments"));
			$vwhereFilter =1;
		}
		
		if ( (isset($_GET["cmbeducationalLeval"])) && (strcmp(trim($_GET["cmbeducationalLeval"]), "0")!=0) ){
			if ( $vwhereFilter == 0){
				$vfilter.=" WHERE c_educationallevel.id_educationalLevel=".trim($_GET["cmbeducationalLeval"]);
				$vwhereFilter =1;
			}
			else {
				$vfilter.=" AND c_educationallevel.id_educationalLevel=".trim($_GET["cmbeducationalLeval"]);
			}
        }
		
        $vflAssignmentes= new clscFLAssignment();
        $vstatus=clscBLAssignment::queryToDataBase($vflAssignmentes, $vfilter);
        if ( $vstatus==1 ){
            $vdataResponse["assignments"]=$vflAssignmentes;
        }
        $vdataResponse["messageNumber"]=$vstatus;
        
        unset($vflAssignmentes, $vstatus);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->get('/educationalLevels', function(){
    //Purpose: It gets educational Levels data.
    //Limitations: The educational Levels must be registered in database.
    //Returns: educational Levels data in JSON format.
    
    $vdataResponse=array();
    try{
        $vflEducationalLevels= new clscFLEducationalLevel();
        $vstatus=clscBLEducationalLevel::queryToDataBase($vflEducationalLevels, $vfilter="");
        if ( $vstatus==1 ){
            $vdataResponse["educationalLevels"]=$vflEducationalLevels;
        }
        $vdataResponse["messageNumber"]=$vstatus;
        
        unset($vflEducationalLevels, $vstatus);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->put('/campuses/{vidCampus}/educationalLevels/{vidEducationalLevels}/fileName/{vfileName}', function ($vrequest){
    // Purpose: It update name file in assignments.
    // Limitations: The name file in assignment data must be registered in database.
    // Returns: Message number in JSON format.
    
    $vdataResponse=array();
    try{
        $vflAssignment= new clspFLAssignment();
        $vflAssignment->campus->idCampus=(int)($vrequest->getAttribute("vidCampus"));
        $vflAssignment->educationalLevel->idEducationalLevel=(int)($vrequest->getAttribute("vidEducationalLevels"));
        $vflAssignment->fileName=trim($vrequest->getAttribute("vfileName"));
		
        $vdataResponse["messageNumber"]=clspBLAssignment::updateInDataBase($vflAssignment);
                
        unset($vrequest, $vflAssignment);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->run();
?>