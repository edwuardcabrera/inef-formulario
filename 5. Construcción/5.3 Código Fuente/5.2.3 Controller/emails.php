<?php
/********************************************************
Name: emails.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 03/06/2017
Modification date: 11/07/2017
Description: PHP file. It provides emails service rest.
********************************************************/

require_once ('../tools/slim-3.3.0/autoload.php');
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLEmailMarketing.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLEmailMarketing.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLEmailMarketingType.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLEmailMarketingType.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLEmailMarketingStatus.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLEmailMarketingStatus.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEmailMarketing.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEmailMarketing.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBackendUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBackendUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLEmailMarketingFile.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLEmailMarketingFile.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEmailMarketingFile.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEmailMarketingScheduled.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLCampusEMailSetting.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLCampusEmailSetting.php");


define("cfileUrl", "../others/attachments/");

$vapp = new Slim\App();
$vapp->get('/emailMarketings', function(){
    //Purpose: It gets email marketing data.
    //Limitations: The email marketing data must be registered in database.  
    //Returns: email marketing data in JSON format.
    
    $vdataResponse=array();
    try{
        $vstatus=0;
        $vfilter ="";
		$vwhereFilter =0;
		
        if ( (isset($_GET["cmbcampus"])) && (strcmp(trim($_GET["cmbcampus"]), "0")!=0) ){
			if ( $vwhereFilter == 0){
				$vfilter.=" WHERE c_campus.id_campus=".trim($_GET["cmbcampus"]);
				$vwhereFilter =1;
			}
			else {
				$vfilter.=" AND c_campus.id_campus=".trim($_GET["cmbcampus"]);
			}
        }
		if ( (isset($_GET["txtdateRecordBegin"])) && (strcmp(trim($_GET["txtdateRecordBegin"]), "")!=0) ){
			if ( (isset($_GET["txtdateRecordEnd"])) && (strcmp(trim($_GET["txtdateRecordEnd"]), "")!=0) ){
				if ( $vwhereFilter == 0){
					$vfilter.=" WHERE p_emailmarketing.fldrecordDate BETWEEN '". date("Y-m-d", strtotime(trim($_GET["txtdateRecordBegin"])))." 00:00:00'";
                    $vfilter.=" AND '".date("Y-m-d", strtotime(trim($_GET["txtdateRecordEnd"])))." 23:59:59'";
					$vwhereFilter =1;
				}
				else {
					$vfilter.=" AND p_emailmarketing.fldrecordDate BETWEEN '".date("Y-m-d", strtotime(trim($_GET["txtdateRecordBegin"])))." 00:00:00'";
                    $vfilter.=" AND '".date("Y-m-d", strtotime(trim($_GET["txtdateRecordEnd"])))." 23:59:59'";
				}
			}
        }
		if ( (isset($_GET["cmbemailMarketingType"])) && (strcmp(trim($_GET["cmbemailMarketingType"]), "0")!=0) ){
			if ( $vwhereFilter == 0){
				$vfilter.=" WHERE c_emailmarketingtype.id_emailMarketingType=".trim($_GET["cmbemailMarketingType"]);
				$vwhereFilter =1;
			}
			else {
				$vfilter.=" AND c_emailmarketingtype.id_emailMarketingType=".trim($_GET["cmbemailMarketingType"]);
			}
        }
		if ( (isset($_GET["cmbemailMarketingStatus"])) && (strcmp(trim($_GET["cmbemailMarketingStatus"]), "0")!=0) ){
			if ( $vwhereFilter == 0){
				$vfilter.=" WHERE c_emailmarketingstatus.id_emailMarketingStatus=".trim($_GET["cmbemailMarketingStatus"]);
				$vwhereFilter =1;
			}
			else {
				$vfilter.=" AND c_emailmarketingstatus.id_emailMarketingStatus=".trim($_GET["cmbemailMarketingStatus"]);
			}
        } 		
        
        $vflEmailMarketing= new clscFLEmailMarketing();
        $vstatus = clscBLEmailMarketing::queryToDataBase($vflEmailMarketing, $vfilter); 
        if ( $vstatus == 1 ){
            $vdataResponse["emailMarketings"]=$vflEmailMarketing;
        }
        $vdataResponse["messageNumber"]=$vstatus;                        
        unset($vstatus, $vfilter, $vflEmailMarketing);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
        $vdataResponse["message"]=$vexception->getMessage();
	}
    echo json_encode($vdataResponse);
});

$vapp->get('/emailMarketingTypes', function(){
    //Purpose: It gets email marketing types data.
    //Limitations: The email marketing types data must be registered in database.  
    //Returns: Email marketing types data in JSON format.
    
    $vdataResponse=array();
    try{
        $vfilter="";
        
        $vflEmailMarketingTypes= new clscFLEmailMarketingType();
        $vstatus=clscBLEmailMarketingType::queryToDataBase($vflEmailMarketingTypes, $vfilter);
        if ( $vstatus==1 ){
            $vdataResponse["emailMarketingTypes"]=$vflEmailMarketingTypes;
        }
        $vdataResponse["messageNumber"]=$vstatus;
        
        unset($vfilter, $vflEmailMarketingTypes, $vstatus);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->get('/emailMarketingStatuses', function(){
    //Purpose: It gets email marketing status data.
    //Limitations: The email marketing status data must be registered in database.  
    //Returns: email marketing status data in JSON format.
    
    $vdataResponse=array();
    try{
        $vstatus=0;
        $vfilter ="";
        
        $vflEmailMarketingStatus= new clscFLEmailMarketingStatus();
        $vstatus = clscBLEmailMarketingStatus::queryToDataBase($vflEmailMarketingStatus, $vfilter); 
        if ( $vstatus == 1 ){
            $vdataResponse["emailMarketingStatus"]=$vflEmailMarketingStatus;
        }
        $vdataResponse["messageNumber"]=$vstatus;                        
        unset($vstatus, $vfilter, $vflEmailMarketingStatus);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->get('/campusEmailSettings/{idCampus}', function($vrequest){   
	//Purpose: It gets email marketing setting data.
    //Limitations: The email marketing setting data must be registered in database.  
    //Returns: email marketing setting data in JSON format.
	
    $vdataResponse=array();
    try{	
		$vcampusEmailSetting= new clspFLCampusEmailSetting();
		$vcampusEmailSetting->campus->idCampus=$vrequest->getAttribute('idCampus');
		$vstatus = clspBLCampusEmailSetting::queryToDataBase($vcampusEmailSetting);
		if ( $vstatus == 1 ) {
			$vdataResponse["campusEmailSettings"]=$vcampusEmailSetting;
		} 
		$vdataResponse["messageNumber"] = $vstatus;
		
		unset($vrequest, $vcampusEmailSetting);
    }
	catch (Exception $vexception){
        $vdataResponse["message"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->post('/emailMarketings', function($vrequest){
    //Purpose: It adds email marketing data.
    //Limitations: The email marketing data must not exist in database.  
    //Returns: Message number and/or email marketing identifier and campus identifier in JSON format.
    
    $vdataResponse=array();
    try{
        $vbody=$vrequest->getBody();
        $vemailMarketing=json_decode($vbody);
        
        session_start();
        $vflEmailMarketing= new clspFLEmailMarketing();
        if ( isset($vemailMarketing->cmbcampus) ){
            $vflEmailMarketing->campus->idCampus=(int)($vemailMarketing->cmbcampus);
        }
        else{
            $vflBackendUser= new clspFLBackendUser();
            $vflBackendUser->idUser=(int)($_SESSION["idUser"]);
            clspBLBackendUser::queryByIdToDataBase($vflBackendUser);
            $vflEmailMarketing->campus->idCampus=$vflBackendUser->campus->idCampus;
            
            unset($vflBackendUser);
        }
        $vflEmailMarketing->emailMarketingType->idEmailMarketingType=(int)($vemailMarketing->cmbemailMarketingType);
        $vflEmailMarketing->emailMarketingStatus->idEmailMarketingStatus=2;
        $vflEmailMarketing->userRecord->idUser=(int)($_SESSION["idUser"]);
        $vflEmailMarketing->educationalOffer->assignment->campus->idCampus=$vflEmailMarketing->campus->idCampus;
        $vflEmailMarketing->educationalOffer->assignment->educationalLevel->idEducationalLevel=(int)($vemailMarketing->cmbeducationalLevel);
        $vflEmailMarketing->educationalOffer->idEducationalOffer=(int)($vemailMarketing->cmbeducationalOffer);
        $vflEmailMarketing->subject=trim($vemailMarketing->txtsubject);
        $vflEmailMarketing->message=trim($vemailMarketing->txtmessage);
        $vflEmailMarketingFiles= new clscFLEmailMarketingFile();
        for ($vi=0; $vi<count($vemailMarketing->attachedFiles); $vi++){
            $vflEmailMarketingFile= new clspFLEmailMarketingFile();
            $vflEmailMarketingFile->emailMarketing=$vflEmailMarketing;
            $vflEmailMarketingFile->realName=trim($vemailMarketing->attachedFiles[$vi][0]);
            $vflEmailMarketingFile->fisicName=trim($vemailMarketing->attachedFiles[$vi][1]);
                
            clscBLEmailMarketingFile::add($vflEmailMarketingFiles,  $vflEmailMarketingFile);
            unset($vflEmailMarketingFile);
        }
        $vflEmailMarketingScheduled= new clspFLEmailMarketingScheduled();
        if ( isset($vemailMarketing->txtdateScheduled) ){
            $vflEmailMarketingScheduled->emailMarketing=$vflEmailMarketing;
            $vflEmailMarketingScheduled->scheduledDate=$vemailMarketing->txtdateScheduled . " ". $vemailMarketing->txttimeScheduled;
        }
        $vstatus=clspBLEmailMarketing::addToDataBase($vflEmailMarketing, $vflEmailMarketingFiles, $vflEmailMarketingScheduled);
        if ( $vstatus==1 ){
            $vdataResponse["idEmailMarketing"]=$vflEmailMarketing->idEmailMarketing;
            $vdataResponse["idCampus"]=$vflEmailMarketing->campus->idCampus;
        }
        $vdataResponse["messageNumber"]=$vstatus;
        
        unset($vrequest, $vbody, $vemailMarketing, $vflEmailMarketingFiles, $vflEmailMarketingScheduled, $vstatus);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->post('/campusEmailSettings', function($vrequest){
    //Purpose: It adds campus email setting data.
    //Limitations: The campus email setting data must not exist in database.  
    //Returns: Message number or campus email setting identifier in JSON format.
    
    $vdataResponse=array();
    try{
        $vbody=$vrequest->getBody();
        $vcampusEmailSetting=json_decode($vbody);         
        $vflCampusEmailSetting= new clspFLCampusEmailSetting();
        $vflCampusEmailSetting->campus->idCampus=1;//(int)($vcampusEmailSetting->cmbcampus);
        $vflCampusEmailSetting->hostName=$vcampusEmailSetting->txthostName;
        $vflCampusEmailSetting->smtpPort=(int)($vcampusEmailSetting->txtsmtpPort);
        $vflCampusEmailSetting->smtpAuth=$vcampusEmailSetting->txtsmtpAuth;
        $vflCampusEmailSetting->smtpSecure=$vcampusEmailSetting->txtsmtpSecure;
        $vflCampusEmailSetting->userName=$vcampusEmailSetting->txtuserName;
        $vflCampusEmailSetting->userPassword=$vcampusEmailSetting->txtuserPassword;

        $vstatus=clspBLCampusEmailSetting::addToDataBase($vflCampusEmailSetting);
        if ( $vstatus==1 ){
            $vdataResponse["idCampus"]=$vflCampusEmailSetting->campus->idCampus;
        }
        $vdataResponse["messageNumber"]=$vstatus;
        
        unset($vrequest, $vbody, $vcampusEmailSetting, $vflCampusEmailSetting, $vstatus);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->put('/emailMarketings/{vidEmailMarketing}', function ($vrequest){
    //Purpose: It sends email marketing to user applicants.
    //Limitations: The email marketing data must be exist in database.  
    //Returns: Message number in JSON format.
    
    $vdataResponse=array();
    try{
        $vbody=$vrequest->getBody();
        $vemailMarketing=json_decode($vbody);
        
        session_start();
        $vflEmailMarketing= new clspFLEmailMarketing();
        $vflEmailMarketing->campus->idCampus=(int)($vemailMarketing->idCampus);
        $vflEmailMarketing->idEmailMarketing=trim($vrequest->getAttribute("vidEmailMarketing"));
        
        $vdataResponse["messageNumber"]=clspBLEmailMarketing::sendToUserApplicants($vflEmailMarketing);
        
        unset($vrequest, $vbody, $vemailMarketing, $vflEmailMarketing);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->put('/campusEmailSettings', function($vrequest){
    //Purpose: It adds campus email setting data.
    //Limitations: The campus email setting data must not exist in database.  
    //Returns: Message number or campus email setting identifier in JSON format.
    
    $vdataResponse=array();
    try {
        $vbody=$vrequest->getBody();
        $vcampusEmailSetting=json_decode($vbody);         
        $vflCampusEmailSetting= new clspFLCampusEmailSetting();
        $vflCampusEmailSetting->campus->idCampus=1;//(int)($vcampusEmailSetting->cmbcampus);
        $vflCampusEmailSetting->hostName=$vcampusEmailSetting->txthostName;
        $vflCampusEmailSetting->smtpPort=(int)($vcampusEmailSetting->txtsmtpPort);
        $vflCampusEmailSetting->smtpAuth=$vcampusEmailSetting->txtsmtpAuth;
        $vflCampusEmailSetting->smtpSecure=$vcampusEmailSetting->txtsmtpSecure;
        $vflCampusEmailSetting->userName=$vcampusEmailSetting->txtuserName;
        $vflCampusEmailSetting->userPassword=$vcampusEmailSetting->txtuserPassword;
		   
        $vstatus=clspBLCampusEmailSetting::updateInDataBase($vflCampusEmailSetting);
        if ( $vstatus==1 ){
            $vdataResponse["idCampus"]=$vflCampusEmailSetting->campus->idCampus;
        }
        $vdataResponse["messageNumber"]=$vstatus;
        
        unset($vrequest, $vbody, $vcampusEmailSetting, $vflCampusEmailSetting, $vstatus);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->put('/campuses/{vidCampus}/emailMarketings/{vidEmailMarketing}/emailMarketingStatuses/{vidEmailMarketingStatuses}', function ($vrequest){
    // Purpose: It update status email marketing data.
    // Limitations: The email marketing  data must be registered in database.
    // Returns: Message number in JSON format.
    
    $vdataResponse=array();
    try{
        $vflEmailMarketing= new clspFLEmailMarketing();
        $vflEmailMarketing->campus->idCampus=(int)($vrequest->getAttribute("vidCampus"));
        $vflEmailMarketing->idEmailMarketing=trim($vrequest->getAttribute("vidEmailMarketing"));
        $vflEmailMarketing->emailMarketingStatus->idEmailMarketingStatus=trim($vrequest->getAttribute("vidEmailMarketingStatuses"));
		
        $vdataResponse["messageNumber"]=clspBLEmailMarketing::updateStatusInDataBase($vflEmailMarketing);
                
        unset($vrequest, $vflEmailMarketing);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->delete('/campuses/{vidCampus}/emailMarketings/{vidEmailSetting}', function ($vrequest){
    // Purpose: It deletes email marketing data.
    // Limitations: The email marketing  data must be registered in database.
    // Returns: Message number in JSON format.
    
    $vdataResponse=array();
    try{
        $vflEmailMarketing= new clspFLEmailMarketing();
        $vflEmailMarketing->campus->idCampus=(int)($vrequest->getAttribute("vidCampus"));
        $vflEmailMarketing->idEmailMarketing=trim($vrequest->getAttribute("vidEmailSetting"));
        $vdataResponse["messageNumber"]=clspBLEmailMarketing::deleteInDataBase($vflEmailMarketing);
                
        unset($vrequest, $vflEmailMarketing);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->delete('/attachFiles/{vfileName}', function ($vrequest){
    //Purpose: It deletes attach file data.
    //Limitations: The attach file data must be registered in database.
    //Returns: Message number in JSON format.
    
    $vdataResponse=array();
    try{
        if ( is_file(cfileUrl . trim($vrequest->getAttribute("vfileName"))) ){
            if ( unlink(cfileUrl . trim($vrequest->getAttribute("vfileName"))) ){
                $vdataResponse["messageNumber"]=1;
            }
            else{
                $vdataResponse["messageNumber"]=0;
            }
		}
        else{
		  $vdataResponse["messageNumber"]=-1;
        }
        
        unset($vrequest);
    }
	catch (Exception $vexception){
        $vdataResponse["messageNumber"]=-100;
	}
    echo json_encode($vdataResponse);
});

$vapp->run();
?>