<?php
/********************************************************
Name: clscBLBackendUser.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 31/05/2017
Modification date:
Description: Backend User Collection Class, Business Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLBackendUser.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspPDO.php");


class clscBLBackendUser
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflBackendUsers, $vfilter="")
	 {
		try{
			$vpdo= new clspPDO();
			$vpdo->openConnection();
            $vstatus=clscDLBackendUser::queryToDataBase($vflBackendUsers, $vfilter, $vpdo);
			$vpdo->closeConnection();
			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflBackendUsers)
	 {
		try{
			return clscDLBackendUser::total($vflBackendUsers);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }
?>