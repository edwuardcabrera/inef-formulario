<?php
/********************************************************
Name: clspBLBackendUser.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 26/05/2017
Modification date: 04/07/2017
Description: Backend User Principal Class, Business Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLUser.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLUser.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLBackendUser.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLCampusEMailSetting.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLCampusEmailSetting.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLMail.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLMail.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspPDO.php");


class clspBLBackendUser
 {
	public function __construct() { }
	
    
    public static function addToDataBase($vflBackendUser)
	 {
		try{
			$vpdo= new clspPDO();
            $vpdo->openConnection();
            $vflUser= new clspFLUser();
		    $vflUser->emailAccount=$vflBackendUser->emailAccount;
            if ( clspDLUser::queryByEmailAccountToDataBase($vflUser, $vpdo)==0 ){
                $vpdo->beginTransaction();
                if ( clspDLBackendUser::addToDataBase($vflBackendUser, $vpdo, 1)==1 ){
                    $vflCampusEmailSetting= new clspFLCampusEmailSetting();
                    $vflCampusEmailSetting->campus->idCampus=$vflBackendUser->campus->idCampus;
                    if ( clspDLCampusEmailSetting::queryToDataBase($vflCampusEmailSetting, $vpdo)==1 ){
                        $vflMail= new clspFLMail();
                        $vflMail->hostName=$vflCampusEmailSetting->hostName;
                        $vflMail->smtpPort=$vflCampusEmailSetting->smtpPort;
                        $vflMail->smtpAuth=(boolean)($vflCampusEmailSetting->smtpAuth);
                        $vflMail->smtpSecure=$vflCampusEmailSetting->smtpSecure;
                        $vflMail->userName=$vflCampusEmailSetting->userName;
                        $vflMail->userPassword=$vflCampusEmailSetting->userPassword;
                        $vflMail->sendingAddress=$vflCampusEmailSetting->userName;
                        $vflMail->sendingUserName=$vflCampusEmailSetting->campus->campus;
                        $vflMail->subject="Registro al sistema de control de solicitudes de información.";
    				    $vflMail->receiverAddress=$vflUser->emailAccount;
                        $vflMail->receiverUserName=$vflUser->name . " " . $vflUser->firstName . " " . $vflUser->lastName;
                        $vflMail->message ="<p>Recientemente ha sido registrado al sistema de control solicitudes de información.</p>";
                        $vflMail->message.="<p>Para ingresar al sistema debe de proporcionar los siguientes datos en:";
                        $vflMail->message.="    <a href='http://" . $_SERVER['HTTP_HOST'] . "/inef/frmlogin.php'>";
                        //$vflMail->message.="    <a href='http://www.bitzero.mx/otros/ces-arsa/frmlogin.php'>";
                        $vflMail->message.="        <strong>Login</strong>";
                        $vflMail->message.="    </a>";
                        $vflMail->message.="</p>";
                        $vflMail->message.="<p>correo electrónico: " . $vflUser->emailAccount . "<br />" ;
                        $vflMail->message.="contraseña: " . $vflBackendUser->password;
                        $vflMail->message.="</p>";
    				    if ( clspDLMail::sendEmail($vflMail)!=1 ){
    					   $vpdo->rollbackTransaction();
    					   $vpdo->closeConnection();
    					   return -3;
    				    }
                        unset($vflMail);
                    }
                    else{
                        $vpdo->rollbackTransaction();
                        $vpdo->closeConnection();
					   return -2;
                    }
                }
                else{
                    $vpdo->rollbackTransaction();
                    $vpdo->closeConnection();
					return -1;
                }
                $vpdo->commitTransaction();
            }
            else{
                $vpdo->closeConnection();
                return 0;
            }
            $vpdo->closeConnection();
            
			unset($vpdo, $vflUser);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflBackendUser)
	 {
		try{
			$vpdo= new clspPDO();
            $vpdo->openConnection();
            
            $vflUser= new clspFLUser();
            $vflUser->idUser=$vflBackendUser->idUser;
		    $vflUser->emailAccount=$vflBackendUser->emailAccount;
            clspDLUser::queryByEmailAccountToDataBase($vflUser, $vpdo);
            if ($vflBackendUser->idUser==$vflUser->idUser){
                $vstatus=clspDLBackendUser::updateInDataBase($vflBackendUser, $vpdo);
            }
            else{
                $vpdo->closeConnection();
                return -1;
            }
            $vpdo->closeConnection();
            
			unset($vpdo, $vflUser);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
      
    public static function deleteInDataBase($vflBackendUser)
	 {
		try{
			$vpdo= new clspPDO();
			$vpdo->openConnection();
            $vpdo->beginTransaction();
            
            $vfilter="WHERE c_backenduseraccessprivilege.id_user=" . $vflBackendUser->idUser;
            $vflBackendUserAccessPrivilege= new clscDLBackendUserAccessPrivilege();
            if ( clscDLBackendUserAccessPrivilege::queryToDataBase($vflBackendUserAccessPrivilege, $vfilter, $vpdo)==1 ){
                clscDLBackendUserAccessPrivilege::deleteInDataBase($vflBackendUserAccessPrivilege, $vpdo);
            }
			$vstatus=clspDLBackendUser::deleteInDataBase($vflBackendUser, $vpdo);
            if ($vstatus<=0){
                $vpdo->rollbackTransaction();
            }
            $vpdo->commitTransaction();
			$vpdo->closeConnection();
			
			unset($vpdo, $vfilter, $vflBackendUserAccessPrivilege);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function queryByIdToDataBase($vflBackendUser)
	 {
		try{
			$vpdo= new clspPDO();
			$vpdo->openConnection();
			$vstatus=clspDLBackendUser::queryByIdToDataBase($vflBackendUser, $vpdo);
			$vpdo->closeConnection();
			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct() { }
 }
?>