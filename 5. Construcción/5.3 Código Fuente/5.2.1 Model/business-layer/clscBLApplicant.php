<?php
/********************************************************
Name: clscBLApplicant.php
Version: 0.0.1
Autor name: Alejandro Hernández Guzmán
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 18/06/2017
Modification date: 21/07/2017
Description: Applicant Collection Class, Business Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLApplicant.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspPDO.php");


class clscBLApplicant
 {
    public function __construct() { }
    
    
	public static function queryToDataBase($vflApplicants, $vfilter="")
	 {
		try{                                  
			$vpdo= new clspPDO();
			$vpdo->openConnection();
            $vstatus=clscDLApplicant::queryToDataBase($vflApplicants, $vfilter, $vpdo);
			$vpdo->closeConnection();
			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
         
    public static function queryByUserGroupToDataBase($vflApplicants, $vfilter="")
	 {
		try{                                  
			$vpdo= new clspPDO();
			$vpdo->openConnection();
            $vstatus=clscDLApplicant::queryByUserGroupToDataBase($vflApplicants, $vfilter, $vpdo);
			$vpdo->closeConnection();
			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
         
    public static function total($vflApplicants)
	 {
		try{
			return clscDLApplicant::total($vflApplicants);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
         
    
	public function __destruct() { } 
 }
?>