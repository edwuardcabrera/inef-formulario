<?php
/********************************************************
Name: clscBLUserStatus.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 01/06/2017
Modification date:
Description: User Status Collection Class, Business Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLUserStatus.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspPDO.php");


class clscBLUserStatus
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflUserStatuses, $vfilter="")
	 {
		try{
			$vpdo= new clspPDO();
			$vpdo->openConnection();
            $vstatus=clscDLUserStatus::queryToDataBase($vflUserStatuses, $vfilter, $vpdo);
			$vpdo->closeConnection();
			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflUserStatuses)
	 {
		try{
			return clscDLUserStatus::total($vflUserStatuses);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }
?>