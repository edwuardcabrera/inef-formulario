<?php
/********************************************************
Name: clscBLEmailMarketingFile.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 12/06/2017
Modification date:
Description: Email Marketing File Collection Class, Business Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLEmailMarketingFile.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspPDO.php");


class clscBLEmailMarketingFile
 {
	public function __construct() { }
    
    
   	public static function queryToDataBase($vflEmailMarketingFiles, $vfilter="")
	 {
		try{
			$vpdo= new clspPDO();
			$vpdo->openConnection();
            $vstatus=clscDLEmailMarketingFile::queryToDataBase($vflEmailMarketingFiles, $vfilter, $vpdo);
			$vpdo->closeConnection();
			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflEmailMarketingFiles)
	 {
		try{
			return clscDLEmailMarketingFile::total($vflEmailMarketingFiles);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function add($vflEmailMarketingFiles, $vflEmailMarketingFile)
	 {
        try{
            clscDLEmailMarketingFile::add($vflEmailMarketingFiles, $vflEmailMarketingFile);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
     
     
	public function __destruct() { }
 }
?>