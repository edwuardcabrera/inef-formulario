<?php
/********************************************************
Name: clscBLAccessPrivilege.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 08/09/2015
Modification date: 25/05/2017
Description: Access Privilege Collection Class, Business Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLAccessPrivilege.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspPDO.php");


class clscBLAccessPrivilege
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflAccessPrivileges, $vfilter="")
	 {
		try{
			$vpdo= new clspPDO();
			$vpdo->openConnection();
            $vstatus=clscDLAccessPrivilege::queryToDataBase($vflAccessPrivileges, $vfilter, $vpdo);
			$vpdo->closeConnection();
			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflAccessPrivileges)
	 {
		try{
			return clscDLAccessPrivilege::total($vflAccessPrivileges);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }
?>