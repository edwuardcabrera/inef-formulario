<?php
/********************************************************
Name: clscBLUser.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 31/05/2017
Modification date:
Description: User Collection Class, Business Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLUser.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspPDO.php");


class clscBLUser
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflUsers, $vfilter="")
	 {
		try{
			$vpdo= new clspPDO();
			$vpdo->openConnection();
            $vstatus=clscDLUser::queryToDataBase($vflUsers, $vfilter, $vpdo);
			$vpdo->closeConnection();
			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflUsers)
	 {
		try{
			return clscDLUser::total($vflUsers);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }
?>