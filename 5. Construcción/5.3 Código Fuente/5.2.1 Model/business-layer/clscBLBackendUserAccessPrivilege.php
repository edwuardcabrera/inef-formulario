<?php
/********************************************************
Name: clscBLBackendUserAccessPrivilege.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 25/05/2017
Modification date: 06/06/2017
Description: Backend User Access Privilege Collection Class, Business Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLUser.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLUser.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLBackendUser.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clscFLAccessPrivilege.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLAccessPrivilege.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLBackendUserAccessPrivilege.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clscFLBackendUserAccessPrivilege.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLBackendUserAccessPrivilege.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspPDO.php");


class clscBLBackendUserAccessPrivilege
 {
	public function __construct() { }
    
    
    public static function getData($vflBackendUserAccessPrivileges, $vidUser)
     {
        $vstatus=0;
        $vpdo= new clspPDO();
        $vpdo->openConnection();
         
        $vflUser= new clspFLUser();
        $vflUser->idUser=$vidUser;
        clspDLUser::queryByIdToDataBase($vflUser, $vpdo);
        if ( $vflUser->userType->idUserType==1 || $vflUser->userType->idUserType==2 || $vflUser->userType->idUserType==3 ){
            $vflBackendUser= new clspFLBackendUser();
            $vflBackendUser->idUser=$vflUser->idUser;
            $vflBackendUser->userType=$vflUser->userType;
            $vflBackendUser->userStatus=$vflUser->userStatus;
            $vflBackendUser->firstName=$vflUser->firstName;
            $vflBackendUser->lastName=$vflUser->lastName;
            $vflBackendUser->name=$vflUser->name;
            $vflBackendUser->password=$vflUser->password;
            $vflBackendUser->emailAccount=$vflUser->emailAccount;
            if ( $vflUser->userType->idUserType==2 || $vflUser->userType->idUserType==3 ){
                $vflBackendUserOther= new clspFLBackendUser();
                $vflBackendUserOther->idUser=$vflBackendUser->idUser;
                clspBLBackendUser::queryByIdToDataBase($vflBackendUserOther);
                $vflBackendUser->campus=$vflBackendUserOther->campus;
                
                unset($vflBackendUserOther);
            }
            $vflAccessPrivileges= new clscFLAccessPrivilege();
            if (clscDLAccessPrivilege::queryToDataBase($vflAccessPrivileges, "", $vpdo)==1 ){
                for($vaccessPrivilegeNumber=0; $vaccessPrivilegeNumber<clscDLAccessPrivilege::total($vflAccessPrivileges); $vaccessPrivilegeNumber++){
                    $vflBackendUserAccessPrivilege= new clspFLBackendUserAccessPrivilege();
                    $vflBackendUserAccessPrivilege->backendUser=$vflBackendUser;
                    $vflBackendUserAccessPrivilege->accessPrivilege=$vflAccessPrivileges->accessPrivileges[$vaccessPrivilegeNumber];
                    clscDLBackendUserAccessPrivilege::add($vflBackendUserAccessPrivileges, $vflBackendUserAccessPrivilege);
                    
                    unset($vflBackendUserAccessPrivilege);
                }
                $vstatus=1;
            }
            
            unset($vflBackendUser, $vflAccessPrivileges);
        }
        else if( $vflUser->userType->idUserType==5 ){
            $vfilter="WHERE c_backenduseraccessprivilege.id_user=" . $vidUser;
            $vstatus=clscDLBackendUserAccessPrivilege::queryToDataBase($vflBackendUserAccessPrivileges, $vfilter, $vpdo);
            
            unset($vfilter);
        }
        $vpdo->closeConnection();
        
		unset($vpdo, $vflUser);
		return $vstatus;
    }
    
    public static function addToDataBase($vflBackendUserAccessPrivileges)
	 {
		try{
			$vpdo= new clspPDO();
            $vpdo->openConnection();
            $vpdo->beginTransaction();
            
            $vfilter="WHERE c_backenduseraccessprivilege.id_user=" . $vflBackendUserAccessPrivileges->backendUserAccessPrivileges[0]->backendUser->idUser;
            $votherFLBackendUserAccessPrivileges= new clscFLBackendUserAccessPrivilege();
            clscDLBackendUserAccessPrivilege::queryToDataBase($votherFLBackendUserAccessPrivileges, $vfilter, $vpdo);
            clscDLBackendUserAccessPrivilege::deleteInDataBase($votherFLBackendUserAccessPrivileges, $vpdo);
            if (clscDLBackendUserAccessPrivilege::addToDataBase($vflBackendUserAccessPrivileges, $vpdo)!=1 ){
                $vpdo->rollbackTransaction();
                $vpdo->closeConnection();
                return 0;
            }
            $vpdo->commitTransaction();
			$vpdo->closeConnection();
            
			unset($vpdo,$vfilter, $votherFLBackendUserAccessPrivileges);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function add($vflBackendUserAccessPrivileges, $vflBackendUserAccessPrivilege)
	 {
        try{
            clscDLBackendUserAccessPrivilege::add($vflBackendUserAccessPrivileges, $vflBackendUserAccessPrivilege);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
     
     
	public function __destruct() { }
 }
?>