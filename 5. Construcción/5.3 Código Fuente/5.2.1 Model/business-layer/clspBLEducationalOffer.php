<?php
/********************************************************
Name: clspBLEducationalOffer.php
Version: 0.0.1
Autor name: Sandro A. Gomez Aceituno.
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 16/07/2017
Modification date: 25/07/2017
Description: Educational Offer Principal Class, Business Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLEducationalOffer.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspPDO.php");


class clspBLEducationalOffer
 {
	public function __construct() { }
	
	public static function queryToDatabase($vflEducationalOffer){
        try{
            $vpdo= new clspPDO();
            $vpdo->openConnection();
            $vstatus=  clspDLEducationalOffer::queryToDatabase($vflEducationalOffer, $vpdo);
            $vpdo->closeConnection();

            unset($vpdo);
            return $vstatus;
        }
        catch (Exception $vexception){
            throw new Exception($vexception->getMessage(), $vexception->getCode());
        }
    }
	
	public static function deleteInDataBase($vflEducationalOffer)
	 {
		try {
			$vpdo= new clspPDO();
			$vpdo->openConnection();
			$vstatus=clspDLEducationalOffer::deleteInDataBase($vflEducationalOffer, $vpdo);
			$vpdo->closeConnection();			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

    public static function addToDataBase($vflEducationalOffer)
	 {
		try {
			$vpdo= new clspPDO();
			$vpdo->openConnection();
			$vstatus=clspDLEducationalOffer::addToDataBase($vflEducationalOffer, $vpdo);
			$vpdo->closeConnection();			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	 
    public static function updateInDataBase($vflEducationalOffer)
	 {
		try {
			$vpdo= new clspPDO();
			$vpdo->openConnection();
			$vstatus=clspDLEducationalOffer::updateInDataBase($vflEducationalOffer, $vpdo);
			$vpdo->closeConnection();			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	 
	public static function updateFileInDataBase($vflEducationalOffer)
	 {
		try {
			$vpdo= new clspPDO();
			$vpdo->openConnection();
			$vstatus=clspDLEducationalOffer::updateFileInDataBase($vflEducationalOffer, $vpdo);
			$vpdo->closeConnection();			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function queryToDatabase1($vflEducationalOffer,$vflApplicant)
     {
        try{
            $vpdo= new clspPDO();
            $vpdo->openConnection();
            $vpdo->beginTransaction();
            $vstatus= clspDLEducationalOffer::queryToDatabase($vflEducationalOffer, $vpdo);
            $vflCampusEmailSetting= new clspFLCampusEmailSetting();
            $vflCampusEmailSetting->campus->idCampus=$vflApplicant->campus->idCampus;
            if ( clspDLCampusEmailSetting::queryToDataBase($vflCampusEmailSetting, $vpdo)==1 ){
                if($vflEducationalOffer->fileName==""){
                    $vpdo->rollbackTransaction();
                    $vpdo->closeConnection();
                    return -3;
                }
                else{
                    $vflMail= new clspFLMail();
                    $vflMail->hostName=$vflCampusEmailSetting->hostName;
                    $vflMail->smtpPort=$vflCampusEmailSetting->smtpPort;
                    $vflMail->smtpAuth=(boolean)($vflCampusEmailSetting->smtpAuth);
                    $vflMail->smtpSecure=$vflCampusEmailSetting->smtpSecure;
                    $vflMail->userName=$vflCampusEmailSetting->userName;
                    $vflMail->userPassword=$vflCampusEmailSetting->userPassword;
                    $vflMail->sendingAddress=$vflCampusEmailSetting->userName;
                    $vflMail->sendingUserName=$vflCampusEmailSetting->campus->campus;
                    $vflMail->subject = "Descarga de Plan de Estudios";
                    $vflMail->receiverAddress = $vflApplicant->user->emailAccount;
                    $vflMail->receiverUserName = $vflApplicant->user->name . " " . $vflApplicant->user->firstName . " " . $vflApplicant->user->lastName;
                    $vflMail->message = "<p>Recientemente se ha realizado la descarga de información de nuestro portal.</p>";
                    $vflMail->message.="<p>Reciba un coordial saludo";
                    $vflMail->message.="        <strong></strong>";
                    $vflMail->message.="    </a>";
                    $vflMail->message.="</p>";
                    if (clspDLMail::sendEmail($vflMail) != 1){
                        $vpdo->rollbackTransaction();
                        $vpdo->closeConnection();
                        return -2;
                    }

                    unset($vflMail);
                    
                }
            }
            $vpdo->commitTransaction();
            $vpdo->closeConnection();

            unset($vpdo);
            return $vstatus;
        }
        catch (Exception $vexception){
            throw new Exception($vexception->getMessage(), $vexception->getCode());
        }
    }
    
    
	public function __destruct() { }
 }
?>