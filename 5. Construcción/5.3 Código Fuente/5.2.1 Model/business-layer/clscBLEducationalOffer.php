<?php
/********************************************************
Name: clscBLEducationalOffer.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 11/07/2017
Modification date:
Description: Educational Offer Collection Class, Business Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLEducationalOffer.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspPDO.php");


class clscBLEducationalOffer
 {
	public function __construct() { }    
    
	public static function queryToDataBase($vflEducationalOffers, $vfilter)
	 {
		try{
			$vpdo= new clspPDO();
			$vpdo->openConnection();
            $vstatus=clscDLEducationalOffer::queryToDataBase($vflEducationalOffers, $vfilter, $vpdo);
			$vpdo->closeConnection();
			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflEducationalOffers)
	 {
		try{
			return clscDLEducationalOffer::total($vflEducationalOffers);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }    
    
	public function __destruct() { }
 }
?>