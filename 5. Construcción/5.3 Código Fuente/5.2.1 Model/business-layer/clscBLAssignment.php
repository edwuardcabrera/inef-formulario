<?php
/********************************************************
Name: clscBLAssignment.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 07/06/2017
Modification date:
Description: Assignment Collection Class, Business Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLAssignment.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspPDO.php");


class clscBLAssignment
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflAssignments, $vfilter="")
	 {
		try{
			$vpdo= new clspPDO();
			$vpdo->openConnection();
            $vstatus=clscDLAssignment::queryToDataBase($vflAssignments, $vfilter, $vpdo);
			$vpdo->closeConnection();
			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflAssignments)
	 {
		try{
			return clscDLAssignment::total($vflAssignments);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }
?>