<?php
/********************************************************
Name: clspBLEmailMarketingScheduled.php
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 14/06/2017
Modification date:
Description: Email Marketing Scheduled Principal Class, Business Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLEmailMarketingScheduled.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspPDO.php");


class clspBLEmailMarketingScheduled
 {
	public function __construct() { }
    
    
    public static function queryByIdsToDataBase($vflEmailMarketingScheduled)
	 {
		try{
			$vpdo= new clspPDO();
			$vpdo->openConnection();
			$vstatus=clspDLEmailMarketingScheduled::queryByIdsToDataBase($vflEmailMarketingScheduled, $vpdo);
			$vpdo->closeConnection();
			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct() { }
 }
?>