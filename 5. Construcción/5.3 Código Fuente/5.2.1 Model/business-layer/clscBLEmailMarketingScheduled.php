<?php
/********************************************************
Name: clscBLEmailMarketingScheduled.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 28/06/2017
Modification date:
Description: Email Marketing Scheduled Collection Class, Business Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLEmailMarketingScheduled.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspPDO.php");


class clscBLEmailMarketingScheduled
 {
	public function __construct() { }
    
    
    	public static function queryToDataBase($vflEmailMarketingScheduleds, $vfilter="")
	 {
		try{
			$vpdo= new clspPDO();
			$vpdo->openConnection();
            $vstatus=clscDLEmailMarketingScheduled::queryToDataBase($vflEmailMarketingScheduleds, $vfilter, $vpdo);
			$vpdo->closeConnection();
			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflEmailMarketingScheduleds)
	 {
		try{
			return clscDLEmailMarketingScheduled::total($vflEmailMarketingScheduleds);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function add($vflEmailMarketingScheduleds, $vflEmailMarketingScheduled)
	 {
        try{
            clscDLEmailMarketingScheduled::add($vflEmailMarketingScheduleds, $vflEmailMarketingScheduled);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
     
     
	public function __destruct() { }
 }
?>