<?php
/********************************************************
Name: clspBLCampusEmailSetting.php
Version: 0.0.1
Autor name: Sandro Alan Gomez Aceituno.
Modification autor name: 
Creation date: 13/06/2017
Modification date: 
Description: Campus Email Setting Principal Class, Business Layer. 
********************************************************/
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLCampusEmailSetting.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspPDO.php");

class clspBLCampusEmailSetting
 {
	public function __construct() { }	
    
    public static function addToDataBase($vflCampusEmailSetting)
	 {
		try {
			$vpdo= new clspPDO();
			$vpdo->openConnection();
			$vstatus=clspDLCampusEmailSetting::addToDataBase($vflCampusEmailSetting, $vpdo);
			$vpdo->closeConnection();			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflCampusEmailSetting)
	 {
		try {
			$vpdo= new clspPDO();
			$vpdo->openConnection();
			$vstatus=clspDLCampusEmailSetting::updateInDataBase($vflCampusEmailSetting, $vpdo);
			$vpdo->closeConnection();			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
      
    public static function deleteInDataBase($vflCampusEmailSetting)
	 {
		try {
			$vpdo= new clspPDO();
			$vpdo->openConnection();
			$vstatus=clspDLCampusEmailSetting::deleteInDataBase($vflCampusEmailSetting, $vpdo);
			$vpdo->closeConnection();			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function queryToDataBase($vflCampusEmailSetting)
	 {
		try {
			$vpdo= new clspPDO();
			$vpdo->openConnection();
			$vstatus=clspDLCampusEmailSetting::queryToDataBase($vflCampusEmailSetting, $vpdo);
			$vpdo->closeConnection();			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct() { }
 }
?>