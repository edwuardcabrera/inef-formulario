<?php
/********************************************************
Name: clscBLEmailMarketingStatus.php
Version: 0.0.1
Autor name: Sandro Alan Gomez Aceituno
Modification autor name:
Creation date: 06/06/2017
Modification date:
Description: Email Marketing Status Collection Class, Business Layer. 
********************************************************/
require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLEmailMarketingStatus.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspPDO.php");

class clscBLEmailMarketingStatus
 {
	public function __construct() { }    
    
	public static function queryToDataBase($vflEmailMarketingStatus, $vfilter)
	 {
		try{
			$vpdo= new clspPDO();
			$vpdo->openConnection();
            $vstatus=clscDLEmailMarketingStatus::queryToDataBase($vflEmailMarketingStatus, $vfilter, $vpdo);
			$vpdo->closeConnection();
			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflEmailMarketingStatus)
	 {
		try{
			return clscDLEmailMarketingStatus::total($vflEmailMarketingStatus);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }    
    
	public function __destruct() { }
 }
?>