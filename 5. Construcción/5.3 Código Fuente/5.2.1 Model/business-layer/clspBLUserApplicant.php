<?php
/********************************************************
Name: clspBLUserApplicant.php
Version: 0.0.1
Autor name: Alejandro Hdez. G.
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 15/05/2017
Modification date: 04/07/2017
Description: UserApplicant Class, Business Layer.
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLUserApplicant.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspPDO.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLMail.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLMail.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLApplicant.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLAssignment.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLCampusEMailSetting.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLCampusEmailSetting.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLEducationalOffer.php");


class clspBLUserApplicant
 {
    public function __construct() { }
    
    
    public static function AddUserApplicantDatabase($vfluserApplicant, $vflApplicant, $vflEducationalOffer)
     {
        try{
            $vpdo = new clspPDO();
            $vpdo->openConnection();
            $vpdo->beginTransaction();
            if (clspDLUserApplicant::AddToDatabase($vfluserApplicant, $vpdo, 1) == 1){
                $vflApplicant->user->idUser = $vfluserApplicant->idUser;
                if (clspDLApplicant::AddToDatabase($vflApplicant, $vpdo) == 1){
                    $vflEducationalOffer->assignment->campus->idCampus= $vflApplicant->campus->idCampus;
                    $vflEducationalOffer->assignment->educationalLevel->idEducationalLevel = $vflApplicant->educationalOffer->assignment->educationalLevel->idEducationalLevel;
                    $vflEducationalOffer->idEducationalOffer=$vflApplicant->educationalOffer->idEducationalOffer;
                    if (clspDLEducationalOffer::queryToDatabase($vflEducationalOffer, $vpdo) == 1){
                        $vflCampusEmailSetting= new clspFLCampusEmailSetting();
                        $vflCampusEmailSetting->campus->idCampus=$vflEducationalOffer->assignment->campus->idCampus;
                        if ( clspDLCampusEmailSetting::queryToDataBase($vflCampusEmailSetting, $vpdo)==1 ){
                            if($vflEducationalOffer->fileName==""){
                                $vpdo->rollbackTransaction();
                                $vpdo->closeConnection();
                                return -5;
                            }
                            else{
                                $vflMail= new clspFLMail();
                                $vflMail->hostName=$vflCampusEmailSetting->hostName;
                                $vflMail->smtpPort=$vflCampusEmailSetting->smtpPort;
                                $vflMail->smtpAuth=(boolean)($vflCampusEmailSetting->smtpAuth);
                                $vflMail->smtpSecure=$vflCampusEmailSetting->smtpSecure;
                                $vflMail->userName=$vflCampusEmailSetting->userName;
                                $vflMail->userPassword=$vflCampusEmailSetting->userPassword;
                                $vflMail->sendingAddress=$vflCampusEmailSetting->userName;
                                $vflMail->sendingUserName=$vflCampusEmailSetting->campus->campus;
                                $vflMail->subject="Registro correctamente .";
                                $vflMail->receiverAddress = $vfluserApplicant->emailAccount;
                                $vflMail->receiverUserName = $vfluserApplicant->name . " " . $vfluserApplicant->firstName . " " . $vfluserApplicant->lastName;
                                $vflMail->message = "<p>Tus datos se han recibido satisfactoriamente.</p>";
                                if (clspDLMail::sendEmail($vflMail) != 1){
                                    $vpdo->rollbackTransaction();
                                    $vpdo->closeConnection();
                                    return -4;
                                }
                                
                                unset($vflMail);    
                            }
                        }
                        else{
                            $vpdo->rollbackTransaction();
                            $vpdo->closeConnection();
                            return -3;
                        }
                    }
                    else{
                        $vpdo->rollbackTransaction();
                        $vpdo->closeConnection();
                        return -2;
                    }
                }
                else{
                    $vpdo->rollbackTransaction();
                    $vpdo->closeConnection();
                    return -1;
                }
            }
            else{
                $vpdo->rollbackTransaction();
                $vpdo->closeConnection();
                return 0;
            }
            $vpdo->commitTransaction();
            $vpdo->closeConnection();

            unset($vpdo);
            return 1;
        }
        catch (Exception $vexception){
            throw new Exception($vexception->getMessage(), $vexception->getCode());
        }
     }

    public static function getUserApplicantfile($vflAssignment, $vemailApplicant, $vEducationLevel, $vcampus)
     {
        try{
            $vpdo = new clspPDO();
            $vpdo->openConnection();

            if (clspDLUserApplicant::getUserApplicant($vpdo, $vemailApplicant) == 1){
                $vstatus = clspDLUserApplicant::getFileEducationLevel($vpdo, $vflAssignment, $vEducationLevel, $vcampus);
            }
            else{
                $vstatus = 0;
            }
            $vpdo->closeConnection();

            unset($vpdo);
            return $vstatus;
        }
        catch (Exception $vexception){
            throw new Exception($vexception->getMessage(), $vexception->getCode());
        }
    }
    
    
    public function __destruct() { }
}
?>