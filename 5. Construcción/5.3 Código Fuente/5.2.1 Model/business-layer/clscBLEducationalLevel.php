<?php
/********************************************************
Name: clscBLEducationalLevel.php
Version: 0.0.1
Autor name: Sandro Alan Gomez Aceituno
Modification autor name:
Creation date: 19/06/2017
Modification date:
Description: Educational Level Collection Class, Business Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLEducationalLevel.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspPDO.php");


class clscBLEducationalLevel
 {
	public function __construct() { }    
    
	public static function queryToDataBase($vflEducationalLevels, $vfilter)
	 {
		try{
			$vpdo= new clspPDO();
			$vpdo->openConnection();
            $vstatus=clscDLEducationalLevel::queryToDataBase($vflEducationalLevels, $vfilter, $vpdo);
			$vpdo->closeConnection();
			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflEducationalLevels)
	 {
		try{
			return clscDLEducationalLevel::total($vflEducationalLevels);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }    
    
	public function __destruct() { }
 }
?>