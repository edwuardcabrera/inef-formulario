<?php
/********************************************************
Name: clspBLAssigment.php
Version: 0.0.1
Autor name: Alejandro Hdez. Guzmán
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 08/06/2017
Modification date: 04/07/2017
Description: Assignment Class, Business Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLAssignment.php");


class clspBLAssignment
 {
    public function __construct() { }
    
    
    public static function queryToDatabase($vflAssigment){
        try{
            $vpdo= new clspPDO();
            $vpdo->openConnection();
            $vstatus=  clspDLAssignment::queryToDatabase($vflAssigment, $vpdo);
            $vpdo->closeConnection();

            unset($vpdo);
            return $vstatus;
        }
        catch (Exception $vexception){
            throw new Exception($vexception->getMessage(), $vexception->getCode());
        }
    }
    
    public static function updateInDataBase($vflAssigment)
	 {
		try {
			$vpdo= new clspPDO();
			$vpdo->openConnection();
			$vstatus=clspDLAssignment::updateInDataBase($vflAssigment, $vpdo);
			$vpdo->closeConnection();			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
    public function __destruct() { }
 }
?>