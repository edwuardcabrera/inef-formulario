<?php
/********************************************************
Name: clscBLUserType.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 01/06/2017
Modification date:
Description: User Type Collection Class, Business Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLUserType.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspPDO.php");


class clscBLUserType
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflUserTypes, $vfilter="")
	 {
		try{
			$vpdo= new clspPDO();
			$vpdo->openConnection();
            $vstatus=clscDLUserType::queryToDataBase($vflUserTypes, $vfilter, $vpdo);
			$vpdo->closeConnection();
			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflUserTypes)
	 {
		try{
			return clscDLUserType::total($vflUserTypes);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }
?>