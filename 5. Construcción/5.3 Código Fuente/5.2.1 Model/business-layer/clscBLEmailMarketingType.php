<?php
/********************************************************
Name: clscBLEmailMarketingType.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 06/06/2017
Modification date:
Description: Email Marketing Type Collection Class, Business Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLEmailMarketingType.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspPDO.php");


class clscBLEmailMarketingType
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflEmailMarketingTypes, $vfilter="")
	 {
		try{
			$vpdo= new clspPDO();
			$vpdo->openConnection();
            $vstatus=clscDLEmailMarketingType::queryToDataBase($vflEmailMarketingTypes, $vfilter, $vpdo);
			$vpdo->closeConnection();
			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflEmailMarketingTypes)
	 {
		try{
			return clscDLEmailMarketingType::total($vflEmailMarketingTypes);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }
?>