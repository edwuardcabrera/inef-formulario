<?php
/********************************************************
Name: clspBLEmailMarketing.php
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 13/06/2017
Modification date: 28/06/2017
Description: Email Marketing Principal Class, Business Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLEmailMarketing.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLEmailMarketingFile.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLEmailMarketingScheduled.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLEmailMarketingScheduled.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clscFLEmailMarketingFile.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLEmailMarketingFile.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLCampusEMailSetting.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLCampusEmailSetting.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clscFLApplicant.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLApplicant.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLEmailMarketingSend.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLEmailMarketingSend.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLMail.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLMail.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspPDO.php");


class clspBLEmailMarketing
 {
	public function __construct() { }
	
    
    public static function addToDataBase($vflEmailMarketing, $vflEmailMarketingFiles, $vflEmailMarketingScheduled)
	 {
		try{
            $vpdo= new clspPDO();
			$vpdo->openConnection();
            $vpdo->beginTransaction();
            if ( clspDLEmailMarketing::addToDataBase($vflEmailMarketing, $vpdo)==1 ){
                $vemailMarketingFilesNumber=clscDLEmailMarketingFile::total($vflEmailMarketingFiles);
                if ($vemailMarketingFilesNumber>0){
                    for ($vi=0; $vi<$vemailMarketingFilesNumber; $vi++){
                        $vflEmailMarketingFiles->emailMarketingFiles[$vi]->emailMarketing->idEmailMarketing=$vflEmailMarketing->idEmailMarketing;
                        if ( clspDLEmailMarketingFile::addToDataBase($vflEmailMarketingFiles->emailMarketingFiles[$vi], $vpdo)==0 ){
                            $vpdo->rollbackTransaction();
                            $vpdo->closeConnection();
                            return -2;
                        }
                    }
                }
                if ( $vflEmailMarketingScheduled->emailMarketing->campus->idCampus>0 ){
                    if ( clspDLEmailMarketingScheduled::addToDataBase($vflEmailMarketingScheduled, $vpdo)==0 ){
                        $vpdo->rollbackTransaction();
                        $vpdo->closeConnection();
                        return -1;
                    }
                }
                
                unset($vemailMarketingFilesNumber);
            }
            else{
                $vpdo->rollbackTransaction();
                $vpdo->closeConnection();
                return 0;
            }
            $vpdo->commitTransaction();
			$vpdo->closeConnection();
            
            unset($vpdo);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflEmailMarketing, $vflEmailMarketingFiles, $vflEmailMarketingScheduled)
	 {
        try{
            $vpdo= new clspPDO();
			$vpdo->openConnection();
            $vpdo->beginTransaction();
            clspDLEmailMarketing::updateInDataBase($vflEmailMarketing, $vpdo);
            
            $vfilter ="WHERE p_emailmarketingfile.id_campus=" . $vflEmailMarketing->campus->idCampus . " ";
            $vfilter.="AND p_emailmarketingfile.id_emailMarketing='" . $vflEmailMarketing->idEmailMarketing . "'";
            $vflEmailMarketingFilesRecorded= new clscFLEmailMarketingFile();
            clscDLEmailMarketingFile::queryToDataBase($vflEmailMarketingFilesRecorded, $vfilter, $vpdo);
            clscDLEmailMarketingFile::deleteInDataBase($vflEmailMarketingFilesRecorded, $vpdo);
            if ( clscDLEmailMarketingFile::addToDataBase($vflEmailMarketingFiles, $vpdo)==0 ){
                $vpdo->rollbackTransaction();
                $vpdo->closeConnection();
                return 0;
            }
            if ( $vflEmailMarketingScheduled->emailMarketing->campus->idCampus>0 ){
                clspDLEmailMarketingScheduled::updateInDataBase($vflEmailMarketingScheduled, $vpdo);
            }
            $vpdo->commitTransaction();
			$vpdo->closeConnection();
            
            unset($vpdo);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
     }
     
     public static function updateStatusInDataBase($vflEmailMarketing)
	 {
		try{
			$vpdo= new clspPDO();
			$vpdo->openConnection();
			$vstatus=clspDLEmailMarketing::updateStatusInDataBase($vflEmailMarketing, $vpdo);
			$vpdo->closeConnection();			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflEmailMarketing)
	 {
		try{
			$vpdo= new clspPDO();
			$vpdo->openConnection();
            $vpdo->beginTransaction();
            
            $vfilter ="WHERE p_emailmarketingfile.id_campus=" . $vflEmailMarketing->campus->idCampus . " ";
            $vfilter.="AND p_emailmarketingfile.id_emailMarketing='" . $vflEmailMarketing->idEmailMarketing . "'";
            $vflEmailMarketingFilesRecorded= new clscFLEmailMarketingFile();
            
            clscDLEmailMarketingFile::queryToDataBase($vflEmailMarketingFilesRecorded, $vfilter, $vpdo);
            clscDLEmailMarketingFile::deleteInDataBase($vflEmailMarketingFilesRecorded, $vpdo);
            clspDLEmailMarketing::queryByIdsToDataBase($vflEmailMarketing, $vpdo);
            if ( $vflEmailMarketing->emailMarketingType->idEmailMarketingType==2 ){
                $vflEmailMarketingScheduled= new clspFLEmailMarketingScheduled();
                $vflEmailMarketingScheduled->emailMarketing->campus->idCampus=$vflEmailMarketing->campus->idCampus;
                $vflEmailMarketingScheduled->emailMarketing->idEmailMarketing=$vflEmailMarketing->idEmailMarketing; 
                if ( clspDLEmailMarketingScheduled::deleteInDataBase($vflEmailMarketingScheduled, $vpdo)==0 ){
                    $vpdo->rollbackTransaction();
                    $vpdo->closeConnection();
                    return -1;
                }
            }
            if ( clspDLEmailMarketing::deleteInDataBase($vflEmailMarketing, $vpdo)==0 ){
                $vpdo->rollbackTransaction();
                $vpdo->closeConnection();
                return 0;
            }
            $vpdo->commitTransaction();
			$vpdo->closeConnection();
            			
			unset($vpdo);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function queryByIdsToDataBase($vflEmailMarketing)
	 {
		try{
			$vpdo= new clspPDO();
			$vpdo->openConnection();
			$vstatus=clspDLEmailMarketing::queryByIdsToDataBase($vflEmailMarketing, $vpdo);
			$vpdo->closeConnection();
			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function sendToUserApplicants($vflEmailMarketing)
     {
        try{
            $vpdo= new clspPDO();
            $vpdo->openConnection();
            if ( clspDLEmailMarketing::queryByIdsToDataBase($vflEmailMarketing, $vpdo)==1 ){
                if ( $vflEmailMarketing->emailMarketingStatus->idEmailMarketingStatus==2 ){
                    $vflCampusEmailSetting= new clspFLCampusEmailSetting();
                    $vflCampusEmailSetting->campus->idCampus=$vflEmailMarketing->campus->idCampus;
                    if ( clspDLCampusEmailSetting::queryToDataBase($vflCampusEmailSetting, $vpdo)==1 ){
                        $vpdo->beginTransaction();
                        $vflEmailMarketing->emailMarketingStatus->idEmailMarketingStatus=1;
                        if ( clspDLEmailMarketing::updateStatusInDataBase($vflEmailMarketing, $vpdo)==0 ){
                            $vpdo->rollbackTransaction();
                            $vpdo->closeConnection();
                            return -7;
                        }
                        $vfilter ="WHERE p_applicant.id_campus=" . $vflEmailMarketing->campus->idCampus . " ";
                        $vfilter.="AND p_applicant.id_educationalLevel=" . $vflEmailMarketing->educationalOffer->assignment->educationalLevel->idEducationalLevel . " ";
                        if ( $vflEmailMarketing->educationalOffer->idEducationalOffer>0 ){
                            $vfilter.="AND p_applicant.id_educationalOffer=" . $vflEmailMarketing->educationalOffer->idEducationalOffer;
                        }
                        $vflApplicants= new clscFLApplicant();
                        if ( clscDLApplicant::queryByUserGroupToDataBase($vflApplicants, $vfilter, $vpdo)==1 ){
                            $vflMail= new clspFLMail();
                            $vflMail->hostName=$vflCampusEmailSetting->hostName;
                            $vflMail->smtpPort=$vflCampusEmailSetting->smtpPort;
                            $vflMail->smtpAuth=(boolean)($vflCampusEmailSetting->smtpAuth);
                            $vflMail->smtpSecure=$vflCampusEmailSetting->smtpSecure;
                            $vflMail->userName=$vflCampusEmailSetting->userName;
                            $vflMail->userPassword=$vflCampusEmailSetting->userPassword;
                            $vflMail->sendingAddress=$vflCampusEmailSetting->userName;
                            $vflMail->sendingUserName=$vflCampusEmailSetting->campus->campus;
                            $vflMail->subject=$vflEmailMarketing->subject;
                            
                            $vreceiverAddresses=array();
                            $vreceiverUserNames=array();
                            for ($vi=0; $vi<clscDLApplicant::total($vflApplicants); $vi++){
                                array_push($vreceiverAddresses, $vflApplicants->applicants[$vi]->user->emailAccount);
                                array_push($vreceiverUserNames, $vflApplicants->applicants[$vi]->user->name . " " .
                                                                $vflApplicants->applicants[$vi]->user->firstName . " " . 
                                                                $vflApplicants->applicants[$vi]->user->lastName);
                                                                //echo $vflApplicants->applicants[$vi]->user->emailAccount;
                            }
                            $vflMail->receiverAddress=$vreceiverAddresses;
                            $vflMail->receiverUserName=$vreceiverUserNames;
                            $vflMail->message=$vflEmailMarketing->message;
                            
                            $vfilter ="WHERE p_emailmarketingfile.id_campus=" . $vflEmailMarketing->campus->idCampus . " ";
                            $vfilter.="AND p_emailmarketingfile.id_emailMarketing='" . $vflEmailMarketing->idEmailMarketing . "'";
                            $vflEmailMarketingFiles= new clscFLEmailMarketingFile();
                            if ( clscDLEmailMarketingFile::queryToDataBase($vflEmailMarketingFiles, $vfilter, $vpdo)==1 ){
                                $vfiles=array();
                                for ($vi=0; $vi<clscDLEmailMarketingFile::total($vflEmailMarketingFiles); $vi++){
                                    $vfile=array();
                                    $vfile["path"]=dirname(dirname(dirname(__FILE__))) . "/others/attachments";
                                    $vfile["name"]=$vflEmailMarketingFiles->emailMarketingFiles[$vi]->fisicName;
                                    array_push($vfiles, $vfile);
                                    
                                    unset($vfile);
                                }
                                $vflMail->file=$vfiles;
                                                                
                                unset($vfiles);                                                                
                            }
                            if ( clspDLMail::sendEmail($vflMail)==1 ){
                                $vflEmailMarketingSend= new clspFLEmailMarketingSend();
                                $vflEmailMarketingSend->emailMarketing=$vflEmailMarketing;
                                if ( $vflEmailMarketing->emailMarketingType->idEmailMarketingType==1 ){
                                    $vflEmailMarketingSend->userMailing->idUser=(int)($_SESSION["idUser"]);
                                }
                                else{
                                    $vflEmailMarketingSend->userMailing->idUser=$vflEmailMarketing->userRecord->idUser;
                                }
                                if ( clspDLEmailMarketingSend::addToDataBase($vflEmailMarketingSend, $vpdo)==0 ){
                                    $vpdo->rollbackTransaction();
                                    $vpdo->closeConnection();
                                    return -6;
                                }
                                
                                unset($vflEmailMarketingSend);
                            }
                            else{
                                $vpdo->rollbackTransaction();
                                $vpdo->closeConnection();
                                return -5;
                            }
                            $vpdo->commitTransaction();
                            
                            unset($vflMail, $vreceiverAddresses, $vreceiverUserNames, $vflEmailMarketingFiles);
                        }
                        else{
                            $vpdo->rollbackTransaction();
                            $vpdo->closeConnection();
                            return -4;
                        }
                        
                        unset($vfilter, $vflApplicants);
                    }
                    else{
                        $vpdo->closeConnection();
                        return -3;
                    }
                    
                    unset($vflCampusEmailSetting);
                }
                else if ( $vflEmailMarketing->emailMarketingStatus->idEmailMarketingStatus==3 ){
                    $vpdo->closeConnection();
                    return -2;
                }
                else{
                    $vpdo->closeConnection();
                    return -1;
                }
            }
            else{
                $vpdo->closeConnection();
                return 0;
            }
            $vpdo->closeConnection();
            
            			
            unset($vpdo);
            return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
     }
    
    
    public function __destruct() { }
 }
?>