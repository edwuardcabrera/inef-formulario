<?php
/********************************************************
Name: clspBLApplicant.php
Version: 0.0.1
Autor name: Alejandro Hdez. G.
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 15/05/2017
Modification date: 25/07/2017
Description: Applicant Class, Business Layer.
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLApplicant.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspPDO.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLMail.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLMail.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLCampusEMailSetting.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLCampusEmailSetting.php");


class clspBLApplicant
 {
    public function __construct() {}
    
    
    public static function ApplicantGetToDatabase($vflApplicant)
     {
        try{
            $vpdo = new clspPDO();
            $vpdo->openConnection();
            $vstatus = clspDLApplicant::AddToDatabase($vflApplicant, $vpdo);
            $vpdo->closeConnection();

            unset($vpdo);
            return $vstatus;
        }
        catch (Exception $vexception){
            throw new Exception($vexception->getMessage(), $vexception->getCode());
        }
    }

    public static function queryToDatabase($vflApplicant)
     {
        try{
            $vpdo = new clspPDO();
            $vpdo->openConnection();
            $vstatus = clspDLApplicant::queryToDatabase($vflApplicant, $vpdo);
            $vpdo->closeConnection();

            unset($vpdo);
            return $vstatus;
        }
        catch (Exception $vexception){
            throw new Exception($vexception->getMessage(), $vexception->getCode());
        }
    }

    public function __destruct() { }
}
?>