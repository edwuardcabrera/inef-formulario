<?php
/********************************************************
Name: clscBLUserApplicant.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 26/05/2017
Modification date: 19/06/2017
Description: User Applicant Collection Class, Business Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLUserApplicant.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspPDO.php");


class clscBLUserApplicant
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflUserApplicants, $vfilter="")
	 {
		try{
			$vpdo= new clspPDO();
			$vpdo->openConnection();
            $vstatus=clscDLUserApplicant::queryToDataBase($vflUserApplicants, $vfilter, $vpdo);
			$vpdo->closeConnection();
			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflUserApplicants)
	 {
		try{
			return clscDLUserApplicant::total($vflUserApplicants);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }
?>