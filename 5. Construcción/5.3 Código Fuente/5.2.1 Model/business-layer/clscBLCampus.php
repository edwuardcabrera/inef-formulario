<?php
/********************************************************
Name: clscBLCampus.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 26/05/2017
Modification date:
Description: Campus Collection Class, Business Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLCampus.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspPDO.php");


class clscBLCampus
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflCampuses, $vfilter="")
	 {
		try{
			$vpdo= new clspPDO();
			$vpdo->openConnection();
            $vstatus=clscDLCampus::queryToDataBase($vflCampuses, $vfilter, $vpdo);
			$vpdo->closeConnection();
			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflCampuses)
	 {
		try{
			return clscDLCampus::total($vflCampuses);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }
?>