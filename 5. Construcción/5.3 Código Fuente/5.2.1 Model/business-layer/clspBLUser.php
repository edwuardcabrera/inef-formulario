<?php
/********************************************************
Name: clspBLUser.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 03/11/2015
Modification date: 29/05/2017
Description: User Principal Class, Business Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLUser.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLPasswordReset.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLPasswordReset.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLMail.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLMail.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspPDO.php");


class clspBLUser
 {
	public function __construct() { }
	
    
    public static function addPasswordResetRequestToDataBase($vflUser)
	 {
		try{
			$vpdo= new clspPDO();
			$vpdo->openConnection();
            if ( clspDLUser::queryByEmailAccountToDataBase($vflUser, $vpdo)==1 ){
                date_default_timezone_set('America/Mexico_City');
                $vflPasswordReset= new clspFLPasswordReset();
                $vflPasswordReset->user->idUser=$vflUser->idUser;
                $vflPasswordReset->passwordResetStatus->idPasswordResetStatus=0;
                $vflPasswordReset->effectiveStartDate=date("Y-m-d H:i:s");
                $vflPasswordReset->effectiveEndDate=date("Y-m-d H:i:s", mktime(date("H", strtotime($vflPasswordReset->effectiveStartDate)),
                                                                               date("i", strtotime($vflPasswordReset->effectiveStartDate)),
                                                                               date("s", strtotime($vflPasswordReset->effectiveStartDate)),
                                                                               date("n", strtotime($vflPasswordReset->effectiveStartDate)),
                                                                               date("j", strtotime($vflPasswordReset->effectiveStartDate))+1,
                                                                               date("Y", strtotime($vflPasswordReset->effectiveStartDate))));
                $vpdo->beginTransaction();
                if ( clspDLPasswordReset::addToDataBase($vflPasswordReset, $vpdo)==1 ){
                    $vflMail= new clspFLMail();
                    $vflMail->sendingUserName="INEF";
                    $vflMail->subject="Solicitud de reestablecimiento de contraseña";
				    $vflMail->receiverAddress=$vflUser->emailAccount;
                    $vflMail->receiverUserName=$vflUser->name . " " . $vflUser->firstName . " " . $vflUser->lastName;
                    $vflMail->message ="<p>Recientemente ha solicitado el proceso de reestablecer su contraseña.</p>";
                    $vflMail->message.="<p>Deberá de ingresar al siguiente link para realizar dicho proceso, el cual tiene una vigencia de 24 hrs.:";
                    $vflMail->message.="    <a href='http://" . $_SERVER['HTTP_HOST'] . "/inef/frmusers-restore-password.php?idPasswordReset=" . $vflPasswordReset->idPasswordReset . "' target='_blank'>";
                    $vflMail->message.="        <strong>Reestablecer contraseña</strong>";
                    $vflMail->message.="    </a>";
                    $vflMail->message.="</p>";
				    if ( clspDLMail::sendEmail($vflMail)!=1 ){
					   $vpdo->rollbackTransaction();
					   $vpdo->closeConnection();
					   return -2;
				    }
                    
                    unset($vflMail);
                }
                else{
                    $vpdo->rollbackTransaction();
                    $vpdo->closeConnection();
                    return -1;
                }
                
                unset($vflPasswordReset);
            }
            else{
                $vpdo->closeConnection();
				return 0;
			}
			$vpdo->commitTransaction();
			$vpdo->closeConnection();			
			
			unset($vpdo);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflUser)
	 {
		try{
			$vpdo= new clspPDO();
            $vpdo->openConnection();
            $vstatus=clspDLUser::updateInDataBase($vflUser, $vpdo);
            $vpdo->closeConnection();
            
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function updatePasswordInDataBase($vflUser, $vpreviousPassword, $vnewPassword)
	 {
		try{
			$vpdo= new clspPDO();
            $vpdo->openConnection();
            if(clspDLUser::verifyPasswordToDataBase($vflUser, $vpreviousPassword, $vpdo)==1){
            	if(clspDLUser::updatePasswordInDataBase($vflUser, $vnewPassword, $vpdo)==0){
	            	return -1;
            	}
            }
            else{
            	return 0;
            }
            unset($vpdo);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function activeInDataBase($vflUser)
	 {
		try{
			$vstatus=-1;
			if ( $vflUser->userStatus->idUserStatus!=1 ){
				$vpdo= new clspPDO();
				$vpdo->openConnection();
                $vflUser->userStatus->idUserStatus=1;
				$vstatus=clspDLUser::updateStatusInDataBase($vflUser, $vpdo);
				$vpdo->closeConnection();
				
				unset($vpdo);
			}
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function deactiveInDataBase($vflUser)
	 {
		try{
			$vstatus=-1;
			if ( $vflUser->userStatus->idUserStatus!=0 ){
				$vpdo= new clspPDO();
				$vpdo->openConnection();
                $vflUser->userStatus->idUserStatus=0;
				$vstatus=clspDLUser::updateStatusInDataBase($vflUser, $vpdo);
				$vpdo->closeConnection();
				
				unset($vpdo);
			}
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function queryByIdToDataBase($vflUser)
	 {
		try{
			$vpdo= new clspPDO();
			$vpdo->openConnection();
			$vstatus=clspDLUser::queryByIdToDataBase($vflUser, $vpdo);
			$vpdo->closeConnection();
			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function queryByEmailAccountToDataBase($vflUser)
	 {
		try{
			$vpdo= new clspPDO();
			$vpdo->openConnection();
			$vstatus=clspDLUser::queryByEmailAccountToDataBase($vflUser, $vpdo);
			$vpdo->closeConnection();
			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function verifyPasswordByEmailAccountToDataBase($vflUser, $vpassword)
	 {
		try{
			$vpdo= new clspPDO();
			$vpdo->openConnection();
			$vstatus=clspDLUser::verifyPasswordByEmailAccountToDataBase($vflUser, $vpassword, $vpdo);
			$vpdo->closeConnection();
			
			unset($vpdo);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct() { }
 }
?>