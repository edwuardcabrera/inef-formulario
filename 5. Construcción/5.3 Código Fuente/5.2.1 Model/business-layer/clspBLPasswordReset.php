<?php
/********************************************************
Name: clspBLPasswordReset.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 10/11/2015
Modification date: 24/05/2017
Description: Password Reset Principal Class, Business Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLPasswordReset.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLUser.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLUser.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspPDO.php");


class clspBLPasswordReset
 {
	public function __construct() { }
	
    
    public static function updateUserPasswordInDataBase($vflPasswordReset, $vpassword)
	 {
		try{
			$vpdo= new clspPDO();
			$vpdo->openConnection();
            clspDLPasswordReset::queryToDataBase($vflPasswordReset, $vpdo);
            if ( $vflPasswordReset->passwordResetStatus->idPasswordResetStatus==0 ){
                $vflUser= new clspFLUser();
                $vflUser->idUser=$vflPasswordReset->user->idUser;
                $vpdo->beginTransaction();
                if ( clspDLUser::updatePasswordInDataBase($vflUser, $vpassword, $vpdo)>=0 ){
                    $vflUser->userStatus->idUserStatus=2;
                    if ( clspDLUser::updateStatusInDataBase($vflUser, $vpdo)>=0 ){
                        $vflPasswordReset->passwordResetStatus->idPasswordResetStatus=1;
                        if ( clspDLPasswordReset::updateStatusInDataBase($vflPasswordReset, $vpdo)==0){
                            $vpdo->rollbackTransaction();
                            $vpdo->closeConnection();
                            return -4;
                        }
                    }
                    else{
                        $vpdo->rollbackTransaction();
                        $vpdo->closeConnection();
                        return -3;
                    }                                 
                }
                else{
                    $vpdo->rollbackTransaction();
                    $vpdo->closeConnection();
                    return -2;
                }
                $vpdo->commitTransaction();
                $vpdo->closeConnection();
                
                unset($vflUser);
            }
            else if ( $vflPasswordReset->passwordResetStatus->idPasswordResetStatus==1 ){
                    return -1;
            }
            else{
                return 0;
            }
            
			unset($vpdo);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function verifyPasswordResetToDataBase($vflPasswordReset)
	 {
		try{
			$vpdo= new clspPDO();
			$vpdo->openConnection();
            if ( clspDLPasswordReset::queryToDataBase($vflPasswordReset, $vpdo)==1 ){
                if ( $vflPasswordReset->passwordResetStatus->idPasswordResetStatus==0 ){
                    date_default_timezone_set('America/Mexico_City');
                    $vcurrentDate=new DateTime(date("Y-m-d H:i:s"));
                    $vstartDate=new DateTime($vflPasswordReset->effectiveStartDate);
                    $vendDate=new DateTime($vflPasswordReset->effectiveEndDate);
                    if ( ! (($vcurrentDate>=$vstartDate) and  ($vcurrentDate<=$vendDate)) ){
                        $vflPasswordReset->passwordResetStatus->idPasswordResetStatus=2;
                        clspDLPasswordReset::updateStatusInDataBase($vflPasswordReset, $vpdo);
                        return -1;
                    }
                }
                else if ( $vflPasswordReset->passwordResetStatus->idPasswordResetStatus==1 ){
                    return -2;
                }
                else{
                    return -1;
                }
            }
            else{
                return 0;
            }
			$vpdo->closeConnection();
			
			unset($vpdo);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }
?>