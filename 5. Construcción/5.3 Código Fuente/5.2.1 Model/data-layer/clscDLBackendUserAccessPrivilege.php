<?php
/********************************************************
Name: clscDLBackendUserAccessPrivilege.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 25/05/2017
Modification date: 31/05/2017
Description: Backend User Access Privilege Collection Class, Data Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLBackendUserAccessPrivilege.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLBackendUserAccessPrivilege.php");


class clscDLBackendUserAccessPrivilege
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflBackendUserAccessPrivileges, $vfilter, $vpdo)
	 {
		try{
			$vsql ="SELECT c_backenduser.id_campus, c_user.*, c_usertype.flduserType, c_userstatus.flduserStatus, c_campus.fldcampus";
            $vsql.=", c_accessprivilege.*, c_accessprivilegetype.fldaccessPrivilegeType ";
            $vsql.="FROM c_backenduseraccessprivilege ";
            $vsql.="INNER JOIN c_backenduser ON c_backenduseraccessprivilege.id_user=c_backenduser.id_user ";
            $vsql.="INNER JOIN c_user ON c_backenduser.id_user=c_user.id_user ";
            $vsql.="INNER JOIN c_usertype ON c_user.id_userType=c_usertype.id_userType ";
			$vsql.="INNER JOIN c_userstatus ON c_user.id_userStatus=c_userstatus.id_userStatus ";
            $vsql.="INNER JOIN c_campus ON c_backenduser.id_campus=c_campus.id_campus ";
            $vsql.="INNER JOIN c_accessprivilege ON c_backenduseraccessprivilege.id_accessprivilege=c_accessprivilege.id_accessprivilege ";
            $vsql.="INNER JOIN c_accessprivilegetype ON c_accessprivilege.id_accessPrivilegeType=c_accessprivilegetype.id_accessPrivilegeType ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY c_accessprivilege.fldlevel1, c_accessprivilege.fldlevel2, c_accessprivilege.fldlevel3, c_accessprivilege.fldlevel4";
            
            self::clean($vflBackendUserAccessPrivileges);
            
			$vpdo->execute($vsql);
            $vrows=$vpdo->getAllDataAlias();
            $vrowsTotal=$vpdo->getAffectedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vflBackendUserAccessPrivilege= new clspFLBackendUserAccessPrivilege();
                $vflBackendUserAccessPrivilege->backendUser->idUser=(int)($vrows[$vrowNumber]["c_user.id_user"]);
                $vflBackendUserAccessPrivilege->backendUser->userType->idUserType=(int)($vrows[$vrowNumber]["c_user.id_userType"]);
				$vflBackendUserAccessPrivilege->backendUser->userType->userType=trim($vrows[$vrowNumber]["c_usertype.flduserType"]);
				$vflBackendUserAccessPrivilege->backendUser->userStatus->idUserStatus=(int)($vrows[$vrowNumber]["c_user.id_userStatus"]);
				$vflBackendUserAccessPrivilege->backendUser->userStatus->userStatus=trim($vrows[$vrowNumber]["c_userstatus.flduserStatus"]);
                $vflBackendUserAccessPrivilege->backendUser->firstName=trim($vrows[$vrowNumber]["c_user.fldfirstName"]);
                $vflBackendUserAccessPrivilege->backendUser->lastName=trim($vrows[$vrowNumber]["c_user.fldlastName"]);
				$vflBackendUserAccessPrivilege->backendUser->name=trim($vrows[$vrowNumber]["c_user.fldname"]);
                $vflBackendUserAccessPrivilege->backendUser->emailAccount=trim($vrows[$vrowNumber]["c_user.fldemailAccount"]);
                $vflBackendUserAccessPrivilege->backendUser->campus->idCampus=(int)($vrows[$vrowNumber]["c_backenduser.id_campus"]);
				$vflBackendUserAccessPrivilege->backendUser->campus->campus=trim($vrows[$vrowNumber]["c_campus.fldcampus"]);
                $vflBackendUserAccessPrivilege->accessPrivilege->idAccessPrivilege=(int)($vrows[$vrowNumber]["c_accessprivilege.id_accessPrivilege"]);
                $vflBackendUserAccessPrivilege->accessPrivilege->accessPrivilegeType->idAccessPrivilegeType=(int)($vrows[$vrowNumber]["c_accessprivilege.id_accessPrivilegeType"]);
                $vflBackendUserAccessPrivilege->accessPrivilege->accessPrivilegeType->accessPrivilegeType=trim($vrows[$vrowNumber]["c_accessprivilegetype.fldaccessPrivilegeType"]);
                $vflBackendUserAccessPrivilege->accessPrivilege->accessPrivilege=trim($vrows[$vrowNumber]["c_accessprivilege.fldaccessPrivilege"]);
				$vflBackendUserAccessPrivilege->accessPrivilege->level1=trim($vrows[$vrowNumber]["c_accessprivilege.fldlevel1"]);
                $vflBackendUserAccessPrivilege->accessPrivilege->level2=trim($vrows[$vrowNumber]["c_accessprivilege.fldlevel2"]);
                $vflBackendUserAccessPrivilege->accessPrivilege->level3=trim($vrows[$vrowNumber]["c_accessprivilege.fldlevel3"]);
                $vflBackendUserAccessPrivilege->accessPrivilege->level4=trim($vrows[$vrowNumber]["c_accessprivilege.fldlevel4"]);
                $vflBackendUserAccessPrivilege->accessPrivilege->url=trim($vrows[$vrowNumber]["c_accessprivilege.fldurl"]);
                $vflBackendUserAccessPrivilege->accessPrivilege->iconName=trim($vrows[$vrowNumber]["c_accessprivilege.fldiconName"]);
                
                self::add($vflBackendUserAccessPrivileges, $vflBackendUserAccessPrivilege);
                unset($vflBackendUserAccessPrivilege);
            }
            if ( $vrowsTotal<=0 ){
                return 0;
            }
			
			unset($vsql, $vrows, $vrowsTotal, $vrowNumber);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function addToDataBase($vflBackendUserAccessPrivileges, $vpdo)
	 {
		try{
			for($vi=0; $vi<self::total($vflBackendUserAccessPrivileges); $vi++){
                $vflBackendUserAccessPrivilege= new clspFLBackendUserAccessPrivilege();
                $vflBackendUserAccessPrivilege=$vflBackendUserAccessPrivileges->backendUserAccessPrivileges[$vi];
                if ( clspDLBackendUserAccessPrivilege::addToDataBase($vflBackendUserAccessPrivilege, $vpdo)==0 ){
                    return 0;
                }
                
                unset($vflBackendUserAccessPrivilege);
			}
            
			unset($vi);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflBackendUserAccessPrivileges, $vpdo)
	 {
		try{
			for($vi=0; $vi<self::total($vflBackendUserAccessPrivileges); $vi++){
                $vflBackendUserAccessPrivilege= new clspFLBackendUserAccessPrivilege();
                $vflBackendUserAccessPrivilege=$vflBackendUserAccessPrivileges->backendUserAccessPrivileges[$vi];
                if ( clspDLBackendUserAccessPrivilege::deleteInDataBase($vflBackendUserAccessPrivilege, $vpdo)==0 ){
                    return 0;
                }
                
                unset($vflBackendUserAccessPrivilege);
			}
            
			unset($vi);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	public static function add($vflBackendUserAccessPrivileges, $vflBackendUserAccessPrivilege)
	 {
        try{
            array_push($vflBackendUserAccessPrivileges->backendUserAccessPrivileges, $vflBackendUserAccessPrivilege);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflBackendUserAccessPrivileges)
	 {
        try{
            return count($vflBackendUserAccessPrivileges->backendUserAccessPrivileges);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflBackendUserAccessPrivileges)
	 {
        try{
            $vflBackendUserAccessPrivileges->backendUserAccessPrivileges=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }
?>