<?php
/********************************************************
Name: clspDLEducationalOffer.php
Version: 0.0.1
Autor name:Alejandro Hdez. G.
Modification autor name:
Creation date: 17/07/2017
Modification date:
Description: Educational Offer Class, Data Layer.
******************************************************* */


class clspDLEducationalOffer
 {
	public function __construct() { }
    
    
   	public static function queryToDatabase($vflEducationalOffer, $vpdo)
	 {
		try{
			$vsql =" SELECT c_educationaloffer.*, c_campus.*, c_educationallevel.fldeducationalLevel ";
			$vsql.=" FROM c_educationaloffer ";
            $vsql.=" INNER JOIN c_assignment ON c_educationaloffer.id_campus=c_assignment.id_campus ";
            $vsql.=" AND c_educationaloffer.id_educationalLevel=c_assignment.id_educationalLevel ";
            $vsql.=" INNER JOIN c_campus ON c_assignment.id_campus=c_campus.id_campus ";
			$vsql.=" INNER JOIN c_educationallevel ON c_assignment.id_educationalLevel=c_educationallevel.id_educationalLevel ";			
			$vsql.=" WHERE c_educationaloffer.id_campus=" .$vflEducationalOffer->assignment->campus->idCampus;
			$vsql.=" AND c_educationaloffer.id_educationalLevel=" .$vflEducationalOffer->assignment->educationalLevel->idEducationalLevel;
			$vsql.=" AND c_educationaloffer.id_educationalOffer=" .$vflEducationalOffer->idEducationalOffer;
            
			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==1 ){
				$vrow=$vpdo->getAllDataAlias();
                $vflEducationalOffer->assignment->campus->idCampus=(int)($vrow[0]["c_educationaloffer.id_campus"]);
                $vflEducationalOffer->assignment->campus->campus=trim($vrow[0]["c_campus.fldcampus"]);
                $vflEducationalOffer->assignment->educationalLevel->idEducationalLevel=(int)($vrow[0]["c_educationaloffer.id_educationalLevel"]);
                $vflEducationalOffer->assignment->educationalLevel->educationalLevel=trim($vrow[0]["c_educationallevel.fldeducationalLevel"]);
                $vflEducationalOffer->idEducationalOffer=(int)($vrow[0]["c_educationaloffer.id_educationalOffer"]);
                $vflEducationalOffer->educationalOffer=trim($vrow[0]["c_educationaloffer.fldeducationalOffer"]);
                $vflEducationalOffer->fileName=trim($vrow[0]["c_educationaloffer.fldfileName"]);                
				unset($vrow);
			}
			else{
				return 0;
			}
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	 
	private static function getIdFromDataBase($vflEducationalOffer, $vpdo)
	 {
		try{
            $vidEducationalOffer=1;
			$vsql =" SELECT MAX(id_educationalOffer) + 1 AS id_educationOfferNew ";
			$vsql.=" FROM c_educationaloffer";
			$vsql.=" WHERE id_campus=". $vflEducationalOffer->assignment->campus->idCampus;
            $vsql.=" AND id_educationalLevel=". $vflEducationalOffer->assignment->educationalLevel->idEducationalLevel;
			
            $vpdo->execute($vsql);
            $vrow=$vpdo->getAllData();
			if ( (! is_null($vrow[0])) && ((int)($vrow[0]["id_educationOfferNew"])>0) ){
                $vidEducationalOffer=(int)($vrow[0]["id_educationOfferNew"]);
            }
            
            unset($vsql, $vrow);
            return $vidEducationalOffer;
		 }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	 
	public static function addToDataBase($vflEducationalOffer, $vpdo, $vtype=0)
	 {
		try {  
			$vflEducationalOffer->idEducationalOffer=self::getIdFromDataBase($vflEducationalOffer, $vpdo);
			
			$vsql =" INSERT INTO c_educationaloffer(id_campus, id_educationalLevel, id_educationalOffer, fldeducationalOffer, fldfileName) ";
			$vsql.=" VALUES( " . $vflEducationalOffer->assignment->campus->idCampus;
			$vsql.="	  ,  " . $vflEducationalOffer->assignment->educationalLevel->idEducationalLevel;
			$vsql.="	  ,  " . $vflEducationalOffer->idEducationalOffer;
			$vsql.="	  , '" . $vflEducationalOffer->educationalOffer . "' ";
			$vsql.="	  , '" . $vflEducationalOffer->fileName . "')";
			
			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==0 ){
				return 0;
			}
			unset( $vsql);           
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	 
	public static function updateInDataBase($vflEducationalOffer, $vpdo)
	 {
		try{
			$vsql =" UPDATE c_educationaloffer ";
            $vsql.=" SET fldeducationalOffer='". $vflEducationalOffer->educationalOffer ."' ";	
			$vsql.=" , fldfileName='". $vflEducationalOffer->fileName . "' ";			
            $vsql.=" WHERE id_campus=". $vflEducationalOffer->assignment->campus->idCampus;
            $vsql.=" AND id_educationalLevel=". $vflEducationalOffer->assignment->educationalLevel->idEducationalLevel;
            $vsql.=" AND id_educationalOffer=". $vflEducationalOffer->idEducationalOffer;
            
            $vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==0 ){
                return 0;
			}            
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	public static function updateFileInDataBase($vflEducationalOffer, $vpdo)
	 {
		try{
			$vsql =' UPDATE c_educationaloffer ';
            $vsql.=' SET fldfileName="'.$vflEducationalOffer->fileName.'" ';
            $vsql.=' WHERE id_campus='. $vflEducationalOffer->assignment->campus->idCampus;
            $vsql.=' AND id_educationalLevel='. $vflEducationalOffer->assignment->educationalLevel->idEducationalLevel;
            $vsql.=' AND id_educationalOffer='. $vflEducationalOffer->idEducationalOffer;

			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==0 ){
				return 0;
			}            
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
    public static function deleteInDataBase($vflEducationalOffer, $vpdo)
	 {
		try{
			$vsql =" DELETE FROM c_educationaloffer ";
            $vsql.=" WHERE id_campus=" . $vflEducationalOffer->assignment->campus->idCampus;
            $vsql.=" AND id_educationalLevel=" . $vflEducationalOffer->assignment->educationalLevel->idEducationalLevel;
            $vsql.=" AND id_educationalOffer=" . $vflEducationalOffer->idEducationalOffer;
			
            $vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==0 ){
                return 0;
			}            
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }    
     
     
    public function __destruct() {}
 }
?>