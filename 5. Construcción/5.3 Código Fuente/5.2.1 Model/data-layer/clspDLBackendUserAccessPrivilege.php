<?php
/********************************************************
Name: clspDLBackendUserAccessPrivilege.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 31/05/2017
Modification date:
Description: Backend User Access Privilege Principal Class, Data Layer. 
********************************************************/


class clspDLBackendUserAccessPrivilege
 {
	public function __construct() { }
	
    
	public static function addToDataBase($vflBackendUserAccessPrivilege, $vmySql)
	 {
		try{
            $vsql ="INSERT INTO c_backenduseraccessprivilege(id_user, id_accessPrivilege) ";
			$vsql.="VALUES(" . $vflBackendUserAccessPrivilege->backendUser->idUser;
			$vsql.=", " . $vflBackendUserAccessPrivilege->accessPrivilege->idAccessPrivilege . ")";
            
			$vmySql->execute($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function deleteInDataBase($vflBackendUserAccessPrivilege, $vmySql)
	 {
		try{
			$vsql ="DELETE FROM c_backenduseraccessprivilege ";
            $vsql.="WHERE id_user=" . $vflBackendUserAccessPrivilege->backendUser->idUser . " ";
            $vsql.="AND id_accessPrivilege=" . $vflBackendUserAccessPrivilege->accessPrivilege->idAccessPrivilege . " ";
			
			$vmySql->execute($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
	public function __destruct(){ }
 }
?>