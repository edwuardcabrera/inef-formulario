<?php
/********************************************************
Name: clspDLCampusEmailSetting.php
Version: 0.0.1
Autor name: Sandro Alan Gomez Aceituno.
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 11/06/2017
Modification date: 20/06/2017
Description: Campus Email Setting Principal Class, Data Layer. 
********************************************************/


class clspDLCampusEmailSetting
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflCampusEmailSetting, $vpdo, $vtype=0)
	 {
		try {            
			$vsql =" INSERT INTO c_campusemailsetting(id_campus, fldhostname, fldsmtpPort, fldsmtpAuth, fldsmtpSecure, flduserName, flduserPassword) ";
			$vsql.=" VALUES( " . $vflCampusEmailSetting->campus->idCampus;
			$vsql.="	  , '" . $vflCampusEmailSetting->hostName . "' ";
			$vsql.="	  , "  . $vflCampusEmailSetting->smtpPort;
            $vsql.="	  , "  . $vflCampusEmailSetting->smtpAuth;
			$vsql.="	  , '" . $vflCampusEmailSetting->smtpSecure . "' ";
			$vsql.="	  , '" . $vflCampusEmailSetting->userName . "' ";
			$vsql.="	  , '" . $vflCampusEmailSetting->userPassword . "')";
			
			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==0 ){
				return 0;
			}
			unset( $vsql);           
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflCampusEmailSetting, $vpdo)
	 {
		try{
			$vsql ="UPDATE c_campusemailsetting ";
            $vsql.="SET fldhostname='" . $vflCampusEmailSetting->hostName . "'";
			$vsql.=", fldsmtpPort=" . $vflCampusEmailSetting->smtpPort;
			$vsql.=", fldsmtpAuth=" . $vflCampusEmailSetting->smtpAuth;
			$vsql.=", fldsmtpSecure='" . $vflCampusEmailSetting->smtpSecure . "'";
			$vsql.=", flduserName='" . $vflCampusEmailSetting->userName . "'";
			$vsql.=", flduserPassword='" . $vflCampusEmailSetting->userPassword . "' ";			
            $vsql.="WHERE id_campus=" . $vflCampusEmailSetting->campus->idCampus;
            
            $vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==0 ){
                return 0;
			}            
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function queryToDataBase($vflCampusEmailSetting, $vpdo)
	 {
		try {
			$vsql ="SELECT c_campusemailsetting.*, c_campus.fldcampus ";
			$vsql.="FROM c_campusemailsetting ";
            $vsql.="INNER JOIN c_campus ON c_campusemailsetting.id_campus=c_campus.id_campus ";
			$vsql.="WHERE c_campusemailsetting.id_campus=" . $vflCampusEmailSetting->campus->idCampus;
			
			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==1 ){
				$vrow=$vpdo->getAllData();
                $vflCampusEmailSetting->campus->campus=trim($vrow[0]["fldcampus"]);
                $vflCampusEmailSetting->hostName=trim($vrow[0]["fldhostname"]);
				$vflCampusEmailSetting->smtpPort=(int)($vrow[0]["fldsmtpPort"]);
				$vflCampusEmailSetting->smtpAuth=(int)($vrow[0]["fldsmtpAuth"]);
				$vflCampusEmailSetting->smtpSecure=trim($vrow[0]["fldsmtpSecure"]);
                $vflCampusEmailSetting->userName=trim($vrow[0]["flduserName"]);
                $vflCampusEmailSetting->userPassword=trim($vrow[0]["flduserPassword"]);
                
				unset($vrow);
			}
			else{
				return 0;
			}
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }
?>