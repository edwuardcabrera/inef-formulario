<?php
/********************************************************
Name: clscDLUser.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 31/05/2017
Modification date:
Description: User Collection Class, Data Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLUser.php");


class clscDLUser
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflUsers, $vfilter, $vpdo)
	 {
		try{
			$vsql ="SELECT c_user.*, c_usertype.flduserType, c_userstatus.flduserStatus ";
			$vsql.="FROM c_user ";
            $vsql.="INNER JOIN c_usertype ON c_user.id_userType=c_usertype.id_userType ";
			$vsql.="INNER JOIN c_userstatus ON c_user.id_userStatus=c_userstatus.id_userStatus ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY c_user.fldname, c_user.fldfirstName, c_user.fldlastName";
            
            self::clean($vflUsers);
            
			$vpdo->execute($vsql);
            $vrows=$vpdo->getAllDataAlias();
            $vrowsTotal=$vpdo->getAffectedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vflUser= new clspFLUser();
                $vflUser->idUser=(int)($vrows[$vrowNumber]["c_user.id_user"]);
                $vflUser->userType->idUserType=(int)($vrows[$vrowNumber]["c_user.id_userType"]);
				$vflUser->userType->userType=trim($vrows[$vrowNumber]["c_usertype.flduserType"]);
				$vflUser->userStatus->idUserStatus=(int)($vrows[$vrowNumber]["c_user.id_userStatus"]);
				$vflUser->userStatus->userStatus=trim($vrows[$vrowNumber]["c_userstatus.flduserStatus"]);
                $vflUser->firstName=trim($vrows[$vrowNumber]["c_user.fldfirstName"]);
                $vflUser->lastName=trim($vrows[$vrowNumber]["c_user.fldlastName"]);
				$vflUser->name=trim($vrows[$vrowNumber]["c_user.fldname"]);
                $vflUser->emailAccount=trim($vrows[$vrowNumber]["c_user.fldemailAccount"]);
                
                self::add($vflUsers, $vflUser);
                unset($vflUser);
            }
            if ( $vrowsTotal<=0 ){
                return 0;
            }
			
			unset($vsql, $vrows, $vrowsTotal, $vrowNumber);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflUsers, $vflUser)
	 {
        try{
            array_push($vflUsers->users, $vflUser);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflUsers)
	 {
        try{
            return count($vflUsers->users);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflUsers)
	 {
        try{
            $vflUsers->users=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }
?>