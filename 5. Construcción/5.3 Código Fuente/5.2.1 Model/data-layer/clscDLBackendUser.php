<?php
/********************************************************
Name: clscDLBackendUser.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 31/05/2017
Modification date:
Description: Backend User Collection Class, Data Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLBackendUser.php");


class clscDLBackendUser
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflBackendUsers, $vfilter, $vpdo)
	 {
		try{
			$vsql ="SELECT c_backenduser.id_campus, c_user.*, c_usertype.flduserType, c_userstatus.flduserStatus, c_campus.fldcampus ";
			$vsql.="FROM c_backenduser ";
            $vsql.="INNER JOIN c_user ON c_backenduser.id_user=c_user.id_user ";
            $vsql.="INNER JOIN c_usertype ON c_user.id_userType=c_usertype.id_userType ";
			$vsql.="INNER JOIN c_userstatus ON c_user.id_userStatus=c_userstatus.id_userStatus ";
            $vsql.="INNER JOIN c_campus ON c_backenduser.id_campus=c_campus.id_campus ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY c_user.fldname, c_user.fldfirstName, c_user.fldlastName";
            
            self::clean($vflBackendUsers);
            
			$vpdo->execute($vsql);
            $vrows=$vpdo->getAllDataAlias();
            $vrowsTotal=$vpdo->getAffectedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vflBackendUser= new clspFLBackendUser();
                $vflBackendUser->idUser=(int)($vrows[$vrowNumber]["c_user.id_user"]);
                $vflBackendUser->userType->idUserType=(int)($vrows[$vrowNumber]["c_user.id_userType"]);
				$vflBackendUser->userType->userType=trim($vrows[$vrowNumber]["c_usertype.flduserType"]);
				$vflBackendUser->userStatus->idUserStatus=(int)($vrows[$vrowNumber]["c_user.id_userStatus"]);
				$vflBackendUser->userStatus->userStatus=trim($vrows[$vrowNumber]["c_userstatus.flduserStatus"]);
                $vflBackendUser->firstName=trim($vrows[$vrowNumber]["c_user.fldfirstName"]);
                $vflBackendUser->lastName=trim($vrows[$vrowNumber]["c_user.fldlastName"]);
				$vflBackendUser->name=trim($vrows[$vrowNumber]["c_user.fldname"]);
                $vflBackendUser->emailAccount=trim($vrows[$vrowNumber]["c_user.fldemailAccount"]);
                $vflBackendUser->campus->idCampus=(int)($vrows[$vrowNumber]["c_backenduser.id_campus"]);
				$vflBackendUser->campus->campus=trim($vrows[$vrowNumber]["c_campus.fldcampus"]);
                
                self::add($vflBackendUsers, $vflBackendUser);
                unset($vflBackendUser);
            }
            if ( $vrowsTotal<=0 ){
                return 0;
            }
			
			unset($vsql, $vrows, $vrowsTotal, $vrowNumber);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflBackendUsers, $vflBackendUser)
	 {
        try{
            array_push($vflBackendUsers->backendUsers, $vflBackendUser);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflBackendUsers)
	 {
        try{
            return count($vflBackendUsers->backendUsers);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflBackendUsers)
	 {
        try{
            $vflBackendUsers->backendUsers=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }
?>