<?php
/********************************************************
Name: clscDLApplicant.php
Version: 0.0.1
Autor name: Alejandro Hdez. Guzmán
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 15/06/2017
Modification date: 21/07/2017
Description: Applicant Collection Class, Data Layer.
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLApplicant.php");


class clscDLApplicant
 {
    public function __construct() { }

    public static function queryToDatabase($vflApplicants, $vfilter, $vpdo)
     {
        try{
            $vsql ="SELECT p_applicant.*, c_campus.fldcampus, c_userapplicant.fldmovilNumber, c_user.* , c_usertype.flduserType";
            $vsql.=", c_userstatus.flduserStatus, c_educationaloffer.fldeducationalOffer, c_educationaloffer.fldfileName";
            $vsql.=", c_educationallevel.fldeducationalLevel ";
            $vsql.="FROM p_applicant ";
            $vsql.="INNER JOIN c_campus ON p_applicant.id_campus=c_campus.id_campus ";
            $vsql.="INNER JOIN c_userapplicant ON p_applicant.id_user=c_userapplicant.id_user ";
            $vsql.="INNER JOIN c_user ON c_userapplicant.id_user=c_user.id_user ";
            $vsql.="INNER JOIN c_usertype ON c_user.id_userType=c_usertype.id_userType ";
            $vsql.="INNER JOIN c_userstatus ON c_user.id_userStatus=c_userstatus.id_userStatus ";
            $vsql.="INNER JOIN c_educationaloffer ON p_applicant.id_campus=c_educationaloffer.id_campus ";
            $vsql.="AND p_applicant.id_educationalLevel=c_educationaloffer.id_educationalLevel ";
            $vsql.="AND p_applicant.id_educationalOffer=c_educationaloffer.id_educationalOffer ";
            $vsql.="INNER JOIN c_assignment ON c_educationaloffer.id_campus=c_assignment.id_campus ";
            $vsql.="AND c_educationaloffer.id_educationalLevel=c_assignment.id_educationalLevel ";
            $vsql.="INNER JOIN c_educationallevel ON c_assignment.id_educationalLevel=c_educationallevel.id_educationalLevel ";
            $vsql.=$vfilter . " ";
            $vsql.="ORDER BY p_applicant.fldrequestDate DESC";
            
            self::clean($vflApplicants);
            
			$vpdo->execute($vsql);
            $vrows=$vpdo->getAllDataAlias();
            $vrowsTotal=$vpdo->getAffectedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vflApplicant= new clspFLApplicant();
                $vflApplicant->campus->idCampus=(int)($vrows[$vrowNumber]["p_applicant.id_campus"]);
                $vflApplicant->campus->campus=trim($vrows[$vrowNumber]["c_campus.fldcampus"]);
                $vflApplicant->idApplicant=trim($vrows[$vrowNumber]["p_applicant.id_applicant"]);
                $vflApplicant->user->idUser= (int)($vrows[$vrowNumber]["p_applicant.id_user"]);
                $vflApplicant->user->userType->idUserType=(int)($vrows[$vrowNumber]["c_user.id_userType"]);
        		$vflApplicant->user->userType->userType=trim($vrows[$vrowNumber]["c_usertype.flduserType"]);
        		$vflApplicant->user->userStatus->idUserStatus=(int)($vrows[$vrowNumber]["c_user.id_userStatus"]);
        		$vflApplicant->user->userStatus->userStatus=trim($vrows[$vrowNumber]["c_userstatus.flduserStatus"]);
                $vflApplicant->user->firstName=trim($vrows[$vrowNumber]["c_user.fldfirstName"]);
                $vflApplicant->user->lastName=trim($vrows[$vrowNumber]["c_user.fldlastName"]);
                $vflApplicant->user->name=trim($vrows[$vrowNumber]["c_user.fldname"]);
                $vflApplicant->user->emailAccount=trim($vrows[$vrowNumber]["c_user.fldemailAccount"]);
                $vflApplicant->user->movilNumber=trim($vrows[$vrowNumber]["c_userapplicant.fldmovilNumber"]);
                $vflApplicant->educationalOffer->assignment->campus=$vflApplicant->campus;
                $vflApplicant->educationalOffer->assignment->educationalLevel->idEducationalLevel=(int)($vrows[$vrowNumber]["p_applicant.id_educationalLevel"]);
                $vflApplicant->educationalOffer->assignment->educationalLevel->educationalLevel=trim($vrows[$vrowNumber]["c_educationallevel.fldeducationalLevel"]);
                $vflApplicant->educationalOffer->idEducationalOffer=(int)($vrows[$vrowNumber]["p_applicant.id_educationalOffer"]);
                $vflApplicant->educationalOffer->educationalOffer=trim($vrows[$vrowNumber]["c_educationaloffer.fldeducationalOffer"]);
                $vflApplicant->educationalOffer->fileName=trim($vrows[$vrowNumber]["c_educationaloffer.fldfileName"]);
                $vflApplicant->requestDate=date("m/d/Y H:i:s", strtotime(trim($vrows[$vrowNumber]["p_applicant.fldrequestDate"])));
				
                self::add($vflApplicants, $vflApplicant);
                unset($vflApplicant);
            }
            if ( $vrowsTotal<=0 ){
                return 0;
            }
			
			unset($vsql, $vrows, $vrowsTotal, $vrowNumber);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
     }
    
    public static function queryByUserGroupToDataBase($vflApplicants, $vfilter, $vpdo)
	 {
		try{
			$vsql ="SELECT p_applicant.*, c_campus.fldcampus, c_userapplicant.fldmovilNumber, c_user.* , c_usertype.flduserType";
            $vsql.=", c_userstatus.flduserStatus, c_educationaloffer.fldeducationalOffer, c_educationaloffer.fldfileName";
            $vsql.=", c_educationallevel.fldeducationalLevel ";
            $vsql.="FROM p_applicant ";
            $vsql.="INNER JOIN c_campus ON p_applicant.id_campus=c_campus.id_campus ";
            $vsql.="INNER JOIN c_userapplicant ON p_applicant.id_user=c_userapplicant.id_user ";
            $vsql.="INNER JOIN c_user ON c_userapplicant.id_user=c_user.id_user ";
            $vsql.="INNER JOIN c_usertype ON c_user.id_userType=c_usertype.id_userType ";
            $vsql.="INNER JOIN c_userstatus ON c_user.id_userStatus=c_userstatus.id_userStatus ";
            $vsql.="INNER JOIN c_educationaloffer ON p_applicant.id_campus=c_educationaloffer.id_campus ";
            $vsql.="AND p_applicant.id_educationalLevel=c_educationaloffer.id_educationalLevel ";
            $vsql.="AND p_applicant.id_educationalOffer=c_educationaloffer.id_educationalOffer ";
            $vsql.="INNER JOIN c_assignment ON c_educationaloffer.id_campus=c_assignment.id_campus ";
            $vsql.="AND c_educationaloffer.id_educationalLevel=c_assignment.id_educationalLevel ";
            $vsql.="INNER JOIN c_educationallevel ON c_assignment.id_educationalLevel=c_educationallevel.id_educationalLevel ";
            $vsql.=$vfilter . " ";
            $vsql.="GROUP BY p_applicant.id_user ";
			$vsql.="ORDER BY p_applicant.id_campus, p_applicant.id_educationalLevel, p_applicant.id_educationalOffer, p_applicant.id_user";
            
            self::clean($vflApplicants);
            
			$vpdo->execute($vsql);
            $vrows=$vpdo->getAllDataAlias();
            $vrowsTotal=$vpdo->getAffectedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vflApplicant= new clspFLApplicant();
                $vflApplicant->campus->idCampus=(int)($vrows[$vrowNumber]["p_applicant.id_campus"]);
                $vflApplicant->campus->campus=trim($vrows[$vrowNumber]["c_campus.fldcampus"]);
                $vflApplicant->idApplicant=trim($vrows[$vrowNumber]["p_applicant.id_applicant"]);
                $vflApplicant->user->idUser= (int)($vrows[$vrowNumber]["p_applicant.id_user"]);
                $vflApplicant->user->userType->idUserType=(int)($vrows[$vrowNumber]["c_user.id_userType"]);
				$vflApplicant->user->userType->userType=trim($vrows[$vrowNumber]["c_usertype.flduserType"]);
				$vflApplicant->user->userStatus->idUserStatus=(int)($vrows[$vrowNumber]["c_user.id_userStatus"]);
				$vflApplicant->user->userStatus->userStatus=trim($vrows[$vrowNumber]["c_userstatus.flduserStatus"]);
                $vflApplicant->user->firstName=trim($vrows[$vrowNumber]["c_user.fldfirstName"]);
                $vflApplicant->user->lastName=trim($vrows[$vrowNumber]["c_user.fldlastName"]);
				$vflApplicant->user->name=trim($vrows[$vrowNumber]["c_user.fldname"]);
                $vflApplicant->user->emailAccount=trim($vrows[$vrowNumber]["c_user.fldemailAccount"]);
                $vflApplicant->user->movilNumber=trim($vrows[$vrowNumber]["c_userapplicant.fldmovilNumber"]);
                $vflApplicant->educationalOffer->assignment->campus=$vflApplicant->campus;
                $vflApplicant->educationalOffer->assignment->educationalLevel->idEducationalLevel=(int)($vrows[$vrowNumber]["p_applicant.id_educationalLevel"]);
                $vflApplicant->educationalOffer->assignment->educationalLevel->educationalLevel=trim($vrows[$vrowNumber]["c_educationallevel.fldeducationalLevel"]);
                $vflApplicant->educationalOffer->idEducationalOffer=(int)($vrows[$vrowNumber]["p_applicant.id_educationalOffer"]);
                $vflApplicant->educationalOffer->educationalOffer=trim($vrows[$vrowNumber]["c_educationaloffer.fldeducationalOffer"]);
                $vflApplicant->educationalOffer->fileName=trim($vrows[$vrowNumber]["c_educationaloffer.fldfileName"]);
                $vflApplicant->requestDate=date("m/d/Y H:i:s", strtotime(trim($vrows[$vrowNumber]["p_applicant.fldrequestDate"])));
				
                self::add($vflApplicants, $vflApplicant);
                unset($vflApplicant);
            }
            if ( $vrowsTotal<=0 ){
                return 0;
            }
			
			unset($vsql, $vrows, $vrowsTotal, $vrowNumber);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    private static function add($vflApplicants, $vflApplicant) {
        try {
            array_push($vflApplicants->applicants, $vflApplicant);
        } catch (Exception $vexception) {
            throw new Exception($vexception->getMessage(), $vexception->getCode());
        }
    }

    public static function total($vflApplicants) {
        try {
            return count($vflApplicants->applicants);
        } catch (Exception $vexception) {
            throw new Exception($vexception->getMessage(), $vexception->getCode());
        }
    }

    private static function clean($vflApplicants) {
        try {
            $vflApplicants->applicants = array();
        } catch (Exception $vexception) {
            throw new Exception($vexception->getMessage(), $vexception->getCode());
        }
    }

    public function __destruct() {
        
    }

}
?>