<?php
/********************************************************
Name: clscDLEducationalOffer.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 11/07/2017
Modification date:
Description: Educational Offer Collection Class, Data Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLEducationalOffer.php");


class clscDLEducationalOffer
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflEducationalOffers, $vfilter, $vpdo)
	 {
		try{
			$vsql ="SELECT c_educationaloffer.*, c_campus.fldcampus, c_educationallevel.fldeducationalLevel ";
			$vsql.="FROM c_educationaloffer ";
            $vsql.="INNER JOIN c_assignment ON c_educationaloffer.id_campus=c_assignment.id_campus ";
            $vsql.="AND c_educationaloffer.id_educationalLevel=c_assignment.id_educationalLevel ";
            $vsql.="INNER JOIN c_campus ON c_assignment.id_campus=c_campus.id_campus ";
			$vsql.="INNER JOIN c_educationallevel ON c_assignment.id_educationalLevel=c_educationallevel.id_educationalLevel ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY c_educationaloffer.id_campus, c_educationaloffer.id_educationalLevel, c_educationaloffer.fldeducationalOffer";
            
            self::clean($vflEducationalOffers);
            
			$vpdo->execute($vsql);
            $vrows=$vpdo->getAllDataAlias();
            $vrowsTotal=$vpdo->getAffectedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vflEducationalOffer= new clspFLEducationalOffer();
                $vflEducationalOffer->assignment->campus->idCampus=(int)($vrows[$vrowNumber]["c_educationaloffer.id_campus"]);
                $vflEducationalOffer->assignment->campus->campus=trim($vrows[$vrowNumber]["c_campus.fldcampus"]);
                $vflEducationalOffer->assignment->educationalLevel->idEducationalLevel=(int)($vrows[$vrowNumber]["c_educationaloffer.id_educationalLevel"]);
                $vflEducationalOffer->assignment->educationalLevel->educationalLevel=trim($vrows[$vrowNumber]["c_educationallevel.fldeducationalLevel"]);
                $vflEducationalOffer->idEducationalOffer=(int)($vrows[$vrowNumber]["c_educationaloffer.id_educationalOffer"]);
                $vflEducationalOffer->educationalOffer=trim($vrows[$vrowNumber]["c_educationaloffer.fldeducationalOffer"]);
                $vflEducationalOffer->fileName=trim($vrows[$vrowNumber]["c_educationaloffer.fldfileName"]);
				
                self::add($vflEducationalOffers, $vflEducationalOffer);
                unset($vflEducationalOffer);
            }
            if ( $vrowsTotal<=0 ){
                return 0;
            }
			
			unset($vsql, $vrows, $vrowsTotal, $vrowNumber);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflEducationalOffers, $vflEducationalOffer)
	 {
        try{
            array_push($vflEducationalOffers->educationalOffers, $vflEducationalOffer);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflEducationalOffers)
	 {
        try{
            return count($vflEducationalOffers->educationalOffers);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflEducationalOffers)
	 {
        try{
            $vflEducationalOffers->educationalOffers=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }
?>