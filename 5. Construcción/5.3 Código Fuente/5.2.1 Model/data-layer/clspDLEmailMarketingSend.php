<?php
/********************************************************
Name: clspDLEmailMarketingSend.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 28/06/2017
Modification date:
Description: Email Marketing Send Principal Class, Data Layer. 
********************************************************/


class clspDLEmailMarketingSend
 {
	public function __construct() { }
	
    
    public static function addToDataBase($vflEmailMarketingSend, $vpdo)
	 {
		try{
            $vsql ="INSERT INTO p_emailmarketingsend(id_campus, id_emailMarketing, id_userMailing, fldmailingDate) ";
			$vsql.="VALUES(" . $vflEmailMarketingSend->emailMarketing->campus->idCampus;
            $vsql.=", '" . $vflEmailMarketingSend->emailMarketing->idEmailMarketing .  "'";
            $vsql.=", " . $vflEmailMarketingSend->userMailing->idUser;
            $vsql.=", '" . date("Y-m-d H:i:s", strtotime($vflEmailMarketingSend->mailingDate)) . "')";
            
			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }  
     
     
	public function __destruct(){ }
 }
?>