<?php
/********************************************************
Name: clscDLEmailMarketingScheduled.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 05/08/2017
Modification date:
Description: Email Marketing Scheduled Collection Class, Data Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLEmailMarketingScheduled.php");


class clscDLEmailMarketingScheduled
 {
    public function __construct() { }    
    
    public static function queryToDataBase($vflEmailMarketingScheduleds, $vfilter, $vpdo)
	 {
		try{            
			$vsql ="SELECT p_emailmarketingscheduled.fldscheduledDate, p_emailmarketing.*, c_campus.fldcampus, c_emailmarketingtype.fldemailMarketingType ";
            $vsql.=", c_emailmarketingstatus.fldemailMarketingStatus, c_user.*, c_usertype.flduserType, c_userstatus.flduserStatus ";
            $vsql.=", c_educationaloffer.fldeducationalOffer, c_educationaloffer.fldfileName, c_educationallevel.fldeducationalLevel ";
            $vsql.="FROM p_emailmarketingscheduled ";
			$vsql.="INNER JOIN p_emailmarketing ON p_emailmarketingscheduled.id_campus=p_emailmarketing.id_campus ";
            $vsql.="AND p_emailmarketingscheduled.id_emailMarketing=p_emailmarketing.id_emailMarketing ";
            $vsql.="INNER JOIN c_campus ON p_emailmarketing.id_campus=c_campus.id_campus ";
            $vsql.="INNER JOIN c_emailmarketingtype ON p_emailmarketing.id_emailMarketingType=c_emailmarketingtype.id_emailMarketingType ";
            $vsql.="INNER JOIN c_emailmarketingstatus ON p_emailmarketing.id_emailMarketingStatus=c_emailmarketingstatus.id_emailMarketingStatus ";
            $vsql.="INNER JOIN c_backenduser ON p_emailmarketing.id_userRecord=c_backenduser.id_user ";
            $vsql.="INNER JOIN c_user ON c_backenduser.id_user=c_user.id_user ";
            $vsql.="INNER JOIN c_usertype ON c_user.id_userType=c_usertype.id_userType ";
			$vsql.="INNER JOIN c_userstatus ON c_user.id_userStatus=c_userstatus.id_userStatus ";
            $vsql.="INNER JOIN c_educationaloffer ON p_emailmarketing.id_campus=c_educationaloffer.id_campus ";
            $vsql.="AND p_emailmarketing.id_educationalLevel=c_educationaloffer.id_educationalLevel ";
            $vsql.="AND p_emailmarketing.id_educationalOffer=c_educationaloffer.id_educationalOffer ";
            $vsql.="INNER JOIN c_assignment ON c_educationaloffer.id_campus=c_assignment.id_campus ";
            $vsql.="AND c_educationaloffer.id_educationalLevel=c_assignment.id_educationalLevel ";
            $vsql.="INNER JOIN c_educationallevel ON c_assignment.id_educationalLevel=c_educationallevel.id_educationalLevel ";
			$vsql.= $vfilter . " ";
            $vsql.="ORDER BY p_emailmarketing.id_campus ASC, p_emailmarketing.id_emailMarketing";
            
            self::clean($vflEmailMarketingScheduleds);            
			$vpdo->execute($vsql);
            $vrows=$vpdo->getAllDataAlias();
            $vrowsTotal=$vpdo->getAffectedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vflEmailMarketingScheduled= new clspFLEmailMarketingScheduled();
                $vflEmailMarketingScheduled->emailMarketing->idEmailMarketing=trim($vrows[$vrowNumber]["p_emailmarketing.id_emailMarketing"]);
				$vflEmailMarketingScheduled->emailMarketing->campus->idCampus=(int)($vrows[$vrowNumber]["p_emailmarketing.id_campus"]);
				$vflEmailMarketingScheduled->emailMarketing->campus->campus=trim($vrows[$vrowNumber]["c_campus.fldcampus"]);
				$vflEmailMarketingScheduled->emailMarketing->emailMarketingType->idEmailMarketingType=(int)($vrows[$vrowNumber]["p_emailmarketing.id_emailMarketingType"]);
				$vflEmailMarketingScheduled->emailMarketing->emailMarketingType->emailMarketingType=trim($vrows[$vrowNumber]["c_emailmarketingtype.fldemailMarketingType"]);
                $vflEmailMarketingScheduled->emailMarketing->emailMarketingStatus->idEmailMarketingStatus=(int)($vrows[$vrowNumber]["p_emailmarketing.id_emailMarketingStatus"]);
				$vflEmailMarketingScheduled->emailMarketing->emailMarketingStatus->emailMarketingStatus=trim($vrows[$vrowNumber]["c_emailmarketingstatus.fldemailMarketingStatus"]);
                $vflEmailMarketingScheduled->emailMarketing->userRecord->idUser= (int)($vrows[$vrowNumber]["p_emailmarketing.id_userRecord"]);
                $vflEmailMarketingScheduled->emailMarketing->userRecord->userType->idUserType=(int)($vrows[$vrowNumber]["c_user.id_userType"]);
				$vflEmailMarketingScheduled->emailMarketing->userRecord->userType->userType=trim($vrows[$vrowNumber]["c_usertype.flduserType"]);
				$vflEmailMarketingScheduled->emailMarketing->userRecord->userStatus->idUserStatus=(int)($vrows[$vrowNumber]["c_user.id_userStatus"]);
				$vflEmailMarketingScheduled->emailMarketing->userRecord->userStatus->userStatus=trim($vrows[$vrowNumber]["c_userstatus.flduserStatus"]);
                $vflEmailMarketingScheduled->emailMarketing->userRecord->firstName=trim($vrows[$vrowNumber]["c_user.fldfirstName"]);
                $vflEmailMarketingScheduled->emailMarketing->userRecord->lastName=trim($vrows[$vrowNumber]["c_user.fldlastName"]);
				$vflEmailMarketingScheduled->emailMarketing->userRecord->name=trim($vrows[$vrowNumber]["c_user.fldname"]);
                $vflEmailMarketingScheduled->emailMarketing->userRecord->emailAccount=trim($vrows[$vrowNumber]["c_user.fldemailAccount"]);
                $vflEmailMarketingScheduled->emailMarketing->userRecord->campus=$vflEmailMarketingScheduled->emailMarketing->campus;
                $vflEmailMarketingScheduled->emailMarketing->educationalOffer->assignment->campus=$vflEmailMarketingScheduled->emailMarketing->campus;
                $vflEmailMarketingScheduled->emailMarketing->educationalOffer->assignment->educationalLevel->idEducationalLevel=(int)($vrows[$vrowNumber]["p_emailmarketing.id_educationalLevel"]);
                $vflEmailMarketingScheduled->emailMarketing->educationalOffer->assignment->educationalLevel->educationalLevel=trim($vrows[$vrowNumber]["c_educationallevel.fldeducationalLevel"]);
                $vflEmailMarketingScheduled->emailMarketing->educationalOffer->idEducationalOffer=(int)($vrows[$vrowNumber]["p_emailmarketing.id_educationalOffer"]);
                $vflEmailMarketingScheduled->emailMarketing->educationalOffer->educationalOffer=trim($vrows[$vrowNumber]["c_educationaloffer.fldeducationalOffer"]);
                $vflEmailMarketingScheduled->emailMarketing->educationalOffer->fileName=trim($vrows[$vrowNumber]["c_educationaloffer.fldfileName"]);
                $vflEmailMarketingScheduled->emailMarketing->subject=trim($vrows[$vrowNumber]["p_emailmarketing.fldsubject"]);
                $vflEmailMarketingScheduled->emailMarketing->message=trim($vrows[$vrowNumber]["p_emailmarketing.fldmessage"]);
                $vflEmailMarketingScheduled->emailMarketing->recordDate=date("m/d/Y H:i:s", strtotime(trim($vrows[$vrowNumber]["p_emailmarketing.fldrecordDate"])));
				$vflEmailMarketingScheduled->scheduledDate=date("m/d/Y H:i:s", strtotime(trim($vrows[$vrowNumber]["p_emailmarketingscheduled.fldscheduledDate"])));
                
                self::add($vflEmailMarketingScheduleds, $vflEmailMarketingScheduled);
                unset($vflEmailMarketingScheduled);
            }
            if ( $vrowsTotal<=0 ){
                return 0;
            }
			
			unset($vsql, $vrows, $vrowsTotal, $vrowNumber);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflEmailMarketingScheduleds, $vflEmailMarketingScheduled)
	 {
        try{
            array_push($vflEmailMarketingScheduleds->emailMarketingScheduleds, $vflEmailMarketingScheduled);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflEmailMarketingScheduleds)
	 {
        try{
            return count($vflEmailMarketingScheduleds->emailMarketingScheduleds);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflEmailMarketingScheduleds)
	 {
        try{
            $vflEmailMarketingScheduleds->emailMarketingScheduleds=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }    
    
    public function __destruct(){ }
 }
?>