<?php
/********************************************************
Name: clscDLAssignment.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 07/06/2017
Modification date: 11/07/2017
Description: Assignment Collection Class, Data Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLAssignment.php");


class clscDLAssignment
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflAssignments, $vfilter, $vpdo)
	 {
		try{
			$vsql ="SELECT c_assignment.*, c_campus.fldcampus, c_educationallevel.fldeducationalLevel ";
			$vsql.="FROM c_assignment ";
            $vsql.="INNER JOIN c_campus ON c_assignment.id_campus=c_campus.id_campus ";
			$vsql.="INNER JOIN c_educationallevel ON c_assignment.id_educationalLevel=c_educationallevel.id_educationalLevel ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY c_assignment.id_campus, c_assignment.id_educationalLevel";
            
            self::clean($vflAssignments);
            
			$vpdo->execute($vsql);
            $vrows=$vpdo->getAllDataAlias();
            $vrowsTotal=$vpdo->getAffectedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vflAssignment= new clspFLAssignment();
                $vflAssignment->campus->idCampus=(int)($vrows[$vrowNumber]["c_assignment.id_campus"]);
                $vflAssignment->campus->campus=trim($vrows[$vrowNumber]["c_campus.fldcampus"]);
                $vflAssignment->educationalLevel->idEducationalLevel=(int)($vrows[$vrowNumber]["c_assignment.id_educationalLevel"]);
                $vflAssignment->educationalLevel->educationalLevel=trim($vrows[$vrowNumber]["c_educationallevel.fldeducationalLevel"]);
				
                self::add($vflAssignments, $vflAssignment);
                unset($vflAssignment);
            }
            if ( $vrowsTotal<=0 ){
                return 0;
            }
			
			unset($vsql, $vrows, $vrowsTotal, $vrowNumber);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflAssignments, $vflAssignment)
	 {
        try{
            array_push($vflAssignments->assignments, $vflAssignment);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflAssignments)
	 {
        try{
            return count($vflAssignments->assignments);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflAssignments)
	 {
        try{
            $vflAssignments->assignments=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }
?>