<?php
/********************************************************
Name: clscDLEmailMarketing.php
Version: 0.0.1
Autor name: Sandro Alan Gomez Aceituno.
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 06/06/2017
Modification date: 22/06/2017
Description: Email Marketing Collection Class, Data Layer. 
********************************************************/
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLEmailMarketing.php");

class clscDLEmailMarketing
 {
    public function __construct() { }    
    
    public static function queryToDataBase($vflEmailMarketings, $vfilter, $vpdo)
	 {
		try{            
			$vsql ="SELECT p_emailmarketing.*, c_campus.fldcampus, c_emailmarketingtype.fldemailMarketingType ";
            $vsql.=", c_emailmarketingstatus.fldemailMarketingStatus, c_user.*, c_usertype.flduserType, c_userstatus.flduserStatus ";
            $vsql.=", c_educationaloffer.*, c_educationallevel.fldeducationalLevel ";
			$vsql.="FROM p_emailmarketing ";
            $vsql.="INNER JOIN c_campus ON p_emailmarketing.id_campus=c_campus.id_campus ";
            $vsql.="INNER JOIN c_emailmarketingtype ON p_emailmarketing.id_emailMarketingType=c_emailmarketingtype.id_emailMarketingType ";
            $vsql.="INNER JOIN c_emailmarketingstatus ON p_emailmarketing.id_emailMarketingStatus=c_emailmarketingstatus.id_emailMarketingStatus ";
            $vsql.="INNER JOIN c_backenduser ON p_emailmarketing.id_userRecord=c_backenduser.id_user ";
            $vsql.="INNER JOIN c_user ON c_backenduser.id_user=c_user.id_user ";
            $vsql.="INNER JOIN c_usertype ON c_user.id_userType=c_usertype.id_userType ";
			$vsql.="INNER JOIN c_userstatus ON c_user.id_userStatus=c_userstatus.id_userStatus ";
            $vsql.="INNER JOIN c_educationaloffer ON p_emailmarketing.id_campus=c_educationaloffer.id_campus ";
            $vsql.="AND p_emailmarketing.id_educationalLevel=c_educationaloffer.id_educationalLevel ";
            $vsql.="AND p_emailmarketing.id_educationalOffer=c_educationaloffer.id_educationalOffer ";
            $vsql.="INNER JOIN c_educationallevel ON p_emailmarketing.id_educationalLevel=c_educationallevel.id_educationalLevel ";
			$vsql.= $vfilter . " ";
            $vsql.="ORDER BY p_emailmarketing.id_campus ASC, p_emailmarketing.id_emailMarketing DESC";
            
            self::clean($vflEmailMarketings);            
			$vpdo->execute($vsql);
            $vrows=$vpdo->getAllDataAlias();
            $vrowsTotal=$vpdo->getAffectedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vflEmailMarketing= new clspFLEmailMarketing();
                $vflEmailMarketing->idEmailMarketing=trim($vrows[$vrowNumber]["p_emailmarketing.id_emailMarketing"]);
				$vflEmailMarketing->campus->idCampus=(int)($vrows[$vrowNumber]["p_emailmarketing.id_campus"]);
				$vflEmailMarketing->campus->campus=trim($vrows[$vrowNumber]["c_campus.fldcampus"]);
				$vflEmailMarketing->emailMarketingType->idEmailMarketingType=(int)($vrows[$vrowNumber]["p_emailmarketing.id_emailMarketingType"]);
				$vflEmailMarketing->emailMarketingType->emailMarketingType=trim($vrows[$vrowNumber]["c_emailmarketingtype.fldemailMarketingType"]);
                $vflEmailMarketing->emailMarketingStatus->idEmailMarketingStatus=(int)($vrows[$vrowNumber]["p_emailmarketing.id_emailMarketingStatus"]);
				$vflEmailMarketing->emailMarketingStatus->emailMarketingStatus=trim($vrows[$vrowNumber]["c_emailmarketingstatus.fldemailMarketingStatus"]);
                $vflEmailMarketing->userRecord->idUser= (int)($vrows[$vrowNumber]["p_emailmarketing.id_userRecord"]);
                $vflEmailMarketing->userRecord->userType->idUserType=(int)($vrows[$vrowNumber]["c_user.id_userType"]);
				$vflEmailMarketing->userRecord->userType->userType=trim($vrows[$vrowNumber]["c_usertype.flduserType"]);
				$vflEmailMarketing->userRecord->userStatus->idUserStatus=(int)($vrows[$vrowNumber]["c_user.id_userStatus"]);
				$vflEmailMarketing->userRecord->userStatus->userStatus=trim($vrows[$vrowNumber]["c_userstatus.flduserStatus"]);
                $vflEmailMarketing->userRecord->firstName=trim($vrows[$vrowNumber]["c_user.fldfirstName"]);
                $vflEmailMarketing->userRecord->lastName=trim($vrows[$vrowNumber]["c_user.fldlastName"]);
				$vflEmailMarketing->userRecord->name=trim($vrows[$vrowNumber]["c_user.fldname"]);
                $vflEmailMarketing->userRecord->emailAccount=trim($vrows[$vrowNumber]["c_user.fldemailAccount"]);
                $vflEmailMarketing->userRecord->campus=$vflEmailMarketing->campus;
                $vflEmailMarketing->educationalOffer->assignment->campus=$vflEmailMarketing->campus;
                $vflEmailMarketing->educationalOffer->assignment->educationalLevel->idEducationalLevel=(int)($vrows[$vrowNumber]["p_emailmarketing.id_educationalLevel"]);
                $vflEmailMarketing->educationalOffer->assignment->educationalLevel->educationalLevel=trim($vrows[$vrowNumber]["c_educationallevel.fldeducationalLevel"]);
                $vflEmailMarketing->educationalOffer->educationalOffer=trim($vrows[$vrowNumber]["c_educationaloffer.fldeducationalOffer"]);
                $vflEmailMarketing->educationalOffer->fileName=trim($vrows[$vrowNumber]["c_educationaloffer.fldfileName"]);
                $vflEmailMarketing->subject=trim($vrows[$vrowNumber]["p_emailmarketing.fldsubject"]);
                $vflEmailMarketing->message=trim($vrows[$vrowNumber]["p_emailmarketing.fldmessage"]);
                $vflEmailMarketing->recordDate=date("m/d/Y H:i:s", strtotime(trim($vrows[$vrowNumber]["p_emailmarketing.fldrecordDate"])));
				
                self::add($vflEmailMarketings, $vflEmailMarketing);
                unset($vflEmailMarketing);
            }
            if ( $vrowsTotal<=0 ){
                return 0;
            }
			
			unset($vsql, $vrows, $vrowsTotal, $vrowNumber);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflEmailMarketings, $vflEmailMarketing)
	 {
        try{
            array_push($vflEmailMarketings->emailMarketings, $vflEmailMarketing);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflEmailMarketings)
	 {
        try{
            return count($vflEmailMarketings->emailMarketings);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflEmailMarketings)
	 {
        try{
            $vflEmailMarketings->emailMarketings=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }    
    
    public function __destruct(){ }
 }
?>