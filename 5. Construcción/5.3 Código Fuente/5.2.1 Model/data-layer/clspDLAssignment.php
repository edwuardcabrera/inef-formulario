<?php
/********************************************************
Name: clspDLAssignment.php
Version: 0.0.1
Autor name: Alejandro Hdez. G.
Modification autor name: Edwuard H. Cabrera Rodr�guez
Creation date: 07/06/2017
Modification date: 04/07/2017
Description: Assignment Class, Data Layer.
********************************************************/

class clspDLAssignment 
 {
    public function __construct() {}
    
    
    public static function queryToDatabase($vflAssigment,$vpdo)
     {
        try{
            $veducational=$vflAssigment->educationalLevel->idEducationalLevel;
            $vcampus=$vflAssigment->campus->idCampus;
             
            $vsql = "SELECT *";
            $vsql.= "FROM c_assignment ";
            $vsql.="INNER JOIN c_campus ON c_assignment.id_campus=c_campus.id_campus ";
            $vsql.="INNER JOIN c_educationallevel ON c_assignment.id_educationalLevel=c_educationallevel.id_educationalLevel ";
            $vsql.="WHERE c_assignment.id_educationalLevel=\"$veducational\" AND c_assignment.id_campus=\"$vcampus\"";

            $vpdo->execute($vsql);
            if ($vpdo->getAffectedRowsNumber() == 1){
                $vrow = $vpdo->getAllData();
                $vflAssigment->campus->idCampus = $vrow[0]['id_campus'];
                $vflAssigment->educationalLevel->idEducationalLevel = $vrow[0]['id_educationalLevel'];
                $vflAssigment->fileName = $vrow[0]['fldfileName'];
                unset($vrow);
            }
            else{
                return 0;
            }
            
            unset($vsql);
            return 1;
        }
        catch (Exception $vexception){
            throw new Exception($vexception->getMessage(), $vexception->getCode());
        }
     }
    
    public static function updateInDataBase($vflAssigment, $vpdo)
	 {
		try{
			$vsql =" UPDATE c_assignment ";
            $vsql.=" SET fldfileName='" . $vflAssigment->fileName."' ";
            $vsql.=" WHERE id_campus=" . $vflAssigment->campus->idCampus;
            $vsql.=" AND id_educationalLevel=" . $vflAssigment->educationalLevel->idEducationalLevel;
			
			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==0 ){
				return 0;
			}            
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
    public function __destruct() { }

}
?>