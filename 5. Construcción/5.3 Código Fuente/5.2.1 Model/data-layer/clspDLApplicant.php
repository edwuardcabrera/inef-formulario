<?php
/********************************************************
Name: clspDLApplicant.php
Version: 0.0.1
Autor name:Alejandro Hdez. G.
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 07/06/2017
Modification date: 03/07/2017
Description: Applicant Class, Data Layer.
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/tools/clspString.php");


class clspDLApplicant
 {
    public function __construct() { }
    
    
    public static function AddToDatabase($vflApplicant, $vpdo)
     {
        try{
            $vflApplicant->idApplicant = self::getIdApplicantFromDataBase($vflApplicant, $vpdo);
            $vsql = "INSERT INTO p_applicant(id_campus, id_applicant, id_user, id_educationalLevel,id_educationalOffer, fldrequestDate) ";
            $vsql.="VALUES(" . $vflApplicant->campus->idCampus;
            $vsql.=", '" . $vflApplicant->idApplicant . "'";
            $vsql.=", " . $vflApplicant->user->idUser;
            $vsql.=", " . $vflApplicant->educationalOffer->assignment->educationalLevel->idEducationalLevel;
            $vsql.=", " . $vflApplicant->educationalOffer->idEducationalOffer;
            $vsql.=", '" . $vflApplicant->requestDate . "')";

            $vpdo->execute($vsql);
            if ($vpdo->getAffectedRowsNumber() == 0){
                return 0;
            }
            unset($vsql);
            return 1;
        } 
        catch (Exception $vexception){
            throw new Exception($vexception->getMessage(), $vexception->getCode());
        }
    }

    private static function getIdApplicantFromDataBase($vflApplicant, $vpdo)
     {
        try{
            $vidApplicant = "000001-" . date("y");
            $vsql = "SELECT (MAX(CONVERT(SUBSTRING(id_applicant, 1, 6), SIGNED)) + 1) AS id_applicantNew ";
            $vsql.="FROM p_applicant ";
            $vsql.="WHERE id_campus=" . $vflApplicant->campus->idCampus;

            $vpdo->execute($vsql);
            $vrow = $vpdo->getAllData();
            if ((!is_null($vrow[0])) && (($vrow[0]["id_applicantNew"]) > 0)) {
                $vstring = new clspString($vrow[0]["id_applicantNew"]);
                $vidApplicant = $vstring->getAdjustedString(6) . "-" . date("y");
            }

            unset($vsql, $vrow);
            return $vidApplicant;
        }
        catch (Exception $vexception){
            throw new Exception($vexception->getMessage(), $vexception->getCode());
        }
    }

    public static function queryToDatabase($vflApplicant, $vpdo)
     {
        try{
            $vapplicant = $vflApplicant->idApplicant;
            $vcampus = $vflApplicant->campus->idCampus;
            
            $vsql="SELECT p_applicant.*, c_campus.fldcampus, c_userapplicant.fldmovilNumber, c_user.* , c_usertype.flduserType, c_userstatus.flduserStatus,";
            $vsql.="c_educationaloffer.fldeducationalOffer, c_educationaloffer.fldfileName, c_educationallevel.fldeducationalLevel ";
            $vsql.="FROM p_applicant ";
            $vsql.="INNER JOIN c_campus ON p_applicant.id_campus=c_campus.id_campus INNER JOIN c_userapplicant ON p_applicant.id_user=c_userapplicant.id_user ";
            $vsql.="INNER JOIN c_user ON c_userapplicant.id_user=c_user.id_user INNER JOIN c_usertype ON c_user.id_userType=c_usertype.id_userType ";
            $vsql.="INNER JOIN c_userstatus ON c_user.id_userStatus=c_userstatus.id_userStatus ";
            $vsql.="INNER JOIN c_educationaloffer ON p_applicant.id_campus=c_educationaloffer.id_campus ";
            $vsql.="AND p_applicant.id_educationalLevel=c_educationaloffer.id_educationalLevel AND p_applicant.id_educationalOffer=c_educationaloffer.id_educationalOffer "; 
            $vsql.="INNER JOIN c_assignment ON c_educationaloffer.id_campus=c_assignment.id_campus "; 
            $vsql.=" AND c_educationaloffer.id_educationalLevel=c_assignment.id_educationalLevel ";
            $vsql.="INNER JOIN c_educationallevel ON c_assignment.id_educationalLevel=c_educationallevel.id_educationalLevel "; 
            $vsql.="WHERE p_applicant.id_campus=$vcampus AND p_applicant.id_applicant='$vapplicant' ";
            $vsql.="ORDER BY p_applicant.id_campus, p_applicant.id_educationalLevel, p_applicant.id_educationalOffer, p_applicant.id_user";

            $vpdo->execute($vsql);
            if ($vpdo->getAffectedRowsNumber() == 1){
                $vrow = $vpdo->getAllData();
                $vflApplicant->campus->idCampus=(int)$vrow[0]["id_campus"];
                $vflApplicant->campus->campus=trim($vrow[0]["fldcampus"]);
                $vflApplicant->idApplicant=trim($vrow[0]["id_applicant"]);
                $vflApplicant->user->idUser= (int)$vrow[0]["id_user"];
                $vflApplicant->user->userType->idUserType=(int)$vrow[0]["id_userType"];
        		$vflApplicant->user->userType->userType=trim($vrow[0]["flduserType"]);
        		$vflApplicant->user->userStatus->idUserStatus=(int)$vrow[0]["id_userStatus"];
        		$vflApplicant->user->userStatus->userStatus=trim($vrow[0]["flduserStatus"]);
                $vflApplicant->user->firstName=trim($vrow[0]["fldfirstName"]);
                $vflApplicant->user->lastName=trim($vrow[0]["fldlastName"]);
        		$vflApplicant->user->name=trim($vrow[0]["fldname"]);
                $vflApplicant->user->emailAccount=trim($vrow[0]["fldemailAccount"]);
                $vflApplicant->user->movilNumber=trim($vrow[0]["fldmovilNumber"]);
                $vflApplicant->educationalOffer->assignment->campus=$vflApplicant->campus;
                $vflApplicant->educationalOffer->assignment->educationalLevel->idEducationalLevel=(int)$vrow[0]["id_educationalLevel"];
                $vflApplicant->educationalOffer->assignment->educationalLevel->educationalLevel=trim($vrow[0]["fldeducationalLevel"]);
                $vflApplicant->educationalOffer->idEducationalOffer=(int)$vrow[0]["id_educationalOffer"];
                $vflApplicant->educationalOffer->educationalOffer=trim($vrow[0]["fldeducationalOffer"]);
                $vflApplicant->educationalOffer->fileName=trim($vrow[0]["fldfileName"]);
                $vflApplicant->requestDate=date("m/d/Y H:i:s", strtotime(trim($vrow[0]["fldrequestDate"])));
				
                unset($vrow);
            }
            else{
                return 0;
            }
            
            unset($vsql);
            return 1;
        }
        catch (Exception $vexception){
            throw new Exception($vexception->getMessage(), $vexception->getCode());
        }
    }
    
    
    public function __destruct() { }
 }
?>