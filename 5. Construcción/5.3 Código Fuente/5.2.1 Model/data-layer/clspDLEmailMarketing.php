<?php
/********************************************************
Name: clspDLEmailMarketing.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 13/06/2017
Modification date: 11/07/2017
Description: Email Marketing Principal Class, Data Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/tools/clspString.php");


class clspDLEmailMarketing
 {
	public function __construct() { }
	
    
    public static function addToDataBase($vflEmailMarketing, $vpdo)
	 {
		try{
            $vflEmailMarketing->idEmailMarketing=self::getIdFromDataBase($vflEmailMarketing, $vpdo);
            $vsql ="INSERT INTO p_emailmarketing(id_campus, id_emailMarketing, id_emailMarketingType, id_emailMarketingStatus, id_userRecord";
            $vsql.=", id_educationalLevel, id_educationalOffer, fldsubject, fldmessage, fldrecordDate) ";
			$vsql.="VALUES(" . $vflEmailMarketing->campus->idCampus;
            $vsql.=", '" . $vflEmailMarketing->idEmailMarketing .  "'";
			$vsql.=", " . $vflEmailMarketing->emailMarketingType->idEmailMarketingType;
            $vsql.=", " . $vflEmailMarketing->emailMarketingStatus->idEmailMarketingStatus;
            $vsql.=", " . $vflEmailMarketing->userRecord->idUser;
            $vsql.=", " . $vflEmailMarketing->educationalOffer->assignment->educationalLevel->idEducationalLevel;
            $vsql.=", " . $vflEmailMarketing->educationalOffer->idEducationalOffer;
			$vsql.=", '" . $vflEmailMarketing->subject . "'";
            $vsql.=", '" . $vflEmailMarketing->message . "'";
            $vsql.=", '" . date("Y-m-d H:i:s", strtotime($vflEmailMarketing->recordDate)) . "')";   
                   
			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
	public static function updateInDataBase($vflEmailMarketing, $vpdo)
	 {
		try{
			$vsql ="UPDATE p_emailmarketing ";
            $vsql.="SET id_userRecord=" . $vflEmailMarketing->userRecord->idUser;
            $vsql.=", id_educationalLevel=" . $vflEmailMarketing->educationalOffer->assignment->educationalLevel->idEducationalLevel;
            $vsql.=", id_educationalOffer=" . $vflEmailMarketing->educationalOffer->idEducationalOffer;
			$vsql.=", fldsubject='" . $vflEmailMarketing->subject . "' ";
            $vsql.=", fldmessage='" . $vflEmailMarketing->message . "' ";
            $vsql.=", fldrecordDate='" . date("Y-m-d H:i:s", strtotime($vflEmailMarketing->recordDate)) . "' ";
            $vsql.="WHERE id_campus=" . $vflEmailMarketing->campus->idCampus . " ";
            $vsql.="AND id_emailMarketing='" . $vflEmailMarketing->idEmailMarketing . "'";
			
			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==0 ){
				return 0;
			}
            
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
      
	public static function updateStatusInDataBase($vflEmailMarketing, $vpdo)
	 {
		try{
			$vsql ="UPDATE p_emailmarketing ";
            $vsql.="SET id_emailMarketingStatus=" . $vflEmailMarketing->emailMarketingStatus->idEmailMarketingStatus . " ";
			$vsql.="WHERE id_campus=" . $vflEmailMarketing->campus->idCampus . " ";
            $vsql.="AND id_emailMarketing='" . $vflEmailMarketing->idEmailMarketing . "'";
			
			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==0 ){
				return 0;
			}
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflEmailMarketing, $vpdo)
	 {
		try{			
			$vsql ="DELETE FROM p_emailmarketing ";
            $vsql.="WHERE id_emailMarketing='". $vflEmailMarketing->idEmailMarketing."'";
            
            $vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==0 ){
                return 0;
			}            
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function queryByIdsToDataBase($vflEmailMarketing, $vpdo)
	 {
		try{
			$vsql ="SELECT p_emailmarketing.*, c_campus.fldcampus, c_emailmarketingtype.fldemailMarketingType ";
            $vsql.=", c_emailmarketingstatus.fldemailMarketingStatus, c_user.*, c_usertype.flduserType, c_userstatus.flduserStatus ";
            $vsql.=", c_educationaloffer.fldeducationalOffer, c_educationaloffer.fldfileName, c_educationallevel.fldeducationalLevel ";
			$vsql.="FROM p_emailmarketing ";
            $vsql.="INNER JOIN c_campus ON p_emailmarketing.id_campus=c_campus.id_campus ";
            $vsql.="INNER JOIN c_emailmarketingtype ON p_emailmarketing.id_emailMarketingType=c_emailmarketingtype.id_emailMarketingType ";
            $vsql.="INNER JOIN c_emailmarketingstatus ON p_emailmarketing.id_emailMarketingStatus=c_emailmarketingstatus.id_emailMarketingStatus ";
            $vsql.="INNER JOIN c_backenduser ON p_emailmarketing.id_userRecord=c_backenduser.id_user ";
            $vsql.="INNER JOIN c_user ON c_backenduser.id_user=c_user.id_user ";
            $vsql.="INNER JOIN c_usertype ON c_user.id_userType=c_usertype.id_userType ";
			$vsql.="INNER JOIN c_userstatus ON c_user.id_userStatus=c_userstatus.id_userStatus ";
            $vsql.="INNER JOIN c_educationaloffer ON p_emailmarketing.id_campus=c_educationaloffer.id_campus ";
            $vsql.="AND p_emailmarketing.id_educationalLevel=c_educationaloffer.id_educationalLevel ";
            $vsql.="AND p_emailmarketing.id_educationalOffer=c_educationaloffer.id_educationalOffer ";
            $vsql.="INNER JOIN c_assignment ON c_educationaloffer.id_campus=c_assignment.id_campus ";
            $vsql.="AND c_educationaloffer.id_educationalLevel=c_assignment.id_educationalLevel ";
            $vsql.="INNER JOIN c_educationallevel ON c_assignment.id_educationalLevel=c_educationallevel.id_educationalLevel ";
            $vsql.="WHERE p_emailmarketing.id_campus=" . $vflEmailMarketing->campus->idCampus . " ";
            $vsql.="AND p_emailmarketing.id_emailMarketing='" . $vflEmailMarketing->idEmailMarketing . "'";
			
			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==1 ){
				$vrow=$vpdo->getAllDataAlias();
                $vflEmailMarketing->campus->campus=trim($vrow[0]["c_campus.fldcampus"]);
				$vflEmailMarketing->emailMarketingType->idEmailMarketingType=(int)($vrow[0]["p_emailmarketing.id_emailMarketingType"]);
				$vflEmailMarketing->emailMarketingType->emailMarketingType=trim($vrow[0]["c_emailmarketingtype.fldemailMarketingType"]);
                $vflEmailMarketing->emailMarketingStatus->idEmailMarketingStatus=(int)($vrow[0]["p_emailmarketing.id_emailMarketingStatus"]);
				$vflEmailMarketing->emailMarketingStatus->emailMarketingStatus=trim($vrow[0]["c_emailmarketingstatus.fldemailMarketingStatus"]);
                $vflEmailMarketing->userRecord->idUser= (int)($vrow[0]["p_emailmarketing.id_userRecord"]);
                $vflEmailMarketing->userRecord->userType->idUserType=(int)($vrow[0]["c_user.id_userType"]);
				$vflEmailMarketing->userRecord->userType->userType=trim($vrow[0]["c_usertype.flduserType"]);
				$vflEmailMarketing->userRecord->userStatus->idUserStatus=(int)($vrow[0]["c_user.id_userStatus"]);
				$vflEmailMarketing->userRecord->userStatus->userStatus=trim($vrow[0]["c_userstatus.flduserStatus"]);
                $vflEmailMarketing->userRecord->firstName=trim($vrow[0]["c_user.fldfirstName"]);
                $vflEmailMarketing->userRecord->lastName=trim($vrow[0]["c_user.fldlastName"]);
				$vflEmailMarketing->userRecord->name=trim($vrow[0]["c_user.fldname"]);
                $vflEmailMarketing->userRecord->emailAccount=trim($vrow[0]["c_user.fldemailAccount"]);
                $vflEmailMarketing->userRecord->campus=$vflEmailMarketing->campus;
                $vflEmailMarketing->educationalOffer->assignment->campus=$vflEmailMarketing->campus;
                $vflEmailMarketing->educationalOffer->assignment->educationalLevel->idEducationalLevel=(int)($vrow[0]["p_emailmarketing.id_educationalLevel"]);
                $vflEmailMarketing->educationalOffer->assignment->educationalLevel->educationalLevel=trim($vrow[0]["c_educationallevel.fldeducationalLevel"]);
                $vflEmailMarketing->educationalOffer->idEducationalOffer=(int)($vrow[0]["p_emailmarketing.id_educationalOffer"]);
                $vflEmailMarketing->educationalOffer->educationalOffer=trim($vrow[0]["c_educationaloffer.fldeducationalOffer"]);
                $vflEmailMarketing->educationalOffer->fileName=trim($vrow[0]["c_educationaloffer.fldfileName"]);
                $vflEmailMarketing->subject=trim($vrow[0]["p_emailmarketing.fldsubject"]);
                $vflEmailMarketing->message=trim($vrow[0]["p_emailmarketing.fldmessage"]);
                $vflEmailMarketing->recordDate=date("m/d/Y H:i:s", strtotime(trim($vrow[0]["p_emailmarketing.fldrecordDate"])));
                
				unset($vrow);
			}
			else{
				return 0;
			}
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    private static function getIdFromDataBase($vflEmailMarketing, $vpdo)
	 {
		try{
            $vidEmailMarketing="000001-". date("y");
			$vsql ="SELECT (MAX(CONVERT(SUBSTRING(id_emailMarketing, 1, 6), SIGNED)) + 1) AS id_emailMarketingNew ";
			$vsql.="FROM p_emailmarketing ";
            $vsql.="WHERE id_campus=" . $vflEmailMarketing->campus->idCampus;
            
            $vpdo->execute($vsql);
            $vrow=$vpdo->getAllData();
			if ( (! is_null($vrow[0])) && ((int)($vrow[0]["id_emailMarketingNew"])>0) ){
                $vstring= new clspString($vrow[0]["id_emailMarketingNew"]);
                $vidEmailMarketing=$vstring->getAdjustedString(6) . "-" . date("y");;
            }
            
            unset($vsql, $vrow);
            return $vidEmailMarketing;
		 }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }
?>