<?php
/********************************************************
Name: clspDLEmailMarketingScheduled.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 13/06/2017
Modification date: 05/08/2017
Description: Email Marketing Scheduled Principal Class, Data Layer. 
********************************************************/


class clspDLEmailMarketingScheduled
 {
	public function __construct() { }
	
    
    public static function addToDataBase($vflEmailMarketingScheduled, $vpdo)
	 {
		try{
            $vsql ="INSERT INTO p_emailmarketingscheduled(id_campus, id_emailMarketing, fldscheduledDate) ";
			$vsql.="VALUES(" . $vflEmailMarketingScheduled->emailMarketing->campus->idCampus;
            $vsql.=", '" . $vflEmailMarketingScheduled->emailMarketing->idEmailMarketing .  "'";
            $vsql.=", '" . date("Y-m-d H:i:s", strtotime($vflEmailMarketingScheduled->scheduledDate)) . "')";
            
			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

	public static function updateInDataBase($vflEmailMarketingScheduled, $vpdo)
	 {
		try{
			$vsql ="UPDATE p_emailmarketingscheduled ";
            $vsql.="SET fldscheduledDate='" . date("Y-m-d H:i:s", strtotime($vflEmailMarketingScheduled->scheduledDate)) . "' ";
            $vsql.="WHERE id_campus=" . $vflEmailMarketingScheduled->emailMarketing->campus->idCampus . " ";
            $vsql.="AND id_emailMarketing='" . $vflEmailMarketingScheduled->emailMarketing->idEmailMarketing . "'";
			
			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==0 ){
				return 0;
			}
            
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function deleteInDataBase($vflEmailMarketingScheduled, $vpdo)
	 {
		try{
			$vsql ="DELETE FROM p_emailmarketingscheduled ";
            $vsql.="WHERE id_campus=" . $vflEmailMarketingScheduled->emailMarketing->campus->idCampus . " ";
            $vsql.="AND id_emailMarketing='" . $vflEmailMarketingScheduled->emailMarketing->idEmailMarketing . "'";
			
			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function queryByIdsToDataBase($vflEmailMarketingScheduled, $vpdo)
	 {
		try{
			$vsql ="SELECT p_emailmarketingscheduled.fldscheduledDate, p_emailmarketing.*, c_campus.fldcampus, c_emailmarketingtype.fldemailMarketingType ";
            $vsql.=", c_emailmarketingstatus.fldemailMarketingStatus, c_user.*, c_usertype.flduserType, c_userstatus.flduserStatus ";
            $vsql.=", c_educationaloffer.fldeducationalOffer, c_educationaloffer.fldfileName, c_educationallevel.fldeducationalLevel ";
			$vsql.="FROM p_emailmarketingscheduled ";
            $vsql.="INNER JOIN p_emailmarketing ON p_emailmarketingscheduled.id_campus=p_emailmarketing.id_campus ";
            $vsql.="AND p_emailmarketingscheduled.id_emailMarketing=p_emailmarketing.id_emailMarketing ";
            $vsql.="INNER JOIN c_campus ON p_emailmarketing.id_campus=c_campus.id_campus ";
            $vsql.="INNER JOIN c_emailmarketingtype ON p_emailmarketing.id_emailMarketingType=c_emailmarketingtype.id_emailMarketingType ";
            $vsql.="INNER JOIN c_emailmarketingstatus ON p_emailmarketing.id_emailMarketingStatus=c_emailmarketingstatus.id_emailMarketingStatus ";
            $vsql.="INNER JOIN c_backenduser ON p_emailmarketing.id_userRecord=c_backenduser.id_user ";
            $vsql.="INNER JOIN c_user ON c_backenduser.id_user=c_user.id_user ";
            $vsql.="INNER JOIN c_usertype ON c_user.id_userType=c_usertype.id_userType ";
			$vsql.="INNER JOIN c_userstatus ON c_user.id_userStatus=c_userstatus.id_userStatus ";
            $vsql.="INNER JOIN c_educationaloffer ON p_emailmarketing.id_campus=c_educationaloffer.id_campus ";
            $vsql.="AND p_emailmarketing.id_educationalLevel=c_educationaloffer.id_educationalLevel ";
            $vsql.="AND p_emailmarketing.id_educationalOffer=c_educationaloffer.id_educationalOffer ";
            $vsql.="INNER JOIN c_assignment ON c_educationaloffer.id_campus=c_assignment.id_campus ";
            $vsql.="AND c_educationaloffer.id_educationalLevel=c_assignment.id_educationalLevel ";
            $vsql.="INNER JOIN c_educationallevel ON c_assignment.id_educationalLevel=c_educationallevel.id_educationalLevel ";
            $vsql.="WHERE p_emailmarketingscheduled.id_campus=" . $vflEmailMarketingScheduled->emailMarketing->campus->idCampus . " ";
            $vsql.="AND p_emailmarketingscheduled.id_emailMarketing='" . $vflEmailMarketingScheduled->emailMarketing->idEmailMarketing . "'";
			
			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==1 ){
				$vrow=$vpdo->getAllDataAlias();
                $vflEmailMarketingScheduled->emailMarketing->campus->campus=trim($vrow[0]["c_campus.fldcampus"]);
				$vflEmailMarketingScheduled->emailMarketing->emailMarketingType->idEmailMarketingType=(int)($vrow[0]["p_emailmarketing.id_emailMarketingType"]);
				$vflEmailMarketingScheduled->emailMarketing->emailMarketingType->emailMarketingType=trim($vrow[0]["c_emailmarketingtype.fldemailMarketingType"]);
                $vflEmailMarketingScheduled->emailMarketing->emailMarketingStatus->idEmailMarketingStatus=(int)($vrow[0]["p_emailmarketing.id_emailMarketingStatus"]);
				$vflEmailMarketingScheduled->emailMarketing->emailMarketingStatus->emailMarketingStatus=trim($vrow[0]["c_emailmarketingstatus.fldemailMarketingStatus"]);
                $vflEmailMarketingScheduled->emailMarketing->userRecord->idUser= (int)($vrow[0]["p_emailmarketing.id_userRecord"]);
                $vflEmailMarketingScheduled->emailMarketing->userRecord->userType->idUserType=(int)($vrow[0]["c_user.id_userType"]);
				$vflEmailMarketingScheduled->emailMarketing->userRecord->userType->userType=trim($vrow[0]["c_usertype.flduserType"]);
				$vflEmailMarketingScheduled->emailMarketing->userRecord->userStatus->idUserStatus=(int)($vrow[0]["c_user.id_userStatus"]);
				$vflEmailMarketingScheduled->emailMarketing->userRecord->userStatus->userStatus=trim($vrow[0]["c_userstatus.flduserStatus"]);
                $vflEmailMarketingScheduled->emailMarketing->userRecord->firstName=trim($vrow[0]["c_user.fldfirstName"]);
                $vflEmailMarketingScheduled->emailMarketing->userRecord->lastName=trim($vrow[0]["c_user.fldlastName"]);
				$vflEmailMarketingScheduled->emailMarketing->userRecord->name=trim($vrow[0]["c_user.fldname"]);
                $vflEmailMarketingScheduled->emailMarketing->userRecord->emailAccount=trim($vrow[0]["c_user.fldemailAccount"]);
                $vflEmailMarketingScheduled->emailMarketing->userRecord->campus=$vflEmailMarketingScheduled->emailMarketing->campus;
                $vflEmailMarketingScheduled->emailMarketing->educationalOffer->assignment->campus=$vflEmailMarketingScheduled->emailMarketing->campus;
                $vflEmailMarketingScheduled->emailMarketing->educationalOffer->assignment->educationalLevel->idEducationalLevel=(int)($vrow[0]["p_emailmarketing.id_educationalLevel"]);
                $vflEmailMarketingScheduled->emailMarketing->educationalOffer->assignment->educationalLevel->educationalLevel=trim($vrow[0]["c_educationallevel.fldeducationalLevel"]);
                $vflEmailMarketingScheduled->emailMarketing->educationalOffer->idEducationalOffer=(int)($vrow[0]["p_emailmarketing.id_educationalOffer"]);
                $vflEmailMarketingScheduled->emailMarketing->educationalOffer->educationalOffer=trim($vrow[0]["c_educationaloffer.fldeducationalOffer"]);
                $vflEmailMarketingScheduled->emailMarketing->educationalOffer->fileName=trim($vrow[0]["c_educationaloffer.fldfileName"]);
                $vflEmailMarketingScheduled->emailMarketing->subject=trim($vrow[0]["p_emailmarketing.fldsubject"]);
                $vflEmailMarketingScheduled->emailMarketing->message=trim($vrow[0]["p_emailmarketing.fldmessage"]);
                $vflEmailMarketingScheduled->emailMarketing->recordDate=date("m/d/Y H:i:s", strtotime(trim($vrow[0]["p_emailmarketing.fldrecordDate"])));
                $vflEmailMarketingScheduled->scheduledDate=date("m/d/Y H:i:s", strtotime(trim($vrow[0]["p_emailmarketingscheduled.fldscheduledDate"])));
                
				unset($vrow);
			}
			else{
				return 0;
			}
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
     
	public function __destruct(){ }
 }
?>