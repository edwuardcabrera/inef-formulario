<?php
/********************************************************
Name: clscDLCampus.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 26/05/2017
Modification date:
Description: Campus Collection Class, Data Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLCampus.php");


class clscDLCampus
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflCampuses, $vfilter, $vpdo)
	 {
		try{
			$vsql ="SELECT * FROM c_campus ";
            $vsql.=$vfilter . " ";
			
            self::clean($vflCampuses);
            
			$vpdo->execute($vsql);
            $vrows=$vpdo->getAllData();
            $vrowsTotal=$vpdo->getAffectedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vflCampus= new clspFLCampus();
                $vflCampus->idCampus=(int)($vrows[$vrowNumber]["id_campus"]);
                $vflCampus->campus=trim($vrows[$vrowNumber]["fldcampus"]);
                
                self::add($vflCampuses, $vflCampus);
                unset($vflCampus);
            }
            if ( $vrowsTotal<=0 ){
                return 0;
            }
			
			unset($vsql, $vrows, $vrowsTotal, $vrowNumber);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflCampuses, $vflCampus)
	 {
        try{
            array_push($vflCampuses->campuses, $vflCampus);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflCampuses)
	 {
        try{
            return count($vflCampuses->campuses);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflCampuses)
	 {
        try{
            $vflCampuses->campuses=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }
?>