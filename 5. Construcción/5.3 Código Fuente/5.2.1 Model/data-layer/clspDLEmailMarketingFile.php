<?php
/********************************************************
Name: clspDLEmailMarketingFile.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 13/06/2017
Modification date: 16/06/2017
Description: Email Marketing File Principal Class, Data Layer. 
********************************************************/


class clspDLEmailMarketingFile
 {
	public function __construct() { }
	
    
    public static function addToDataBase($vflEmailMarketingFile, $vpdo)
	 {
		try{
            $vflEmailMarketingFile->idEmailMarketingFile=self::getIdFromDataBase($vflEmailMarketingFile, $vpdo);
            $vsql ="INSERT INTO p_emailmarketingfile(id_campus, id_emailMarketing, id_emailMarketingFile, fldrealName, fldfisicName) ";
			$vsql.="VALUES(" . $vflEmailMarketingFile->emailMarketing->campus->idCampus;
            $vsql.=", '" . $vflEmailMarketingFile->emailMarketing->idEmailMarketing .  "'";
            $vsql.=", " . $vflEmailMarketingFile->idEmailMarketingFile;
            $vsql.=", '" . $vflEmailMarketingFile->realName .  "'";
            $vsql.=", '" . $vflEmailMarketingFile->fisicName .  "')";
            
			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function deleteInDataBase($vflEmailMarketingFile, $vpdo)
	 {
		try{
			$vsql ="DELETE FROM p_emailmarketingfile ";
            $vsql.="WHERE id_campus=" . $vflEmailMarketingFile->emailMarketing->campus->idCampus . " ";
            $vsql.="AND id_emailMarketing='" . $vflEmailMarketingFile->emailMarketing->idEmailMarketing . "'";
			
			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    private static function getIdFromDataBase($vflEmailMarketingFile, $vpdo)
	 {
		try{
            $vidEmailMarketingFile=1;
			$vsql ="SELECT MAX(id_emailMarketingFile) + 1 AS id_emailMarketingFileNew ";
			$vsql.="FROM p_emailmarketingfile ";
            $vsql.="WHERE id_campus=" . $vflEmailMarketingFile->emailMarketing->campus->idCampus . " ";
            $vsql.="AND id_emailMarketing='" . $vflEmailMarketingFile->emailMarketing->idEmailMarketing . "'";
			
            $vpdo->execute($vsql);
            $vrow=$vpdo->getAllData();
			if ( (! is_null($vrow[0])) && ((int)($vrow[0]["id_emailMarketingFileNew"])>0) ){
                $vidEmailMarketingFile=(int)($vrow[0]["id_emailMarketingFileNew"]);
            }
            
            unset($vsql, $vrow);
            return $vidEmailMarketingFile;
		 }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }
?>