<?php
/********************************************************
Name: clscDLAccessPrivilege.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 08/09/2015
Modification date: 26/05/2017
Description: Access Privilege Collection Class, Data Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLAccessPrivilege.php");


class clscDLAccessPrivilege
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflAccessPrivileges, $vfilter, $vpdo)
	 {
		try{
			$vsql ="SELECT c_accessprivilege.*, c_accessprivilegetype.fldaccessPrivilegeType ";
            $vsql.="FROM c_accessprivilege ";
            $vsql.="INNER JOIN c_accessprivilegetype ON c_accessprivilege.id_accessPrivilegeType=c_accessprivilegetype.id_accessPrivilegeType ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY c_accessprivilege.fldlevel1, c_accessprivilege.fldlevel2, c_accessprivilege.fldlevel3, c_accessprivilege.fldlevel4";
            
            self::clean($vflAccessPrivileges);
            
			$vpdo->execute($vsql);
            $vrows=$vpdo->getAllData();
            $vrowsTotal=$vpdo->getAffectedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vflAccessPrivilege= new clspFLAccessPrivilege();
                $vflAccessPrivilege->idAccessPrivilege=(int)($vrows[$vrowNumber]["id_accessPrivilege"]);
                $vflAccessPrivilege->accessPrivilegeType->idAccessPrivilegeType=(int)($vrows[$vrowNumber]["id_accessPrivilegeType"]);
                $vflAccessPrivilege->accessPrivilegeType->accessPrivilegeType=trim($vrows[$vrowNumber]["fldaccessPrivilegeType"]);
                $vflAccessPrivilege->accessPrivilege=trim($vrows[$vrowNumber]["fldaccessPrivilege"]);
				$vflAccessPrivilege->level1=trim($vrows[$vrowNumber]["fldlevel1"]);
                $vflAccessPrivilege->level2=trim($vrows[$vrowNumber]["fldlevel2"]);
                $vflAccessPrivilege->level3=trim($vrows[$vrowNumber]["fldlevel3"]);
                $vflAccessPrivilege->level4=trim($vrows[$vrowNumber]["fldlevel4"]);
                $vflAccessPrivilege->url=trim($vrows[$vrowNumber]["fldurl"]);
                $vflAccessPrivilege->iconName=trim($vrows[$vrowNumber]["fldiconName"]);
                
                self::add($vflAccessPrivileges, $vflAccessPrivilege);
                unset($vflAccessPrivilege);
            }
            if ( $vrowsTotal<=0 ){
                return 0;
            }
			
			unset($vsql, $vrows, $vrowsTotal, $vrowNumber);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflAccessPrivileges, $vflAccessPrivilege)
	 {
        try{
            array_push($vflAccessPrivileges->accessPrivileges, $vflAccessPrivilege);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflAccessPrivileges)
	 {
        try{
            return count($vflAccessPrivileges->accessPrivileges);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflAccessPrivileges)
	 {
        try{
            $vflAccessPrivileges->accessPrivileges=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }
?>