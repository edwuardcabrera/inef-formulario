<?php
/********************************************************
Name: clscDLUserType.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 01/06/2017
Modification date:
Description: User Type Collection Class, Data Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLUserType.php");


class clscDLUserType
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflUserTypes, $vfilter, $vpdo)
	 {
		try{
			$vsql ="SELECT * ";
			$vsql.="FROM c_usertype ";
            $vsql.=$vfilter . " ";
            
            self::clean($vflUserTypes);
            
			$vpdo->execute($vsql);
            $vrows=$vpdo->getAllData();
            $vrowsTotal=$vpdo->getAffectedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vflUserType= new clspFLUserType();
                $vflUserType->idUserType=(int)($vrows[$vrowNumber]["id_userType"]);
				$vflUserType->userType=trim($vrows[$vrowNumber]["flduserType"]);
				
                self::add($vflUserTypes, $vflUserType);
                unset($vflUserType);
            }
            if ( $vrowsTotal<=0 ){
                return 0;
            }
			
			unset($vsql, $vrows, $vrowsTotal, $vrowNumber);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflUserTypes, $vflUserType)
	 {
        try{
            array_push($vflUserTypes->userTypes, $vflUserType);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflUserTypes)
	 {
        try{
            return count($vflUserTypes->userTypes);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflUserTypes)
	 {
        try{
            $vflUserTypes->userTypes=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }
?>