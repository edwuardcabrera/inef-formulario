<?php

/* * ******************************************************
  Name: clspDLUserApplicant.php
  Version: 0.0.1
  Autor name:Alejandro Hdez. G.
  Modification autor name:
  Creation date: 26/05/2017
  Modification date:
  Description: UserApplicant Class, Data Layer.
 * ****************************************************** */
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLUser.php");

class clspDLUserApplicant {

    public function __construct() {
        
    }

    public static function AddToDatabase($vfluserApplicant, $vpdo,$vtype) {
try{
            if ($vtype==0){
                $vpdo->beginTransaction();
            }
            if ( clspDLUser::addToDataBase($vfluserApplicant, $vpdo)==1 ){
            $vsql = "INSERT INTO c_userapplicant(id_user,fldmovilNumber) ";
            $vsql.="VALUES(" . $vfluserApplicant->idUser;
            $vsql.=", '" . $vfluserApplicant->movilNumber . "')";
                
                $vpdo->execute($vsql);
                if ( $vpdo->getAffectedRowsNumber()==0 ){
                    if ($vtype==0){
                        $vpdo->rollbackTransaction();
                    }
                    return -1;
                }
                unset( $vsql);
            }
            else{
                if ($vtype==0){
                    $vpdo->rollbackTransaction();
                }
                return 0;
            }
            if ($vtype==0){
                $vpdo->commitTransaction();
            }
            
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
//        try {
//            $vfluserApplicant->idUser = self::getIdFromDataBase($vfluserApplicant, $vpdo);
//
//            $vsql = "INSERT INTO c_user(id_user, id_userType, id_userStatus, fldfirstName, fldlastName, fldname, fldemailAccount) ";
//            $vsql.="VALUES(" . $vfluserApplicant->idUser;
//            $vsql.=", " . $vfluserApplicant->userType->idUserType;
//            $vsql.=", " . $vfluserApplicant->userStatus->idUserStatus;
//            $vsql.=", '" . $vfluserApplicant->firstName . "'";
//            $vsql.=", '" . $vfluserApplicant->lastName . "'";
//            $vsql.=", '" . $vfluserApplicant->name . "'";
//            $vsql.=", '" . $vfluserApplicant->emailAccount . "')";
//            // var_dump($vfluserApplicant);
//
//            $vpdo->execute($vsql);
//            if ($vpdo->getAffectedRowsNumber() == 0) {
//                return 0;
//            }
//
//
//            unset($vsql);
//            return 1;
//        } catch (Exception $vexception) {
//            throw new Exception($vexception->getMessage(), $vexception->getCode());
//        }
    }
    //PONERLO EN LA CAPA DE DATOS CLSPDLAPPLICANT


    private static function getIdFromDataBase($vfluserApplicant, $vpdo) {
        try {
            $vidUser = 1;
            $vsql = "SELECT MAX(id_user) + 1 AS id_userNew ";
            $vsql.="FROM c_user";

            $vpdo->execute($vsql);
            $vrow = $vpdo->getAllData();
            if ((!is_null($vrow[0])) && ((int) ($vrow[0]["id_userNew"]) > 0)) {
                $vidUser = (int) ($vrow[0]["id_userNew"]);
            }

            unset($vsql, $vrow);
            return $vidUser;
        } catch (Exception $vexception) {
            throw new Exception($vexception->getMessage(), $vexception->getCode());
        }
    }
       

    public static function AddUserApplicantToDatabase($vfluserApplicant,$vflApplicant, $vpdo) {
        try {


            $vsql = "INSERT INTO c_userapplicant(id_user,fldmovilNumber) ";
            $vsql.="VALUES(" . $vfluserApplicant->idUser;
            $vsql.=", '" . $vfluserApplicant->movilNumber . "')";
            // var_dump($vfluserApplicant);
            $vflApplicant->user->idUser=$vfluserApplicant->idUser;
            $vpdo->execute($vsql);
            if ($vpdo->getAffectedRowsNumber() == 0) {
                return 0;
            }


            unset($vsql);
            return 1;
        } catch (Exception $vexception) {
            throw new Exception($vexception->getMessage(), $vexception->getCode());
        }
    }

    public static function getUserApplicant($vpdo, $vemailApplicant) {
        try {

            $vsql = "SELECT fldemailAccount ";
            $vsql.="FROM c_user ";
            $vsql.="WHERE fldemailAccount=\"$vemailApplicant \"";

            $vpdo->execute($vsql);

            if ($vpdo->getAffectedRowsNumber()!=1) {
                return 0;
                echo '0';
            }
            unset($vsql);
            return 1;
        } catch (Exception $vexception) {
            throw new Exception($vexception->getMessage(), $vexception->getCode());
        }
    }
    public static function getFileEducationLevel($vpdo,$vflAssigment,$vEducationLevel,$vcampus){
        try{
            $vsql = "SELECT * ";
            $vsql.="FROM c_assignment ";
            $vsql.="WHERE id_campus=\"$vcampus \" AND id_educationalLevel=\"$vEducationLevel\"";
            $vpdo->execute($vsql);
            
            if($vpdo->getAffectedRowsNumber()==1){
               $vrow=$vpdo->getAllData();
              // var_dump($vrow);
               $vflAssigment->idcampus=$vrow[0]['id_campus'];
               $vflAssigment->ideducationalLevel=$vrow[0]['id_educationalLevel'];
               $vflAssigment->fileName=$vrow[0]['fldfileName'];
               unset($vrow);
            }  else {
                return 0;
            }
            unset($vsql);
            return 1;
            
        }catch(Exception $vexception){
             throw new Exception($vexception->getMessage(), $vexception->getCode());
        }
        
    }
   

    public function __destruct() {
        
    }

}

?>