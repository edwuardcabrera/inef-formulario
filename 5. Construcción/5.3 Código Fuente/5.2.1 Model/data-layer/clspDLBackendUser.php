<?php
/********************************************************
Name: clspDLBackendUser.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 25/05/2017
Modification date: 30/06/2017
Description: Backend User Principal Class, Data Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLUser.php");


class clspDLBackendUser
 {
	public function __construct() { }
    
   	
    public static function addToDataBase($vflBackendUser, $vpdo, $vtype=0)
	 {
		try{
            if ($vtype==0){
                $vpdo->beginTransaction();
            }
            if ( clspDLUser::addToDataBase($vflBackendUser, $vpdo)==1 ){
                $vsql ="INSERT INTO c_backenduser(id_user, id_campus) ";
				$vsql.="VALUES(" . $vflBackendUser->idUser;
                $vsql.=", " . $vflBackendUser->campus->idCampus . ")";
                
                $vpdo->execute($vsql);
                if ( $vpdo->getAffectedRowsNumber()==0 ){
                    if ($vtype==0){
                        $vpdo->rollbackTransaction();
                    }
                    return -1;
                }
                unset( $vsql);
            }
            else{
                if ($vtype==0){
                    $vpdo->rollbackTransaction();
                }
                return 0;
            }
            if ($vtype==0){
                $vpdo->commitTransaction();
            }
            
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflBackendUser, $vpdo)
	 {
		try{
            $vpdo->beginTransaction();
            $vupdateUserStatus=clspDLUser::updateInDataBase($vflBackendUser, $vpdo);
            
			$vsql ="UPDATE c_backenduser ";
            $vsql.="SET id_campus=" . $vflBackendUser->campus->idCampus . " ";
            $vsql.="WHERE id_user=" . $vflBackendUser->idUser;
            
            $vpdo->execute($vsql);
			if ( ($vupdateUserStatus==0) and ($vpdo->getAffectedRowsNumber()==0) ){
                $vpdo->rollbackTransaction();
                return 0;
			}
            $vpdo->commitTransaction();
            
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
            $vpdo->rollbackTransaction();
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflBackendUser, $vpdo)
	 {
		try{
            $vsql ="DELETE FROM c_backenduser ";
            $vsql.="WHERE id_user=" . $vflBackendUser->idUser;
            
            $vpdo->execute($vsql);
            if ( $vpdo->getAffectedRowsNumber()==1 ){
                if ( clspDLUser::deleteInDataBase($vflBackendUser, $vpdo)==0 ){
                    return -1;
                }
            }
            else{
                return 0;
            }
            
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function queryByIdToDataBase($vflBackendUser, $vpdo)
	 {
		try{
			$vsql ="SELECT c_backenduser.id_campus, c_user.*, c_usertype.flduserType, c_userstatus.flduserStatus, c_campus.fldcampus";
            $vsql.=", c_campus.fldslogan ";
			$vsql.="FROM c_backenduser ";
            $vsql.="INNER JOIN c_user ON c_backenduser.id_user=c_user.id_user ";
            $vsql.="INNER JOIN c_usertype ON c_user.id_userType=c_usertype.id_userType ";
			$vsql.="INNER JOIN c_userstatus ON c_user.id_userStatus=c_userstatus.id_userStatus ";
            $vsql.="INNER JOIN c_campus ON c_backenduser.id_campus=c_campus.id_campus ";
			$vsql.="WHERE c_backenduser.id_user=" . $vflBackendUser->idUser;
			
            $vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==1 ){
				$vrow=$vpdo->getAllDataAlias();
                $vflBackendUser->userType->idUserType=(int)($vrow[0]["c_user.id_userType"]);
				$vflBackendUser->userType->userType=trim($vrow[0]["c_usertype.flduserType"]);
				$vflBackendUser->userStatus->idUserStatus=(int)($vrow[0]["c_user.id_userStatus"]);
				$vflBackendUser->userStatus->userStatus=trim($vrow[0]["c_userstatus.flduserStatus"]);
                $vflBackendUser->firstName=trim($vrow[0]["c_user.fldfirstName"]);
                $vflBackendUser->lastName=trim($vrow[0]["c_user.fldlastName"]);
				$vflBackendUser->name=trim($vrow[0]["c_user.fldname"]);
                $vflBackendUser->emailAccount=trim($vrow[0]["c_user.fldemailAccount"]);
                $vflBackendUser->campus->idCampus=(int)($vrow[0]["c_backenduser.id_campus"]);
				$vflBackendUser->campus->campus=trim($vrow[0]["c_campus.fldcampus"]);
                $vflBackendUser->campus->slogan=trim($vrow[0]["c_campus.fldslogan"]);                
                
				unset($vrow);
			}
			else{
				return 0;
			}
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }
?>