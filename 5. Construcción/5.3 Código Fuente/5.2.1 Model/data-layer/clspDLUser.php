<?php
/********************************************************
Name: clspDLUser.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 25/08/2015
Modification date: 13/06/2017
Description: User Principal Class, Data Layer. 
********************************************************/


class clspDLUser
 {
	public function __construct() { }
	
    
    public static function addToDataBase($vflUser, $vpdo)
	 {
		try{
            $vflUser->idUser=self::getIdFromDataBase($vflUser, $vpdo); 
            if (empty($vflUser->password)) {
                $vflUser->password=self::generatePassword();
            }
            
            $vsql ="INSERT INTO c_user(id_user, id_userType, id_userStatus, fldfirstName, fldlastName, fldname, fldpassword, fldemailAccount) ";
			$vsql.="VALUES(" . $vflUser->idUser;
            $vsql.=", " . $vflUser->userType->idUserType;
			$vsql.=", " . $vflUser->userStatus->idUserStatus;
			$vsql.=", '" . $vflUser->firstName . "'";
            $vsql.=", '" . $vflUser->lastName . "'";
            $vsql.=", '" . $vflUser->name . "'";
            $vsql.=", '" . md5($vflUser->password) . "'";
            $vsql.=", '" . $vflUser->emailAccount . "')";
                        
			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function updateInDataBase($vflUser, $vpdo)
	 {
		try{
			$vsql ="UPDATE c_user ";
            $vsql.="SET id_userType=" . $vflUser->userType->idUserType;
            $vsql.=", id_userStatus=" . $vflUser->userStatus->idUserStatus;
			$vsql.=", fldfirstName='" . $vflUser->firstName . "' ";
            $vsql.=", fldlastName='" . $vflUser->lastName . "' ";
            $vsql.=", fldname='" . $vflUser->name . "' ";
            $vsql.=", fldemailAccount='" . $vflUser->emailAccount . "' ";
			$vsql.="WHERE id_user=" . $vflUser->idUser;
			
			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==0 ){
				return 0;
			}
            
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function updatePasswordInDataBase($vflUser, $vpassword, $vpdo)
	 {
		try{
			$vsql ="UPDATE c_user ";
			$vsql.="SET fldpassword='" . md5($vpassword) . "' ";
			$vsql.="WHERE id_user=" . $vflUser->idUser;
			
			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==0 ){
				return 0;
			}
			$vflUser->password=$vpassword;
            
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function updateStatusInDataBase($vflUser, $vpdo)
	 {
		try{
			$vsql ="UPDATE c_user ";
			$vsql.="SET id_userStatus=" . $vflUser->userStatus->idUserStatus . " ";
			$vsql.="WHERE id_user=" . $vflUser->idUser;
			
			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==0 ){
				return 0;
			}
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexcepcion){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function deleteInDataBase($vflUser, $vpdo)
	 {
		try{
			$vsql ="DELETE FROM c_user ";
            $vsql.="WHERE id_user=" . $vflUser->idUser;
			
			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
          
    public static function queryByIdToDataBase($vflUser, $vpdo)
	 {
		try{
			$vsql ="SELECT c_user.*, c_usertype.flduserType, c_userstatus.flduserStatus ";
			$vsql.="FROM c_user ";
            $vsql.="INNER JOIN c_usertype ON c_user.id_userType=c_usertype.id_userType ";
			$vsql.="INNER JOIN c_userstatus ON c_user.id_userStatus=c_userstatus.id_userStatus ";
			$vsql.="WHERE c_user.id_user='" . $vflUser->idUser. "'";
			
			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==1 ){
				$vrow=$vpdo->getAllData();
                $vflUser->userType->idUserType=(int)($vrow[0]["id_userType"]);
				$vflUser->userType->userType=trim($vrow[0]["flduserType"]);
				$vflUser->userStatus->idUserStatus=(int)($vrow[0]["id_userStatus"]);
				$vflUser->userStatus->userStatus=trim($vrow[0]["flduserStatus"]);
                $vflUser->firstName=trim($vrow[0]["fldfirstName"]);
                $vflUser->lastName=trim($vrow[0]["fldlastName"]);
				$vflUser->name=trim($vrow[0]["fldname"]);
				$vflUser->emailAccount=trim($vrow[0]["fldemailAccount"]);
                
				unset($vrow);
			}
			else{
				return 0;
			}
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function queryByEmailAccountToDataBase($vflUser, $vpdo)
	 {
		try{
			$vsql ="SELECT c_user.*, c_usertype.flduserType, c_userstatus.flduserStatus ";
			$vsql.="FROM c_user ";
            $vsql.="INNER JOIN c_usertype ON c_user.id_userType=c_usertype.id_userType ";
			$vsql.="INNER JOIN c_userstatus ON c_user.id_userStatus=c_userstatus.id_userStatus ";
			$vsql.="WHERE c_user.fldemailAccount='" . $vflUser->emailAccount. "'";
			
			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==1 ){
				$vrow=$vpdo->getAllData();
                $vflUser->idUser=(int)($vrow[0]["id_user"]);
				$vflUser->userType->idUserType=(int)($vrow[0]["id_userType"]);
				$vflUser->userType->userType=trim($vrow[0]["flduserType"]);
				$vflUser->userStatus->idUserStatus=(int)($vrow[0]["id_userStatus"]);
				$vflUser->userStatus->userStatus=trim($vrow[0]["flduserStatus"]);
                $vflUser->firstName=trim($vrow[0]["fldfirstName"]);
                $vflUser->lastName=trim($vrow[0]["fldlastName"]);
				$vflUser->name=trim($vrow[0]["fldname"]);
				
				unset($vrow);
			}
			else{
				return 0;
			}
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function verifyPasswordByEmailAccountToDataBase($vflUser, $vpassword, $vpdo)
	 {
		try{
			$vsql ="SELECT id_userStatus ";
			$vsql.="FROM c_user ";
			$vsql.="WHERE fldemailAccount='" . $vflUser->emailAccount . "' ";
			$vsql.="AND fldpassword=MD5('" . $vpassword . "')";
			
			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()!=1 ){
				return 0;
			}
			$vrow=$vpdo->getAllData();
			if ( ((int)(trim($vrow[0]["id_userStatus"])))==0 ){
				return -1;
			}
            
			unset($vsql, $vrow);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function verifyPasswordToDataBase($vflUser, $vpassword, $vpdo)
	 {
		try{
			$vsql ="SELECT * ";
			$vsql.="FROM c_user ";
			$vsql.="WHERE id_user=" . $vflUser->idUser . " ";
			$vsql.="AND fldpassword=MD5('" . $vpassword . "')";
			
            $vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==1 ){
				return 1;
			}
			else{
				return 0;
			}
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
     }
    
    private static function getIdFromDataBase($vflUser, $vpdo)
	 {
		try{
            $vidUser=1;
			$vsql ="SELECT MAX(id_user) + 1 AS id_userNew ";
			$vsql.="FROM c_user";
			
            $vpdo->execute($vsql);
            $vrow=$vpdo->getAllData();
			if ( (! is_null($vrow[0])) && ((int)($vrow[0]["id_userNew"])>0) ){
                $vidUser=(int)($vrow[0]["id_userNew"]);
            }
            
            unset($vsql, $vrow);
            return $vidUser;
		 }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function generatePassword()
     {
        try{
            $vbytes=openssl_random_pseudo_bytes(5);
            return bin2hex($vbytes);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
     }
     
     
	public function __destruct(){ }
 }
?>