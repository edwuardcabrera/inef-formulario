<?php
/********************************************************
Name: clscDLUserStatus.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 11/11/2015
Modification date: 01/06/2017
Description: User Status Collection Class, Data Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLUserStatus.php");


class clscDLUserStatus
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflUserStatuses, $vfilter, $vpdo)
	 {
		try{
			$vsql ="SELECT * FROM c_userstatus ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY flduserStatus";
            
            self::clean($vflUserStatuses);
            
            $vpdo->execute($vsql);
            $vrows=$vpdo->getAllData();
            $vrowsTotal=$vpdo->getAffectedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vflUserStatus= new clspFLUserStatus();
                $vflUserStatus->idUserStatus=(int)($vrows[$vrowNumber]["id_userStatus"]);
                $vflUserStatus->userStatus=trim($vrows[$vrowNumber]["flduserStatus"]);
                
                self::add($vflUserStatuses, $vflUserStatus);
                unset($vflUserStatus);
            }
            if ( $vrowsTotal<=0 ){
                return 0;
            }
			
			unset($vsql, $vrows, $vrowsTotal, $vrowNumber);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflUserStatuses, $vflUserStatus)
	 {
        try{
            array_push($vflUserStatuses->userStatuses, $vflUserStatus);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflUserStatuses)
	 {
        try{
            return count($vflUserStatuses->userStatuses);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflUserStatuses)
	 {
        try{
            $vflUserStatuses->userStatuses=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }
?>