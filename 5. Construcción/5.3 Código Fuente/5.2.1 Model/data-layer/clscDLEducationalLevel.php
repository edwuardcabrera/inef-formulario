<?php
/********************************************************
Name: clscDLEducationalLevel.php
Version: 0.0.1
Autor name: Sandro Alan Gomez Aceituno.
Modification autor name: 
Creation date: 19/06/2017
Modification date: 
Description: Education Level Collection Class, Data Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLEducationalLevel.php");


class clscDLEducationalLevel
 {
    public function __construct() { }
        
    public static function queryToDataBase($vflEducationalLevels, $vfilter, $vpdo)
	 {
		try{
			$vsql ="SELECT * FROM c_educationallevel ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY id_educationalLevel";
            
            self::clean($vflEducationalLevels);            
            $vpdo->execute($vsql);
            $vrows=$vpdo->getAllData();
            $vrowsTotal=$vpdo->getAffectedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vflEducationalsLevel= new clspFLEducationalLevel();
                $vflEducationalsLevel->idEducationalLevel=(int)($vrows[$vrowNumber]["id_educationalLevel"]);
                $vflEducationalsLevel->educationalLevel=trim($vrows[$vrowNumber]["fldeducationalLevel"]);
                
                self::add($vflEducationalLevels, $vflEducationalsLevel);
                unset($vflEducationalsLevel);
            }
            if ( $vrowsTotal<=0 ){
                return 0;
            }			
			unset($vsql, $vrows, $vrowsTotal, $vrowNumber);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflEducationalLevels, $vflEducationalsLevel)
	 {
        try{
            array_push($vflEducationalLevels->educationalLevels, $vflEducationalsLevel);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflEducationalLevels)
	 {
        try{
            return count($vflEducationalLevels->educationalLevels);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflEducationalLevels)
	 {
        try{
            $vflEducationalLevels->educationalLevels=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }    
    
    public function __destruct(){ }
 }
?>