<?php
/********************************************************
Name: clscDLEmailMarketingType.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 01/06/2017
Modification date:
Description: Email Marketing Type Collection Class, Data Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLEmailMarketingType.php");


class clscDLEmailMarketingType
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflEmailMarketingTypes, $vfilter, $vpdo)
	 {
		try{
			$vsql ="SELECT * ";
			$vsql.="FROM c_emailmarketingtype ";
            $vsql.=$vfilter . " ";
            
            self::clean($vflEmailMarketingTypes);
            
			$vpdo->execute($vsql);
            $vrows=$vpdo->getAllData();
            $vrowsTotal=$vpdo->getAffectedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vflEmailMarketingType= new clspFLEmailMarketingType();
                $vflEmailMarketingType->idEmailMarketingType=(int)($vrows[$vrowNumber]["id_emailMarketingType"]);
				$vflEmailMarketingType->emailMarketingType=trim($vrows[$vrowNumber]["fldemailMarketingType"]);
				
                self::add($vflEmailMarketingTypes, $vflEmailMarketingType);
                unset($vflEmailMarketingType);
            }
            if ( $vrowsTotal<=0 ){
                return 0;
            }
			
			unset($vsql, $vrows, $vrowsTotal, $vrowNumber);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflEmailMarketingTypes, $vflEmailMarketingType)
	 {
        try{
            array_push($vflEmailMarketingTypes->emailMarketingTypes, $vflEmailMarketingType);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflEmailMarketingTypes)
	 {
        try{
            return count($vflEmailMarketingTypes->emailMarketingTypes);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflEmailMarketingTypes)
	 {
        try{
            $vflEmailMarketingTypes->emailMarketingTypes=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }
?>