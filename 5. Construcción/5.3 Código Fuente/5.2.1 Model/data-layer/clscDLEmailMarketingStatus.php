<?php
/********************************************************
Name: clscDLEmailMarketingStatus.php
Version: 0.0.1
Autor name: Sandro Alan Gomez Aceituno.
Modification autor name: 
Creation date: 06/06/2017
Modification date: 
Description: Email Marketing Status Collection Class, Data Layer. 
********************************************************/
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLEmailMarketingStatus.php");

class clscDLEmailMarketingStatus
 {
    public function __construct() { }
        
    public static function queryToDataBase($vflEmailMarketingStatus, $vfilter, $vpdo)
	 {
		try{
			$vsql ="SELECT * FROM c_emailmarketingstatus ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY id_emailMarketingStatus";
            
            self::clean($vflEmailMarketingStatus);            
            $vpdo->execute($vsql);
            $vrows=$vpdo->getAllData();
            $vrowsTotal=$vpdo->getAffectedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vflEmailMarketingStatu= new clspFLEmailMarketingStatus();
                $vflEmailMarketingStatu->idEmailMarketingStatus=(int)($vrows[$vrowNumber]["id_emailMarketingStatus"]);
                $vflEmailMarketingStatu->emailMarketingStatus=trim($vrows[$vrowNumber]["fldemailMarketingStatus"]);
                
                self::add($vflEmailMarketingStatus, $vflEmailMarketingStatu);
                unset($vflEmailMarketingStatu);
            }
            if ( $vrowsTotal<=0 ){
                return 0;
            }
			
			unset($vsql, $vrows, $vrowsTotal, $vrowNumber);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflEmailMarketingStatus, $vflEmailMarketingStatu)
	 {
        try{
            array_push($vflEmailMarketingStatus->emailMarketingStatus, $vflEmailMarketingStatu);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflEmailMarketingStatus)
	 {
        try{
            return count($vflEmailMarketingStatus->emailMarketingStatus);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflEmailMarketingStatus)
	 {
        try{
            $vflEmailMarketingStatus->emailMarketingStatus=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }    
    
    public function __destruct(){ }
 }
?>