<?php
/********************************************************
Name: clspDLPasswordReset.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 03/11/2015
Modification date: 24/05/2017
Description: Password Reset Principal Class, Data Layer. 
********************************************************/


class clspDLPasswordReset
 {
	public function __construct() { }
	
    
	public static function addToDataBase($vflPasswordReset, $vpdo)
	 {
		try{
            $vflPasswordReset->idPasswordReset=self::generateId();
            $vsql ="INSERT INTO p_passwordreset(id_passwordReset, id_passwordResetStatus, id_user, fldeffectiveStartDate, fldeffectiveEndDate) ";
			$vsql.="VALUES('" . $vflPasswordReset->idPasswordReset . "'";
            $vsql.=", " . $vflPasswordReset->passwordResetStatus->idPasswordResetStatus;
			$vsql.=", " . $vflPasswordReset->user->idUser;
            $vsql.=", '" . date("Y-m-d H:i:s", strtotime($vflPasswordReset->effectiveStartDate)) . "'";
            $vsql.=", '" . date("Y-m-d H:i:s", strtotime($vflPasswordReset->effectiveEndDate)) . "')";
            
			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function updateStatusInDataBase($vflPasswordReset, $vpdo)
	 {
		try{
			$vsql ="UPDATE p_passwordreset ";
			$vsql.="SET id_passwordResetStatus=" . $vflPasswordReset->passwordResetStatus->idPasswordResetStatus . " ";
			$vsql.="WHERE id_passwordReset='" . $vflPasswordReset->idPasswordReset . "'";
			
			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==0 ){
				return 0;
			}
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexcepcion){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function queryToDataBase($vflPasswordReset, $vpdo)
	 {
		try{
			$vsql ="SELECT p_passwordreset.*, c_user.*, c_usertype.flduserType, c_userstatus.flduserStatus, c_passwordresetstatus.fldpasswordResetStatus ";
            $vsql.="FROM p_passwordreset ";
            $vsql.="INNER JOIN c_user ON p_passwordreset.id_user=c_user.id_user ";
            $vsql.="INNER JOIN c_usertype ON c_user.id_userType=c_usertype.id_userType ";
			$vsql.="INNER JOIN c_userstatus ON c_user.id_userStatus=c_userstatus.id_userStatus ";
			$vsql.="INNER JOIN c_passwordresetstatus ON p_passwordreset.id_passwordResetStatus=c_passwordresetstatus.id_passwordResetStatus ";
			$vsql.="WHERE p_passwordreset.id_passwordReset='" . $vflPasswordReset->idPasswordReset. "'";
            
			$vpdo->execute($vsql);
			if ( $vpdo->getAffectedRowsNumber()==1 ){
				$vrow=$vpdo->getAllData();
                $vflPasswordReset->user->idUser=(int)($vrow[0]["id_user"]);
				$vflPasswordReset->user->userType->idUserType=(int)($vrow[0]["id_userType"]);
				$vflPasswordReset->user->userType->userType=trim($vrow[0]["flduserType"]);
				$vflPasswordReset->user->userStatus->idUserStatus=(int)($vrow[0]["id_userStatus"]);
				$vflPasswordReset->user->userStatus->userStatus=trim($vrow[0]["flduserStatus"]);
                $vflPasswordReset->user->firstName=trim($vrow[0]["fldfirstName"]);
                $vflPasswordReset->user->lastName=trim($vrow[0]["fldlastName"]);
				$vflPasswordReset->user->name=trim($vrow[0]["fldname"]);
                $vflPasswordReset->user->emailAccount=trim($vrow[0]["fldemailAccount"]);
                $vflPasswordReset->passwordResetStatus->idPasswordResetStatus=(int)($vrow[0]["id_passwordResetStatus"]);
				$vflPasswordReset->passwordResetStatus->passwordResetStatus=trim($vrow[0]["fldpasswordResetStatus"]);
                $vflPasswordReset->effectiveStartDate=date("Y-m-d H:i:s", strtotime(trim($vrow[0]["fldeffectiveStartDate"])));
                $vflPasswordReset->effectiveEndDate=date("Y-m-d H:i:s", strtotime(trim($vrow[0]["fldeffectiveEndDate"])));
                
				unset($vrow);
			}
			else{
				return 0;
			}
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
     }
     
    public static function generateId()
     {
        try{
            $vbytes=openssl_random_pseudo_bytes(16);
            return bin2hex($vbytes);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
     }
	
    
	public function __destruct(){ }
 }
?>