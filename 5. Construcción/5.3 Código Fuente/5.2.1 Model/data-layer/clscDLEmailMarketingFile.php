<?php
/********************************************************
Name: clscDLEmailMarketingFile.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 12/06/2015
Modification date: 11/07/2015
Description: Email Marketing File Collection Class, Data Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLEmailMarketingFile.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLEmailMarketingFile.php");


class clscDLEmailMarketingFile
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflEmailMarketingFiles, $vfilter, $vpdo)
	 {
		try{
			$vsql ="SELECT p_emailmarketingfile.*, p_emailmarketing.*, c_campus.fldcampus, c_emailmarketingtype.fldemailMarketingType ";
            $vsql.=", c_emailmarketingstatus.fldemailMarketingStatus, c_user.*, c_usertype.flduserType, c_userstatus.flduserStatus ";
            $vsql.=", c_educationaloffer.fldeducationalOffer, c_educationaloffer.fldfileName, c_educationallevel.fldeducationalLevel ";
            $vsql.="FROM p_emailmarketingfile ";
            $vsql.="INNER JOIN p_emailmarketing ON p_emailmarketingfile.id_campus=p_emailmarketing.id_campus ";
            $vsql.="AND p_emailmarketingfile.id_emailMarketing=p_emailmarketing.id_emailMarketing ";
            $vsql.="INNER JOIN c_campus ON p_emailmarketing.id_campus=c_campus.id_campus ";
            $vsql.="INNER JOIN c_emailmarketingtype ON p_emailmarketing.id_emailMarketingType=c_emailmarketingtype.id_emailMarketingType ";
            $vsql.="INNER JOIN c_emailmarketingstatus ON p_emailmarketing.id_emailMarketingStatus=c_emailmarketingstatus.id_emailMarketingStatus ";
            $vsql.="INNER JOIN c_backenduser ON p_emailmarketing.id_userRecord=c_backenduser.id_user ";
            $vsql.="INNER JOIN c_user ON c_backenduser.id_user=c_user.id_user ";
            $vsql.="INNER JOIN c_usertype ON c_user.id_userType=c_usertype.id_userType ";
			$vsql.="INNER JOIN c_userstatus ON c_user.id_userStatus=c_userstatus.id_userStatus ";
            $vsql.="INNER JOIN c_educationaloffer ON p_emailmarketing.id_campus=c_educationaloffer.id_campus ";
            $vsql.="AND p_emailmarketing.id_educationalLevel=c_educationaloffer.id_educationalLevel ";
            $vsql.="AND p_emailmarketing.id_educationalOffer=c_educationaloffer.id_educationalOffer ";
            $vsql.="INNER JOIN c_assignment ON c_educationaloffer.id_campus=c_assignment.id_campus ";
            $vsql.="AND c_educationaloffer.id_educationalLevel=c_assignment.id_educationalLevel ";
            $vsql.="INNER JOIN c_educationallevel ON c_assignment.id_educationalLevel=c_educationallevel.id_educationalLevel ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY p_emailmarketingfile.id_campus, p_emailmarketingfile.id_emailMarketing, p_emailmarketingfile.id_emailMarketingFile";
            
            self::clean($vflEmailMarketingFiles);
            
			$vpdo->execute($vsql);
            $vrows=$vpdo->getAllDataAlias();
            $vrowsTotal=$vpdo->getAffectedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vflEmailMarketingFile= new clspFLEmailMarketingFile();
                $vflEmailMarketingFile->emailMarketing->campus->idCampus=(int)($vrows[$vrowNumber]["p_emailmarketingfile.id_campus"]);
                $vflEmailMarketingFile->emailMarketing->campus->campus=trim($vrows[$vrowNumber]["c_campus.fldcampus"]);
                $vflEmailMarketingFile->emailMarketing->idEmailMarketing=trim($vrows[$vrowNumber]["p_emailmarketingfile.id_emailMarketing"]);
				$vflEmailMarketingFile->emailMarketing->emailMarketingType->idEmailMarketingType=(int)($vrows[$vrowNumber]["p_emailmarketing.id_emailMarketingType"]);
				$vflEmailMarketingFile->emailMarketing->emailMarketingType->emailMarketingType=trim($vrows[$vrowNumber]["c_emailmarketingtype.fldemailMarketingType"]);
                $vflEmailMarketingFile->emailMarketing->emailMarketingStatus->idEmailMarketingStatus=(int)($vrows[$vrowNumber]["p_emailmarketing.id_emailMarketingStatus"]);
				$vflEmailMarketingFile->emailMarketing->emailMarketingStatus->emailMarketingStatus=trim($vrows[$vrowNumber]["c_emailmarketingstatus.fldemailMarketingStatus"]);
                $vflEmailMarketingFile->emailMarketing->userRecord->idUser= (int)($vrows[$vrowNumber]["p_emailmarketing.id_userRecord"]);
                $vflEmailMarketingFile->emailMarketing->userRecord->userType->idUserType=(int)($vrows[$vrowNumber]["c_user.id_userType"]);
				$vflEmailMarketingFile->emailMarketing->userRecord->userType->userType=trim($vrows[$vrowNumber]["c_usertype.flduserType"]);
				$vflEmailMarketingFile->emailMarketing->userRecord->userStatus->idUserStatus=(int)($vrows[$vrowNumber]["c_user.id_userStatus"]);
				$vflEmailMarketingFile->emailMarketing->userRecord->userStatus->userStatus=trim($vrows[$vrowNumber]["c_userstatus.flduserStatus"]);
                $vflEmailMarketingFile->emailMarketing->userRecord->firstName=trim($vrows[$vrowNumber]["c_user.fldfirstName"]);
                $vflEmailMarketingFile->emailMarketing->userRecord->lastName=trim($vrows[$vrowNumber]["c_user.fldlastName"]);
				$vflEmailMarketingFile->emailMarketing->userRecord->name=trim($vrows[$vrowNumber]["c_user.fldname"]);
                $vflEmailMarketingFile->emailMarketing->userRecord->emailAccount=trim($vrows[$vrowNumber]["c_user.fldemailAccount"]);
                $vflEmailMarketingFile->emailMarketing->userRecord->campus=$vflEmailMarketingFile->emailMarketing->campus;
                $vflEmailMarketingFile->emailMarketing->educationalOffer->assignment->campus=$vflEmailMarketingFile->emailMarketing->campus;
                $vflEmailMarketingFile->emailMarketing->educationalOffer->assignment->educationalLevel->idEducationalLevel=(int)($vrows[$vrowNumber]["p_emailmarketing.id_educationalLevel"]);
                $vflEmailMarketingFile->emailMarketing->educationalOffer->assignment->educationalLevel->educationalLevel=trim($vrows[$vrowNumber]["c_educationallevel.fldeducationalLevel"]);
                $vflEmailMarketingFile->emailMarketing->educationalOffer->idEducationalOffer=(int)($vrows[$vrowNumber]["p_emailmarketing.id_educationalOffer"]);
                $vflEmailMarketingFile->emailMarketing->educationalOffer->educationalOffer=trim($vrows[$vrowNumber]["c_educationaloffer.fldeducationalOffer"]);
                $vflEmailMarketingFile->emailMarketing->educationalOffer->fileName=trim($vrows[$vrowNumber]["c_educationaloffer.fldfileName"]);
                $vflEmailMarketingFile->emailMarketing->subject=trim($vrows[$vrowNumber]["p_emailmarketing.fldsubject"]);
                $vflEmailMarketingFile->emailMarketing->message=trim($vrows[$vrowNumber]["p_emailmarketing.fldmessage"]);
                $vflEmailMarketingFile->emailMarketing->recordDate=date("m/d/Y H:i:s", strtotime(trim($vrows[$vrowNumber]["p_emailmarketing.fldrecordDate"])));
                $vflEmailMarketingFile->idEmailMarketingFile=(int)($vrows[$vrowNumber]["p_emailmarketingfile.id_emailMarketingFile"]);
                $vflEmailMarketingFile->realName=trim($vrows[$vrowNumber]["p_emailmarketingfile.fldrealName"]);
                $vflEmailMarketingFile->fisicName=trim($vrows[$vrowNumber]["p_emailmarketingfile.fldfisicName"]);
                
                self::add($vflEmailMarketingFiles, $vflEmailMarketingFile);
                unset($vflEmailMarketingFile);
            }
            if ( $vrowsTotal<=0 ){
                return 0;
            }
			
			unset($vsql, $vrows, $vrowsTotal, $vrowNumber);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function addToDataBase($vflEmailMarketingFiles, $vpdo)
	 {
		try{
			for($vi=0; $vi<self::total($vflEmailMarketingFiles); $vi++){
                if ( clspDLEmailMarketingFile::addToDataBase($vflEmailMarketingFiles->emailMarketingFiles[$vi], $vpdo)==0 ){
                    return 0;
                }
                
                unset($vflBackendUserAccessPrivilege);
			}
            
			unset($vi);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
    public static function deleteInDataBase($vflEmailMarketingFiles, $vpdo)
	 {
		try{
			for($vi=0; $vi<self::total($vflEmailMarketingFiles); $vi++){
                if ( clspDLEmailMarketingFile::deleteInDataBase($vflEmailMarketingFiles->emailMarketingFiles[$vi], $vpdo)==0 ){
                    return 0;
                }
			}
            
			unset($vi);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
	public static function add($vflEmailMarketingFiles, $vflEmailMarketingFile)
	 {
        try{
            array_push($vflEmailMarketingFiles->emailMarketingFiles, $vflEmailMarketingFile);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflEmailMarketingFiles)
	 {
        try{
            return count($vflEmailMarketingFiles->emailMarketingFiles);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflEmailMarketingFiles)
	 {
        try{
            $vflEmailMarketingFiles->emailMarketingFiles=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }
?>