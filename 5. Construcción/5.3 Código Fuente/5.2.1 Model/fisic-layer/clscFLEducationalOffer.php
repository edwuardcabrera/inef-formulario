<?php
/********************************************************
Name: clscFLEducationalOffer.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 11/07/2017
Modification date:
Description: Educational Offer Collection Class, Fisic Layer. 
********************************************************/

class clscFLEducationalOffer
 {
	public $educationalOffers;
	
	public function __construct()
     {
		$this->educationalOffers=array();
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }
	
	public function __destruct()
	 {
		unset($this->educationalOffers);
	 }
 }
?>