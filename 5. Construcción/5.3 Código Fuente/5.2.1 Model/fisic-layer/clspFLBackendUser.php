<?php
/********************************************************
Name: clspFLBackendUser.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 25/05/2017
Modification date:
Description: Backend User Principal Class, Fisic Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLUser.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLCampus.php");


class clspFLBackendUser extends clspFLUser
 {
	public $campus;
    
	public function __construct()
	 {
		parent::__construct();
        $this->campus=new clspFLCampus();
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }
	
	public function __destruct()
	 {
		parent::__destruct();
		unset($this->campus);
	 }
 }
?>