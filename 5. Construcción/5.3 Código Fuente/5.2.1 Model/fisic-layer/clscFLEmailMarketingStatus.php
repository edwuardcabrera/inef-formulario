<?php
/********************************************************
Name: clscFLEmailMarketingStatus.php
Version: 0.0.1
Autor name: Sandro Alan G�mez Aceituno.
Modification autor name:
Creation date: 31/05/2017
Modification date:
Description: Email Marketing status Collection Class, Fisic Layer. 
********************************************************/

class clscFLEmailMarketingStatus
 {
	public $emailMarketingStatus;
	
	public function __construct()
     {
		$this->emailMarketingStatus=array();
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }
	
	public function __destruct()
	 {
		unset($this->emailMarketingStatus);
	 }
 }
?>