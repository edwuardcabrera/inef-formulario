<?php
/********************************************************
Name: clspFLEducationalLevel.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 06/06/2017
Modification date:
Description: Educational Level Principal Class, Fisic Layer. 
********************************************************/

class clspFLEducationalLevel
 {
	public $idEducationalLevel;
	public $educationalLevel;
	
	public function __construct()
	 {
		$this->idEducationalLevel=0;
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }
	
	
	public function __destruct()
	 {
		unset($this->idEducationalLevel, $this->educationalLevel);
	 }
 }
?>