<?php
/********************************************************
Name: clspFLUserStatus.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 18/08/2015
Modification date:
Description: User Status Principal Class, Fisic Layer. 
********************************************************/

class clspFLUserStatus
 {
	public $idUserStatus;
	public $userStatus;
	
	public function __construct()
	 {
		$this->idUserStatus=0;
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue) //Set value property
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }
	
	public function __destruct()
	 {
		unset($this->idUserStatus, $this->userStatus);
	 }
 }
?>