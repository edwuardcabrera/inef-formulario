<?php
/********************************************************
Name: clspFLCampusEMailSetting.php
Version: 0.0.1
Autor name: Sandro Alan Gomez Aceituno.
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 07/06/2017
Modification date: 20/06/2017
Description: Campus EMail Setting Principal Class, Fisic Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLCampus.php");


class clspFLCampusEMailSetting
 {
	public $campus;
	public $hostName;
	public $smtpPort;
	public $smtpAuth;
	public $smtpSecure;
	public $userName;
	public $userPassword;
	
	public function __construct()
	 {
		$this->campus=new clspFLCampus();
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }
	
	public function __destruct()
	 {
		unset(
			$this->campus, 
			$this->hostName, 
			$this->smtpPort,
			$this->smtpAuth,
			$this->smtpSecure,
			$this->userName,
			$this->userPassword
		);
	 }
 }
?>