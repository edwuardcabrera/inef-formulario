<?php
/********************************************************
Name: clspFLEmailMarketingSend.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 28/06/2017
Modification date:
Description: Email Marketing Send Principal Class, Fisic Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . '/fisic-layer/clspFLEmailMarketing.php');
require_once (dirname(dirname(__FILE__)) . '/fisic-layer/clspFLBackendUser.php');


class clspFLEmailMarketingSend
 {
	public $emailMarketing;
    public $userMailing;
	public $mailingDate;	
	
	public function __construct()
	 {
        date_default_timezone_set('America/Mexico_City');
		$this->emailMarketing= new clspFLEmailMarketing();
        $this->userMailing= new clspFLBackendUser();
        $this->mailingDate=date("Y-m-d H:i:s");
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }
	
	
	public function __destruct()
	 {
		unset($this->emailMarketing, $this->userMailing, $this->mailingDate);
	 }
 }
?>