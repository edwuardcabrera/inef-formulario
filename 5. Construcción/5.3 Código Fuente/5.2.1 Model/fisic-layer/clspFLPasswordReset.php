<?php
/********************************************************
Name: clspFLPasswordReset.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 30/10/2015
Modification date:
Description: Password Reset Principal Class, Fisic Layer.
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLUser.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLPasswordResetStatus.php");


class clspFLPasswordReset
 {
	public $idPasswordReset;
    public $passwordResetStatus;
    public $user;
    public $effectiveStartDate;
    public $effectiveEndDate;
    
	public function __construct()
	 {
        $this->idPasswordReset="";
        $this->passwordResetStatus= new clspFLPasswordResetStatus();
		$this->user= new clspFLUser();
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }
	 
	public function __destruct()
	 {
		unset($this->idPasswordReset, $this->passwordResetStatus, $this->user, $this->effectiveStartDate, $this->effectiveEndDate);
	 }
 }
?>