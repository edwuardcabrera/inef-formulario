<?php
/********************************************************
Name: clscFLEmailMarketingFile.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 12/06/2017
Modification date:
Description: Email Marketing File Collection Class, Fisic Layer. 
********************************************************/

class clscFLEmailMarketingFile
 {
	public $emailMarketingFiles;
	
	public function __construct()
     {
		$this->emailMarketingFiles=array();
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }
	
	public function __destruct()
	 {
		unset($this->emailMarketingFiles);
	 }
 }
?>