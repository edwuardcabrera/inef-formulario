<?php
/********************************************************
Name: clscFLAccessPrivilege.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 08/09/2015
Modification date: 16/04/2016
Description: Access Privilege Collection Class, Fisic Layer. 
********************************************************/

class clscFLAccessPrivilege
 {
	public $accessPrivileges;
	
	public function __construct()
     {
		$this->accessPrivileges=array();
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }
	
	public function __destruct()
	 {
		unset($this->accessPrivileges);
	 }
 }
?>