<?php
/********************************************************
  Name: clspFLUserApplicant.php
  Autor name: Alejandro Hdez. G.
  Modification autor name: Edwuard H. Cabrera Rodríguez
  Creation date: 11/05/2017
  Modification date: 19/06/2017
  Description: User Applicant Principal Class, Fisic Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . '/fisic-layer/clspFLUser.php');


class clspFLUserApplicant extends clspFLUser
 {

    public $movilNumber;
    
    public function __construct()
	 {
        parent::__construct();
     }

	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }

	public function __destruct()
	 {
        parent::__destruct();
		unset($this->movilNumber);
     }
 }
?>