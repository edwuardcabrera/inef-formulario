<?php
/********************************************************
Name: clspFLUser.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 18/08/2015
Modification date:
Description: User Principal Class, Fisic Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLUserType.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLUserStatus.php");


class clspFLUser
 {
	public $idUser;
	public $userType;
	public $userStatus;
    public $firstName;
    public $lastName;
	public $name;
	public $password;
    public $emailAccount;
	
	public function __construct()
	 {
		$this->idUser=0;
		$this->userType=new clspFLUserType();
		$this->userStatus=new clspFLUserStatus();
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }
	
	public function __destruct()
	 {
		unset($this->idUser, $this->userType, $this->userStatus, $this->firstName, $this->lastName, $this->name, $this->password,
              $this->emailAccount);
	 }
 }
?>