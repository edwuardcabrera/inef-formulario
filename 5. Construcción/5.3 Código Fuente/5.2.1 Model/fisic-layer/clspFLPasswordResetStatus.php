<?php
/********************************************************
Name: clspFLPasswordResetStatus.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 30/10/2015
Modification date:
Description: Password Reset Status Principal Class, Fisic Layer. 
********************************************************/

class clspFLPasswordResetStatus
 {
	public $idPasswordResetStatus;
	public $passwordResetStatus;
	
	public function __construct()
	 {
		$this->idPasswordResetStatus=0;
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }
	
	public function __destruct()
	 {
		unset($this->idPasswordResetStatus, $this->passwordResetStatus);
	 }
 }
?>