<?php
/********************************************************
Name: clspFLCampus.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 25/05/2017
Modification date: 30/06/2017
Description: Campus Principal Class, Fisic Layer. 
********************************************************/

class clspFLCampus
 {
	public $idCampus;
	public $campus;
    public $slogan;
	
	public function __construct()
	 {
		$this->idCampus=0;
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }
	
	
	public function __destruct()
	 {
		unset($this->idCampus, $this->campus, $this->slogan);
	 }
 }
?>