<?php
/********************************************************
Name: clspFLAssignment.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 07/06/2017
Modification date: 10/07/2017
Description: Assignment Principal Class, Fisic Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLCampus.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLEducationalLevel.php");


class clspFLAssignment
 {
	public $campus;
	public $educationalLevel;
	
	public function __construct()
	 {
		$this->campus=new clspFLCampus();
		$this->educationalLevel=new clspFLEducationalLevel();
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }
	
	public function __destruct()
	 {
		unset($this->campus, $this->educationalLevel);
	 }
 }
?>