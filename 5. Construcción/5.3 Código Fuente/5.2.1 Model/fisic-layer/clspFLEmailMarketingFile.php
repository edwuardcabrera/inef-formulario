<?php
/********************************************************
Name: clspFLEmailMarketingFile.php
Version: 0.0.1
Autor name: Sandro Alan G�mez Aceituno.
Modification autor name: Edwuard H. Cabrera Rodr�guez
Creation date: 06/06/2017
Modification date: 13/06/2017
Description: Email Marketing File Principal Class, Fisic Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . '/fisic-layer/clspFLEmailMarketing.php');


class clspFLEmailMarketingFile
 {
	public $emailMarketing;
	public $idEmailMarketingFile;
	public $realName;
    public $fisicName;	
	
	public function __construct()
	 {
		$this->idEmailMarketingFile=0;
		$this->emailMarketing= new clspFLEmailMarketing();
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }
	
	
	public function __destruct()
	 {
		unset($this->emailMarketing, $this->idemailMarketingFile, $this->realName, $this->fisicName);
	 }
 }
?>