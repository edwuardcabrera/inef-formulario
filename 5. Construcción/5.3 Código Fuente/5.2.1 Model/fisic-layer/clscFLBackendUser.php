<?php
/********************************************************
Name: clscFLBackendUser.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 31/05/2017
Modification date:
Description: Backend User Collection Class, Fisic Layer. 
********************************************************/

class clscFLBackendUser
 {
	public $backendUsers;
	
	public function __construct()
     {
		$this->backendUsers=array();
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }
	
	public function __destruct()
	 {
		unset($this->backendUsers);
	 }
 }
?>