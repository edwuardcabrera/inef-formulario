<?php
/********************************************************
Name: clspFLEducationalOffer.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 10/07/2017
Modification date: 11/07/2017
Description: Educational Offer Principal Class, Fisic Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLAssignment.php");


class clspFLEducationalOffer
 {
	public $assignment;
    public $idEducationalOffer;
    public $educationalOffer;
	public $fileName;
	
	public function __construct()
	 {
		$this->assignment=new clspFLAssignment();
        $this->idEducationalOffer=0;
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }
	
	public function __destruct()
	 {
		unset($this->assignment, $this->idEducationalOffer, $this->educationalOffer, $this->fileName);
	 }
 }
?>