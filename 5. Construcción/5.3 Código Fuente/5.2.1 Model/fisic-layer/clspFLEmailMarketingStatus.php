<?php
/********************************************************
Name: clspFLEmailMarketingStatus.php
Version: 0.0.1
Autor name: Sandro Alan Gomez Aceituno.
Modification autor name:
Creation date: 06/06/2017
Modification date:
Description: Email Marketing Status Principal Class, Fisic Layer. 
********************************************************/

class clspFLEmailMarketingStatus
 {
	public $idEmailMarketingStatus;
	public $emailMarketingStatus;
	
	public function __construct()
	 {
		$this->idEmailMarketingStatus=0;
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }
	
	
	public function __destruct()
	 {
		unset($this->idEmailMarketingStatus, $this->emailMarketingStatus);
	 }
 }
?>