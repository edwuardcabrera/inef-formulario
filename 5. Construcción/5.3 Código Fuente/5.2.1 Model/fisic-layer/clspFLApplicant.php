<?php
/********************************************************
Name:clspFLApplicant.php
Autor name: Alejandro Hdez. G.
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 11/05/2017
Modification date: 12/07/2017
Description: Applicant Principal Class, Fisic Layer. 
 ********************************************************/
 
require_once (dirname(dirname(__FILE__)) . '/fisic-layer/clspFLCampus.php');
require_once (dirname(dirname(__FILE__)) . '/fisic-layer/clspFLUserApplicant.php');
require_once (dirname(dirname(__FILE__)) . '/fisic-layer/clspFLEducationalOffer.php');


class clspFLApplicant
 {
    public $campus;
    public $idApplicant;
    public $user;
    public $educationalOffer;
    public $requestDate;

    public function __construct()
     {
        date_default_timezone_set('America/Mexico_City');
        $this->campus= new clspFLCampus();
		$this->idApplicant="000000-" . date("y");
        $this->user= new clspFLUserApplicant();
        $this->educationalOffer = new clspFLEducationalOffer();
        $this->requestDate = date("Y-m-d H:i:s");
     }

 	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }

	public function __destruct()
	 {
        unset($this->campus, $this->idApplicant, $this->user, $this->educationalOffer, $this->requestDate);
     }
 }
?>