<?php
/********************************************************
Name: clscFLCampus.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 26/05/2017
Modification date:
Description: Campus Collection Class, Fisic Layer. 
********************************************************/

class clscFLCampus
 {
	public $campuses;
	
	public function __construct()
     {
		$this->campuses=array();
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }
	
	public function __destruct()
	 {
		unset($this->campuses);
	 }
 }
?>