<?php
/********************************************************
Name: clspFLEmailMarketing.php
Version: 0.0.1
Autor name: Sandro Alan Gómez Aceituno.
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 06/06/2017
Modification date: 10/07/2017
Description: Email Marketing Principal Class, Fisic Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . '/fisic-layer/clspFLCampus.php');
require_once (dirname(dirname(__FILE__)) . '/fisic-layer/clspFLEmailMarketingType.php');
require_once (dirname(dirname(__FILE__)) . '/fisic-layer/clspFLEmailMarketingStatus.php');
require_once (dirname(dirname(__FILE__)) . '/fisic-layer/clspFLBackendUser.php');
require_once (dirname(dirname(__FILE__)) . '/fisic-layer/clspFLEducationalOffer.php');


class clspFLEmailMarketing
 {
	public $campus;
	public $idEmailMarketing;
	public $emailMarketingType;
	public $emailMarketingStatus;
	public $userRecord;
    public $educationalOffer;
	public $subject;
	public $message;
	public $recordDate;
	
	public function __construct()
	 {
        date_default_timezone_set('America/Mexico_City');
		$this->idEmailMarketing="000000-" . date("y");
		$this->campus=new clspFLCampus();
		$this->emailMarketingType=new clspFLEmailMarketingType();
		$this->emailMarketingStatus=new clspFLEmailMarketingStatus();
		$this->userRecord=new clspFLBackendUser();
        $this->educationalOffer=new clspFLEducationalOffer();
        $this->recordDate=date("Y-m-d H:i:s");
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }
	
	public function __destruct()
	 {
		unset($this->campus, $this->idEmailMarketing, $this->emailMarketingType, $this->emailMarketingStatus, $this->userRecord, $this->educationalOffer,
              $this->subject, $this->message, $this->recordDate);
	 }
 }
?>