<?php
/********************************************************
Name: clspFLAccessPrivilege.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 19/10/2015
Modification date: 19/11/2015
Description: Access Privilege Principal Class, Fisic Layer. 
********************************************************/

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLAccessPrivilegeType.php");

class clspFLAccessPrivilege
 {
	public $idAccessPrivilege;
    public $accessPrivilegeType;
	public $accessPrivilege;
    public $level1;
    public $level2;
    public $level3;
    public $level4;
    public $url;
    public $iconName;
	
	public function __construct()
	 {
		$this->idAccessPrivilege=0;
        $this->accessPrivilegeType=new clspFLAccessPrivilegeType();
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }
     
	public function __destruct()
	 {
		unset($this->idAccessPrivilege, $this->accessPrivilegeType, $this->accessPrivilege, $this->level1, $this->level2, $this->level3,
              $this->level4, $this->url, $this->iconName);
	 }
 }
?>