<?php
/********************************************************
Name: clscFLAssignment.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 07/06/2017
Modification date:
Description: Assignment Collection Class, Fisic Layer. 
********************************************************/

class clscFLAssignment
 {
	public $assignments;
	
	public function __construct()
     {
		$this->assignments=array();
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }
	
	public function __destruct()
	 {
		unset($this->assignments);
	 }
 }
?>