<?php
/********************************************************
Name: clspFLAccessPrivilegeType.php
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name:
Creation date: 19/10/2015
Modification date:
Description: Acces Privilege Type Principal Class, Fisic Layer. 
********************************************************/

class clspFLAccessPrivilegeType
 {
	public $idAccessPrivilegeType;
	public $accessPrivilegeType;
	
	public function __construct()
	 {
		$this->idAccessPrivilegeType=0;
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }
	
	
	public function __destruct()
	 {
		unset($this->idAccessPrivilegeType, $this->accessPrivilegeType);
	 }
 }
?>