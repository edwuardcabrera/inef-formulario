<?php
class clspPDO
 {
    private	$dbms, $serverName, $dbName, $userName, $userPassword; 
	private	$db, $pdoStatement;
	
    public function __construct()
     {
        $this->dbms="mysql";
		$this->serverName="localhost";
        $this->dbName="rparedes_solicitud_db";
		$this->userName="rparedes_solicit";
		$this->userPassword="50l1c1tud";
        $this->db=null;
        $this->pdoStatement=null;
     }
	
	public function openConnection($vtype=0)
	 {
        try{
            $voptions=array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'");
            if ( $vtype!=0 ){
                $voptions=array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'latin1'");
            }
            $vdsn=$this->dbms . ":host=" . $this->serverName . ";dbname=" . $this->dbName;
            $this->db=new PDO($vdsn, $this->userName, $this->userPassword, $voptions);
        
            unset($voptions, $vdsn);
            return true; 
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	public function closeConnection()
	 {
		$this->__destruct();
	 }
	
	public function execute($vsql)
	 {
		try{
		    if ( $this->db==null ){
                throw new Exception ("Imposible ejecutar el m�todo 'execute', no existe conexi�n a la base de datos", -500);
		    }
            $this->pdoStatement=$this->db->prepare($vsql);
            if ( (is_bool($this->pdoStatement)) && ($this->pdoStatement==false) ){
                $verror=$this->db->errorInfo();
                throw new Exception ($verror[2], $verror[1]);
            }
            $vexecute=$this->pdoStatement->execute();
            if ( (is_bool($vexecute)) && ($vexecute==false) ){
                $verror=$this->pdoStatement->errorInfo();
                throw new Exception ($verror[2], $verror[1]);
            }
            
            return $vexecute;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
   
   public function getColumnsNumber()
	 {
		try{
            if ( $this->pdoStatement==null ){
                throw new Exception ("Imposible ejecutar el m�todo 'getColumnsNumber', el servidor de la base de datos no tiene preparada una sentencia SQL", -501);
		    }
		 	if ( (is_bool($this->pdoStatement)) && ($this->pdoStatement==false) ){
                $verror=$this->db->errorInfo();
                throw new Exception ($verror[2], $verror[1]);
            }
            
            return $this->pdoStatement->columnCount();
		}
		catch (Exception $vexception){	
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
   
    public function getAffectedRowsNumber()
	 {
		try{
            if ( $this->pdoStatement==null ){
                throw new Exception ("Imposible ejecutar el m�todo 'getAffectedRowsNumber', el servidor de la base de datos no tiene preparada una sentencia SQL", -501);
		    }
		 	if ( (is_bool($this->pdoStatement)) && ($this->pdoStatement==false) ){
                $verror=$this->db->errorInfo();
                throw new Exception ($verror[2], $verror[1]);
            }
            
            return $this->pdoStatement->rowCount(); 
		}
		catch (Exception $vexception){	
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public function getAllData()
	 {
		try{
            if ( $this->pdoStatement==null ){
                throw new Exception ("Imposible ejecutar el m�todo 'getAllData', el servidor de la base de datos no tiene preparada una sentencia SQL", -501);
		    }
		 	if ( (is_bool($this->pdoStatement)) && ($this->pdoStatement==false) ){
                $verror=$this->db->errorInfo();
                throw new Exception ($verror[2], $verror[1]);
            }
            $vfetchAll=$this->pdoStatement->fetchAll();
            if ( (is_bool($vfetchAll)) && ($vfetchAll==false) ){
                $verror=$this->pdoStatement->errorInfo();
                throw new Exception ($verror[2], $verror[1]);
            }
            
            return $vfetchAll; 
		}
		catch (Exception $vexception){	
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public function getAllDataAlias()
	 {
		try{
            $vfetchData=$this->getAllData();
			if ( !$vfetchData ){
        		return false;
    		}
            $vfetchDataAlias=array();
            $vcolumnsMetaData=array();
			$vrowsNumber=count($vfetchData);
            $vcolumnsNumber=$this->getColumnsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsNumber; $vrowNumber++){
				for ($vcolumnNumber=0; $vcolumnNumber<$vcolumnsNumber; $vcolumnNumber++){
				    if ( $vrowNumber==0 ){
				        $vcolumnMetaData=$this->getColumnMetadata($vcolumnNumber);
                        array_push($vcolumnsMetaData, $vcolumnMetaData);
                    }
                    $vtableName=$vcolumnsMetaData[$vcolumnNumber]['table'];
				    $vfieldName=$vcolumnsMetaData[$vcolumnNumber]['name'];
                    if ( (! is_null($vtableName)) && ( ! empty($vtableName)) ){
                        $vfetchDataAlias[$vrowNumber]["$vtableName.$vfieldName"]=$vfetchData[$vrowNumber][$vcolumnNumber];
                    }
                    else{
                        $vfetchDataAlias[$vrowNumber]["$vfieldName"]=$vfetchData[$vrowNumber][$vcolumnNumber];
                    }
                    
                    unset($vtableName, $vfieldName);	                
				}
                
                unset($vcolumnNumber);
			}
            
            unset($vfetchData, $vcolumnsMetaData, $vrowsNumber, $vcolumnsNumber, $vrowNumber);
            return $vfetchDataAlias;
		}
		catch (Exception $vexception){	
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

    public function getColumnMetadata($vcolumnNumber)
	 {
		try{
            if ( $this->pdoStatement==null ){
                throw new Exception ("Imposible ejecutar el m�todo 'getColumnMetadata', el servidor de la base de datos no tiene preparada una sentencia SQL", -501);
		    }
		 	if ( (is_bool($this->pdoStatement)) && ($this->pdoStatement==false) ){
                $verror=$this->db->errorInfo();
                throw new Exception ($verror[2], $verror[1]);
            }
            
            return $this->pdoStatement->getColumnMeta($vcolumnNumber);
		}
		catch (Exception $vexception){	
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

    public function getLastInsertId()
     {
        try{
            if ( $this->db==null ){
                throw new Exception ("Imposible ejecutar el m�todo 'getLastInsertId', no existe conexi�n a la base de datos", -500);
		    }
            
            return $this->db->lastInsertId();
        }
		catch (Exception $vexception){	
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
     }
	
	public function beginTransaction()
	 {
		try{
            if ( $this->db==null ){
                throw new Exception ("Imposible ejecutar el m�todo 'beginTransaction', no existe conexi�n a la base de datos", -500);
		    }
            
            return $this->db->beginTransaction();
        }
		catch (Exception $vexception){	
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	public function commitTransaction()
	 {
		try{
            if ( $this->db==null ){
                throw new Exception ("Imposible ejecutar el m�todo 'commitTransaction', no existe conexi�n a la base de datos", -500);
		    }
            
            return $this->db->commit();
        }
		catch (Exception $vexception){	
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	public function rollbackTransaction()
	 {
		try{
            if ( $this->db==null ){
                throw new Exception ("Imposible ejecutar el m�todo 'rollbackTransaction', no existe conexi�n a la base de datos", -500);
		    }
            
            return $this->db->rollBack();
        }
		catch (Exception $vexception){	
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
    
	public function __destruct()
	 {
		unset($this->dbms, $this->serverName, $this->dbName, $this->userName, $this->userPassword, $this->db, $this->pdoStatement);
	 }
 }
?>