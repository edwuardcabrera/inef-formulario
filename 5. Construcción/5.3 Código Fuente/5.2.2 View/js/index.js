/********************************************************
Name: index.js
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 21/05/2017
Modification date: 24/05/2017
Description: JS file. It provides support between view and controller files, in order to show the index page.
********************************************************/

$(document).ready(function(){
    //Purpose: It sets default options of the page WEB (index).
    //Limitations: The current page (view) must be fully loaded.  
    //Returns: None.
    
    menu(0, 0);
    showGeneralFooter();
});