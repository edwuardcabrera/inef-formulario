/********************************************************
Name: users-restore-password.js
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodr�guez
Modification autor name: Edwuard H. Cabrera Rodr�guez
Creation date: 09/11/2015
Modification date: 30/05/2017
Description: JS file. It provides support between view and controller files, in order to restore a user password.
********************************************************/

var vrootURL="./controller/users.php";
var vidPasswordReset="";

function _default(vidPasswordReset_)
 {  
    //Purpose: It sets default options of the page WEB (users-restore-password).
    //Limitations: The current page (view) must be fully loaded.  
    //Returns: None.
    
    vidPasswordReset=vidPasswordReset_;
    showLoginFooter();
    setControls();
    verifyPasswordReset();
 }

function setControls()
 {
    //Purpose: It sets controls of the page WEB (users-restore-password).
    //Limitations: The controls must exist in the page WEB.  
    //Returns: None.
    
    $("#btnlogin").attr("onclick", "location.href='./frmlogin.php'");
    
    $.backstretch(["./tools/template-3.2/global/images/gallery/login2.jpg"], {
        fade: 600,
        duration: 4000
    });
 }

function verifyPasswordReset()
 {
    //Purpose: It verifies a password reset.
    //Limitations: The password reset data must exist in database server and brought by service rest (passwordReset/{idPasswordReset}/users).  
    //Returns: None.
    
    if ( trim(vidPasswordReset)!="" ){
        $.ajax({
            type: "get",
            url: vrootURL + "/passwordReset/" + trim(vidPasswordReset) + "/users",
            dataType: "json",
            success: function(vresponse, vtextStatus, vjqXHR){
                switch(vresponse.messageNumber){
                    case -100: var vhtml ='<p>&nbsp;</p>'
                                   vhtml+='<p style="text-align:center" class="text-danger">';
                                   vhtml+='	<strong>Ocurri� un error al tratar de verificar la solicitud de restablecimiento de contrase�a, intente de nuevo.</strong>';
                                   vhtml+='</p>';
                                   vhtml+='<p>&nbsp;</p>';
                               $("#vpasswordResetContent").html(vhtml);  
                               break;
                    case -2:   var vhtml ='<p>&nbsp;</p>'
                                   vhtml+='<p style="text-align:center" class="text-danger">';
                                   vhtml+=' <strong>La solicitud de restablecimiento de contrase�a, ya fu� usada.</strong>';
                                   vhtml+='</p>';
                                   vhtml+='<p>&nbsp;</p>';
                               $("#vpasswordResetContent").html(vhtml);
                               break;
                    case -1:   var vhtml ='<p>&nbsp;</p>'
                                   vhtml+='<p style="text-align:center" class="text-danger">';
                                   vhtml+=' <strong>La solicitud de restablecimiento de contrase�a, se encuentra expirada.</strong>';
                                   vhtml+='</p>';
                                   vhtml+='<p>&nbsp;</p>';
                               $("#vpasswordResetContent").html(vhtml);  
                               break;
                    case 0:    var vhtml ='<p>&nbsp;</p>'
                                   vhtml+='<p style="text-align:center" class="text-danger">';
                                   vhtml+=' <strong>La solicitud de restablecimiento de contrase�a, no se encuentra registrada.</strong>';	
                                   vhtml+='</p>';
                                   vhtml+='<p>&nbsp;</p>';
                               $("#vpasswordResetContent").html(vhtml);
                               break;
                    case 1:   var vhtml ='<p class="text-center m-t-md">';
                                   vhtml+='	<span style="color:#ffffff"><strong>' + vresponse.passwordReset.user.emailAccount + '</strong></span>';
                                   vhtml+='</p>'; 
                                   vhtml+='<form id="vpasswordRestoreData" name="vpasswordRestoreData">';
                                   vhtml+='<div id="vpassword" class="form-group">';
                                   vhtml+=' <input type="password" id="txtpassword" name="txtpassword" maxlength="20" class="form-control input-rounded form-white" placeholder="Nueva contrase�a">';
                                   vhtml+='</div>';
                                   vhtml+='<div id="vconfirmPassword" class="form-group">';
                                   vhtml+=' <input type="password" id="txtconfirmPassword" name="txtconfirmPassword" maxlength="20" class="form-control input-rounded form-white" placeholder="Confirmar contrase�a">';
                                   vhtml+='</div>';
                                   vhtml+='</form>';
                                   vhtml+='<button type="button" id="btnrestorePassword" class="btn btn-lg btn-success btn-rounded btn-block ladda-button" data-style="expand-left" title="Reestablecer Contrase�a">';
                                   vhtml+=' <i class="fa fa-refresh m-r-xs"></i>&nbsp;&nbsp;Reestablecer Contrase�a';
                                   vhtml+='</button>';
                               $("#vpasswordResetContent").html(vhtml);
                               $("#btnrestorePassword").attr("onclick", "restorePassword();");
                               break;
                    default:   var vhtml ='<p>&nbsp;</p>'
                                   vhtml+='<p style="text-align:center" class="text-danger">';
                                   vhtml+='	<strong>Ocurri� un error al tratar de verificar la solicitud de restablecimiento de contrase�a.</strong>';
                                   vhtml+='</p>';
                                   vhtml+='<p>&nbsp;</p>';
                               $("#vpasswordResetContent").html(vhtml);
                               break;
                }
            },
            error: function(vjqXHR, vtextStatus, verrorThrown){
                toastrAlert(verrorThrown, "Verificaci�n de restablecimiento de contrase�a.", 0);
            }
        });
    }
    else{
        var vhtml ='<p>&nbsp;</p>'
            vhtml+='<p style="text-align:center" class="text-danger">';
            vhtml+='	<strong>Imposible reestablecer una contrase�a, no hay solicitud establecida.</strong>';
            vhtml+='</p>';
            vhtml+='<p>&nbsp;</p>';
            $("#vpasswordResetContent").html(vhtml);
    }
 }

function restorePassword()
 {
    //Purpose: It restore user password data.
    //Limitations: The user data must be exist in database server and brought by service rest (passwordReset/{idPasswordReset}/users).  
    //Returns: None.
    
    var vjson=$("#vpasswordRestoreData").serializeObject();
    var vtitle="Reestablecimiento de Contrase�a.";
    
    if ( validatePasswordResetFormFields() ){
        $.ajax({
            type: "put",
            url: vrootURL + "/passwordReset/" + trim(vidPasswordReset) + "/users",
            dataType: "json",
            data: JSON.stringify(vjson),
            success: function(vresponse, vtextStatus, vjqXHR){
                switch(vresponse.messageNumber){
                    case -100: toastrAlert("Ocurri� un error al tratar de reestablecer la contrase�a, intente de nuevo.", vtitle, 0);
                               break;
                    case -4:   toastrAlert("No se ha reestablecido la contrase�a, intente de nuevo (1).", vtitle, 3);
                               break;
                    case -3:   toastrAlert("No se ha reestablecido la contrase�a, intente de nuevo (2).", vtitle, 3);
                               break;
                    case -2:   toastrAlert("No se ha reestablecido la contrase�a, intente de nuevo (3).", vtitle, 3);
                               break;
                    case -1:   toastrAlert("No se ha reestablecido la contrase�a, la solicitud ya fu� usada.", vtitle, 3);
                               break;
                    case 0:    toastrAlert("No se ha reestablecido la contrase�a, la solicitud ha expirado.", vtitle, 3);
                               break;
                    case 1:    toastrAlert("La contrase�a ha sido reestablecida correctamente. <br />Para ingresar haga clic en el bot�n 'login'.", vtitle, 1);
                               break;
                    default:   toastrAlert("Ocurri� un error al tratar de reestablecer la contrase�a.", vtitle, 0);
                               break;
                }
            },
            error: function(vjqXHR, vtextStatus, verrorThrown){
                toastrAlert(verrorThrown, vtitle, 0);
            }
        });
    }
    else{
        toastrAlert("El reestablecimiento de la contrase�a no puede ser realizado, no tiene los datos necesarios, complete los datos.", vtitle, 3);
    }
 }
 
function validatePasswordResetFormFields()
 {
    //Purpose: It validates password reset form fields.
    //Limitations: The password reset form fields must exist.
    //Returns: None.
    
	vstatus=true;
	
    clearPasswordResetFormFields();
	if ( trim($("#txtpassword").val()) == "" ){
        $("#vpassword").addClass("has-warning");
		$("#txtpassword").focus();
		vstatus=false;
	}
	else if ( trim($("#txtconfirmPassword").val()) == "" ){
        $("#vconfirmPassword").addClass("has-warning");
		$("#txtconfirmPassword").focus();
		vstatus=false;
	}
	else if( trim($("#txtconfirmPassword").val()) != trim($("#txtpassword").val()) ){
	   toastrAlert("La confirmaci�n de la contrase�a no corresponde a la contrase�a.", "Reestablecimiento de Contrase�a.", 3);
       $("#vconfirmPassword").addClass("has-warning");
       $("#txtconfirmPassword").focus();
       vstatus=false;
	}
	
	return vstatus;
 }
 
function clearPasswordResetFormFields()
 {
    //Purpose: It clears password reset form fields.
    //Limitations: The class "has-warning" must exist in password reset form fields.
    //Returns: None.
    
    if ( $("#vpassword").hasClass("has-warning") ){
        $("#vpassword").removeClass("has-warning");
    }
    if ( $("#vconfirmPassword").hasClass("has-warning") ){
        $("#vconfirmPassword").removeClass("has-warning");
    }
 }