/********************************************************
Name: emails-record.js
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodr�guez
Modification autor name: Edwuard H. Cabrera Rodr�guez
Creation date: 02/06/2017
Modification date: 11/07/2017
Description: JS file. It provides support between view and controller files, in order to record emails.
********************************************************/

var vrootURL = "./controller/emails.php";
var vusersURL = "./controller/users.php";
var vcampusesURL = "./controller/campuses.php";
var vidCampus=0;
var vidEmailMarketing="000000-00";
var vidUserType=0;
var vaccessModule=[false, false, false, false];
var vattachedFiles=[];
var vcmbEmailMarketingTypeList;
var vcmbCampusesList;
var vcmbEducationalLevelsList;
var vbtnAttachFile;
var vconfirmDeleteAttachedFile;
var vconfirmSendEmail;

function _default(vidCampus_, vidEmailMarketing_)
 {  
    //Purpose: It sets default options of the page WEB (emails-record).
    //Limitations: The current page (view) must be fully loaded.  
    //Returns: None.
    
    vidCampus=vidCampus_;
    vidEmailMarketing=vidEmailMarketing_;
    menu(12, 0);
    verifyAccessPrivileges();
       
 }

function verifyAccessPrivileges()
 {
	//Purpose: It verifies access privileges (end user) in order to set controls of the page WEB (emails-record).
    //Limitations: The user access privileges data must exist in database server and brought by service rest (users/{idUser}/accessPrivileges).   
    //Returns: None.
    
    var verrorMessage="";
    $.ajax({
		type: "get",
		url: vusersURL + "/users/0/accessPrivileges",
		dataType: "json",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: verrorMessage ="Ocurri� un error al tratar de obtener sus privilegios de acceso.<br />";
                           verrorMessage+="Intente de nuevo.";
                           openError(verrorMessage);
                           break;
                case 0:    verrorMessage ="No existen privilegios de acceso registrados.<br />";
                           verrorMessage+="Verifiquelo con el Administrador del Sistema.";
                           openError(verrorMessage);
                           break;
                case 1:    if ( vresponse[1].backendUserAccessPrivileges.length>0 ){
                                setControls(vresponse[1].backendUserAccessPrivileges);
                           }
                           else{
                                verrorMessage ="No tiene privilegios de acceso registrados.<br />";
                                verrorMessage+="Verifiquelo con el Administrador del Sistema.";
                                openError(verrorMessage);
                           }
                           break;
                default:   verrorMessage ="Ocurri� un error al tratar de obtener sus privilegios de acceso.";
                           openError(verrorMessage);
                           break;
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            verrorMessage=verrorThrown + "<br />";
            verrorMessage+="Verifiquelo con el Administrador del Sistema.";
            openError(verrorMessage);
		}
	});
 }

function setControls(vbackendUserAccessPrivileges)
 {
    //Purpose: It sets controls of the page WEB (emails-record).
    //Limitations: The controls must exist in the page WEB (emails-record).  
    //Returns: None.
    
    for(vaccessPrivilegeNumber=0; vaccessPrivilegeNumber<vbackendUserAccessPrivileges.length; vaccessPrivilegeNumber++){
        switch( vbackendUserAccessPrivileges[vaccessPrivilegeNumber].accessPrivilege.idAccessPrivilege ){
            case 16:  vaccessModule[0]=true;
                      vaccessModule[1]=true;
                      break;
            case 19:  vaccessModule[0]=true;
                      vaccessModule[2]=true;
                      break;
            case 15:  vaccessModule[3]=true;
                      break;
        }
    }
    if ( ! vaccessModule[0] ){
        verrorMessage ="Acceso Denegado.<br />";
        verrorMessage+="No tiene privilegios para acceder al m�dulo de registro de correos electr�nicos.";
        openError(verrorMessage);
    }
    
    $("#txtSubject").attr("onkeypress", "return setEntriesType(event, 'letter');");
    vidUserType=vbackendUserAccessPrivileges[0].backendUser.userType.idUserType;
    if ( vidEmailMarketing=="000000-00" ){
        setEmailMarketingTypesList(0);    
        if (vidUserType==1){
            $("#vaddressee").html(getAddresseeControls(1));
            setCampusesList(0);
        }
        else{
            $("#vaddressee").html(getAddresseeControls(2));
            vidCampus=vbackendUserAccessPrivileges[0].backendUser.campus.idCampus;
            setEducationalLevelList(vidCampus, 0);
        }
        $("#vothers").html(getOthersControls(2));
        setAttachFile();
    }
    else{
        $("#vpageTitle").html("Modificaci�n - Email");
		showEmailData();
    }
    
    $("#btnbackToEmails").attr("onclick", "location.href='./frmemails.php';");
    if ( vaccessModule[2] ){
        $("#btnnewEmail").prop("disabled", false);
        $("#btnnewEmail").attr("onclick", "newEmailData();");
    }
    if ( vaccessModule[1] || vaccessModule[2] ){
        $("#btnsaveEmailData").prop("disabled", false);
        $("#btnsaveEmailData").attr("onclick", "recordEmailData();");
    }
 }

function setEmailMarketingTypesList(vid)
 {
    //Purpose: It sets email marketing types list of the page WEB (emails-record).
    //Limitations: The email marketing types data must exist in database server and brought by service rest (emailMarketingTypes).
    //Returns: None.
    
    var vtitle="Listado de Tipos de Correos Electr�nicos.";
    
    $.ajax({
		type: "get",
		url: vrootURL + "/emailMarketingTypes",
		dataType: "json",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurri� un error al tratar de mostrar los tipos de correos electr�nicos, intente de nuevo.", vtitle, 0);
                           break;
                case 0:    toastrAlert("No existen tipos de correos electr�nicos registrados.", vtitle, 3);
                           break;
                case 1:    fillEmailMarketingTypesList(vresponse.emailMarketingTypes.emailMarketingTypes, vid);
                           break;
                default:   toastrAlert("Ocurri� un error al tratar de mostrar los tipos de correos electr�nicos.", vtitle, 0);
                           break;  
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
 }

function setCampusesList(vid)
 {
    //Purpose: It sets campuses list of the page WEB (emails-record).
    //Limitations: The campuses data must exist in database server and brought by service rest (campuses).
    //Returns: None.
    
    var vtitle="Listado de Campus.";
    
    $.ajax({
		type: "get",
		url: vcampusesURL + "/campuses",
		dataType: "json",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurri� un error al tratar de mostrar los campus, intente de nuevo.", vtitle, 0);
                           break;
                case 0:    toastrAlert("No existen campus registrados.", vtitle, 3);
                           break;
                case 1:    fillCampusesList(vresponse.campuses.campuses, vid);
                           break;
                default:   toastrAlert("Ocurri� un error al tratar de mostrar los campus.", vtitle, 0);
                           break; 
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
 }

function setEducationalLevelList(vidCampus, vid)
 {
    //Purpose: It sets educational levels list of the page WEB (emails-record).
    //Limitations: The educational levels data must exist in database server and brought by service rest (campuses/{idCampus}/educationalLevels).
    //Returns: None.
    
    var vtitle="Listado de niveles educativos.";
    
    $.ajax({
		type: "get",
		url: vcampusesURL + "/campuses/" + vidCampus + "/educationalLevels",
		dataType: "json",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurri� un error al tratar de mostrar los niveles educativos, intente de nuevo.", vtitle, 0);
                           break;
                case 0:    toastrAlert("No existen niveles educativos registrados.", vtitle, 3);
                           break;
                case 1:    fillEducationalLevelList(vresponse.assignments.assignments, vid);
                           break;
                default:   toastrAlert("Ocurri� un error al tratar de mostrar los niveles educativos.", vtitle, 0);
                           break;
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
 }
 
function setEducationalOfferList(vidCampus, vidEducationalLevel, vid)
 {
    //Purpose: It sets educational offers list of the page WEB (emails-record).
    //Limitations: The educational offers data must exist in database server and brought by service rest (campuses/{idCampus}/educationalLevels/{idEducationalLevel}/educationalOffers).
    //Returns: None.
    
    var vtitle="Listado de ofertas educativas.";
    
    $.ajax({
		type: "get",
		url: vcampusesURL + "/campuses/" + vidCampus + "/educationalLevels/" + vidEducationalLevel + "/educationalOffers",
		dataType: "json",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurri� un error al tratar de mostrar las ofertas educativas, intente de nuevo.", vtitle, 0);
                           break;
                case 0:    toastrAlert("No existen ofertas educativas registradas.", vtitle, 3);
                           break;
                case 1:    fillEducationalOfferList(vresponse.educationalOffers.educationalOffers, vid);
                           break;
                default:   toastrAlert("Ocurri� un error al tratar de mostrar las ofertas educativas.", vtitle, 0);
                           break;
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
 }

function fillEmailMarketingTypesList(vemailMarketingTypes, vid)
 {
    //Purpose: It fills email marketing types list.
    //Limitations: The email marketing type control must exist in the page WEB and the email marketing types (data) must exist in format JSON.  
    //Returns: None.
    
    var vstringJSON='[{"id":-1, "text":"--Seleccionar--"}';
    for(vi=0; vi<vemailMarketingTypes.length; vi++){
        vstringJSON+=', {"id":' + vemailMarketingTypes[vi].idEmailMarketingType;
        vstringJSON+=',  "text":"' + vemailMarketingTypes[vi].emailMarketingType + '"}';
    }
    vstringJSON+="]";
    var vobjectJSON=jQuery.parseJSON(vstringJSON);
    
    vcmbEmailMarketingTypeList=$("#cmbemailMarketingType").select2({
        data:vobjectJSON,
        placeholder: {
            id: -1,
            text: "--Seleccionar--"
        },
        language:{
            noResults: function() {
                return "Sin tipos de correos electr�nicos encontrados.";
            }
        }, 
    });
    if ( vid!=0 ){
        $("#cmbemailMarketingType").select2("val", vid);
    }
    vcmbEmailMarketingTypeList.on("select2:select", function (e){
        if ( e.params.data.element.value==2 ){
            $("#vothers").html(getOthersControls(1));
            configOthersControls();
        }
        else{
            $("#vothers").html(getOthersControls(2));
        }
        showAttachedFilesList(1);
        setAttachFile();
    });
 }

function fillCampusesList(vcampuses, vid)
 {
    //Purpose: It fills campuses list.
    //Limitations: The campus control must exist in the page WEB and the campuses (data) must exist in format JSON.  
    //Returns: None.
    
    var vstringJSON='[{"id":-1, "text":"--Seleccionar--"}';
    for(vi=0; vi<vcampuses.length; vi++){
        vstringJSON+=', {"id":' + vcampuses[vi].idCampus;
        vstringJSON+=',  "text":"' + vcampuses[vi].campus + '"}';
    }
    vstringJSON+="]";
    var vobjectJSON=jQuery.parseJSON(vstringJSON);
    
    vcmbCampusesList=$("#cmbcampus").select2({
        data:vobjectJSON,
        placeholder: {
            id: -1,
            text: "--Seleccionar--"
        },
        language:{
            noResults: function() {
                return "Sin campus encontrados.";
            }
        }, 
    });
    if ( vid!=0 ){
        $("#cmbcampus").select2("val", vid);
    }
    else{
        fillEducationalLevelList({}, 0);
        $("#cmbeducationalLevel").prop("disabled", true);
    }
    vcmbCampusesList.on("select2:select", function (e){
        if ( e.params.data.element.value>=0 ){
            setEducationalLevelList(e.params.data.element.value, 0);
            $("#cmbeducationalLevel").prop("disabled", false);
        }
        else{
            fillEducationalLevelList({}, 0);
            $("#cmbeducationalLevel").prop("disabled", true);
        }
    });
 }

function fillEducationalLevelList(vassignments, vid)
 {
    //Purpose: It fills assignments list.
    //Limitations: The assigment control must exist in the page WEB and the assignments (data) must exist in format JSON.  
    //Returns: None.
    
    var vstringJSON='[{"id":-1, "text":"--Seleccionar--"}';
    for(vi=0; vi<vassignments.length; vi++){
        vstringJSON+=', {"id":' + vassignments[vi].educationalLevel.idEducationalLevel;
        vstringJSON+=',  "text":"' + vassignments[vi].educationalLevel.educationalLevel + '"}';
    }
    vstringJSON+="]";
    var vobjectJSON=jQuery.parseJSON(vstringJSON);
    
    $("#cmbeducationalLevel").empty();
    vcmbEducationalLevelsList=$("#cmbeducationalLevel").select2({
        data:vobjectJSON,
        placeholder: {
            id: -1,
            text: "--Seleccionar--"
        },
        language:{
            noResults: function() {
                return "Sin campus encontrados.";
            }
        }, 
    });
    if ( vid!=0 ){
        $("#cmbeducationalLevel").select2("val", vid);
    }
    else{
        fillEducationalOfferList({}, 0);
        $("#cmbeducationalOffer").prop("disabled", true);
    }
    vcmbEducationalLevelsList.on("select2:select", function (e){
        if ( e.params.data.element.value>=0 ){
            var vidCampus_=vidCampus;
            if ( vidUserType==1 ){
                vidCampus_=$("#cmbcampus").val();
            }
            setEducationalOfferList(vidCampus_, e.params.data.element.value, 0);
            $("#cmbeducationalOffer").prop("disabled", false);
        }
        else{
            fillEducationalOfferList({}, 0);
            $("#cmbeducationalOffer").prop("disabled", true);
        }
    });
 }

function fillEducationalOfferList(veducationalOffers, vid)
 {
    //Purpose: It fills educational offers list.
    //Limitations: The educational offer control must exist in the page WEB and the educationalOffers (data) must exist in format JSON.  
    //Returns: None.
    
    var vstringJSON='[{"id":0, "text":"--Todas--"}';
    for(vi=0; vi<veducationalOffers.length; vi++){
        vstringJSON+=', {"id":' + veducationalOffers[vi].idEducationalOffer;
        vstringJSON+=',  "text":"' + veducationalOffers[vi].educationalOffer + '"}';
    }
    vstringJSON+="]";
    var vobjectJSON=jQuery.parseJSON(vstringJSON);
    
    $("#cmbeducationalOffer").empty();
    $("#cmbeducationalOffer").select2({
        data:vobjectJSON,
        placeholder: {
            id: -1,
            text: "--Seleccionar--"
        },
        language:{
            noResults: function() {
                return "Sin ofertas educativas encontradas.";
            }
        }, 
    });
    if ( vid!=0 ){
        $("#cmbeducationalOffer").select2("val", vid);
    }
 }

function showEmailData()
 {
    //Purpose: It shows email marketing data.
    //Limitations: The email marketing data must exist in database server and brought by service rest (campuses/{idCampus}/emailMaketings/{idEmailMarketing}).
    //Returns: None.
    
    var vtitle="Visualizaci�n de Datos de Correo Electr�nico.";
    
    $.ajax({
		type: "get",
		url: vcampusesURL + "/campuses/" + vidCampus + '/emailMarketings/' + vidEmailMarketing,
		dataType: "json",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurri� un error al tratar de mostrar los datos del correo electr�nico, intente de nuevo.", vtitle, 0);
                           break;
                case 0:    toastrAlert("Los datos del correo electr�nico no se encuentran registrados.", vtitle, 3);
                           break;
                case 1:    setEmailDataToFormControls(vresponse.emailMarketing, vresponse.emailMarketingFiles.emailMarketingFiles, vresponse.emailMarketingScheduled);
                           break;
                default:   toastrAlert("Ocurri� un error al tratar de mostrar los datos del correo electr�nico.", vtitle, 0);
                           break;
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
 }

function setEmailDataToFormControls(vemailMarketing, vemailMarketingFiles, vemailMarketingScheduled)
 {
    //Purpose: It sets email marketing data to form controls.
    //Limitations: The email marketing data must be in JSON format.  
    //Returns: None.
    
    var vattachmentType=1;
    if ( (vemailMarketing.emailMarketingType.idEmailMarketingType==1) && (vemailMarketing.emailMarketingStatus.idEmailMarketingStatus==2) ){
        if ( vaccessModule[3] ){
            $("#vsendEmail").html(getSendEmailControls());
            $("#btnsendEmail").attr("onclick", "sendEmail();");
        }
    }
    else if( (vemailMarketing.emailMarketingStatus.idEmailMarketingStatus==1) || (vemailMarketing.emailMarketingStatus.idEmailMarketingStatus==3) ){
        $("#btnsaveEmailData").prop("disabled", true);
        vattachmentType=2;
    }
    
    setEmailMarketingTypesList(vemailMarketing.emailMarketingType.idEmailMarketingType);
    $("#cmbemailMarketingType").prop("disabled", true);
    $("#txtsubject").val(vemailMarketing.subject);
    if (vidUserType==1){
        $("#vaddressee").html(getAddresseeControls(1));
        setCampusesList(vemailMarketing.campus.idCampus);
        $("#cmbcampus").prop("disabled", true);
    }
    else{
        $("#vaddressee").html(getAddresseeControls(2));
    }
    setEducationalLevelList(vemailMarketing.campus.idCampus, vemailMarketing.educationalOffer.assignment.educationalLevel.idEducationalLevel);
    setEducationalOfferList(vemailMarketing.campus.idCampus, vemailMarketing.educationalOffer.assignment.educationalLevel.idEducationalLevel, vemailMarketing.educationalOffer.idEducationalOffer);    
    
    if ( vemailMarketing.emailMarketingType.idEmailMarketingType==2 ){
        $("#vothers").html(getOthersControls(1));
        configOthersControls();
        $("#txtdateScheduled").val(vemailMarketingScheduled.scheduledDate.substr(0, 10));
        $("#txttimeScheduled").val(vemailMarketingScheduled.scheduledDate.substr(11, 5));
    }
    else{
        $("#vothers").html(getOthersControls(2));
    }
    if ( vattachmentType==2 ){
        $("#btnattachFile").prop("disabled", true);
    }
    setAttachFile();
    
    if ( vemailMarketingFiles.length>=1 ){
        for (var vi=0; vi<vemailMarketingFiles.length; vi++){
            var vattachedFile=[];
                vattachedFile[0]=vemailMarketingFiles[vi].realName;
                vattachedFile[1]=vemailMarketingFiles[vi].fisicName; 
                vattachedFiles.push(vattachedFile);
        } 
        showAttachedFilesList(vattachmentType);
    }
    $("#txtmessage").val(vemailMarketing.message);
 }

function setAttachFile()
 {
	//Purpose: It sets attach file in server.
	//Limitations: none.
	//Returns: None.
	
    var vtitle="Adjuntar Archivo.";
	var vurl='./controller/upload-file.php';
	var vparams= new Array("/others/attachments/");    
	
    $(document).ready(function(){		
        var vbutton = document.getElementById("btnattachFile").name;
        new AjaxUpload(vbutton, {
            action: vurl,
            data: {
                vuploadDirectory: vparams[0]
            },
            onSubmit: function(vfile , vext){
                $("#vattachStatus").html("<i class='fa fa-spinner faa-spin animated'></i>&nbsp;Cargando...</span>"); 
            },
            onComplete: function(vfile, vresponse){
                $("#vattachStatus").html("");
                switch(vresponse){
                    case -100: toastrAlert("Ocurri� un error al tratar de cargar el archivo, intente de nuevo.", vtitle, 0);
                               break;
                    case 0:    toastrAlert("Ocurri� un error al tratar de renombrar el nombre del archivo, intente de nuevo.", vtitle, 3);
                               break;
                    default:   var vattachedFile=[];
                               vattachedFile[0]=vfile;
                               vattachedFile[1]=vresponse; 
                               vattachedFiles.push(vattachedFile); 
                               showAttachedFilesList(1);
                               toastrAlert("Se cargo correctamente el archivo.", vtitle, 1);
                               break;
                }
            },
            onError: function (){
                $("#vattachStatus").removeClass("fa fa-spinner faa-spin animated");
            }
        });
    });
 }

function showAttachedFilesList(vtype)
 {
    var vhtml="";
    for (var vi=0; vi<vattachedFiles.length; vi++){
        vhtml+='<span class="label label-primary">' + vattachedFiles[vi][0] + '</span>';
        if ( vtype==1 ){
            vhtml+='<span style="cursor:pointer" onclick="deleteAttachedFile(\'' + vattachedFiles[vi][1] + '\');" class="label label-success">';
            vhtml+='	<strong>X</strong>';
            vhtml+='</span>';
        }
        vhtml+='&nbsp;&nbsp;&nbsp;';
    }
    vhtml+='<span id="vattachStatus">&nbsp;</span>';
    $("#vfileNames").html(vhtml);
 }

function sendEmailData()
 {
    //Purpose: It send email marketing data.
    //Limitations: The email marketing data must exist in database server and brought by service rest (campuses/{idCampus}/educationalLevels/{idEducationalLevel}/emailMarketings/{idEmailMarketing}).
    //Returns: None.
    
    var vjson={};
    var vproperty="idCampus";
    vjson[vproperty]=vidCampus;
    var vtitle="Env�o de Correo Electr�nico.";
    
    vconfirmSendEmail.remove();
    toastr.clear(vconfirmSendEmail, { force: true });
    
    $.ajax({
		type: "put",
		url: vrootURL + "/emailMarketings/" + vidEmailMarketing,
		dataType: "json",
        data: JSON.stringify(vjson),
        beforeSend: function(vjqXHR, vplainObject){
            $("#vsendingEmailMarketingStatus").html("<i class='fa fa-spinner faa-spin animated'></i>&nbsp;Enviando...</span>");
        },
        success: function(vresponse, vtextStatus, vjqXHR){
            $("#vsendingEmailMarketingStatus").html("");
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurri� un error al tratar de enviar el correo electr�nico, intente de nuevo.", vtitle, 0);
                           break;
                case -7:   toastrAlert("Imposible enviar el correo electr�nico, no se pudo actualizar su estado a enviado.", vtitle, 3);
                           break;
                case -6:   toastrAlert("Imposible enviar el correo electr�nico, no se pudo registrar los datos de env�o.", vtitle, 3);
                           break;
                case -5:   toastrAlert("Imposible enviar el correo electr�nico, intente de nuevo.", vtitle, 3);
                           break;
                case -4:   toastrAlert("Imposible enviar el correo electr�nico, no existen usuarios que hayan solicitado planes de estudio para el nivel educativo indicado.", vtitle, 3);
                           break;
                case -3:   toastrAlert("Imposible enviar el correo electr�nico, no se encuentra configurado los datos del correo de salida.", vtitle, 3);
                           break;
                case -2:   toastrAlert("Imposible enviar el correo electr�nico, se encuentra cancelado.", vtitle, 3);
                           break;
                case -1:   toastrAlert("Imposible enviar el correo electr�nico, ya se encuentra enviado.", vtitle, 3);
                           break;
                case 0:    toastrAlert("Imposible enviar el correo electr�nico, no se encuentra registrado.", vtitle, 3);
                           break;
                case 1:    toastrAlert("El correo electr�nico ha sido enviado correctamente.", vtitle, 1);
                           $("#vsendEmail").html("");
                           $("#btnsaveEmailData").prop("disabled", true);
                           $("#btnattachFile").prop("disabled", true);
                           showAttachedFilesList(2);                           
                           break;
                default:   toastrAlert("Ocurri� un error al tratar de enviar el correo electr�nico.", vtitle, 0);
                           break;
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            $("#vsendingEmailMarketingStatus").html("");
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
 }

function newEmailData()
 {
    //Purpose: It enables form fields controls in order to add a new email marketing.
    //Limitations: The main page WEB must exist.  
    //Returns: None.
    
    vidEmailMarketing="000000-00";
    $("#vpageTitle").html("Registro - Email");
    cleanEmailFormFields();
    $("#cmbemailMarketingType").prop("disabled", false);
    if ( vidUserType==1 ){
        $("#cmbcampus").prop("disabled", false);
    }
    $("#btnsaveEmailData").prop("disabled", false);
    $("#btnattachFile").prop("disabled", false);
    setAttachFile();
    $("#txtsubject").focus();
    $("#scroll-to-top").click();
 }
 
function cleanEmailFormFields()
 {
    //Purpose: It cleans email marketing form fields.
    //Limitations: The email marketing form fields must exist.
    //Returns: None.
    
    clearEmailFormFields();
    $("#vsendEmail").html("");
    $("#cmbemailMarketingType").select2("val", -1);
    $("#txtsubject").val("");
    if ( vidUserType==1 ){
        $("#cmbcampus").select2("val", -1);
        fillEducationalLevelList({}, 0);
        $("#cmbeducationalLevel").prop("disabled", true);
    }
    $("#cmbeducationalLevel").select2("val", -1);
    fillEducationalOfferList({}, 0);
    $("#cmbeducationalOffer").prop("disabled", true);
    $("#cmbeducationalOffer").select2("val", 0);    
    vattachedFiles=[];
    $("#vothers").html(getOthersControls(2));
    $("#txtmessage").val("");
 }

function recordEmailData()
 {
    //Purpose: It records (add/update) emails data.
    //Limitations: The page WEB controls must have data.  
    //Returns: None.
    
    if ( vidEmailMarketing=="000000-00" ){
        if( validateEmailFormFields(1) ){
            addEmailMarketingData();
        }
		else{
            toastrAlert("El registro de los datos del correo electr�nico no puede ser realizado, no tiene los datos necesarios, complete los datos.", "Registro de Datos del Correo Electr�nico.", 3);
        }
    }
	else{
        if( validateEmailFormFields(2) ){
            updateEmailMarketingData();
        }
        else{
            toastrAlert("La modificaci�n de los datos del correo electr�nico no puede ser realizada, no tiene los datos necesarios, complete los datos.", "Modificaci�n de Datos del Correo Electr�nico..", 3);
        }
    }
 }

function addEmailMarketingData()
 {
    //Purpose: It adds email marketing data.
    //Limitations: The email marketing data must not exist in database server and brought by service rest (emailMarketings).  
    //Returns: None.
    
    var vjson=$("#vemailData").serializeObject();
    var vproperty="cmbemailMarketingType";
    vjson[vproperty]=trim($("#cmbemailMarketingType").val());
    if ( vidUserType==1 ){
        vproperty="cmbcampus";
        vjson[vproperty]=trim($("#cmbcampus").val());
    }
    vproperty="cmbeducationalLevel";
    vjson[vproperty]=trim($("#cmbeducationalLevel").val());
    vproperty="cmbeducationalOffer";
    vjson[vproperty]=trim($("#cmbeducationalOffer").val());
    vproperty="attachedFiles";
    vjson[vproperty]=vattachedFiles;
    
    var vtitle="Registro de Datos del Correo Electr�nico.";
            
    $.ajax({
		type: "post",
		url: vrootURL + "/emailMarketings",
		dataType: "json",
        data: JSON.stringify(vjson),
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurri� un error al tratar de registrar el correo electr�nico, intente de nuevo.", vtitle, 0);
                           break;
                case -2:   toastrAlert("Imposible registrar el correo electr�nico, no se puede(n) registrar el(los) archivo(s) adjunto(s).", vtitle, 3);
                           break;
                case -1:   toastrAlert("Imposible registrar el correo electr�nico, no se puede registrar la fecha programada de env�o.", vtitle, 3);
                           break;
                case 0:    toastrAlert("Imposible registrar el correo electr�nico, intente de nuevo.", vtitle, 3);
                           break;     
                case 1:    vidEmailMarketing=vresponse.idEmailMarketing;
                           vidCampus=vresponse.idCampus; 
                           $("#vpageTitle").html("Modificaci�n - Email");
                           $("#cmbemailMarketingType").prop("disabled", true);
                           if ( vidUserType==1 ){
                                $("#cmbcampus").prop("disabled", true);
                           }
                           clearEmailFormFields();
                           if ( ($("#cmbemailMarketingType").val()==1) && (vaccessModule[3]) ){
                                sendEmail();
                           }
                           toastrAlert("El correo electr�nico ha sido registrado correctamente.", vtitle, 1); 
                           break;
                default:   toastrAlert("Ocurri� un error al tratar de registrar el correo electr�nico.", vtitle, 0);
                           break;
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
 }

function updateEmailMarketingData()
 {
    //Purpose: It updates email marketing data.
    //Limitations: The email marketing data must be exist in database server and brought by service rest (campuses/{vidCampus}/emailMarketings/{vidEmailMarketing}).  
    //Returns: None.
    
    var vjson=$("#vemailData").serializeObject();
    var vproperty="cmbeducationalLevel";
    vjson[vproperty]=trim($("#cmbeducationalLevel").val());
    vproperty="cmbeducationalOffer";
    vjson[vproperty]=trim($("#cmbeducationalOffer").val());
    vproperty="attachedFiles";
    vjson[vproperty]=vattachedFiles;
    
    var vtitle="Modificaci�n de Datos del Correo Electr�nico.";
    
    $.ajax({
		type: "put",
		url: vcampusesURL + "/campuses/" + vidCampus + '/emailMarketings/' + vidEmailMarketing,
		dataType: "json",
        data: JSON.stringify(vjson),
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurri� un error al tratar de modificar los datos del correo electr�nico, intente de nuevo.", vtitle, 0);
                           break;
                case 0:    toastrAlert("Imposible modificar el correo electr�nico, no se puede(n) registrar el(los) archivo(s) adjuntado(s).", vtitle, 3);
                           break;
                case 1:    toastrAlert("Los datos del correo electr�nico han sido modificados correctamente.", vtitle, 1);
                           clearEmailFormFields();
                           break;
                default:   toastrAlert("Ocurri� un error al tratar de modificar los datos del correo electr�nico.", vtitle, 0);
                           break;
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
 }

function deleteAttachedFileData(vfileName)
 {
    //Purpose: It deletes attached file data.
    //Limitations: The attached file data must exist in database server and brought by service rest (attachFiles/:vfileName).
    //Returns: None.
    
    var vtitle="Eliminaci�n de Archivo Adjuntado.";
    vconfirmDeleteAttachedFile.remove();
    toastr.clear(vconfirmDeleteAttachedFile, { force: true });
    
	$.ajax({
	   type: 'delete',
        url: vrootURL + "/attachFiles/" + vfileName,
		dataType: "json",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurri� un error al tratar de eliminar el archivo adjuntado, intente de nuevo.", vtitle, 0);
                           break;
                case -1:   toastrAlert("Imposible eliminar el archivo adjuntado, no existe.", vtitle, 3);
                           break;     
                case 0:    toastrAlert("Imposible eliminar el archivo, intente de nuevo.", vtitle, 3);
                           break;
                case 1:    var vattachedFileIndex=getAttachedFileIndexOnList(vfileName);
                           if ( vattachedFileIndex>=0 ){
                                vattachedFiles.splice(vattachedFileIndex, 1);
                           }
                           showAttachedFilesList(1);
                           toastrAlert("El archivo adjuntado ha sido eliminado correctamente.", vtitle, 1);
                           break;
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});    
 }

function sendEmail()
 {
    //Purpose: It sends email marketing.
    //Limitations: The end user must accept the email sending.
    //Returns: None.
    
    var vacceptFunctionName="sendEmailData();";
    var vcancelFunctionName="cancelSendEmail();";
    var vmessage="�Desea enviar el correo electr�nico?.";
    var vtitle="Env�o de Correo Electr�nico.";
    
    vconfirmSendEmail=toastrConfirm(vacceptFunctionName, vcancelFunctionName, vmessage, vtitle);
 }

function deleteAttachedFile(vfileName)
 {
    //Purpose: It deletes attached file.
    //Limitations: The end user must accept the deleting attached file.
    //Returns: None.
    
    var vacceptFunctionName="deleteAttachedFileData('" + vfileName + "');";
    var vcancelFunctionName="cancelAttachedFileRemoval();";
    var vmessage="�Est� seguro que desea eliminar el archivo adjuntado?.";
    var vtitle="Eliminaci�n de Archivo Adjuntado.";
    
    vconfirmDeleteAttachedFile=toastrConfirm(vacceptFunctionName, vcancelFunctionName, vmessage, vtitle);
 }

function cancelSendEmail()
 {
    //Purpose: It sends emai marketing.
    //Limitations: None.
    //Returns: None.
    
    $("#vsendEmail").html(getSendEmailControls());
    $("#btnsendEmail").attr("onclick", "sendEmail();");
    vconfirmSendEmail.remove();
    toastr.clear(vconfirmSendEmail, { force: true });
    toastrAlert("El env�o del correo electr�nico ha sido cancelado.", "Env�o de Correo Electr�nico.", 2);
 }

function cancelAttachedFileRemoval()
 {
    //Purpose: It cancels deleting attached file.
    //Limitations: None.
    //Returns: None.
    
    vconfirmDeleteAttachedFile.remove();
    toastr.clear(vconfirmDeleteAttachedFile, { force: true });
    toastrAlert("La eliminaci�n del archivo adjuntado ha sido cancelado.", "Eliminaci�n de Archivo Adjuntado.", 2);
 }

function getAttachedFileIndexOnList(vfileName)
 {
    //Purpose: It gets attached file index on attached files list.
    //Limitations: None.
    //Returns: Attached file index.
    
    var vattachedFileIndex=-1;
    for (var vi=0; vi<vattachedFiles.length; vi++){
        if ( vfileName==vattachedFiles[vi][1] ){
            vattachedFileIndex=vi;
            vi=vattachedFiles.length;
        }
    }
    
    return vattachedFileIndex;
 }

function getAddresseeControls(vtype)
 {
    //Purpose: It gets addressee controls.
    //Limitations: None.
    //Returns: Addressee controls in HTML5 code.
    
    var vhtml="";
    var vclass="col-md-4 form-group";
    if (vtype==1){
        vhtml+='<div id="vcampus" class="col-md-4 form-group">';
		vhtml+='	<label for="cmbcampus"><strong>Campus:</strong></label>';
		vhtml+='	<select id="cmbcampus" class="form-control form-white" tabindex="-1" style="display: none; width: 100%"></select>';
        vhtml+='</div>';
    }
    else if (vtype==2){
        vclass="col-md-6 form-group";
    }
    vhtml+='<div id="veducationalLevel" class="' + vclass + '">';
	vhtml+='	<label for="cmbeducationalLevel"><strong>Nivel Educativo:</strong></label>';
	vhtml+='	<select id="cmbeducationalLevel" class="form-control form-white" tabindex="-1" style="display: none; width: 100%"></select>';
    vhtml+='</div>';
    vhtml+='<div id="veducationalOffer" class="' + vclass + '">';
	vhtml+='	<label for="cmbeducationalOffer"><strong>Oferta Educativa:</strong></label>';
	vhtml+='	<select id="cmbeducationalOffer" class="form-control form-white" tabindex="-1" style="display: none; width: 100%"></select>';
    vhtml+='</div>';
        
    return vhtml;
 }

function getOthersControls(vtype)
 {
    //Purpose: It gets others controls.
    //Limitations: None.
    //Returns: Others controls in HTML5 code.
    
    var vhtml ="";
    var vwidth="col-md-10";
    
    if( vtype==1 ){
        vhtml+='<div id="vdateSchedule" class="col-md-2 form-group">';
        vhtml+='	<label for="txtdateScheduled"><strong>Fecha de Env�o:</strong></label>';
        vhtml+='	<input type="text" id="txtdateScheduled" name="txtdateScheduled" class="b-datepicker form-control input-rounded form-white" data-lang="es" placeholder="Elija una Fecha">';
        vhtml+='</div>';
        vhtml+='<div id="vtimeSchedule" class="col-md-2 form-group">';
        vhtml+='	<label for="txttimeScheduled"><strong>Hora de Env�o:</strong></label>';
        vhtml+='	<input type="text" id="txttimeScheduled" name="txttimeScheduled" class="timepicker form-control input-rounded form-white" placeholder="Elija una Hora">';
        vhtml+='</div>';
        vwidth="col-md-6";
    }
    vhtml+='<div class="col-md-2">';
	vhtml+='	<button type="button" id="btnattachFile" name="btnattachFile" class="btn btn-success m-b-sm btn-rounded btn-md" title="Adjuntar Archivo">';
	vhtml+='		<i class="fa fa-paperclip m-r-xs"></i>&nbsp;&nbsp;&nbsp;Adjuntar';
	vhtml+='	</button>';
    vhtml+='</div>';
    vhtml+='<div id="vfileNames" class="' + vwidth + '">';
    vhtml+='	<span id="vattachStatus">&nbsp</span>';
    vhtml+='</div>';
    
    return vhtml;
 }
 
function getSendEmailControls()
 {
    var vhtml ='<button type="button" id="btnsendEmail" class="btn btn-success m-b-sm btn-rounded btn-md" title="Enviar Correo Electr�nico">';
        vhtml+='	<i class="fa fa-send m-r-xs"></i>&nbsp;&nbsp;&nbsp;Enviar';
        vhtml+='</button>';
        
    return vhtml;
 }

function configOthersControls()
 {
    //Purpose: It configs others controls.
    //Limitations: None.
    //Returns: None.
    
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '< Ant',
        nextText: 'Sig >',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Mi�rcoles', 'Jueves', 'Viernes', 'S�bado'],
        dayNamesShort: ['Dom','Lun','Mar','Mi�','Juv','Vie','S�b'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S�'],
        weekHeader: 'Sm',
        dateFormat: 'mm/dd/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
    $('.b-datepicker').datepicker();
    $('.timepicker').timepicker();
 }

function validateEmailFormFields(vtype)
 {
    //Purpose: It validates emails form fields.
    //Limitations: The email form fields must exist.
    //Returns: None.
    
    var vtitle="Registro de Datos del Email.";
	var vstatus=true;
    
    clearEmailFormFields();
    if ( ($("#cmbemailMarketingType").val() == -1) || (trim($("#cmbemailMarketingType").val())=="") ){
        $("#vemailMarketingType").addClass("has-warning");
        if ( vstatus ){
            $("#cmbemailMarketingType").focus();
        }
        vstatus=false;
    }
    if ( trim($("#txtsubject").val()) == "" ){
        $("#vsubject").addClass("has-warning");
        if ( vstatus ){
            $("#txtsubject").focus();
        }
        vstatus=false;
    }
    if ( (vidUserType==1) && (($("#cmbcampus").val() == -1) || (trim($("#cmbcampus").val())=="")) ){
        $("#vcampus").addClass("has-warning");
        if ( vstatus ){
            $("#cmbcampus").focus();
        }
        vstatus=false;
    }
    if ( ($("#cmbeducationalLevel").val() == -1) || (trim($("#cmbeducationalLevel").val())=="") ){
        $("#veducationalLevel").addClass("has-warning");
        if ( vstatus ){
            $("#cmbeducationalLevel").focus();
        }
        vstatus=false;
    }
    if ( ($("#cmbemailMarketingType").val()==2) && (trim($("#txtdateScheduled").val())=="") ){
        $("#vdateSchedule").addClass("has-warning");
        if ( vstatus ){
            $("#txtdateScheduled").focus();
        }
        vstatus=false;
    }
    else if ( ($("#cmbemailMarketingType").val()==2) && (! isDate(trim($("#txtdateScheduled").val()))) ){
        toastrAlert("Proporcione una Fecha de Env�o v�lida, formato: 'mes/dia/a�o'.", vtitle, 3);
        $("#vdateSchedule").addClass("has-warning");
        if ( vstatus ){
            $("#txtdateScheduled").focus();
        }
        vstatus=false;
    }
    if ( ($("#cmbemailMarketingType").val()==2) && (trim($("#txttimeScheduled").val())=="") ){
        $("#vtimeSchedule").addClass("has-warning");
        if ( vstatus ){
            $("#txttimeScheduled").focus();
        }
        vstatus=false;
    }
    else if ( ($("#cmbemailMarketingType").val()==2) && (! isTime(trim($("#txttimeScheduled").val()))) ){
        toastrAlert("Proporcione una Hora de Env�o v�lida, formato: 'hh:ss'.", vtitle, 3);
        $("#vtimeSchedule").addClass("has-warning");
        if ( vstatus ){
            $("#txttimeScheduled").focus();
        }
        vstatus=false;
    }
    if ( trim($("#txtmessage").val()) == "" ){
        toastrAlert("Proporcione el contenido (mensaje) del correo electr�nico.", vtitle, 3);
        vstatus=false;
    }
    
    return vstatus;
 }

function clearEmailFormFields()
 {
    //Purpose: It clears email form fields.
    //Limitations: The class "has-warning" must exist in email form fields.
    //Returns: None.
    
    if ( $("#vemailMarketingType").hasClass("has-warning") ){
        $("#vemailMarketingType").removeClass("has-warning");
    }
    if ( $("#vsubject").hasClass("has-warning") ){
        $("#vsubject").removeClass("has-warning");
    }
    if ( vidUserType==1 && $("#vcampus").hasClass("has-warning") ){
        $("#vcampus").removeClass("has-warning");
    }
    if ( $("#veducationalLevel").hasClass("has-warning") ){
        $("#veducationalLevel").removeClass("has-warning");
    }
    if ( $("#cmbemailMarketingType").val()==2 && $("#vdateSchedule").hasClass("has-warning") ){
        $("#vdateSchedule").removeClass("has-warning");
    }
    if ( $("#cmbemailMarketingType").val()==2 && $("#vtimeSchedule").hasClass("has-warning") ){
        $("#vtimeSchedule").removeClass("has-warning");
    }
 }