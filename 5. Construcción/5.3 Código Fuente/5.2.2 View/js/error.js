/********************************************************
Name: error.js
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 15/04/2016
Modification date: 24/05/2017
Description: JS file. It provides support in order to show the error page.
********************************************************/

function _default(vmessage)
 {  
    //Purpose: It sets message of the page WEB (error).
    //Limitations: The current page (view) must be fully loaded.  
    //Returns: None.
    
    showErrorFooter();
    
    $("#vmessage1").typed({
        strings: ["Error!"],
        typeSpeed: 200,
        backDelay: 500,
        loop: false,
        contentType: 'html',
        loopCount: false,
        callback: function() {
            $('.typed-cursor').css('-webkit-animation', 'none').animate({opacity: 0}, 400);
            $("#vmessage2").typed({
                strings: [vmessage],
                typeSpeed: 1,
                backDelay: 500,
                loop: false,
                contentType: 'html', 
                loopCount: false,
                callback: function() {
                    $('.typed-cursor').css('-webkit-animation', 'none').animate({opacity: 0}, 400);
                    $("#vmessage3").typed({
                        strings: ['Para ingresar al sistema debe de hacer click en <a href="./frmlogin.php"><strong>Login</strong></a> o en <a href="./"><strong>Inicio</strong></a> para ir a la pantalla principal del sistema.'],
                        typeSpeed: 1,
                        backDelay: 500,
                        loop: false,
                        contentType: 'html', 
                        loopCount: false,
                        callback: function() {
                            $('#content-404').delay(300).slideDown();
                        },
                    }); 
                }
            });  
        }
    });
 }