/********************************************************
 Name: records.js
 Autor name: Alejandro Hernández Guzmán
 Modification autor name: Edwuard H. Cabrera Rodríguez
 Creation date: 13/05/2017
 Modification date: 04/07/2017
 Description:JS file. It provides support between view and controller files, in order to user and applicants
 ********************************************************/

var curlFile="./others/career-plan/";
var  vcmbEducationalList;
var vidEducationalLevel=0;
var vidCampus=0;
$(document).ready(function () {
    //Purpose: It sets default options of the page WEB (userapplicants).
    //Limitations: The current page (view) must be fully loaded.  
    //Returns: None.
    
    $("#btnregistrar").attr("onclick", "Registrar()");
    $("#btndescargar").attr("onclick", "Descargar()");
    hide();
    setEducationalLevelList(0);
});

function Registrar()
 {
    //Purpose: It register new userapplicant.
    //Limitations: The applicant user can only be registered if it doesn't exists in the database.
    //Returns: EducationalOffer.
    
    var url = 'controller/users.php';
    var vtitle = "Descarga";
    var datosformulario = $("#ApplicantRegister").serializeObject();

    if (validateRecordFields()){
        $.ajax({
            type: 'POST',
            url: url + "/users",
            dataType: "JSON",
            data: JSON.stringify(datosformulario),
            success: function (vresponse){
                switch (vresponse.messageNumber){
                    case -100: toastrAlert("Ocurrió un error ,intente de nuevo.", vtitle, 0);
                               break;
                    case 0:    toastrAlert("Error al descargar,intente de nuevo", vtitle, 0);
                               break;
                    case -5:   toastrAlert("No envió correo,Archivo no encontrado", vtitle, 0);
                               break;
                    case -4:   toastrAlert("No se envio el correo", vtitle, 0);
                               break;           
                    case -3:   toastrAlert("Imposible descargar el plan de estudio, no se encuentra configurado los datos del correo de salida.", vtitle, 0);
                               break;
                    case -2:   toastrAlert("No existe pdf", vtitle, 0);
                               break;
                    case -1:   toastrAlert("No se realizo la solicitud", vtitle, 0);
                               break;
                    case 1:    
                               clean();
                               hide();
                              $("#btndescargar").show();
                              if(vresponse.educationalOffer.fileName==""){
                             toastrAlert("Imposible descargar el plan de estudio, no hay archivo.", vtitle, 0);
                            }
                             else{
                                 toastrAlert("Descarga de carrera", "Su descarga se realizó con éxito", 1);
                                 window.open(curlFile + vresponse.educationalOffer.fileName);       
                                     }
                               break;
                    default:   toastrAlert("Ocurri� un error al tratar de enviar los datos.", vtitle, 0);
                               break;
                }
            },
            error: function (vjqXHR, vtextStatus, verrorThrown){
                toastrAlert(verrorThrown, vtitle, 0);
            }
        });
    }
 }

function Descargar()
 {
    //Purpose: It download pdf.
    //Limitations: The applicant,If the applicant is registered you can download.
    //Returns: EducationalOffer.
    
    var url = 'controller/applicants.php';
    var vtitle = "Descarga de pdf";
    var emailApplicant = $("#txtemailAccount").val();
    var educationalLevel = $("#cmbeducationalLevel").val();
    var educationalOffer= $("#cmbeducationalOffer").val();
    var campus = $("#vcampus").val();
    if (validateLoginFields()) {
        $.ajax({
            type: 'POST',
            url: url + "/applicants",
            dataType: "JSON",
            data: "vemailAccount=" + emailApplicant + "&veducationalLevel=" + educationalLevel + "&vcampus=" + campus + "&veducationalOffer="+ educationalOffer,
            success: function (vresponse){
                switch (vresponse.messageNumber){
                    case -100:  toastrAlert("Ocurrió un error al descargar.", vtitle, 0);
                                break;
                    case -3:   toastrAlert("No envió correo,Archivo no encontrado", vtitle, 0);
                               break;
                    case -2:   toastrAlert("No se pudo realizar el envio de correo", vtitle, 0);
                               break;
                    case 0:     toastrAlert("Complete los datos por favor.", vtitle, 3);
                                show();
                                break;
                    case 1:     
                                clean();
                                clearfields();
                                if(vresponse.educationalOffer.fileName==""){
                                toastrAlert("Imposible descargar el plan de estudio, no hay archivo.", vtitle, 0);
                                  }
                                 else{
                                toastrAlert("Descarga de carrera", "Su descarga se realizó con éxito", 1);
                                window.open(curlFile + vresponse.educationalOffer.fileName);       
                                     }
                    break;
                    default:
                }
            },
            error: function (vjqXHR, vtextStatus, verrorThrown) {
                toastrAlert(verrorThrown, vtitle, 0);
            }

        });
    }
 }

function setEducationalLevelList(vid)
{
    //Purpose: It sets educational levels list of the page WEB (emails-record).
    //Limitations: The educational levels data must exist in database server and brought by service rest (campuses/{idCampus}/educationalLevels).
    //Returns: None.
    
    var vcampusesURL = "./controller/campuses.php";
    vidCampus = $("#vcampus").val();
    var vtitle = "Listado de niveles educativos.";

    $.ajax({
        type: "get",
        url: vcampusesURL + "/campuses/" + vidCampus + "/educationalLevels",
        dataType: "json",
        success: function (vresponse, vtextStatus, vjqXHR) {
            switch (vresponse.messageNumber) {
                case -100:  toastrAlert("Ocurrió un error al tratar de mostrar los niveles educativos, intente de nuevo.", vtitle, 0);
                            break;
                case 0:     toastrAlert("No existen niveles educativos registrados.", vtitle, 3);
                            break;
                case 1:     fillEducationalLevelList(vresponse.assignments.assignments,vid);
                    
                            break;
                default:    toastrAlert("Ocurrió un error al tratar de mostrar los niveles educativos.", vtitle, 0);
                            break;
            }
        },
        error: function (vjqXHR, vtextStatus, verrorThrown) {
            toastrAlert(verrorThrown, vtitle, 0);
        }
    });
 }
 
 function  setEducationalOffer(vidEducationalLevel,vid){
     //Purpose: It sets educationalOffer list of the page WEB (applicants).
    //Limitations: The educational Offer data must exist in database server and brought by service rest (campuses/{idCampus}/educationalLevels/{idEducationalLevel}/educationalOffers).
    //Returns: None.
    var vcampusesURL = "./controller/campuses.php";
  
    var vtitle="Listado de ofertas educativas.";
    
    $.ajax({
		type: "get",
		url: vcampusesURL + "/campuses/" + vidCampus + "/educationalLevels/"+vidEducationalLevel+"/educationalOffers",
		dataType: "json",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurriï¿½ un error al tratar de mostrar los niveles educativos, intente de nuevo.", vtitle, 0);
                           break;
                case 0:    toastrAlert("No existen niveles educativos registrados.", vtitle, 3);
                           break;
                case 1:    fillEducationalOffer(vresponse.educationalOffers.educationalOffers, vid);
                           break;
                default:   toastrAlert("Ocurriï¿½ un error al tratar de mostrar los niveles educativos.", vtitle, 0);
                           break;
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
     
 }

function fillEducationalLevelList(vassignments,vid)
 {
    //Purpose: It fills assignments list.
    //Limitations: The assigment control must exist in the page WEB and the assignments (data) must exist in format JSON.  
    //Returns: None.

    var vstringJSON = '[{"id":-1, "text":"--Seleccionar--"}';
    for (vi = 0; vi < vassignments.length; vi++) {
        vstringJSON += ', {"id":' + vassignments[vi].educationalLevel.idEducationalLevel;
        vstringJSON += ',  "text":"' + vassignments[vi].educationalLevel.educationalLevel + '"}';
    }
    vstringJSON += "]";
    var vobjectJSON = jQuery.parseJSON(vstringJSON);

    $("#cmbeducationalLevel").empty();
    vcmbEducationalList = $("#cmbeducationalLevel").select2({
        data: vobjectJSON,
        placeholder: {
            id: -1,
            text: "--Seleccionar--"
        },
        language: {
            noResults: function () {
                return "Sin campus encontrados.";
            }
        },
    });
    if(vid!=0){
        $("#cmbeducationalLevel").select2("val",vid);
    }
    fillEducationalOffer({},0);
    $("#cmbeducationalOffer").prop("disabled",true);
     vcmbEducationalList.on("select2:select",function(e){
        if( e.params.data.element.value >=0 ){
            setEducationalOffer(e.params.data.element.value,0);
            $("#cmbeducationalOffer").prop("disabled",false);
        } else{
            fillEducationalOffer({},0);
         $("#cmbeducationalOffer").prop("disabled",true);

        }
     });
    
 }
 
 function fillEducationalOffer(veducationalOffers,vid){
    //Purpose: It fills educationalOffers list.
    //Limitations: The assigment control must exist in the page WEB and the assignments (data) must exist in format JSON.  
    //Returns: None.
    
    var vstringJSON='[{"id":-1, "text":"--Seleccionar--"}';
    for(vi=0; vi<veducationalOffers.length; vi++){
        vstringJSON+=', {"id":' + veducationalOffers[vi].idEducationalOffer;
        vstringJSON+=',  "text":"' + veducationalOffers[vi].educationalOffer + '"}';
    }
    vstringJSON+="]";
    var vobjectJSON=jQuery.parseJSON(vstringJSON);
    
    $("#cmbeducationalOffer").empty();
    $("#cmbeducationalOffer").select2({
        data:vobjectJSON,
        placeholder: {
            id: -1,
            text: "--Seleccionar--"
        },
        language:{
            noResults: function() {
                return "Sin offertas encontrados.";
            }
        }, 
    });
    if ( vid!=0 ){
        $("#cmbeducationalOffer").select2("val", vid);
    }
     
 }
 
 

function hide()
 {
    //Purpose: It hide inputs fields
    //Limitations: The  fields must exist.
    //Returns: None.

    $('.oculto').hide();
   
 }
function show()
 {
    //Purpose: It show inputs fields.
    //Limitations: The  fields must exist.
    //Returns: None.
    
    $('.oculto').show();
    $('#btndescargar').hide();
    $('#descarga').hide();
 }

function validateLoginFields()
 {
    //Purpose: It validates inputs fields.
    //Limitations: The  fields must exist.
    //Returns: None.
    
    var vstatus = true;
    clearfields();
    if (trim($("#txtemailAccount").val()) == ""){
        $("#vemailAccount").addClass("has-warning");
        if (vstatus){
            $("#vemailAccount").focus();
        }
        vstatus = false;
    }
    else if (!isEmail(trim($("#txtemailAccount").val()))){
        $("#vemailAccount").addClass("has-warning");
        if (vstatus){
            $("#vemailAccount").focus();
        }
        vstatus = false;
    }
    if (($("#cmbeducationalLevel").val() == -1) || trim($("#cmbeducationalLevel").val()) == ""){
        $("#veducationalLevel").addClass("has-warning");
        if (vstatus) {
            $("#cmbeducationalLevel").focus();
        }
        vstatus = false;
    }
    if (trim($("#txtemailAccount").val()) == "" || trim($("#cmbeducationalLevel").val()) == ""){
        toastrAlert("Campos requeridos", "Descarga", 3);

        vstatus = false;
    }
    return vstatus;
 }


function validateRecordFields()
 {
    //Purpose: It validates inputs fields.
    //Limitations: The fields must exist.
    //Returns: None.
    
    clearfields();
    var vstatus = true;

    if (trim($("#txtemailAccount").val()) == ""){
        $("#vemailAccount").addClass("has-warning");
        if (vstatus){
            $("#vemailAccount").focus();
        }
        vstatus = false;
    }
    else if (!isEmail(trim($("#txtemailAccount").val()))){
        toastrAlert("correo electrónico inválido.", "", 3);
        $("#vemailAccount").addClass("has-warning");
        if (vstatus){
            $("#vemailAccount").focus();
        }
        vstatus = false;
    }
    if (trim($("#txtname").val()) == ""){
        $("#vname").addClass("has-warning");
        if (vstatus){
            $("#txtname").focus();
        }
        vstatus = false;
    }
    if (trim($("#txtfirstName").val()) == ""){
        $("#vfirstName").addClass("has-warning");
        if (vstatus){
            $("#txtfirstName").focus();
        }
        vstatus = false;
    }
    if (trim($("#txtlastName").val()) == ""){
        $("#vlastName").addClass("has-warning");
        if (vstatus) {
            $("#txtlastName").focus();
        }
        vstatus = false;
    }
    if (trim($("#txttelefono").val()) == ""){
        $("#vtelefono").addClass("has-warning");
        if (vstatus) {
            $("#txttelefono").focus();
        }
        vstatus = false;
    } else if (!isInteger(trim($("#txttelefono").val()))){
        toastrAlert("Campo teléfono inválido", "Descarga pdf", 3);
        $("#txttelefono").focus();
        vstatus = false;
    }
    if (($("#cmbeducationalLevel").val() == -1) || trim($("#cmbeducationalLevel").val()) == ""){
        $("#veducationalLevel").addClass("has-warning");
        if (vstatus){
            $("#cmbeducationalLevel").focus();
        }
        vstatus = false;
    }
    
    return vstatus;
 }

function  clean()
 {
    //Purpose: It clean inputs fields.
    //Limitations: The  fields must exist.
    //Returns: None.
    
    $("#txtname").val("");
    $("#txtfirstName").val("");
    $("#txtlastName").val("");
    $("#txttelefono").val("");
    $("#txtemailAccount").val("");
    $("#cmbeducationalLevel").select2("val",-1);
    $("#cmbeducationalOffer").select2("val",-1);
    $("#cmbeducationalOffer").prop("disabled",true);
 
 }
function clearfields()
 {
    //Purpose: It remove the class inputs fields.
    //Limitations: The  fields must exist.
    //Returns: None.
    
    if ($("#vemailAccount").hasClass("has-warning")){
        $("#vemailAccount").removeClass("has-warning");
    }
    if ($("#veducationalLevel").hasClass("has-warning")){
        $("#veducationalLevel").removeClass("has-warning");
    }
    if ($("#vname").hasClass("has-warning")){
        $("#vname").removeClass("has-warning");
    }
    if ($("#vfirstName").hasClass("has-warning")){
        $("#vfirstName").removeClass("has-warning");
    }
    if ($("#vlastName").hasClass("has-warning")){
        $("#vlastName").removeClass("has-warning");
    }
    if ($("#vtelefono").hasClass("has-warning")){
        $("#vtelefono").removeClass("has-warning");
    }
 }