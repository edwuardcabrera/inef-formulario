/********************************************************
Name: emails.js
Version: 0.0.1
Autor name: Sandro Alan Gómez Aceituno.
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 06/06/2017
Modification date: 03/07/2017
Description: JS file. It provides support between view and controller files, in order to emails.
********************************************************/

var vrootURL = "./controller/emails.php";
var vrootURLUser= "./controller/users.php";
var vcampusesURL = "./controller/campuses.php";
var vdatetimeURL="./controller/datetime.php";
var vaccessModule=new Array(false, false, false, false, false, false, false);
var vidEmailMarketing="000000-00";
var vdataCampusEmailSetting = 0;
var vidEmailMarketingStatus = 0;
var vidEmailMarketingStatuses = 0;
var vidUserType=0;
var vidCampus=0;
var vidCampusSetting=0;
var vconfirmSendEmail;
var vdatetime;

$(document).ready(function() {
	//Purpose: It sets default options of the page WEB (Emails Marketing).
    //Limitations: The current page (view) must be fully loaded.  
    //Returns: None.
	
	menu(12, 0);
	verifyAccessPrivileges();
	disableMainButtonsStartAplication();
});

function verifyAccessPrivileges()
 {
	//Purpose: It verifies access privileges (end email marketing) in order to set controls of the page WEB (users).
    //Limitations: The email marketing access privileges data must exist in database server and brought by service rest (users/{idUser}/accessPrivileges).   
    //Returns: None.
    
    var verrorMessage="";
    $.ajax({
		type: "GET",
		url: vrootURLUser + "/users/0/accessPrivileges",
		dataType: "JSON",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: 	verrorMessage ="Ocurrió un error al tratar de obtener sus privilegios de acceso.<br />";
                            verrorMessage+="Intente de nuevo.";
                            openError(verrorMessage);
                            break;
                case 0:     verrorMessage ="No existen privilegios de acceso registrados.<br />";
                            verrorMessage+="Verifiquelo con el Administrador del Sistema.";
                            openError(verrorMessage);
                            break;
                case 1: 	if ( vresponse[1].backendUserAccessPrivileges.length > 0 ){
                                setControls(vresponse[1].backendUserAccessPrivileges);
							}
							else{
								verrorMessage ="No tiene privilegios de acceso registrados.<br />";
								verrorMessage+="Verifiquelo con el Administrador del Sistema.";
								openError(verrorMessage);
							}
                            break;
                default:    verrorMessage ="Ocurrió un error al tratar de obtener sus privilegios de acceso.";
                            openError(verrorMessage);
                            break;
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            verrorMessage=verrorThrown + "<br />";
            verrorMessage+="Verifiquelo con el Administrador del Sistema.";
            openError(verrorMessage);
		}
	});
 }

function setControls(vbackendUserAccessPrivileges)
 {
    //Purpose: It sets controls of the page WEB (users).
    //Limitations: The email marketing access privileges data must exist in format JSON and the controls must exist in the page WEB (users).  
    //Returns: None.
	
	var vhtml="";
	
	for(vaccessPrivilegeNumber=0; vaccessPrivilegeNumber<vbackendUserAccessPrivileges.length; vaccessPrivilegeNumber++){
		switch( vbackendUserAccessPrivileges[vaccessPrivilegeNumber].accessPrivilege.idAccessPrivilege ){			
			case 14: vaccessModule[0]=true;
					 vaccessModule[1]=true;
					 break;	
			case 15: vaccessModule[0]=true;
					 vaccessModule[2]=true;
					 break;	
			case 16: vaccessModule[0]=true;
					 vaccessModule[3]=true;
					 break;
			case 17: vaccessModule[0]=true;
					 vaccessModule[4]=true;
					 break;
			case 18: vaccessModule[0]=true;
					 vaccessModule[5]=true;
					 break;
			case 19: vaccessModule[0]=true;
					 vaccessModule[6]=true;
					 break;
		}
	}
	if ( ! vaccessModule[0] ){
        verrorMessage ="Acceso Denegado.<br />";
        verrorMessage+="No tiene privilegios para acceder al módulo de correos electrónicos.";
        openError(verrorMessage);
    }
    vidUserType=vbackendUserAccessPrivileges[0].backendUser.userType.idUserType;
    vidCampusSetting=vbackendUserAccessPrivileges[0].backendUser.campus.idCampus;
    
    getServerDateTime();
	if( vaccessModule[1] || vaccessModule[2] || vaccessModule[3] || vaccessModule[4] ) {
		$("#btnacceptFilter").prop( "disabled", false);
        $("#btnacceptFilter").attr("onclick", "showEmailMarketingList();");
		$("#btncancelFilter").prop( "disabled", false);
        $("#btncancelFilter").attr("onclick", "cleanEmailsFormFilterFields();");
        $("#txtdateRecordBegin").attr("onkeypress", "return setEntriesType(event, 'date');");
        $("#txtdateRecordBegin").val(vdatetime);
        $("#txtdateRecordEnd").attr("onkeypress", "return setEntriesType(event, 'date');");
        $("#txtdateRecordEnd").val(vdatetime);
		showEmailMarketingList();
	}
	
	if ( vaccessModule[1] ){
		$("#btncancelEmailMarketing").attr("onclick", "updateEmailSettingAccessAddress(3);");
	}
	if ( vaccessModule[2] ){
        $("#btnsendEmailMarketing").attr("onclick", "sendEmail();");
	}
	if ( vaccessModule[3] ){
		$("#btnupdateEmailMarketing").attr("onclick", "openEmailMarketingUpdate();");
	}
	if ( vaccessModule[4] ){		
		$("#btndeleteEmailMarketing").attr("onclick", "deleteEmailSettingAccessAddress();");
	}
	if ( vaccessModule[5] ){		
		$("#btnshowSetingEmailMarketing").prop( "disabled", false);
		$("#btnrecordSetingEmail").attr("onclick", "recordCampusEmailSettingData();");
		$("#btnshowSetingEmailMarketing").attr("onclick", "getCampusEmailSettingData();");		
	}
	if ( vaccessModule[6] ){
		$("#btnaddEmailMarketing").prop( "disabled", false);
		$("#btnaddEmailMarketing").attr("onclick", "openEmailMarketingRecord();");
	}
	
	if( vidUserType == 1) { 				
		vhtml+='	<label for="cmbcampus"><strong>Campus:</strong></label>';
		vhtml+='	<select id="cmbcampus" name="cmbcampus" class="form-control form-white" tabindex="-1" style="display: none; width: 100%"></select>';
		$("#vcampus").html(vhtml);	
		
		$("#vcampusSetting").addClass("col-md-12 form-group");
		$("#vcampus").addClass("col-md-4 form-group");
		$("#vdateRecordBegin").addClass("col-md-4");
		$("#vdateRecordEnd").addClass("col-md-4");		
		$("#vemailMarketingType").addClass("col-md-4");
		$("#vemailMarketingStatus").addClass("col-md-4");
		setCampusesList();
	}
	else {
		$("#vdateRecordBegin").addClass("col-md-6");
		$("#vdateRecordEnd").addClass("col-md-6");
		$("#vemailMarketingType").addClass("col-md-3");
		$("#vemailMarketingStatus").addClass("col-md-3");	
	}
	setEmailMarketingTypesList();
	setEmailMarketingStatusList();
	configOthersControls();
 }

function configOthersControls()
 {
    //Purpose: It configs others controls.
    //Limitations: None.
    //Returns: None.
    
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '< Ant',
        nextText: 'Sig >',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'mm/dd/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
    $('.datepicker').datepicker();
 }
 
function deleteEmailSettingAccessAddress() 
 {
	//Purpose: It deletes Emails Marketing.
    //Limitations: The end Emails Marketing must accept the deleting Emails Marketing.
    //Returns: None.
	
    var vacceptFunctionName = "deleteEmailMarketingData()";
    var vcancelFunctionName = "cancelEmailMarketingAccessAddressRemoval()";
    var vmessage = "¿Está seguro que desea eliminar la configuración del correo electrónico?";
    var vtitle = "Eliminación de Configuración de Correo Electrónico.";
    vconfirmDeleteEmailMarketingAccessAddress = toastrConfirm(vacceptFunctionName, vcancelFunctionName, vmessage, vtitle);
 }

function cancelEmailMarketingAccessAddressRemoval() 
 {
	//Purpose: It cancels deleting Emails Marketing.
    //Limitations: None.
    //Returns: None.
	
    vconfirmDeleteEmailMarketingAccessAddress.remove();
    toastr.clear(vconfirmDeleteEmailMarketingAccessAddress, {
        force: true
    });
    toastrAlert("La eliminación del correo electrónico ha sido cancelada.", "Eliminación de Configuración de Correo Electrónico.", 2);
 }

function deleteEmailMarketingData() 
 {
	//Purpose: It deletes Emails Marketing data.
    //Limitations: The Emails Marketing data must exist in database server and brought by service rest (emailMarketing/{idEmailMarketing}).
    //Returns: None.
	
    var vtitle = "Eliminación de Correo Electrónico.";
    vconfirmDeleteEmailMarketingAccessAddress.remove();
    toastr.clear(vconfirmDeleteEmailMarketingAccessAddress, {
        force: true
    });
	
    $.ajax({
        type: 'DELETE',
        url: vrootURL + "/campuses/" + vidCampus + "/emailMarketings/" + vidEmailMarketing,
        dataType: "JSON",
        success: function(vresponse, vtextStatus, vjqXHR) {
            switch (vresponse.messageNumber) {
                case -100:
                    toastrAlert("Ocurrió un error al tratar de eliminar los datos del correo electrónico, intente de nuevo", vtitle, 0);
                    break;
                case -1:
                    toastrAlert("Imposible eliminar el correo electrónico, no se pueden eliminar los archivos adjuntados.", vtitle, 3);
                    break;
                case 0:
                    toastrAlert("Imposible eliminar el correo electrónico, intente de nuevo." , vtitle, 3);
                    break;
                case 1:
					$("#vemailMarketingListRow" + vidEmailMarketing).remove();
                    disableMainButtons();
                    toastrAlert("El correo electrónico ha sido eliminado correctamente.", vtitle, 1);
                    break;
                default:   toastrAlert("Ocurrió un error al tratar de eliminar los datos del correo electrónico.", vtitle, 0);
                           break; 
            }
        },
        error: function(vjqXHR, vtextStatus, verrorThrown) {
            toastrAlert(verrorThrown, vtitle, 0);
        }
    });
 } 
 
function getCampusEmailSettingData()
 {
	//Purpose: It sets campuses list of the page WEB (emails-record).
    //Limitations: The campuses data must exist in database server and brought by service rest (campuses).
    //Returns: None.
	
	if( vidCampusSetting != 0) {		
		var vtitle="Visualización de Configuración de Correo Electrónico."; 
		$.ajax({
			type: 'GET',
			url: vrootURL + "/campusEmailSettings/" + vidCampusSetting,
			dataType: "JSON",
			success: function(vresponse, vtextStatus, vjqXHR){
				switch(vresponse.messageNumber){
					case -100:  toastrAlert("Ocurrió un error al tratar de mostrar los datos de la configuración del correo electrónico., intente de nuevo ", vtitle, 0);
							break;
					case 0:     toastrAlert("Los datos de la configuración del correo electrónico no se encuentran registrados.", vtitle, 3);
								clearTextField();
								vdataCampusEmailSetting = 1;
							break;
					case 1:		clearTextField();
								$("#txthostName").val(vresponse.campusEmailSettings.hostName);
								$("#txtsmtpPort").val(vresponse.campusEmailSettings.smtpPort);
								$("#txtsmtpAuth").val(vresponse.campusEmailSettings.smtpAuth);
								$("#txtsmtpSecure").val(vresponse.campusEmailSettings.smtpSecure);
								$("#txtuserName").val(vresponse.campusEmailSettings.userName);
								$("#txtuserPassword").val(vresponse.campusEmailSettings.userPassword);	
								vdataCampusEmailSetting	= 0;								
							break;
					default:   	toastrAlert("Ocurrió un error al tratar de mostrar los datos de la configuración del correo electrónico.", vtitle, 0);
							break;
				}
			},
			error: function(vjqXHR, vtextStatus, verrorThrown){
				toastrAlert(verrorThrown, vtitle, 0);
			}
		});
	}
	else {
		clearTextField();
		vdataCampusEmailSetting = 1;		
	}	
 }

function clearTextField()
 { 
	//Purpose: It cleans email marketing form filter fields.
    //Limitations: The email marketing form filter fields must exist.
    //Returns: None.
	
	$("#txthostName").val('');
	$("#txtsmtpPort").val('');
	$("#txtsmtpAuth").val('');
	$("#txtsmtpSecure").val('');
	$("#txtuserName").val('');
	$("#txtuserPassword").val('');	
 }

function recordCampusEmailSettingData()
 {
	//Purpose: It records (add/update) campus emails setting data.
    //Limitations: The page WEB controls must have data.  
    //Returns: None.
	
    var vtitle="Registro de Configuración de Correo Electrónico"; 
	if( parseInt(vidCampusSetting) != 0 ){
		if ( vdataCampusEmailSetting == 1 ){
			if ( validateCampusEmailSettingDataFormFields() ){
				addCampusEmailSettingData(); 
			}
			else{
				toastrAlert("El registro de los datos de la configuración del correo electrónico no puede ser realizado, no tiene los datos necesarios, complete los datos.", vtitle, 3);     
			}        
		}
		else {
			if( validateCampusEmailSettingDataFormFields() ){
				updateCampusEmailSettingData();
			}
			else{
				toastrAlert("La actualización de los datos de la configuración del correo electrónico no puede ser realizado, no tiene los datos necesarios, complete los datos.", vtitle, 3);
			}
		}			
	}
	else {
		toastrAlert("Imposible registrar la configuración del correo electrónico., Seleccione un Campus (3)", vtitle, 3);
	}    
 }
 
function validateCampusEmailSettingDataFormFields()
 {
	//Purpose: It validates campus email setting form fields.
    //Limitations: The campus email setting form fields must exist.
    //Returns: None.
	
    var vstatus=true;       
    clearCampusFormFields();  
	
	if(trim($("#txthostName").val()) === ""){
		$("#vhostName").addClass("has-warning");
		if ( vstatus ){
			$("#txthostName").focus();
		}
		vstatus = false;
	}        
	if(trim($("#txtsmtpPort").val()) === ""){
		$( "#vsmtpPort").addClass("has-warning");
		if ( vstatus ){
			$( "#txtsmtpPort" ).focus();
		}
		vstatus = false;
	}
	if(trim($( "#txtsmtpAuth" ).val()) === ""){
		$( "#vsmtpAuth" ).addClass("has-warning");
		if ( vstatus ){
			$( "#txtsmtpAuth" ).focus();
		}
		vstatus = false;
	}	
	if(trim($( "#txtsmtpSecure" ).val()) === ""){
		$( "#vsmtpSecure" ).addClass("has-warning");
		if ( vstatus ){
			$( "#txtsmtpSecure" ).focus();
		}
		vstatus = false;
	}   	
	if(trim($( "#txtuserName" ).val()) === ""){
		$( "#vuserName" ).addClass("has-warning");
		if ( vstatus ){
			$( "#txtuserName" ).focus();
		}
		vstatus = false;
	}  	
    if(trim($( "#txtuserPassword" ).val()) === ""){
        $( "#vuserPassword" ).addClass("has-warning");
        if ( vstatus ){
            $( "#txtuserPassword" ).focus();
        }
        vstatus = false;
    } 
    return vstatus;
} 

function clearCampusFormFields()
 {  
	//Purpose: It clears campus email setting form fields.
    //Limitations: The class "has-warning" must exist in campus email setting form fields.
    //Returns: None.
	
    if( $("#vhostName").hasClass("has-warning") ){
        $("#vhostName").removeClass("has-warning");
    }
    if( $("#vsmtpPort").hasClass("has-warning") ){
        $("#vsmtpPort").removeClass("has-warning");
    }
    if( $("#vsmtpAuth").hasClass("has-warning") ){
        $("#vsmtpAuth").removeClass("has-warning");
    }   
    if( $("#vsmtpSecure").hasClass("has-warning") ){
        $("#vsmtpSecure").removeClass("has-warning");
    }
    if( $("#vuserName").hasClass("has-warning") ){
        $("#vuserName").removeClass("has-warning");
    }    
	if( $("#vuserPassword").hasClass("has-warning") ){
        $("#vuserPassword").removeClass("has-warning");
    }	
 }
 
function updateCampusEmailSettingData()
 {    
	//Purpose: It updates campus email setting data.
    //Limitations: The campus email setting data must be exist in database server and brought by service rest (campusEmailSetting/{idCampus}).  
    //Returns: None.
	
    var vjson=$('#vdataCampusSettingEmail').serializeObject();    
	vtitle="Actualización de Configuración de Correo Electrónico.";
	
    $.ajax({
        type: 'PUT',
        url: vrootURL + "/campusEmailSettings",
        dataType: "JSON",
        data: JSON.stringify(vjson),
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100:  toastrAlert("Ocurrió un error al tratar de modificar los datos de la configuracion del correo electrónico, intente de nuevo.", vtitle, 0);
                            break;     
                case 0:     toastrAlert("Ningún dato de la configuración del correo electrónico ha sido modificado.", vtitle, 3);
                            break;
                case 1:     toastrAlert("Los datos de la configuración del correo electrónico han sido modificados correctamente.", vtitle, 1);
                            break;
				default:   	toastrAlert("Ocurrió un error al tratar de modificar los datos de la configuración del correo electrónico.", vtitle, 0);
						break;
            }
        },
        error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
        }
    });         
 } 

function addCampusEmailSettingData()
 {
	//Purpose: It adds campus email setting data.
    //Limitations: The campus email setting data must not exist in database server and brought by service rest (campusEmailSettings).  
    //Returns: None.
	
    var vjson=$('#vdataCampusSettingEmail').serializeObject();    
    var vtitle="Registro de Configuración de Correo Electrónico.";
	$.ajax({
		type: 'POST',
		url: vrootURL + "/campusEmailSettings",
		dataType: "JSON",
		data: JSON.stringify(vjson),
		success: function(vresponse, vtextStatus, vjqXHR){
			switch(vresponse.messageNumber){
				case -100:  toastrAlert("Ocurrió un error al tratar de registrar los datos de la configuración del correo electrónico, intente de nuevo", vtitle, 0);
							break;     
				case -1:    toastrAlert("Imposible registrar la configuración del correo electrónico., intente de nuevo (1)", vtitle, 3);
							break;
				case 0:     toastrAlert("Imposible registrar la configuración del correo electrónico., intente de nuevo (2)", vtitle, 3);
							break;
				case 1:     toastrAlert("Los datos de la configuración del correo electrónico han sido registrados correctamente", vtitle, 1);
							vdataCampusEmailSetting = 0;
							break;
				default:   	toastrAlert("Ocurrió un error al tratar de registrar los datos de la configuración del correo electrónico.", vtitle, 0);
						break;
			}
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
			toastrAlert(verrorThrown, vtitle, 0);
		}
	});	      
 }
 
function setCampusesList()
 {
    //Purpose: It sets campuses list of the page WEB (emails-record).
    //Limitations: The campuses data must exist in database server and brought by service rest (campuses).
    //Returns: None.
    
    var vtitle="Listado de Campus.";
    
    $.ajax({
		type: "GET",
		url: vcampusesURL + "/campuses",
		dataType: "JSON",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurrió un error al tratar de mostrar los campus, intente de nuevo.", vtitle, 0);
                           break;
                case 0:    toastrAlert("No existen campus registrados.", vtitle, 3);
                           break;
                case 1:    fillCampusesList(vresponse.campuses.campuses, 0);							
                           break;
                default:   toastrAlert("Ocurrió un error al tratar de mostrar los campus.", vtitle, 0);
                           break; 
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
 } 

function fillCampusesList(vcampuses, vid)
 {
    //Purpose: It fills campuses list.
    //Limitations: The campus control must exist in the page WEB and the campuses (data) must exist in format JSON.  
    //Returns: None.
    
    var vstringJSON='[{"id":0, "text":"--Todos--"}';
    for(vi=0; vi<vcampuses.length; vi++){
        vstringJSON+=', {"id":' + vcampuses[vi].idCampus;
        vstringJSON+=',  "text":"' + vcampuses[vi].campus + '"}';
    }
    vstringJSON+="]";
    var vobjectJSON=jQuery.parseJSON(vstringJSON);
    
    vcmbCampusesList=$("#cmbcampus").select2({
        data:vobjectJSON,
        placeholder: {
            id: 0,
            text: "--Todos--"
        },
        language:{
            noResults: function() {
                return "Sin campus encontrados.";
            }
        }, 
    });
    if ( vid!=0 ){
        $("#cmbcampus").select2("val", vid);
    }  

	if( vidUserType == 1 ) {
		var vhtmlSetting="";
		vhtmlSetting+='	<label for="cmbcampusSetting"><strong>Campus:</strong></label>';
		vhtmlSetting+='	<select id="cmbcampusSetting" name="cmbcampusSetting" class="form-control" style="width: 100%;">';
		vhtmlSetting+='	<option value="0">--Todos--</option>';
	
		$.each(vcampuses, function(vi) {
			var vidCampus = vcampuses[vi].idCampus;
			var vcampus = vcampuses[vi].campus;
			vhtmlSetting+='<option value="' + vidCampus + '">' + vcampus + '</option>';	
		});
		vhtmlSetting+="</select>";
		$("#vcampusSetting").html(vhtmlSetting);
		$("#cmbcampusSetting").attr("onchange", "loadDataToCampusEmailSetting();");
	}
 } 

function setCampusesListRecord()
 {
    //Purpose: It sets campuses list of the page WEB (emails-record).
    //Limitations: The campuses data must exist in database server and brought by service rest (campuses).
    //Returns: None.
    
    var vtitle="Listado de Campus.";
    if( vidCampusSetting != 0) {		
		$.ajax({
			type: "GET",
			url: vcampusesURL + "/campuses",
			dataType: "JSON",
			success: function(vresponse, vtextStatus, vjqXHR){
				switch(vresponse.messageNumber){
					case -100: toastrAlert("Ocurrió un error al tratar de mostrar los campus, intente de nuevo.", vtitle, 0);
							   break;
					case 0:    toastrAlert("No existen campus registrados.", vtitle, 3);
							   break;
					case 1:    
								var vhtml="";
									vhtml+='	<label for="cmbcampusSetting"><strong>Campus:</strong></label>';
									vhtml+='	<select id="cmbcampusSetting" name="cmbcampusSetting" class="form-control" style="width: 100%;">';
									vhtml+='	<option value="0">--Todos--</option>';
								
									$.each(vcampuses, function(vi) {
										var vidCampus = vcampuses[vi].idCampus;
										var vcampus = vcampuses[vi].campus;
										vhtml+='<option value="' + vidCampus + '">' + vcampus + '</option>';	
									});
									vhtml+="</select>";
									$("#vcampusSetting").html(vhtml);
			
			
							   break;
					default:   toastrAlert("Ocurrió un error al tratar de mostrar los campus.", vtitle, 0);
							   break; 
				}
			},
			error: function(vjqXHR, vtextStatus, verrorThrown){
				toastrAlert(verrorThrown, vtitle, 0);
			}
		});
	}
 } 
 
function loadDataToCampusEmailSetting()
 {
	//Purpose: It sets email marketing types list of the page WEB (emails-record).
    //Limitations: The email marketing types data must exist in database server and brought by service rest (emailMarketingTypes).
    //Returns: None.
	
	vidCampusSetting = $("#cmbcampusSetting").val();
	getCampusEmailSettingData();
	
 }

function setEmailMarketingStatusList()
 {
    //Purpose: It sets email marketing types list of the page WEB (emails-record).
    //Limitations: The email marketing types data must exist in database server and brought by service rest (emailMarketingTypes).
    //Returns: None.
    
    var vtitle="Listado de Tipos de Emails.";
    
    $.ajax({
		type: "GET",
		url: vrootURL + "/emailMarketingStatuses",
		dataType: "JSON",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurrió un error al tratar de mostrar los tipos de correo electrónicos, intente de nuevo.", vtitle, 0);
                           break;
                case 0:    toastrAlert("No existen tipos de correo electrónicos registrados.", vtitle, 3);
                           break;
                case 1:    fillEmailMarketingStatusList(vresponse.emailMarketingStatus.emailMarketingStatus, 0);
                           break;
                default:   toastrAlert("Ocurrió un error al tratar de mostrar los tipos de correo electrónicos.", vtitle, 0);
                           break;  
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
 } 

function fillEmailMarketingStatusList(vemailMarketingStatus, vid)
 {
    //Purpose: It fills email marketing status list.
    //Limitations: The email marketing status control must exist in the page WEB and the email marketing types (data) must exist in format JSON.  
    //Returns: None.
    
    var vstringJSON='[{"id":0, "text":"--Todos--"}';
    for(vi=0; vi<vemailMarketingStatus.length; vi++){
        vstringJSON+=', {"id":' + vemailMarketingStatus[vi].idEmailMarketingStatus;
        vstringJSON+=',  "text":"' + vemailMarketingStatus[vi].emailMarketingStatus + '"}';
    }
    vstringJSON+="]";
    var vobjectJSON=jQuery.parseJSON(vstringJSON);
    
    vcmbEmailMarketingTypeList=$("#cmbemailMarketingStatus").select2({
        data:vobjectJSON,
        placeholder: {
            id: 0,
            text: "--Todos--"
        },
        language:{
            noResults: function() {
                return "Sin estados de email encontrados.";
            }
        }, 
    });
    if ( vid!=0 ){
        $("#cmbemailMarketingStatus").select2("val", vid);
    }
 } 
 

function updateEmailSettingAccessAddress(vidEmailMarketingStatus) 
 {
	//Purpose: It cancel status Emails Marketing.
    //Limitations: The end Emails Marketing must accept the deleting Emails Marketing.
    //Returns: None.
	
    var vacceptFunctionName = "updateEmailMarketingStatusData(" + vidEmailMarketingStatus + ")";
    var vcancelFunctionName = "cancelUpdateEmailMarketingAccessAddressRemoval()";
    var vmessage = "¿Está seguro que desea cancelar el status de la configuración del correo electrónico?";
    var vtitle = "Cancelación de status de la Configuración de Correo Electrónico.";
    vconfirmUpdateEmailMarketingAccessAddress = toastrConfirm(vacceptFunctionName, vcancelFunctionName, vmessage, vtitle);
 }

function cancelUpdateEmailMarketingAccessAddressRemoval() 
 {
	//Purpose: It cancels status Emails Marketing.
    //Limitations: None.
    //Returns: None.
	
    vconfirmUpdateEmailMarketingAccessAddress.remove();
    toastr.clear(vconfirmUpdateEmailMarketingAccessAddress, {
        force: true
    });
    toastrAlert("La cancelacion del status del correo electrónico ha sido cancelada.", "Cancelación de Configuración de Correo Electrónico.", 2);
 }

 
function updateEmailMarketingStatusData(vidEmailMarketingStatus)
 {    
	//Purpose: It updates campus email setting data.
    //Limitations: The campus email setting data must be exist in database server and brought by service rest (campusEmailSetting/{idCampus}).  
    //Returns: None.
	  
	vtitle="Cancelación de status de Correo Electrónico.";
	vidEmailMarketingStatuses = $("#cmbupdateEmailMarketingStatus").val();
	
    $.ajax({
        type: 'PUT',
		url: vrootURL + "/campuses/" + vidCampus + "/emailMarketings/" + vidEmailMarketing + "/emailMarketingStatuses/" + vidEmailMarketingStatus,
        dataType: "JSON",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100:  toastrAlert("Ocurrió un error al tratar de cancelar el status del correo electrónico, intente de nuevo.", vtitle, 0);
                            break;     
                case 0:     toastrAlert("No se ha cancelado el status del correo electrónico, intente de nuevo.", vtitle, 3);
                            break;
                case 1:     toastrAlert("El correo electrónico ha sido cancelado correctamente.", vtitle, 1);
                            showEmailMarketingList();
							break;
				default:   	toastrAlert("Ocurrió un error al tratar de cancelar el status del correo electrónico.", vtitle, 0);
						break;
            }
        },
        error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
        }
    });         
 }  

function setEmailMarketingTypesList()
 {
    //Purpose: It sets email marketing types list of the page WEB (emails-record).
    //Limitations: The email marketing types data must exist in database server and brought by service rest (emailMarketingTypes).
    //Returns: None.
    
    var vtitle="Listado de Tipos de Correo Electrónicos.";
    
    $.ajax({
		type: "GET",
		url: vrootURL + "/emailMarketingTypes",
		dataType: "JSON",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurrió un error al tratar de mostrar los tipos de correos electrónicos, intente de nuevo.", vtitle, 0);
                           break;
                case 0:    toastrAlert("No existen tipos de correos electrónicos registrados.", vtitle, 3);
                           break;
                case 1:    fillEmailMarketingTypesList(vresponse.emailMarketingTypes.emailMarketingTypes, 0);
                           break;
                default:   toastrAlert("Ocurrió un error al tratar de mostrar los tipos de correos electrónicos.", vtitle, 0);
                           break;  
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
 }
 
function fillEmailMarketingTypesList(vemailMarketingTypes, vid)
 {
    //Purpose: It fills email marketing types list.
    //Limitations: The email marketing type control must exist in the page WEB and the email marketing types (data) must exist in format JSON.  
    //Returns: None.
    
    var vstringJSON='[{"id":0, "text":"--Todos--"}';
    for(vi=0; vi<vemailMarketingTypes.length; vi++){
        vstringJSON+=', {"id":' + vemailMarketingTypes[vi].idEmailMarketingType;
        vstringJSON+=',  "text":"' + vemailMarketingTypes[vi].emailMarketingType + '"}';
    }
    vstringJSON+="]";
    var vobjectJSON=jQuery.parseJSON(vstringJSON);
    
    vcmbEmailMarketingTypeList=$("#cmbemailMarketingType").select2({
        data:vobjectJSON,
        placeholder: {
            id: 0,
            text: "--Todos--"
        },
        language:{
            noResults: function() {
                return "Sin tipos de correos electrónicos encontrados.";
            }
        }, 
    });
    if ( vid!=0 ){
        $("#cmbemailMarketingType").select2("val", vid);
    }
 } 
 
function showEmailMarketingList() 
{	
	//Purpose: It sets email marketings list of the page WEB.
    //Limitations: The email marketings data must exist in database server and brought by service rest (emailMarketings).
    //Returns: None.
	
	disableMainButtons();
    var vfilterData=$('#vfilterData').serialize();
    if (vidUserType!=1){
        vfilterData+="&cmbcampus=" + vidCampusSetting;
    }
    $.ajax({
        type: "GET",
        url: vrootURL + "/emailMarketings",
        dataType: "JSON",
        data: vfilterData,
        success: function(vresponse, vtextStatus, vjqXHR) {
            switch (vresponse.messageNumber) {
                case -100:	vmessageHTML = '<p style="text-align:center" class="text-danger">';
							vmessageHTML += ' <strong>Ocurrió un error al tratar de mostrar los correos electrónicos, intente de nuevo </strong>';
							vmessageHTML += '</p>';
							$("#vemailsList").html(vmessageHTML);
							break;
                case 0:		vmessageHTML = '<p style="text-align:center" class="text-danger">';
							vmessageHTML += ' <strong> No existen correos electrónicos registrados</strong>';
							vmessageHTML += '</p>';
							$("#vemailsList").html(vmessageHTML);
							break;
				case 1:		var vemailMarketing=null;
							if (vresponse.emailMarketings.emailMarketings != null ){
								vemailMarketing=vresponse.emailMarketings.emailMarketings;
							}
							else{
								vemailMarketing=vresponse.users.backendUsers;
							}
							fillEmailMarketingList(vemailMarketing);                   
                            break;					
                default:	verrorMessage ="Ocurrió un error al tratar de mostrar los correos electrónicos.";
							$("#vemailsList").html(vmessageHTML);
							break;
            }
        },
        error: function(vjqXHR, vtextStatus, verrorThrown) {
            vmessageHTML = '<p style="text-align:center" class="text-danger">';
            vmessageHTML += ' <strong> ' + verrorThrown + ' </strong>';
            vmessageHTML += ' <strong>Ocurrió un error al tratar de mostrar los correos electrónicos.</strong>';
            vmessageHTML += '</p>';
            $("#vemailsList").html(vmessageHTML);
        }
    });
}

function fillEmailMarketingList(vemailMarketing)
 {
    //Purpose: It fills users list.
    //Limitations: The users data must exist in format JSON.
    //Returns: None.
    
    var vhtml ='<div class="table-responsive">';
        vhtml+='	<table id="vgrdEmailMarketingList" class="table table-hover">';
        vhtml+='		<thead>';
        vhtml+='			<tr>';
        vhtml+='				<th style="text-align:center;" class="no-sort">&nbsp;</th>';
		if(vidUserType == 1){
			vhtml+='			<th style="text-align:center;">Campus</th>';
		}
        vhtml+='				<th style="text-align:center;">Fecha registro</th>';
        vhtml+='				<th style="text-align:center;">Tipo de correo</th>';
        vhtml+='				<th style="text-align:center;">Nivel Educativo</th>';
        vhtml+='				<th style="text-align:center;">Oferta Educativa</th>';
        vhtml+='				<th style="text-align:center;">Estado</th>';
        vhtml+='			</tr>';
        vhtml+='		</thead>';
        vhtml+='		<tbody>';
    for(vi=0; vi<vemailMarketing.length; vi++){
        vhtml+='			<tr id="vemailMarketingListRow' + vemailMarketing[vi].idEmailMarketing + '">';
        vhtml+='				<td style="text-align:center;">';
        vhtml+='					<label>';
        vhtml+='						<input type="radio" name="cmremailMarketing" value="' + vemailMarketing[vi].emailMarketingStatus.idEmailMarketingStatus + "-" + 
																								vemailMarketing[vi].campus.idCampus + "-" +
																								vemailMarketing[vi].idEmailMarketing + '" title="Seleccionar Correo" class="radio">';
        vhtml+='					</label>';
        vhtml+='				</td>';
		if(vidUserType == 1){
			vhtml+='				<td style="text-align:center;">' + vemailMarketing[vi].campus.campus + '</td>';
		}
        vhtml+='				<td style="text-align:center;">' + vemailMarketing[vi].recordDate + '</td>';
        vhtml+='				<td style="text-align:center;">' + vemailMarketing[vi].emailMarketingType.emailMarketingType + '</td>';
        vhtml+='				<td style="text-align:center;">' + vemailMarketing[vi].educationalOffer.assignment.educationalLevel.educationalLevel + '</td>';
        if (vemailMarketing[vi].educationalOffer.educationalOffer == "S/O"){
			vhtml+='			<td style="text-align:center;">TODOS</td>';
		}
		else {
			vhtml+='			<td style="text-align:center;">' + vemailMarketing[vi].educationalOffer.educationalOffer + '</td>';
		}
		
        vhtml+='				<td style="text-align:center;">';
        vhtml+='					<span ';
        switch (vemailMarketing[vi].emailMarketingStatus.idEmailMarketingStatus){            
            case 1:  vhtml+='class="label label-success">';
                     break;
			case 2:  vhtml+='class="label label-warning">';
                     break;
            case 3: vhtml+='class="label label-danger">';
                     break;
        }
        vhtml+=vemailMarketing[vi].emailMarketingStatus.emailMarketingStatus;
        vhtml+='					</span>';
        vhtml+='				</td>';
        vhtml+='			</tr>';
    }
        vhtml+='		</tbody>';
        vhtml+='	</table>';
        vhtml+='</div>';
    $("#vemailsList").html(vhtml);
    
    setEmailMarketingList();
    setRadioButtons();
 }
 
 function setEmailMarketingList()
 {
    //Purpose: It sets users list.
    //Limitations: The users must exist in users list.  
    //Returns: None.
    
    $("#vgrdEmailMarketingList").dataTable({
        "bLengthChange": false,
        "searching": false,
        "processing": true,
        "displayLength": 15,
        "columnDefs": [{
            "targets": 'no-sort',
            "orderable": false}],
        "language": {
            "sProcessing": "Procesando...",
            "sInfo": "_START_ - _END_ de _TOTAL_ correos electrónicos",
            "sInfoFiltered": "(filtrado de un total de _MAX_ correos electrónicos)",
            "sInfoPostFix":    "",
            "sUrl": "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
    $("#vgrdEmailMarketingList").on("draw.dt", function(){
        setRadioButtons();  
    });
}

function setRadioButtons()
 {
    //Purpose: It sets radio buttons.
    //Limitations: The radio buttons must be created.  
    //Returns: None.
    
    $('input[type="checkbox"], input[type="radio"].radio').iCheck({
        checkboxClass:'icheckbox-primary',
        radioClass:'iradio_flat-blue'
    }).on('ifClicked', function(e){
        setEmailMarketingIds(this.value);
    });
 }
 
function setEmailMarketingIds(vvalue)
 {
    //Purpose: It sets email marketing identifier.
    //Limitations: The email marketing identifier must exist in users list.  
    //Returns: None.
	
	vidEmailMarketingStatus = parseInt(getStringMatch("-", vvalue, 0, 1));
	vidCampus = parseInt(getStringMatch("-", vvalue, 1, 2));
    vidEmailMarketing = getStringMatch("-", vvalue, 2, 3) + "-" + getStringMatch("-", vvalue, 3, 4);
	
	// if( vidEmailMarketingStatus == 2){
		// enableMainButtons();
	// }
	// else {
		// disableMainButtons();
	// }
	
	if ( vaccessModule[2] ){
		if( vidEmailMarketingStatus == 2)
			$("#btncancelEmailMarketing").prop("disabled", false); 		
		else 
			$("#btncancelEmailMarketing").prop("disabled", true);		 
	}
	if ( vaccessModule[3] ){
		if( vidEmailMarketingStatus == 2)
			$("#btnsendEmailMarketing").prop("disabled", false); 		
		else 
			$("#btnsendEmailMarketing").prop("disabled", true);
	}
	if ( vaccessModule[4] ){
		if( vidEmailMarketingStatus == 2)
			$("#btnupdateEmailMarketing").prop("disabled", false); 		
		else 
			$("#btnupdateEmailMarketing").prop("disabled", true);
	}
	if ( vaccessModule[5] ){
		if( vidEmailMarketingStatus == 2)
			$("#btndeleteEmailMarketing").prop("disabled", false); 		
		else 
			$("#btndeleteEmailMarketing").prop("disabled", true);	
	}
	if ( vaccessModule[6] ){
		$("#btnrecordSetingEmail").prop("disabled", false);
		$("#btnshowSetingEmailMarketing").prop("disabled", false);
	}
	if ( vaccessModule[7] ){
		$("#btnaddEmailMarketing").prop("disabled", false);
	}
    if ( vaccessModule[8] ){
        if( vidEmailMarketingStatus == 2){
            $("#btnsendEmailMarketing").prop("disabled", false);
        }
        else{
            $("#btnsendEmailMarketing").prop("disabled", true);
        }
    }
 }

function openEmailMarketingRecord()
 {
    //Purpose: It opens email marketing record page WEB.
    //Limitations: The email marketing must be clic on add email marketing button.
    //Returns: None.
    
    var vhtml ='<form id="vform" style="display:hidden" action="./frmemails-record.php" method="POST">';
        vhtml+='	<input type="hidden" id="vidEmailMarketing" name="vidEmailMarketing" value="0" />';
		vhtml+='</form>';
	$('<div id="vformContent"></div>').appendTo('body');
	$("#vformContent").append(vhtml);
	$("#vform").submit();
 }

function openEmailMarketingUpdate()
 {
    //Purpose: It opens email marketing update page WEB.
    //Limitations: The email marketing must be clic on update email marketing button.  
    //Returns: None.
    
    var vhtml ='<form id="vform" style="display:hidden" action="./frmemails-record.php" method="POST">';
        vhtml+='	<input type="hidden" id="vidEMailMarketing" name="vidEMailMarketing" value="' + vidEmailMarketing + '" />';
        vhtml+='	<input type="hidden" id="vidCampus" name="vidCampus" value="' + vidCampus + '" />';
		vhtml+='</form>';
        
	$('<div id="vformContent"></div>').appendTo('body');
	$("#vformContent").append(vhtml);
	$("#vform").submit();
 }

function enableMainButtons()
 {
    //Purpose: It enables main buttons: View email marketing access privileges, update email marketing and delete email marketing.
    //Limitations: The main buttons must be disable.  
    //Returns: None.
    
    $("#btnupdateEmailMarketing").prop( "disabled", false);
    $("#btndeleteEmailMarketing").prop( "disabled", false);
    $("#btncancelEmailMarketing").prop( "disabled", false);
    $("#btnsendEmailMarketing").prop( "disabled", false);
    // $("#btnupdateEmailMarketingStatus").prop( "disabled", false);
 }

function disableMainButtons()
 {
    //Purpose: It disables main buttons: View email marketing access privileges, update email marketing and delete email marketing.
    //Limitations: The main buttons must be enable.  
    //Returns: None.
    
    $("#btnupdateEmailMarketing").prop( "disabled", true);
    $("#btndeleteEmailMarketing").prop( "disabled", true);
    $("#btncancelEmailMarketing").prop( "disabled", true);
    $("#btnsendEmailMarketing").prop( "disabled", true);
	$("#btnupdateEmailMarketingStatus").prop( "disabled", true);
 }
 
function disableMainButtonsStartAplication()
 {
    //Purpose: It disables main buttons: View email marketing access privileges, update email marketing and delete email marketing.
    //Limitations: The main buttons must be enable.  
    //Returns: None.
    
    $("#btnupdateEmailMarketing").prop( "disabled", true);
    $("#btndeleteEmailMarketing").prop( "disabled", true);
    $("#btncancelEmailMarketing").prop( "disabled", true);
    $("#btnsendEmailMarketing").prop( "disabled", true);
	$("#btnshowSetingEmailMarketing").prop( "disabled", true);
	$("#btnaddEmailMarketing").prop( "disabled", true);
	$("#btnacceptFilter").prop( "disabled", true);
	$("#btncancelFilter").prop( "disabled", true);
 }
 
function cleanEmailsFormFilterFields()
 {
    //Purpose: It cleans email marketing form filter fields.
    //Limitations: The email marketing form filter fields must exist.
    //Returns: None.
    
    if ( vidUserType==1 ){
        $("#cmbcampus").select2("val", -1);
    }
    $("#cmbemailMarketingType").select2("val", -1);
    $("#cmbemailMarketingStatus").select2("val", -1);
    $("#txtdateRecordBegin").val(vdatetime);
    $("#txtdateRecordEnd").val(vdatetime);
	showEmailMarketingList();
 }
 
function sendEmail()
 {
    //Purpose: It sends email marketing.
    //Limitations: The end user must accept the email sending.
    //Returns: None.
    
    var vacceptFunctionName="sendEmailData();";
    var vcancelFunctionName="cancelSendEmail();";
    var vmessage="¿Desea enviar el correo electrónico?.";
    var vtitle="Envío de Correo Electrónico.";
    
    vconfirmSendEmail=toastrConfirm(vacceptFunctionName, vcancelFunctionName, vmessage, vtitle);
 }

function sendEmailData()
 {
    //Purpose: It send email marketing data.
    //Limitations: The email marketing data must exist in database server and brought by service rest (campuses/{idCampus}/educationalLevels/{idEducationalLevel}/emailMarketings/{idEmailMarketing}).
    //Returns: None.
    
    var vjson={};
    var vproperty="idCampus";
    vjson[vproperty]=vidCampus;
    var vtitle="Envío de Correo Electrónico.";
    
    vconfirmSendEmail.remove();
    toastr.clear(vconfirmSendEmail, { force: true });
    
    $.ajax({
		type: "put",
		url: vrootURL + "/emailMarketings/" + vidEmailMarketing,
		dataType: "json",
        data: JSON.stringify(vjson),
        beforeSend: function(vjqXHR, vplainObject){
            $("#vsendingEmailMarketingStatus").html("<i class='fa fa-spinner faa-spin animated'></i>&nbsp;Enviando...</span>");
        },
        success: function(vresponse, vtextStatus, vjqXHR){
            $("#vsendingEmailMarketingStatus").html("");
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurrió un error al tratar de enviar el correo electrónico, intente de nuevo.", vtitle, 0);
                           break;
                case -7:   toastrAlert("Imposible enviar el correo electrónico, no se pudo actualizar su estado a enviado.", vtitle, 3);
                           break;
                case -6:   toastrAlert("Imposible enviar el correo electrónico, no se pudo registrar los datos de envío.", vtitle, 3);
                           break;
                case -5:   toastrAlert("Imposible enviar el correo electrónico, intente de nuevo.", vtitle, 3);
                           break;
                case -4:   toastrAlert("Imposible enviar el correo electrónico, no existen usuarios que hayan solicitado planes de estudio para el nivel educativo indicado.", vtitle, 3);
                           break;
                case -3:   toastrAlert("Imposible enviar el correo electrónico, no se encuentra configurado los datos del correo de salida.", vtitle, 3);
                           break;
                case -2:   toastrAlert("Imposible enviar el correo electrónico, se encuentra cancelado.", vtitle, 3);
                           break;
                case -1:   toastrAlert("Imposible enviar el correo electrónico, ya se encuentra enviado.", vtitle, 3);
                           break;
                case 0:    toastrAlert("Imposible enviar el correo electrónico, no se encuentra registrado.", vtitle, 3);
                           break;
                case 1:    toastrAlert("El correo electrónico ha sido enviado correctamente.", vtitle, 1);
                           $("#btnsendEmailMarketing").prop("disabled", true);
                           showEmailMarketingList();
                           break;
                default:   toastrAlert("Ocurrió un error al tratar de enviar el correo electrónico.", vtitle, 0);
                           break;
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            $("#vsendingEmailMarketingStatus").html("");
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
 }
 
function getServerDateTime()
 {
    //Purpose: It gets server datetime.
    //Limitations: None.
    //Returns: None.
    
    var vtitle="Obtención de Fecha (Servidor)";
    
    $.ajax({
		type: "GET",
		url: vdatetimeURL,
		dataType: "json",
        async:false,
        success: function(vresponse, vtextStatus, vjqXHR){
            vdatetime=vresponse.substring(0, 10);
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
 }
 
function cancelSendEmail()
 {
    //Purpose: It sends emai marketing.
    //Limitations: None.
    //Returns: None.
    
    vconfirmSendEmail.remove();
    toastr.clear(vconfirmSendEmail, { force: true });
    toastrAlert("El envío del correo electrónico ha sido cancelado.", "Envío de Correo Electrónico.", 2);
 }