/********************************************************
Name: users-record.js
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodr�guez
Modification autor name: Edwuard H. Cabrera Rodr�guez
Creation date: 01/06/2017
Modification date: 04/07/2017
Description: JS file. It provides support between view and controller files, in order to record users.
********************************************************/

var vrootURL = "./controller/users.php";
var vcampusesURL = "./controller/campuses.php";
var vidUser=0;
var vidUserType=0;

function _default(vidUser_)
 {  
    //Purpose: It sets default options of the page WEB (users-record).
    //Limitations: The current page (view) must be fully loaded.  
    //Returns: None.
    
    vidUser=vidUser_;
    menu(1, 2);
    verifyAccessPrivileges();
 }

function verifyAccessPrivileges()
 {
	//Purpose: It verifies access privileges (end user) in order to set controls of the page WEB (users-record).
    //Limitations: The user access privileges data must exist in database server and brought by service rest (users/{idUser}/accessPrivileges).   
    //Returns: None.
    
    var verrorMessage="";
    $.ajax({
		type: "get",
		url: vrootURL + "/users/0/accessPrivileges",
		dataType: "json",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: verrorMessage ="Ocurri� un error al tratar de obtener sus privilegios de acceso.<br />";
                           verrorMessage+="Intente de nuevo.";
                           openError(verrorMessage);
                           break;
                case 0:    verrorMessage ="No existen privilegios de acceso registrados.<br />";
                           verrorMessage+="Verifiquelo con el Administrador del Sistema.";
                           openError(verrorMessage);
                           break;
                case 1:    if ( vresponse[1].backendUserAccessPrivileges.length>0 ){
                                setControls(vresponse[1].backendUserAccessPrivileges);
                           }
                           else{
                                verrorMessage ="No tiene privilegios de acceso registrados.<br />";
                                verrorMessage+="Verifiquelo con el Administrador del Sistema.";
                                openError(verrorMessage);
                           }
                           break;
                default:   verrorMessage ="Ocurri� un error al tratar de obtener sus privilegios de acceso.";
                           openError(verrorMessage);
                           break;
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            verrorMessage=verrorThrown + "<br />";
            verrorMessage+="Verifiquelo con el Administrador del Sistema.";
            openError(verrorMessage);
		}
	});
 }

function setControls(vbackendUserAccessPrivileges)
 {
    //Purpose: It sets controls of the page WEB (users-record).
    //Limitations: The controls must exist in the page WEB (users-record).  
    //Returns: None.
    
    if ( (vbackendUserAccessPrivileges[0].backendUser.userType.idUserType!=1) && 
         (vbackendUserAccessPrivileges[0].backendUser.userType.idUserType!=2) &&
         (vbackendUserAccessPrivileges[0].backendUser.userType.idUserType!=3) ){
        verrorMessage ="Acceso Denegado.<br />";
        verrorMessage+="No tiene privilegios para acceder al m�dulo de registro de usuarios.";
        openError(verrorMessage);
    }
    
    $("#txtname").attr("onkeypress", "return setEntriesType(event, 'letter');");
    $("#txtfirstName").attr("onkeypress", "return setEntriesType(event, 'letter');");
    $("#txtlastName").attr("onkeypress", "return setEntriesType(event, 'letter');");
    vidUserType=vbackendUserAccessPrivileges[0].backendUser.userType.idUserType;
    if ( vidUser==0 ){
        setUserTypesList(0);
        if (vidUserType==1){
            $("#vcampus").html(getCampusControls());
            setCampusesList(0);
        }
        setUserStatusesList(2);
        $("#cmbuserStatus").prop( "disabled", true);
    }
    else{
        $("#vpageTitle").html("Modificaci�n - Usuario");
		showUserData();
    }
    setButtons();
 }

function setUserTypesList(vid)
 {
    //Purpose: It sets user types list of the page WEB (users-record).
    //Limitations: The user types data must exist in database server and brought by service rest (userTypes).
    //Returns: None.
    
    var vtitle="Listado de Tipos de Usuario.";
    
    $.ajax({
		type: "get",
		url: vrootURL + "/userTypes",
		dataType: "json",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurri� un error al tratar de mostrar los tipos de usuarios, intente de nuevo.", vtitle, 0);
                           break;
                case 0:    toastrAlert("No existen tipos de usuarios registrados.", vtitle, 3);
                           break;
                case 1:    fillUserTypesList(vresponse.userTypes.userTypes, vid);
                           break;
                default:   toastrAlert("Ocurri� un error al tratar de mostrar los tipos de usuarios.", vtitle, 0);
                           break;  
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
 }
 
function setCampusesList(vid)
 {
    //Purpose: It sets campuses list of the page WEB (my-profile).
    //Limitations: The campuses data must exist in database server and brought by service rest (campuses).
    //Returns: None.
    
    var vtitle="Listado de Campus.";
    
    $.ajax({
		type: "get",
		url: vcampusesURL + "/campuses",
		dataType: "json",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurri� un error al tratar de mostrar los campus, intente de nuevo.", vtitle, 0);
                           break;
                case 0:    toastrAlert("No existen campus registrados.", vtitle, 3);
                           break;
                case 1:    fillCampusesList(vresponse.campuses.campuses, vid);
                           break;
                default:   toastrAlert("Ocurri� un error al tratar de mostrar los campus.", vtitle, 0);
                           break; 
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
 }
 
function setUserStatusesList(vid)
 {
    //Purpose: It sets user statuses list of the page WEB (users-record).
    //Limitations: The user statuses data must exist in database server and brought by service rest (userStatuses).
    //Returns: None.
    
    var vtitle="Listado de Estados de Usuario.";
    
    $.ajax({
		type: "get",
		url: vrootURL + "/userStatuses",
		dataType: "json",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurri� un error al tratar de mostrar los estados de usuarios, intente de nuevo.", vtitle, 0);
                           break;
                case 0:    toastrAlert("No existen estados de usuarios registradas.", vtitle, 3);
                           break;
                case 1:    fillUserStatusesList(vresponse.userStatuses.userStatuses, vid);
                           break;
                default:   toastrAlert("Ocurri� un error al tratar de mostrar los estados de usuarios.", vtitle, 0);
                           break;
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
 }

 
function fillUserTypesList(vuserTypes, vid)
 {
    //Purpose: It fills user types list.
    //Limitations: The user type control must exist in the page WEB and the user types (data) must exist in format JSON.  
    //Returns: None.
    
    var vstringJSON='[{"id":-1, "text":"--Seleccionar--"}';
    for(vi=0; vi<vuserTypes.length; vi++){
        vstringJSON+=', {"id":' + vuserTypes[vi].idUserType;
        vstringJSON+=',  "text":"' + vuserTypes[vi].userType + '"}';
    }
    vstringJSON+="]";
    var vobjectJSON=jQuery.parseJSON(vstringJSON);
    
    $("#cmbuserType").select2({
        data:vobjectJSON,
        placeholder: {
            id: -1,
            text: "--Seleccionar--"
        },
        language:{
            noResults: function() {
                return "Sin tipos de usuario encontrados.";
            }
        }, 
    });
    if ( vid!=0 ){
        $("#cmbuserType").select2("val", vid);
    }
 }

function fillCampusesList(vcampuses, vid)
 {
    //Purpose: It fills campuses list.
    //Limitations: The campus control must exist in the page WEB and the campuses (data) must exist in format JSON.  
    //Returns: None.
    
    var vstringJSON='[{"id":-1, "text":"--Seleccionar--"}';
    for(vi=0; vi<vcampuses.length; vi++){
        vstringJSON+=', {"id":' + vcampuses[vi].idCampus;
        vstringJSON+=',  "text":"' + vcampuses[vi].campus + '"}';
    }
    vstringJSON+="]";
    var vobjectJSON=jQuery.parseJSON(vstringJSON);
    
    $("#cmbcampus").select2({
        data:vobjectJSON,
        placeholder: {
            id: -1,
            text: "--Seleccionar--"
        },
        language:{
            noResults: function() {
                return "Sin campus encontrados.";
            }
        }, 
    });
    if ( vid!=0 ){
        $("#cmbcampus").select2("val", vid);
    }
 }

function fillUserStatusesList(vuserStatuses, vid)
 {
    //Purpose: It fills user status list.
    //Limitations: The user status control must exist in the page WEB and the user status (data) must exist in format JSON.  
    //Returns: None.
    
    var vstringJSON='[{"id":-1, "text":"--Seleccionar--"}';
    for(vi=0; vi<vuserStatuses.length; vi++){
        vstringJSON+=', {"id":' + vuserStatuses[vi].idUserStatus;
        vstringJSON+=',  "text":"' + vuserStatuses[vi].userStatus + '"}';
    }
    vstringJSON+="]";
    var vobjectJSON=jQuery.parseJSON(vstringJSON);
    
    $("#cmbuserStatus").select2({
        data:vobjectJSON,
        placeholder: {
            id: -1,
            text: "--Seleccionar--"
        },
        language:{
            noResults: function() {
                return "Sin estados de usuario encontrados.";
            }
        }, 
    });
    if ( vid>=0 ){
        $("#cmbuserStatus").select2("val", vid);
    }
 }

function setButtons()
 {
    //Purpose: It sets buttons of the page WEB (users-record).
    //Limitations: The buttons must exist in the page WEB.  
    //Returns: None.
    
    $("#btnbackToUsers").attr("onclick", "location.href='./frmusers.php';");
    $("#btnnewUser").attr("onclick", "newUserData();");
    $("#btnsaveUserData").attr("onclick", "recordUserData();");
 }

function showUserData()
 {
    //Purpose: It shows user data.
    //Limitations: The user data must exist in database server and brought by service rest (users/{idUser}).
    //Returns: None.
    
    var vtitle="Visualizaci�n de Datos de Usuario.";
    
    $.ajax({
		type: "get",
		url: vrootURL + "/users/" + vidUser,
		dataType: "json",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurri� un error al tratar de mostrar los datos del usuario, intente de nuevo.", vtitle, 0);
                           break;
                case 0:    toastrAlert("Los datos del usuario no se encuentran registrados.", vtitle, 3);
                           break;
                case 1:    setUserDataToFormControls(vresponse.user);
                           break;
                default:   toastrAlert("Ocurri� un error al tratar de mostrar los datos del usuario.", vtitle, 0);
                           break;
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
 }

function setUserDataToFormControls(vuser)
 {
    //Purpose: It sets user data to form controls.
    //Limitations: The user data must be in JSON format.  
    //Returns: None.
    
    $("#txtname").val(vuser.name);
    $("#txtfirstName").val(vuser.firstName);
    $("#txtlastName").val(vuser.lastName);
    setUserTypesList(vuser.userType.idUserType);
    if (vidUserType==1){
        $("#vcampus").html(getCampusControls());
        setCampusesList(vuser.campus.idCampus);
    }
    setUserStatusesList(vuser.userStatus.idUserStatus);
    $("#txtemailAccount").val(vuser.emailAccount);
 }

function newUserData()
 {
    //Purpose: It enables form fields controls in order to add a new user.
    //Limitations: The main page WEB must exist.  
    //Returns: None.
    
    vidUser=0;
    $("#vpageTitle").html("Registro - Usuario");
    cleanUserFormFields();
    $("#cmbuserStatus").prop( "disabled", true);
    $("#txtname").focus();
    $("#scroll-to-top").click();
 }

function recordUserData()
 {
    //Purpose: It records (add/update) users data.
    //Limitations: The page WEB controls must have data.  
    //Returns: None.
    
    if ( vidUser==0 ){
        if( validateUserFormFields(1) ){
            addUserData();
        }
		else{
            toastrAlert("El registro de los datos del usuario no puede ser realizado, no tiene los datos necesarios, complete los datos.", "Registro de Datos de Usuario.", 3);
        }
    }
	else{
        if( validateUserFormFields(2) ){
            updateUserData();
        }
        else{
            toastrAlert("La modificaci�n de los datos del usuario no puede ser realizada, no tiene los datos necesarios, complete los datos.", "Modificaci�n de Datos de Usuario.", 3);
        }
    }
 }

function addUserData()
 {
    //Purpose: It adds user data.
    //Limitations: The user data must not exist in database server and brought by service rest (users).  
    //Returns: None.
    
    var vjson=$("#vuserData").serializeObject();
    var vproperty="cmbuserType";
    vjson[vproperty]=trim($("#cmbuserType").val());
    if ( vidUserType==1 ){
        vproperty="cmbcampus";
        vjson[vproperty]=trim($("#cmbcampus").val());
    }
    vproperty="cmbuserStatus";
    vjson[vproperty]=trim($("#cmbuserStatus").val());
    
    var vtitle="Registro de Datos de Usuario.";
    
    $.ajax({
		type: "post",
		url: vrootURL + "/users",
		dataType: "json",
        data: JSON.stringify(vjson),
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurri� un error al tratar de registrar al usuario, intente de nuevo.", vtitle, 0);
                           break;
                case -3:   toastrAlert("Imposible registrar al usuario. <br />No se envi� la contrase�a al correo electr�nico especificado.", vtitle, 3); 
                           break;
                case -2:   toastrAlert("Imposible registrar al usuario, no se encuentra configurado los datos del correo de salida.", vtitle, 3); 
                           break;
                case -1:   toastrAlert("Imposible registrar al usuario, intente de nuevo", vtitle, 3);
                           break;
                case 0:    toastrAlert("Imposible registrar al usuario, ya existe un usuario registrado con el correo electr�nico proporcionado", vtitle, 3);
                           break;     
                case 1:    vidUser=vresponse.idUser;
                           $("#vpageTitle").html("Modificaci�n - Usuario");
                           $("#cmbuserStatus").prop( "disabled", false);
                           toastrAlert("Los datos del usuario <" + $("#txtname").val() + ' ' + $("#txtfirstName").val() + ' ' + 
                                       $("#txtlastName").val() + "> han sido registrados correctamente. <br />La contrase�a ha sido enviada al correo electr�nico proporcionado.", vtitle, 1);
                           clearUserFormFields();
                           break;
                default:   toastrAlert("Ocurri� un error al tratar de registrar al usuario.", vtitle, 0);
                           break;
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
 }

function updateUserData()
 {
    //Purpose: It updates user data.
    //Limitations: The user data must be exist in database server and brought by service rest (users/{idUser}).  
    //Returns: None.
    
    var vjson=$("#vuserData").serializeObject();
    var vproperty="cmbuserType";
    vjson[vproperty]=trim($("#cmbuserType").val());
    if ( vidUserType==1 ){
        vproperty="cmbcampus";
        vjson[vproperty]=trim($("#cmbcampus").val());
    }
    vproperty="cmbuserStatus";
    vjson[vproperty]=trim($("#cmbuserStatus").val());
    
    var vtitle="Modificaci�n de Datos de Usuario.";
    
    $.ajax({
		type: "put",
		url: vrootURL + "/users/" + vidUser,
		dataType: "json",
        data: JSON.stringify(vjson),
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurri� un error al tratar de modificar los datos del usuario, intente de nuevo.", vtitle, 0);
                           break;
                case -1:   toastrAlert("Imposible modificar el correo electr�nico proporcionado, ya se encuentra registrado por otro usuario.", vtitle, 3);
                           break;     
                case 0:    toastrAlert("Ning�n dato se ha modificado del usuario.", vtitle, 3);
                           break;
                case 1:    toastrAlert("Los datos del usuario han sido modificados correctamente.", vtitle, 1);
                           clearUserFormFields(); 
                           break;
                default:   toastrAlert("Ocurri� un error al tratar de modificar los datos del usuario.", vtitle, 0);
                           break;
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
 }

function cleanUserFormFields()
 {
    //Purpose: It cleans user form fields.
    //Limitations: The user form fields must exist.
    //Returns: None.
    
    $("#txtname").val("");
    $("#txtfirstName").val("");
    $("#txtlastName").val("");
    $("#cmbuserType").select2("val", -1);
    if ( vidUserType==1 ){
        $("#cmbcampus").select2("val", -1);
    }
    $("#cmbuserStatus").select2("val", 2);
    $("#txtemailAccount").val("");
    clearUserFormFields();
 }

function getCampusControls()
 {
    //Purpose: It gets campus controls.
    //Limitations: None.
    //Returns: Campus controls in HTML5 code.
    
    var vhtml ='<label for="cmbcampus"><strong>Campus:</strong></label>';
        vhtml+='<select id="cmbcampus" class="form-control form-white" tabindex="-1" style="display: none; width: 100%"></select>';
        
    return vhtml;
 }


function validateUserFormFields(vtype)
 {
    //Purpose: It validates users form fields.
    //Limitations: The user form fields must exist.
    //Returns: None.
    
	var vstatus=true;
	
    clearUserFormFields();
    if ( trim($("#txtname").val()) == "" ){
        $("#vname").addClass("has-warning");
        $("#txtname").focus();
        vstatus=false;
    }
    if ( trim($("#txtfirstName").val()) == "" ){
        $("#vfirstName").addClass("has-warning");
        if ( vstatus ){
            $("#txtfirstName").focus();
        }
        vstatus=false;
    }
    if ( trim($("#txtlastName").val()) == "" ){
        $("#vlastName").addClass("has-warning");
        if ( vstatus ){
            $("#txtlastName").focus();
        }
        vstatus=false;
    }
    if ( ($("#cmbuserType").val() == -1) || (trim($("#cmbuserType").val())=="") ){
        $("#vuserType").addClass("has-warning");
        if ( vstatus ){
            $("#cmbuserType").focus();
        }
        vstatus=false;
    }
    if ( (vidUserType==1) && (($("#cmbcampus").val() == -1) || (trim($("#cmbcampus").val())=="")) ){
        $("#vcampus").addClass("has-warning");
        if ( vstatus ){
            $("#cmbcampus").focus();
        }
        vstatus=false;
    }
    if ( ($("#cmbuserStatus").val()== -1) || (trim($("#cmbuserStatus").val())=="") ){
        $("#vuserStatus").addClass("has-warning");
        if ( vstatus ){
            $("#cmbuserStatus").focus();
        }
        vstatus=false;
    }
    if ( trim($("#txtemailAccount").val()) == "" ){
        $("#vemailAccount").addClass("has-warning");
        if ( vstatus ){
            $("#txtemailAccount").focus();
        }
        vstatus=false;
    }
    else if ( ! isEmail(trim($("#txtemailAccount").val())) ){
        var vtitle="Registro de Datos de Usuario.";
        if ( vtype==2 ){
            vtitle="Modificaci�n de Datos de Usuario.";
        }
		toastrAlert("La cuenta de correo electr�nico debe tener un formato v�lido.", vtitle, 3);
        $("#vemailAccount").addClass("has-warning");
        if ( vstatus ){
		  $("#txtemailAccount").focus();
        }
		vstatus=false;
	}
    
    return vstatus;
 }
 
function clearUserFormFields()
 {
    //Purpose: It clears user form fields.
    //Limitations: The class "has-warning" must exist in user form fields.
    //Returns: None.
    
    if ( $("#vname").hasClass("has-warning") ){
        $("#vname").removeClass("has-warning");
    }
    if ( $("#vfirstName").hasClass("has-warning") ){
        $("#vfirstName").removeClass("has-warning");
    }
    if ( $("#vlastName").hasClass("has-warning") ){
        $("#vlastName").removeClass("has-warning");
    }
    if ( $("#vuserType").hasClass("has-warning") ){
        $("#vuserType").removeClass("has-warning");
    }
    if ( vidUserType==1 && $("#vcampus").hasClass("has-warning") ){
        $("#vcampus").removeClass("has-warning");
    }
    if ( $("#vuserStatus").hasClass("has-warning") ){
        $("#vuserStatus").removeClass("has-warning");
    }
    if ( $("#vemailAccount").hasClass("has-warning") ){
        $("#vemailAccount").removeClass("has-warning");
    }
 }