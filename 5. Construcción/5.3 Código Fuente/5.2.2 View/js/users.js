/********************************************************
Name: users.js
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodr�guez
Modification autor name: Edwuard H. Cabrera Rodr�guez
Creation date: 29/05/2017
Modification date: 03/07/2017
Description: JS file. It provides support between view and controller files, in order to show the users list page.
********************************************************/

var vrootURL="./controller/users.php";
var vconfirmDeleteUser;
var vidUser=0;
var vidUserType=0;

$(document).ready(function(){
    //Purpose: It sets default options of the page WEB (users).
    //Limitations: The current page (view) must be fully loaded.  
    //Returns: None.
    
    menu(1, 2);
    verifyAccessPrivileges();
});

$('#vwndwshowUserAccessPrivileges').on('show.bs.modal', function (event) {
    //Purpose: It shows user access privileges windows modal and data
    //Limitations: The end user must do clic on add user access privilege button, the user access privileges data must exist in database server and brought by service rest (users/{idUser}/accessPrivileges).  
    //Returns: None.
    
    $.ajax({
		type: "get",
		url: vrootURL + "/users/" + vidUser + "/accessPrivileges",
		dataType: "json",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: var vhtml ='<p style="text-align:center" class="text-danger">';
                               vhtml+='	<strong>Ocurri� un error al tratar de mostrar sus privilegios de acceso, intente de nuevo.</strong>';
                               vhtml+='</p>';
                           $("#vaccessPrivileges").html(vhtml);
                           break;
                case 0:    var vhtml ='<p style="text-align:center" class="text-warning">';
                               vhtml+='	<strong>No tiene privilegios de acceso registrados.</strong>';
                               vhtml+='</p>'; 
                           $("#vaccessPrivileges").html(vhtml); 
                           break;
                case 1:    fillUserAccessPrivilegesTree(vresponse[0].accessPrivileges, vresponse[1].backendUserAccessPrivileges);
                           break;
                default:   var vhtml ='<p style="text-align:center" class="text-danger">';
                               vhtml+='	<strong>Ocurri� un error al tratar de mostrar sus privilegios de acceso.</strong>';
                               vhtml+='</p>';
                           $("#vaccessPrivileges").html(vhtml);
                           break;            
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, "Visualizaci�n de Privilegios de Acceso.", 0);
		}
	});
});

function verifyAccessPrivileges()
 {
	//Purpose: It verifies access privileges (end user) in order to set controls of the page WEB (users).
    //Limitations: The user access privileges data must exist in database server and brought by service rest (users/{idUser}/accessPrivileges).   
    //Returns: None.
    
    var verrorMessage="";
    $.ajax({
		type: "get",
		url: vrootURL + "/users/0/accessPrivileges",
		dataType: "json",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: verrorMessage ="Ocurri� un error al tratar de obtener sus privilegios de acceso.<br />";
                           verrorMessage+="Intente de nuevo.";
                           openError(verrorMessage);
                           break;
                case 0:    verrorMessage ="No existen privilegios de acceso registrados.<br />";
                           verrorMessage+="Verifiquelo con el Administrador del Sistema.";
                           openError(verrorMessage);
                           break;
                case 1:    if ( vresponse[1].backendUserAccessPrivileges.length>0 ){
                                setControls(vresponse[1].backendUserAccessPrivileges);
                           }
                           else{
                                verrorMessage ="No tiene privilegios de acceso registrados.<br />";
                                verrorMessage+="Verifiquelo con el Administrador del Sistema.";
                                openError(verrorMessage);
                           }
                           break;
                default:   verrorMessage ="Ocurri� un error al tratar de obtener sus privilegios de acceso.";
                           openError(verrorMessage);
                           break;
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            verrorMessage=verrorThrown + "<br />";
            verrorMessage+="Verifiquelo con el Administrador del Sistema.";
            openError(verrorMessage);
		}
	});
}

function setControls(vbackendUserAccessPrivileges)
 {
    //Purpose: It sets controls of the page WEB (users).
    //Limitations: The user access privileges data must exist in format JSON and the controls must exist in the page WEB (users).  
    //Returns: None.
    
    if ( (vbackendUserAccessPrivileges[0].backendUser.userType.idUserType!=1) && 
         (vbackendUserAccessPrivileges[0].backendUser.userType.idUserType!=2) &&
         (vbackendUserAccessPrivileges[0].backendUser.userType.idUserType!=3) ){
        verrorMessage ="Acceso Denegado.<br />";
        verrorMessage+="No tiene privilegios para acceder al m�dulo de usuarios.";
        openError(verrorMessage);
    }
    $("#txtname").attr("onkeypress", "showUsersListByEnterKey(event); return setEntriesType(event, 'letter');");
    $("#txtfirstName").attr("onkeypress", "showUsersListByEnterKey(event); return setEntriesType(event, 'letter'); ");
    $("#txtlastName").attr("onkeypress", "showUsersListByEnterKey(event); return setEntriesType(event, 'letter');");
    $("#btnacceptFilter").attr("onclick", "showUsersList();");
    $("#btncancelFilter").attr("onclick", "cancelUsersList();");
    $("#btnrecordUserAcessPrivileges").attr("onclick", "recordUserAccessPrivileges();");
    $("#btnaddUser").attr("onclick", "openUserRecord();");
    $("#btnupdateUser").attr("onclick", "openUserUpdate();");
    $("#btndeleteUser").attr("onclick", "deleteUser();");
    
    showUsersList();
 }

function showUsersList()
 {
    //Purpose: It shows users list.
    //Limitations: The users data must exist in database server and brought by service rest (users).  
    //Returns: None.
    
    disableMainButtons();
    $.ajax({
		type: "get",
		url: vrootURL + "/users",
		dataType: "json",
        data: $('#vfilterData').serialize(),
        success: function(vresponse, vtextStatus, vjqXHR){
             switch(vresponse.messageNumber){
                case -100: var vhtml ='<p style="text-align:center" class="text-danger">';
                               vhtml+='	<strong>Ocurri� un error al tratar de mostrar los usuarios, intente de nuevo.</strong>';
                               vhtml+='</p>';
                           $("#vusersList").html(vhtml);
                           break;
                case 0:    var vhtml ='<p style="text-align:center" class="text-warning">';
                               vhtml+='	<strong>No existen usuarios registrados.</strong>';
                               vhtml+='</p>';
                           $("#vusersList").html(vhtml);
                           break;
                case 1:    var vusers=null;
                           if (vresponse.users.users!=null ){
                                vusers=vresponse.users.users;
                           }
                           else{
                                vusers=vresponse.users.backendUsers;
                           }
                           fillUsersList(vusers);
                           break;
                default:   var vhtml ='<p style="text-align:center" class="text-danger">';
                               vhtml+='	<strong>Ocurri� un error al tratar de mostrar los usuarios.</strong>';
                               vhtml+='</p>';
                           $("#vusersList").html(vhtml);
                           break;
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, "Visualizaci�n de Usuarios.", 0);
		}
	});
 }

function showUsersListByEnterKey(vgetKeyCode)
 {
    //Purpose: It shows users list when the user type enter key.
    //Limitations: The users must be type enter key in the inputs: txtname, txtfirstName and txtlastName.  
    //Returns: None.
    
    if ( isEnter(vgetKeyCode) ){
        $("#btnacceptFilter").click();
    }
 }

function cancelUsersList()
 {
    //Purpose: It cancel users list.
    //Limitations: None.  
    //Returns: None.
    
    cleanUserFormFilterFields();
	showUsersList();
 }
 
function fillUsersList(vbackendUsers)
 {
    //Purpose: It fills users list.
    //Limitations: The users data must exist in format JSON.
    //Returns: None.
    
    var vhtml ='<div class="table-responsive">';
        vhtml+='	<table id="vgrdusersList" class="table table-hover">';
        vhtml+='		<thead>';
        vhtml+='			<tr>';
        vhtml+='				<th style="text-align:center;" class="no-sort">&nbsp;</th>';
		vhtml+='				<th style="text-align:center;">Nombre</th>';
        vhtml+='				<th style="text-align:center;">Cuenta de Correo Electr�nico</th>';
        vhtml+='				<th style="text-align:center;">Tipo</th>';
        vhtml+='				<th style="text-align:center;">Estado</th>';
        vhtml+='			</tr>';
        vhtml+='		</thead>';
        vhtml+='		<tbody>';
    for(vi=0; vi<vbackendUsers.length; vi++){
        vhtml+='			<tr id="vuserListRow' + vbackendUsers[vi].idUser + '">';
        vhtml+='				<td style="text-align:center;">';
        vhtml+='					<label>';
        vhtml+='						<input type="radio" name="cmruser" value="' + vbackendUsers[vi].idUser + "-" +
                                                                                      vbackendUsers[vi].userType.idUserType + '" title="Seleccionar Usuario" class="radio">';
        vhtml+='					</label>';
        vhtml+='				</td>';
        vhtml+='				<td style="text-align:left;">' + vbackendUsers[vi].name + " "
                                                               + vbackendUsers[vi].firstName + " "
                                                               + vbackendUsers[vi].lastName;
        vhtml+='				</td>';
        vhtml+='				<td style="text-align:left;">' + vbackendUsers[vi].emailAccount + '</td>';
        vhtml+='				<td style="text-align:center;">' + vbackendUsers[vi].userType.userType + '</td>';
        vhtml+='				<td style="text-align:center;">';
        vhtml+='					<span ';
        switch (vbackendUsers[vi].userStatus.idUserStatus){
            case 0:  vhtml+='class="label label-danger">';
                     break;
            case 1:  vhtml+='class="label label-success">';
                     break;
            default: vhtml+='class="label label-warning">';
                     break;
        }
        vhtml+=vbackendUsers[vi].userStatus.userStatus;
        vhtml+='					</span>';
        vhtml+='				</td>';
        vhtml+='			</tr>';
    }
        vhtml+='		</tbody>';
        vhtml+='	</table>';
        vhtml+='</div>';
    $("#vusersList").html(vhtml);
    
    setUsersList();
    setRadioButtons();
 }

function fillUserAccessPrivilegesTree(vaccessPrivileges, vbackendUserAccessPrivileges)
 {
    //Purpose: It fills user access privileges tree.
    //Limitations: The user access privileges data must exist in format JSON.    
    //Returns: None.
    
    var vstringJSON="[";
    var vbranch=[false, false, false];
    var vaccessPrivilegesTotal=vaccessPrivileges.length;
    
    for (var vi=0; vi<vaccessPrivilegesTotal; vi++){
        vstringJSON+='{"id":' + vaccessPrivileges[vi].idAccessPrivilege;
        vstringJSON+=',"text":' + '"' + trim(vaccessPrivileges[vi].accessPrivilege) + '"';
        if ( isUserAccessPrivilege(vaccessPrivileges[vi].idAccessPrivilege, vbackendUserAccessPrivileges) ){
           vstringJSON+=',"state":{"selected":true}';
        }
        else if( vaccessPrivileges[vi].idAccessPrivilege==2 ){
            vstringJSON+=',"state":{"disabled":true}';
        }
        if ( vaccessPrivileges[vi].accessPrivilegeType.idAccessPrivilegeType==5 ){
            vstringJSON+=',"type":"action"';
        }
        if ( trim(vaccessPrivileges[vi].level2)=="00" ){
            if ( (vi+1)<vaccessPrivilegesTotal ){
                if ( trim(vaccessPrivileges[vi].level1)==trim(vaccessPrivileges[vi+1].level1) ){
                    vbranch[0]=true;
                    vbranch[1]=false;
                    vbranch[2]=false;
                    vstringJSON+=', "children": [';
                }
                else{
                    vstringJSON+='}, ';
                }
            }
            else{
                if( vbranch[0]==true ){
                    vstringJSON+=']';
                }
                
                vstringJSON+='}';
            }
        }
        else if ( trim(vaccessPrivileges[vi].level3)=="00" ){
            if ( (vi+1)<vaccessPrivilegesTotal ){	
                if ( trim(vaccessPrivileges[vi].level2)==trim(vaccessPrivileges[vi+1].level2) ){
                    vbranch[1]=true;
                    vbranch[2]=false;
                    vstringJSON+=', "children": [';
                }
                else{
                    if ( (trim(vaccessPrivileges[vi].level1)!=trim(vaccessPrivileges[vi+1].level1)) && (vbranch[0]==true) ){
                        vbranch[0]=false;
                        vstringJSON+='}]';
                    }
                    vstringJSON+='}, ';
                }
            }
            else{
                if( vbranch[0]==true ){
                    vstringJSON+='}]';
                }
                vstringJSON+='}';
            }
        }
        else if ( trim(vaccessPrivileges[vi].level4)=="00" ){
            if ( (vi+1)<vaccessPrivilegesTotal ){	
                if ( trim(vaccessPrivileges[vi].level3)==trim(vaccessPrivileges[vi+1].level3) ){
                    vbranch[2]=true;
                    vstringJSON+=', "children": [';
                }
                else{
                    if ( (trim(vaccessPrivileges[vi].level2)!=trim(vaccessPrivileges[vi+1].level2)) && (vbranch[1]==true) ){
                        vbranch[1]=false;
                        vstringJSON+='}]';
                    }
                    if ( (trim(vaccessPrivileges[vi].level1)!=trim(vaccessPrivileges[vi+1].level1)) && (vbranch[0]==true) ){
                        vbranch[0]=false;
                        vstringJSON+='}]';
                    }
                    vstringJSON+='}, ';
                }
            }
            else{
                if( vbranch[1]==true ){
                    vstringJSON+='}]';
                }
                if( vbranch[0]==true ){
                    vstringJSON+='}]';
                }
                vstringJSON+='}';
            }
        }
        else{
            if ( (vi+1)<vaccessPrivilegesTotal ){	
                if ( trim(vaccessPrivileges[vi].level3)==trim(vaccessPrivileges[vi+1].level3) ){
                    vstringJSON+='}, ';
                }
                else{
                    if ( vbranch[2]==true ){
                        vbranch[2]=false;
                        vstringJSON+='}]';
                    }
                    if ( (trim(vaccessPrivileges[vi].level2)!=trim(vaccessPrivileges[vi+1].level2)) && (vbranch[1]==true) ){
                        vbranch[1]=false;
                        vstringJSON+='}]';
                    }
                    if ( (trim(vaccessPrivileges[vi].level1)!=trim(vaccessPrivileges[vi+1].level1)) && (vbranch[0]==true) ){
                        vbranch[0]=false;
                        vstringJSON+='}]';
                    }
                    vstringJSON+='}, ';
                }
            }
            else{
                if( vbranch[2]==true ){
                    vstringJSON+='}]';
                }
                if( vbranch[1]==true ){
                    vstringJSON+='}]';
                }
                if( vbranch[0]==true ){
                    vstringJSON+='}]';
                }
                vstringJSON+='}';
            }
        }
    }
    vstringJSON+="]";
    var vobjectJSON=jQuery.parseJSON(vstringJSON);
    $("#vaccessPrivileges").html("<div id='vuserAccessPrivilegesTree'>&nbsp;</div>");
    $('#vuserAccessPrivilegesTree').jstree({
        'core': {
            'data': vobjectJSON,
            'themes': {
				'responsive': true
			}
        },
        'types': {
            'default': {
                'icon': 'fa fa-folder icon-state-info icon-md'
            },
            'action': {
                'icon': 'fa fa-wrench icon-state-info icon-md'
            }
        },
        'plugins': ['types', 'checkbox']
    });
 }

function setUsersList()
 {
    //Purpose: It sets users list.
    //Limitations: The users must exist in users list.  
    //Returns: None.
    
    $("#vgrdusersList").dataTable({
        "bLengthChange": false,
        "searching": false,
        "processing": true,
        "displayLength": 10,
        "columnDefs": [{
            "targets": 'no-sort',
            "orderable": false}],
        "language": {
            "sProcessing": "Procesando...",
            "sInfo": "_START_ - _END_ de _TOTAL_ usuarios",
            "sInfoFiltered": "(filtrado de un total de _MAX_ usuarios)",
            "sInfoPostFix":    "",
            "sUrl": "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "�ltimo",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
    $("#vgrdusersList").on("draw.dt", function(){
        setRadioButtons();  
    });
}

function setRadioButtons()
 {
    //Purpose: It sets radio buttons.
    //Limitations: The radio buttons must be created.  
    //Returns: None.
    
    $('input[type="checkbox"], input[type="radio"].radio').iCheck({
        checkboxClass:'icheckbox-primary',
        radioClass:'iradio_flat-blue'
    }).on('ifClicked', function(e){
        setUserIds(this.value);
    });
 }

function isUserAccessPrivilege(vidAccessPrivilege, vbackendUserAccessPrivileges)
 {
    //Purpose: It verifies if exist a user access privilege in access privileges.
    //Limitations: The user access privilege and access privileges data must exist in format JSON.    
    //Returns: True or false.
    
    var visUserAccessPrivilege=false;
    var vbackendUserAccessPrivilegesTotal=vbackendUserAccessPrivileges.length;
    
    for (var vj=0; vj<vbackendUserAccessPrivilegesTotal; vj++){
        if ( vidAccessPrivilege==vbackendUserAccessPrivileges[vj].accessPrivilege.idAccessPrivilege ){
            visUserAccessPrivilege=true;
            vj=vbackendUserAccessPrivilegesTotal;
        }  
    }
    
    return visUserAccessPrivilege;
 }

function setUserIds(vvalue)
 {
    //Purpose: It sets user identifier.
    //Limitations: The user identifier must exist in users list.  
    //Returns: None.
    
    vidUser=parseInt(getStringMatch("-", vvalue, 0, 1));
    vidUserType=parseInt(getStringMatch("-", vvalue, 1, 2));
    enableMainButtons();
 }

function recordUserAccessPrivileges()
 {
    //Purpose: It records user access privileges data.
    //Limitations: The user access privileges must exit in access privileges tree, almost selected one and brought by service rest (users/{idUser}/accessPrivileges).
    //Returns: None.
    
    var vcheckedUserAcessPrivileges=[];
    var vtitle="Registro de Privilegios de Acceso.";
    
    if ( $('#vuserAccessPrivilegesTree').jstree(true) ){
        var vcheckedUserAcessPrivilegesTree = $('#vuserAccessPrivilegesTree').jstree("get_selected", true);
        $.each(vcheckedUserAcessPrivilegesTree, function(){
            vcheckedUserAcessPrivileges.push(this.id);
        });
        
        if ( vcheckedUserAcessPrivileges.length>=1 ){
            $.ajax({
                type: "post",
                url: vrootURL + "/users/" + vidUser + "/accessPrivileges",
                dataType: "json",
                data: JSON.stringify(vcheckedUserAcessPrivileges),
                success: function(vresponse, vtextStatus, vjqXHR){
                    switch(vresponse.messageNumber){
                        case -100: toastrAlert("Ocurri� un error al tratar de registrar los privilegios de acceso al usuario, intente de nuevo.", vtitle, 0);
                                   break;     
                        case 0:    toastrAlert("Imposible registrar los privilegios de acceso al usuario, intente de nuevo.", vtitle, 3);
                                   break;
                        default:   toastrAlert("Los privilegios de acceso del usuario han sido registrados correctamente.", vtitle, 1);
                                   break;
                    }
                },
                error: function(vjqXHR, vtextStatus, verrorThrown){
                    toastrAlert(verrorThrown, vtitle, 0);
                }
            });
        }
        else{
            toastrAlert("Imposible registrar privilegios de acceso al usuario, seleccione almenos uno.", vtitle, 3);
        }
    }
    else{
        toastrAlert("Imposible registrar privilegios de acceso al usuario, no existen.", vtitle, 3);
    }
 }

function openUserRecord()
 {
    //Purpose: It opens user record page WEB.
    //Limitations: The user must be clic on add user button.
    //Returns: None.
    
    var vhtml ='<form id="vform" style="display:hidden" action="./frmusers-record.php" method="POST">';
        vhtml+='	<input type="hidden" id="vidUser" name="vidUser" value="0" />';
		vhtml+='</form>';
	$('<div id="vformContent"></div>').appendTo('body');
	$("#vformContent").append(vhtml);
	$("#vform").submit();
 }

function openUserUpdate()
 {
    //Purpose: It opens user update page WEB.
    //Limitations: The user must be clic on update user button.  
    //Returns: None.
    
    var vhtml ='<form id="vform" style="display:hidden" action="./frmusers-record.php" method="POST">';
        vhtml+='	<input type="hidden" id="vidUser" name="vidUser" value="' + vidUser + '" />';
		vhtml+='</form>';
	$('<div id="vformContent"></div>').appendTo('body');
	$("#vformContent").append(vhtml);
	$("#vform").submit();
 }

function deleteUser()
 {
    //Purpose: It deletes user.
    //Limitations: The end user must accept the deleting user.
    //Returns: None.
    
    var vacceptFunctionName="deleteUserData();";
    var vcancelFunctionName="cancelUserRemoval();";
    var vmessage="�Est� seguro que desea eliminar el usuario?.";
    var vtitle="Eliminaci�n de Usuario.";
    
    vconfirmDeleteUser=toastrConfirm(vacceptFunctionName, vcancelFunctionName, vmessage, vtitle);
 }

function cancelUserRemoval()
 {
    //Purpose: It cancels deleting user.
    //Limitations: None.
    //Returns: None.
    
    vconfirmDeleteUser.remove();
    toastr.clear(vconfirmDeleteUser, { force: true });
    toastrAlert("La eliminaci�n del usuario ha sido cancelada.", "Eliminaci�n de Usuario.", 2);
 }

function deleteUserData()
 {
    //Purpose: It deletes user data.
    //Limitations: The user data must exist in database server and brought by service rest (users/{idUser}).
    //Returns: None.
    
    var vtitle="Eliminaci�n de Usuario.";
    vconfirmDeleteUser.remove();
    toastr.clear(vconfirmDeleteUser, { force: true });
    
	$.ajax({
	   type: 'delete',
        url: vrootURL + "/users/" + vidUser,
		dataType: "json",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurri� un error al tratar de eliminar los datos del usuario, intente de nuevo.", vtitle, 0);
                           break;
                case -1:   toastrAlert("Imposible eliminar al usuario, intente de nuevo (1).", vtitle, 3);
                           break;     
                case 0:    toastrAlert("Imposible eliminar al usuario, intente de nuevo (2).", vtitle, 3);
                           break;
                case 1:    $("#vuserListRow" + vidUser).remove();
                           disableMainButtons();
                           toastrAlert("Los datos del usuario han sido eliminados correctamente.", vtitle, 1);
                           break;
                default:   toastrAlert("Ocurri� un error al tratar de eliminar los datos del usuario.", vtitle, 0);
                           break;
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});    
 }

function enableMainButtons()
 {
    //Purpose: It enables main buttons: View user access privileges, update user and delete user.
    //Limitations: The main buttons must be disable.  
    //Returns: None.
    if ( vidUserType==5 ){
        $("#btnshowUserAccessPrivileges").prop( "disabled", false);
    }
    else{
        $("#btnshowUserAccessPrivileges").prop( "disabled", true);
    }
    $("#btnupdateUser").prop( "disabled", false);
    $("#btndeleteUser").prop( "disabled", false);
 }

function disableMainButtons()
 {
    //Purpose: It disables main buttons: View user access privileges, update user and delete user.
    //Limitations: The main buttons must be enable.  
    //Returns: None.
    
    $("#btnshowUserAccessPrivileges").prop( "disabled", true);
    $("#btnupdateUser").prop( "disabled", true);
    $("#btndeleteUser").prop( "disabled", true);
 }

function cleanUserFormFilterFields()
 {
    //Purpose: It cleans user form filter fields.
    //Limitations: The user form filter fields must exist.
    //Returns: None.
    
    $("#txtname").val("");
    $("#txtfirstName").val("");
    $("#txtlastName").val("");
 }