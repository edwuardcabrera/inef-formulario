﻿/********************************************************
Name: my-profile.js
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 26/05/2017
Modification date: 30/06/2017
Description: Javascript file. It provides support between view and controller files, in order to show the my profile.
********************************************************/

var vrootURL = "./controller/users.php";
var vcampusesURL = "./controller/campuses.php";
var vidUser=0;

$('#vwndwshowUserAccessPrivileges').on('show.bs.modal', function (event) {
    //Purpose: It shows user access privileges windows modal and data
    //Limitations: The end user must do clic on add user access privilege button, the user access privileges data must exist in database server and brought by service rest (users/{idUser}/accessPrivileges).  
    //Returns: None.
    
    $.ajax({
		type: "get",
		url: vrootURL + "/users/" + vidUser + "/accessPrivileges",
		dataType: "json",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: var vhtml ='<p style="text-align:center" class="text-danger">';
                               vhtml+='	<strong>Ocurrió un error al tratar de mostrar sus privilegios de acceso, intente de nuevo.</strong>';
                               vhtml+='</p>';
                           $("#vaccessPrivileges").html(vhtml);
                           break;
                case 0:    var vhtml ='<p style="text-align:center" class="text-warning">';
                               vhtml+='	<strong>No tiene privilegios de acceso registrados.</strong>';
                               vhtml+='</p>'; 
                           $("#vaccessPrivileges").html(vhtml); 
                           break;
                case 1:    fillUserAccessPrivilegesTree(vresponse[0].accessPrivileges, vresponse[1].backendUserAccessPrivileges);
                           break;
                default:   var vhtml ='<p style="text-align:center" class="text-danger">';
                               vhtml+='	<strong>Ocurrió un error al tratar de mostrar sus privilegios de acceso.</strong>';
                               vhtml+='</p>';
                           $("#vaccessPrivileges").html(vhtml);
                           break;            
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, "Visualización de Privilegios de Acceso.", 0);
		}
	});
});

function config(vidUser_)
 {
    vidUser=vidUser_;
	menu(-1, 0);
	verifyAccessPrivileges();
 }

function verifyAccessPrivileges()
 {
	//Purpose: It verifies access privileges (end user) in order to set controls of the page WEB (my-profile).
    //Limitations: The user access privileges data must exist in database server and brought by service rest (users/{idUser}/accessPrivileges).   
    //Returns: None.
    
    var verrorMessage="";
    $.ajax({
		type: "get",
		url: vrootURL + "/users/0/accessPrivileges",
		dataType: "json",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: verrorMessage ="Ocurrió un error al tratar de obtener sus privilegios de acceso.<br />";
                           verrorMessage+="Intente de nuevo.";
                           openError(verrorMessage);
                           break;
                case 0:    verrorMessage ="No existen privilegios de acceso registrados.<br />";
                           verrorMessage+="Verifiquelo con el Administrador del Sistema.";
                           openError(verrorMessage);
                           break;
                case 1:    if ( vresponse[1].backendUserAccessPrivileges.length>0 ){
                                setControls();
                           }
                           else{
                                verrorMessage ="No tiene privilegios de acceso registrados.<br />";
                                verrorMessage+="Verifiquelo con el Administrador del Sistema.";
                                openError(verrorMessage);
                           }
                           break;
                default:   verrorMessage ="Ocurrió un error al tratar de obtener sus privilegios de acceso.";
                           openError(verrorMessage);
                           break;
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            verrorMessage=verrorThrown + "<br />";
            verrorMessage+="Verifiquelo con el Administrador del Sistema1.";
            openError(verrorMessage);
		}
	});
}

function setControls()
 {
    //Purpose: It sets controls of the page WEB (my-profile).
    //Limitations: The user access privileges data must exist in format JSON and the controls must exist in the page WEB (my-profile).  
    //Returns: None.
    
    showUserData();
	$("#txtname").attr("onkeypress","return setEntriesType(event,'letter');");
	$("#txtlastname").attr("onkeypress","return setEntriesType(event,'letter');");
	$("#txtfirstName").attr("onkeypress","return setEntriesType(event,'letter');");
	$("#cmdsaveData").attr('onclick','updateUserData();');
	$("#cmdsavePassword").attr('onclick','updateUserPassword();');
 }

function showUserData()
 {
    //Purpose: It shows user data.
    //Limitations: The user data must exist in database server and brought by service rest (users/{idUser}).
    //Returns: None.
    
	var vtitle="Visualización de Datos de Usuario.";
	$.ajax({
		type: "get",
		url: vrootURL + "/users/" + vidUser,
		dataType: "json",
		success: function(vresponse, vtextStatus, vjqXHR){
			switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurrió un error al tratar de mostrar sus datos, intente de nuevo.", vtitle, 0);
                           break;
                case 0:    toastrAlert("Sus datos no se encuentran registrados.", vtitle, 3);
                           break;
				case 1:    $("#txtname").val(vresponse.user.name);                    
                           $("#txtfirstName").val(vresponse.user.firstName);
                           $("#txtlastName").val(vresponse.user.lastName);
                           $("#txtemail").html("<p> "+ vresponse.user.emailAccount + "</p>");
                           $("#txttypeUser").html("<p> "+vresponse.user.userType.userType+ "</p>");
                           if ( vresponse.user.userType.idUserType!=1 ){
                                if ( vresponse.user.userType.idUserType==2 ){
                                    $("#vcampusData").html(getCampusControls(0));
                                    $("#btnupdateCampus").attr("onclick", "updateCampus();");
                                    setCampusesList(vresponse.user.campus.idCampus);
                                }
                                else if ( vresponse.user.userType.idUserType==3 || vresponse.user.userType.idUserType==5 ){
                                    $("#vcampusData").html(getCampusControls(1));
                                    $("#vcampusName").html("<p> " + vresponse.user.campus.campus + "</p>");
                                }
                           }
                           break;
                default:   toastrAlert("Ocurrió un error al tratar de mostrar sus datos.", vtitle, 0);
                           break;                                        
			}
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
			toastrAlert(verrorThrown, vtitle, 0);
		}
	});
 } 

function updateUserData()
 {
    //Purpose: It updates user data.
    //Limitations: The user data must be exist in database server and brought by service rest (users/{idUser}).  
    //Returns: None.
    
    var vjson=$("#vuserData").serializeObject();
    var vtitle="Actualización de Datos.";
    
    if( validateUserFormFields() ){
        $.ajax({
		  type: "put",
		  url: vrootURL + "/users/" + vidUser,
		  dataType: "json",
            data: JSON.stringify(vjson),
            success: function(vresponse, vtextStatus, vjqXHR){
                switch(vresponse.messageNumber){
                    case -100: toastrAlert("Ocurrió un error al tratar de modificar sus datos, intente de nuevo.", vtitle, 0);
                               break;
                    case 0:    toastrAlert("Ningún dato se ha modificado.", vtitle, 3);
                               break;
                    case 1:    toastrAlert("Sus datos se han modificado correctamente.", vtitle, 1); 
                               break;
                    default:   toastrAlert("Ocurrió un error al tratar de modificar sus datos.", vtitle, 0);
                               break;
                }
            },
            error: function(vjqXHR, vtextStatus, verrorThrown){
                toastrAlert(verrorThrown, vtitle, 0);
            }
        });
    }
    else{
        toastrAlert("La modificación de sus datos no puede ser realizada, no tiene los datos necesarios, complete los datos.", vtitle, 3);
    }
 }

function updateUserPassword()
 {
    //Purpose: It updates user password.
    //Limitations: The user password must be exist in database server and brought by service rest (users/{idUser}/password).  
    //Returns: None.
    
    var vjson=$("#vuserPasswordData").serializeObject();
    var vtitle="Modificación de Contraseña.";
    
    if( validateUserPasswordFormFields() ){
        $.ajax({
		  type: "put",
		  url: vrootURL + "/users/" + vidUser + "/password",
		  dataType: "json",
            data: JSON.stringify(vjson),
            success: function(vresponse, vtextStatus, vjqXHR){
                switch(vresponse.messageNumber){
                    case -100: toastrAlert("Ocurrió un error al tratar de modificar su contraseña, intente de nuevo.", vtitle, 0);
                               break;
                    case -1:   toastrAlert("Imposible modificar su contraseña, intente de nuevo.", vtitle, 3);
                               break;
                    case 0:    toastrAlert("Imposible modificar su contraseña, no coincide con la registrada.", vtitle, 3);
                               break;
                    case 1:    toastrAlert("Su contraseña ha sido modificada correctamente.", vtitle, 1); 
                               break;
                    default:   toastrAlert("Ocurrió un error al tratar de modificar su contraseña.", vtitle, 0);
                               break;
                }
            },
            error: function(vjqXHR, vtextStatus, verrorThrown){
                toastrAlert(verrorThrown, vtitle, 0);
            }
        });
    }
    else{
        toastrAlert("La modificación de su contraseña no puede ser realizada, no tiene los datos necesarios, complete los datos.", vtitle, 3);
    }
 }

function fillUserAccessPrivilegesTree(vaccessPrivileges, vbackendUserAccessPrivileges)
 {
	//Purpose: It fills user access privileges tree.
	//Limitations: The user access privileges data must exist in format JSON.    
	//Returns: None.
	
	var vstringJSON="[";
    var vbranch=[false, false, false];
    var vaccessPrivilegesTotal=vaccessPrivileges.length;
    
    for (var vi=0; vi<vaccessPrivilegesTotal; vi++){
        vstringJSON+='{"id":' + vaccessPrivileges[vi].idAccessPrivilege;
        vstringJSON+=',"text":' + '"' + trim(vaccessPrivileges[vi].accessPrivilege) + '"';
        vstringJSON+=',"state":{"disabled":true'
        if ( isUserAccessPrivilege(vaccessPrivileges[vi].idAccessPrivilege, vbackendUserAccessPrivileges) ){
           vstringJSON+=', "selected":true}';
        }
        if ( vaccessPrivileges[vi].accessPrivilegeType.idAccessPrivilegeType==5 ){
            vstringJSON+=',"type":"action"';
        }
        if ( trim(vaccessPrivileges[vi].level2)=="00" ){
            if ( (vi+1)<vaccessPrivilegesTotal ){
                if ( trim(vaccessPrivileges[vi].level1)==trim(vaccessPrivileges[vi+1].level1) ){
                    vbranch[0]=true;
                    vbranch[1]=false;
                    vbranch[2]=false;
                    vstringJSON+=', "children": [';
                }
                else{
                    vstringJSON+='}, ';
                }
            }
            else{
                if( vbranch[0]==true ){
                    vstringJSON+=']';
                }
                
                vstringJSON+='}';
            }
        }
        else if ( trim(vaccessPrivileges[vi].level3)=="00" ){
            if ( (vi+1)<vaccessPrivilegesTotal ){	
                if ( trim(vaccessPrivileges[vi].level2)==trim(vaccessPrivileges[vi+1].level2) ){
                    vbranch[1]=true;
                    vbranch[2]=false;
                    vstringJSON+=', "children": [';
                }
                else{
                    if ( (trim(vaccessPrivileges[vi].level1)!=trim(vaccessPrivileges[vi+1].level1)) && (vbranch[0]==true) ){
                        vbranch[0]=false;
                        vstringJSON+='}]';
                    }
                    vstringJSON+='}, ';
                }
            }
            else{
                if( vbranch[0]==true ){
                    vstringJSON+='}]';
                }
                vstringJSON+='}';
            }
        }
        else if ( trim(vaccessPrivileges[vi].level4)=="00" ){
            if ( (vi+1)<vaccessPrivilegesTotal ){	
                if ( trim(vaccessPrivileges[vi].level3)==trim(vaccessPrivileges[vi+1].level3) ){
                    vbranch[2]=true;
                    vstringJSON+=', "children": [';
                }
                else{
                    if ( (trim(vaccessPrivileges[vi].level2)!=trim(vaccessPrivileges[vi+1].level2)) && (vbranch[1]==true) ){
                        vbranch[1]=false;
                        vstringJSON+='}]';
                    }
                    if ( (trim(vaccessPrivileges[vi].level1)!=trim(vaccessPrivileges[vi+1].level1)) && (vbranch[0]==true) ){
                        vbranch[0]=false;
                        vstringJSON+='}]';
                    }
                    vstringJSON+='}, ';
                }
            }
            else{
                if( vbranch[1]==true ){
                    vstringJSON+='}]';
                }
                if( vbranch[0]==true ){
                    vstringJSON+='}]';
                }
                vstringJSON+='}';
            }
        }
        else{
            if ( (vi+1)<vaccessPrivilegesTotal ){	
                if ( trim(vaccessPrivileges[vi].level3)==trim(vaccessPrivileges[vi+1].level3) ){
                    vstringJSON+='}, ';
                }
                else{
                    if ( vbranch[2]==true ){
                        vbranch[2]=false;
                        vstringJSON+='}]';
                    }
                    if ( (trim(vaccessPrivileges[vi].level2)!=trim(vaccessPrivileges[vi+1].level2)) && (vbranch[1]==true) ){
                        vbranch[1]=false;
                        vstringJSON+='}]';
                    }
                    if ( (trim(vaccessPrivileges[vi].level1)!=trim(vaccessPrivileges[vi+1].level1)) && (vbranch[0]==true) ){
                        vbranch[0]=false;
                        vstringJSON+='}]';
                    }
                    vstringJSON+='}, ';
                }
            }
            else{
                if( vbranch[2]==true ){
                    vstringJSON+='}]';
                }
                if( vbranch[1]==true ){
                    vstringJSON+='}]';
                }
                if( vbranch[0]==true ){
                    vstringJSON+='}]';
                }
                vstringJSON+='}';
            }
        }
    }
    vstringJSON+="]";
    var vobjectJSON=jQuery.parseJSON(vstringJSON);
    $("#vaccessPrivileges").html("<div id='vuserAccessPrivilegesTree'>&nbsp;</div>");
    $('#vuserAccessPrivilegesTree').jstree({
        'core': {
            'data': vobjectJSON,
            'themes': {
				'responsive': true
			}
        },
        'types': {
            'default': {
                'icon': 'fa fa-folder icon-state-info icon-md'
            },
            'action': {
                'icon': 'fa fa-wrench icon-state-info icon-md'
            }
        },
        'plugins': ['types', 'checkbox']
    });
 }

function isUserAccessPrivilege(vidAccessPrivilege, vbackendUserAccessPrivileges)
 {
	//Purpose: It verifies if exist a user access privilege in access privileges.
	//Limitations: The user access privilege and access privileges data must exist in format JSON.    
	//Returns: True or false.
	
	var visUserAccessPrivilege=false;
	var vbackendUserAccessPrivilegesTotal=vbackendUserAccessPrivileges.length;
	
	for (var vj=0; vj<vbackendUserAccessPrivilegesTotal; vj++){
		if ( vidAccessPrivilege==vbackendUserAccessPrivileges[vj].accessPrivilege.idAccessPrivilege ){
			visUserAccessPrivilege=true;
			vj=vbackendUserAccessPrivilegesTotal;
		}  
	}
	
	return visUserAccessPrivilege;
 }

function getCampusControls(vtype)
 {
    //Purpose: It gets campus controls.
    //Limitations: None.
    //Returns: Campus controls in HTML5 code.
    
    var vhtml ='<div class="panel">';
        vhtml+='	<div class="panel-header">';
		vhtml+='		<h4 class="panel-title">';
		vhtml+='			<strong>Datos del Campus</strong>';
		vhtml+='		</h4>';
		vhtml+='	</div>';
		vhtml+='	<div class="panel-body">';
		vhtml+='		<div class="row">';
    if ( vtype==0 ){
        vhtml+='			<div style="text-align:left" class="col-md-4 form-group">';
        vhtml+='				<label for="cmbcampus">';
        vhtml+='					<strong>Campus:</strong>';
        vhtml+='				</label>';
        vhtml+='				<select id="cmbcampus" class="form-control form-white" tabindex="-1" style="display: none; width: 100%"></select>';
        vhtml+='			</div>';
        vhtml+='			<div style="text-align:left" class="col-md-8 form-group"><br />';
        vhtml+='				<button type="button" id="btnupdateCampus" class="btn btn-success m-b-sm btn-rounded btn-md" title="Modificar Campus">';
		vhtml+='					<i class="fa fa-edit m-r-xs"></i>&nbsp;&nbsp;&nbsp;Modificar';
		vhtml+='				</button>';
        vhtml+='			</div>';
    }
    else{
        vhtml+='			<div style="text-align:left" class="col-md-12 form-group">';
        vhtml+='				<label>';
        vhtml+='					<strong>Campus:</strong>';
        vhtml+='				</label>';
        vhtml+='				<span id="vcampusName">&nbsp;</span>';
        vhtml+='			</div>';
    }
    vhtml+='		</div>';
    vhtml+='	</div>';
    vhtml+='</div>';
    
    return vhtml;
 }

function setCampusesList(vid)
 {
    //Purpose: It sets campuses list of the page WEB (my-profile).
    //Limitations: The campuses data must exist in database server and brought by service rest (campuses).
    //Returns: None.
    
    var vtitle="Listado de Campus.";
    
    $.ajax({
		type: "get",
		url: vcampusesURL + "/campuses",
		dataType: "json",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurrió un error al tratar de mostrar los campus, intente de nuevo.", vtitle, 0);
                           break;
                case 0:    toastrAlert("No existen campus registrados.", vtitle, 3);
                           break;
                case 1:    fillCampusesList(vresponse.campuses.campuses, vid);
                           break;
                default:   toastrAlert("Ocurrió un error al tratar de mostrar los campus.", vtitle, 0);
                           break; 
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
 }

function fillCampusesList(vcampuses, vid)
 {
    //Purpose: It fills campuses list.
    //Limitations: The campus control must exist in the page WEB and the campuses (data) must exist in format JSON.  
    //Returns: None.
    
    var vstringJSON='[{"id":-1, "text":"--Seleccionar--"}';
    for(vi=0; vi<vcampuses.length; vi++){
        vstringJSON+=', {"id":' + vcampuses[vi].idCampus;
        vstringJSON+=',  "text":"' + vcampuses[vi].campus + '"}';
    }
    vstringJSON+="]";
    var vobjectJSON=jQuery.parseJSON(vstringJSON);
    
    $("#cmbcampus").select2({
        data:vobjectJSON,
        placeholder: {
            id: -1,
            text: "--Seleccionar--"
        },
        language:{
            noResults: function() {
                return "Sin campus encontrados.";
            }
        }, 
    });
    if ( vid!=0 ){
        $("#cmbcampus").select2("val", vid);
    }
 }

function updateCampus()
 {
    //Purpose: It updates user campus datum.
    //Limitations: The user campus datum must be exist in database server and brought by service rest (users/vidUser/campuses).  
    //Returns: None.
    
    var vtitle="Modificación de Campus.";
    
    if ( (trim($("#cmbcampus").val())!="") && ($("#cmbcampus").val() != -1) ){
        var vjson={};
        var vproperty="cmbcampus";
        vjson[vproperty]=trim($("#cmbcampus").val());
        $.ajax({
            type: "put",
            url: vrootURL + "/users/" +  + vidUser + "/campuses",
            dataType: "json",
            data: JSON.stringify(vjson),
            success: function(vresponse, vtextStatus, vjqXHR){
                switch(vresponse.messageNumber){
                    case -100: toastrAlert("Ocurrió un error al tratar de modificar el campus, intente de nuevo.", vtitle, 0);
                               break;
                    case 0:    toastrAlert("No se ha modificado ningún campus.", vtitle, 3);
                               break;
                    case 1:    menu(-1, 0);
                               toastrAlert("Se ha modificado el campus correctamente.", vtitle, 1);                    
                               break;
                    default:   toastrAlert("Ocurrió un error al tratar de modificar el campus.", vtitle, 0);
                               break;
                }
            },
            error: function(vjqXHR, vtextStatus, verrorThrown){
                toastrAlert(verrorThrown, vtitle, 0);
            }
        });
    }
    else{
        toastrAlert("La modificación del campus no tiene los datos necesarios, complete los datos.", vtitle, 3);
    }
 }
 
function validateUserFormFields()
 {
    //Purpose: It validates users form fields.
    //Limitations: The user form fields must exist.
    //Returns: None.
    
	var vstatus=true;
	
    clearUserFormFields();
    if ( trim($("#txtname").val()) == "" ){
        $("#vname").addClass("has-warning");
        $("#txtname").focus();
        vstatus=false;
    }
    else if ( trim($("#txtfirstName").val()) == "" ){
        $("#vfirstName").addClass("has-warning");
        $("#txtfirstName").focus();
        vstatus=false;
    }
    else if ( trim($("#txtlastName").val()) == "" ){
        $("#vlastName").addClass("has-warning");
        $("#txtlastName").focus();
        vstatus=false;
    }
    
    return vstatus;
 }
 
function validateUserPasswordFormFields()
 {
    //Purpose: It validates user password form fields.
    //Limitations: The user password form fields must exist.
    //Returns: None.
    
	var vstatus=true;
	var vtitle="Modificación de Contraseña.";
    
    clearUserPasswordFormFields()
    if ( trim($("#txtpreviousPassword").val()) == "" ){
        $("#vpreviousPassword").addClass("has-warning");
        $("#txtpreviousPassword").focus();
        vstatus=false;
    }
    else if ( trim($("#txtnewPassword").val()) == "" ){
        $("#vnewPassword").addClass("has-warning");
        $("#txtnewPassword").focus();
        vstatus=false;
    }
    else if ( trim($("#txtconfirmPassword").val()) == "" ){
        $("#vconfirmPassword").addClass("has-warning");
        $("#txtconfirmPassword").focus();
        vstatus=false;
    }
    else if ( trim($("#txtconfirmPassword").val())!=trim($("#txtnewPassword").val()) ){
        $("#vconfirmPassword").addClass("has-warning");
        toastrAlert("La confirmación de la contraseña no es igual a la nueva contraseña.", vtitle, 3);
        $("#txtconfirmPassword").focus();
        vstatus=false;
    }
    
    return vstatus;
 }
 
function clearUserFormFields()
 {
    //Purpose: It clears user form fields.
    //Limitations: The class "has-warning" must exist in user form fields.
    //Returns: None.
    
    if ( $("#vname").hasClass("has-warning") ){
        $("#vname").removeClass("has-warning");
    }
    if ( $("#vfirstName").hasClass("has-warning") ){
        $("#vfirstName").removeClass("has-warning");
    }
    if ( $("#vlastName").hasClass("has-warning") ){
        $("#vlastName").removeClass("has-warning");
    }
 }

function clearUserPasswordFormFields()
 {
    //Purpose: It clears user password form fields.
    //Limitations: The class "has-warning" must exist in user form fields.
    //Returns: None.
    
    if ( $("#vpreviousPassword").hasClass("has-warning") ){
        $("#vpreviousPassword").removeClass("has-warning");
    }
    if ( $("#vnewPassword").hasClass("has-warning") ){
        $("#vnewPassword").removeClass("has-warning");
    }
    if ( $("#vconfirmPassword").hasClass("has-warning") ){
        $("#vconfirmPassword").removeClass("has-warning");
    }
}