/********************************************************
Name: functions.js
Version: 0.0.2
Autor name: Edwuard H. Cabrera Rodríguez
Modification autor name: Edwuard H. Cabrera Rodríguez
Creation date: 12/04/2016
Modification date: 10/06/2016
Description: JS file. Functions.
********************************************************/

function toastrConfirm(vacceptFunctionName, vcancelFunctionName, vmessage, vtitle) //Confirmación tipo toaster
 {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-center",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": 0,
        "extendedTimeOut": 0,
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false
    }
    var vhtml ='<div class="row">';
        vhtml+='	<div style="text-align:justify" class="form-group col-md-12">' + vmessage + '</div>';
        vhtml+='</div>';
        vhtml+='<div class="row">';
        vhtml+='	<div style="text-align:right" class="form-group col-md-6">';
        vhtml+='		<button type="button" onclick="' + vacceptFunctionName + '" class="btn btn-warning btn-rounded">Si</button>';
        vhtml+='	</div>';
        vhtml+='	<div style="text-align:left" class="form-group col-md-6">';
        vhtml+='		<button type="button" onclick="' + vcancelFunctionName + '" class="btn btn-warning btn-rounded">No</button>';
        vhtml+=' </div>';
        vhtml+='</div>';
       
   return toastr.info(vhtml, vtitle);
 }

function toastrAlert(vmessage, vtitle, vtype) //Alert tipo toaster
 {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    var vhtml ='<div class="row">';
        vhtml+='	<div style="text-align:justify" class="form-group col-md-12">' + vmessage + '</div>';
        vhtml+='</div>';
   
    switch(vtype){
        case 1:  toastr.success(vhtml, vtitle);
                 break;
        case 2:  toastr.info(vhtml, vtitle);
                 break;
        case 3:  toastr.warning(vhtml, vtitle);
                 break;
        default: toastr.error(vhtml, vtitle);
                 break;
    }
 }
 
function uploadFileToDivResult(vcmdUpload, vcmdDelete, vfileDiv, vuploadState, vextensionList, vurl, vparams)
 {
	$(document).ready(function(){
		var vbutton = vcmdUpload.name;
		new AjaxUpload(vbutton, {
			action: vurl,
            data: {
                vuploadDirectory: vparams[0]
            },
			onSubmit : function(vfile , vext){
				if ( ! isFileExtension(vextensionList, vext) ){
					alert("Únicamente puede subir archivos con extensiones \"." + (vextensionList.join(" .")) + "\"\nSeleccione un archivo válido.");
					return false;
				}
				else{
					//xajax.$(vuploadState).innerHTML="Cargando...";
					$(vuploadState).append("Cargando...");
					vcmdDelete.disabled=false;
					//xajax.$(vfileDiv).innerHTML="";
					$(vfileDiv).append("");
				}
			},
			onComplete: function(vfile, vresponse){
				$(vuploadState).append("");
					
				if ( trim(vresponse)!="error" ){
					vcmdUpload.disabled=true;
					vcmdDelete.disabled=false;
					$(vfileDiv).append(vresponse);
					putImage(vresponse);
					$(vuploadState).empty();
				
					//xajax.$(vfileDiv).innerHTML=vresponse;
					return true;
				}
				else{
					$(vfileDiv).html("");
					alert("Ocurrió un error al cargar el archivo.\n Intente de nuevo." );
					return false;
				}
			}
		});
	});
}

function uploadFileDiv(vcmdUpload, vfileDiv, vuploadState, vextensionList, vurl, vparams)
 {
	
	vname=$("#txtrfc").val()+""+$("#txthomoclave").val();
	$(document).ready(function(){		

		if(trim($("#txthomoclave").val()) !="" && trim($("#txtrfc").val())!=""){					
			var vbutton = vcmdUpload.name;
			new AjaxUpload(vbutton, {
				action: vurl,
	            data: {
	                vuploadDirectory: vparams[0],
	                vNewNameFile: vname
	            },
				onSubmit : function(vfile , vext){
					if ( ! isFileExtension(vextensionList, vext) ){
						alert("Únicamente puede subir archivos con extensiones \"." + (vextensionList.join(" .")) + "\"\nSeleccione un archivo válido.");
						return false;
					}
					else{
						$(vuploadState).append("Cargando...");
						$(vfileDiv).append("");
					}
				},
				onComplete: function(vfile, vresponse){
					$(vuploadState).append("");
						
					if ( trim(vresponse)!=0  || trim(vresponse)!=-1 ){											
						$(vuploadState).html("");	
						getDataEnterpriseFile();
						toastrAlert("Se cargo correctamente el archivo","Datos empresas",1);				
						return true;
					}
					else{
						$(vfileDiv).html("");
						alert("Ocurrió un error al cargar el archivo.\n Intente de nuevo." );
						return false;
					}
				}
			});
		}
		else{
			toastrAlert("Los campos RFC y Homoclave deben estar llenos para poder subir archivos","Datos empresa",2);		
		}
	});
	
}

function removeCommas(vstring) //Remueve comas de una cadena
 {
	var vlist, vfilteredString="";

	vlist = vstring.split("");
	for(vi=0; vi<vstring.length; vi++){
        if ( vstring.charCodeAt(vi)!=44 ){
            vfilteredString= vfilteredString + vlist[vi];
        }
    }
	
    return vfilteredString;
 }

function getDateArray(vdate) //Obtiene la fecha en un array
 {
	var vday=vdate.getDate(), vmonth=vdate.getMonth() + 1, vyear=vdate.getFullYear();
	var vactualDate= new Array(2);
	
	vactualDate[0]=(vday<=9) ? "0" + vday : vday;
	vactualDate[1]=(vmonth<=9) ? "0" + vmonth : vmonth;
	vactualDate[2]=vyear;
	
	return vactualDate;
 }

function trim(vstring) //Elimina espacios a la derecha e izquierda de una cadena
 {
    var vresultString = "";
  
    vresultString = trimLeft(vstring);
    vresultString = trimRight(vresultString);
    return vresultString;
 }

function trimLeft(vstring) //Elimina espacios a la izquierda de una cadena
 {
    var vresultString = "";
    var vi = len = 0;
	 
    if (vstring+"" == "undefined" || vstring == null) 
        return "";
    vstring += "";
	
    if (vstring.length == 0) 
        vresultString = "";
    else{ 
        len = vstring.length;
		while ((vi <= len) && (vstring.charAt(vi) == " "))
			vi++;
        vresultString = vstring.substring(vi, len);
    }
    return vresultString;
 }

function trimRight(vstring) //Elimina espacios a la derecha de una cadena
 {
    var vresultString = "";
    var vi = 0;
     
    if (vstring+"" == "undefined" || vstring == null) 
        return "";
    vstring += "";
    if (vstring.length == 0)
        vresultString = "";
    else{
		vi = vstring.length - 1;
		while ((vi >= 0) && (vstring.charAt(vi) == " "))
			vi--;
			vresultString = vstring.substring(0, vi + 1);
    }
    return vresultString; 
 }

function isDate(vdateStr) //Verifica fecha válida
 {
	var vdatePat = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
	var vmatchArray = vdateStr.match(vdatePat); 
	var vdatestatus=true;
	var vdatemsg="";
	
	if (vmatchArray == null || vmatchArray[1]==null || vmatchArray[2]==null || vmatchArray[3]==null)
	 {
		vdatemsg="----- Please enter date as mm/dd/yyyy " + "\n";
		return false;
	 }
	
	vmonth = vmatchArray[1];
	vday = vmatchArray[3];
	vyear = vmatchArray[5];
	
	if (vmonth < 1 || vmonth > 12) 
	 { 
		vdatemsg=vdatemsg + "----- Month must be between 1 and 12." + "\n";
		return false;
	 }
	if (vday < 1 || vday > 31) 
	 {
		vdatemsg=vdatemsg + "----- Day must be between 1 and 31." + "\n";
		return false;
	 }
	
	if ((vmonth==4 || vmonth==6 || vmonth==9 || vmonth==11) && vday==31) 
	 {
		vdatemsg=vdatemsg + "----- Month " + vmonth + " doesn`t have 31 vdays!" + "\n";
		return false;
	 }
	
	if (vmonth == 2) 
	 {
		var isleap = (vyear % 4 == 0 && (vyear % 100 != 0 || vyear % 400 == 0));
		if (vday > 29 || (vday==29 && !isleap)) 
		 {
			vdatemsg=vdatemsg + "----- February " + vyear + " doesn`t have " + vday + " vdays!" + "\n";
			return false;
		 }
	 }
	return true;
 }

function isTime(vtimeStr) //Verifica hora válida
 {
    var vregexp = /^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$/;
  
    if (vregexp.test(vtimeStr)){
        return true;
    }
    else{
        return false;
    }
}

function isInteger(vstring) //Verifica entero válido
 {
	var i;

	if (isEmpty(vstring))
	if (isInteger.arguments.length == 1) return 0;
	else return (isInteger.arguments[1] == true);

	for (i = 0; i < vstring.length; i++)
	 {
		var c = vstring.charAt(i);
		if (!isDigit(c)) return false;
	 }
	return true;
 }

function isEmpty(vstring) //Verifica vacio
 {
	return ((vstring == null) || (vstring.length == 0))
 }

function isDigit (vchar)	//Verifica digito
 {
	return ((vchar >= "0") && (vchar <= "9"))
 }

function isEnter(vgetKeyCode) //It verifies a enter key
 {
    var visEnter=false;
    var vkey = (document.all) ? vgetKeyCode.keyCode : vgetKeyCode.which;
    
    if ( vkey==13 ){
	   visEnter=true;
	}
    return visEnter;
 }

function setEntriesType(vgetKeyCode, vtype) //It sets defined entries type
 {
    var vkey = (document.all) ? vgetKeyCode.keyCode : vgetKeyCode.which;
	var vregularExpression;
	
	switch(vtype)
	 {
		case "digit":			if (vkey==0 || vkey==8) return true;
								vregularExpression =/\d/;
								break;
		case "integer":			if (vkey==0 || vkey==8) return true;
								vregularExpression =/^(\d|-)?(\d|,)*$/;
								break;
		case "float":			if (vkey==0 || vkey==8) return true;
								vregularExpression = /^(\d|-)?(\d|,)*\.?\d*$/;
								break;
		case "date":			if (vkey==0 || vkey==8) return true;
								vregularExpression = /^(\d)?(\d|\/|-)*$/;
								break;
		case "letter":			if (vkey==0 || vkey==8 || vkey==13 || vkey==32 || isValidChar(vkey)) return true;
								vregularExpression = /[A-Za-z|ñ|Ñ]/;
								break;
		case "letter-numeric":	if (vkey==0 || vkey==8 || vkey==13 || vkey==32 ) return true;
								vregularExpression = /[A-Za-z0-9|ñ|Ñ]/;
								break;
		case "alfanumeric":		if (vkey==0 || vkey==8 || vkey==13 || vkey==32 ) return true;
								vregularExpression = /\w/;
								break;
	 }
	
	vkeyChar = String.fromCharCode(vkey);
	return vregularExpression.test(vkeyChar);
 }
 
function isValidChar(vkey) //It verifies if a char is valid (letter)
 {
	var vkeyChar= String.fromCharCode(vkey);
	vkeyChar= vkeyChar.toLowerCase();
	var vkeyCode= vkeyChar.charCodeAt(0); //String.charCodeAt(0);
	
	switch(vkeyCode)
	 {
		case 225: return true; //á	
				  break;
		case 233: return true; //é
				  break;
		case 237: return true; //í
				  break;
		case 241: return true; //ñ
				  break;
		case 243: return true; //ó
				  break;
		case 250: return true; //ú
				  break;
		case 252: return true; //ü
				  break;
		default: return false;
	 }
 }

function isEmail(vstring) //It verifies a valid email 
 {
	var vregularExpression=/[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
	
	return vstring.match(vregularExpression);
 }

function isFileExtension(vextensionsList, vextension)
 {
	vvalidExtension=false;
	for (var vi=0; vi<vextensionsList.length; vi++){ 
		if (vextensionsList[vi] == vextension){	
			vvalidExtension = true; 
			break; 
		} 
	} 
	return vvalidExtension;
 }
 
function getStringMatch(vchar, vstring, vinitialMatch, vfinalMatch)
 {
    vcharAmount=0;
	vstringMatch="";
    
    vlist=vstring.split("");
    for (vi=0; vi<vstring.length; vi++){
        vstringChar=vlist[vi];
        if ( vchar==vstringChar ){
            vcharAmount+=1;
        }
        if ( (vcharAmount==vinitialMatch) && (vcharAmount<vfinalMatch) && (vchar!=vstringChar) ){
            vstringMatch+=vstringChar;
        }
    }
    
    return vstringMatch;
 }

Number.prototype.toFixedDown = function(vdigits) {
  var n = this - Math.pow(10, -vdigits)/2;
  n += n / Math.pow(2, 53); // added 1360765523: 17.56.toFixedDown(2) === "17.56"
  return n.toFixed(vdigits);
}