/********************************************************
Name: campuses.js
Version: 0.0.1
Autor name: Sandro Alan G�mez Aceituno.
Modification autor name: Edwuard H. Cabrera Rodr�guez
Creation date: 06/06/2017
Modification date: 03/07/2017
Description: JS file. It provides support between view and controller files, in order to show the campuses list page.
********************************************************/

var vrootURL = "./controller/educationalOffers.php";
var vrootURLUser="./controller/users.php";
var vcampusesURL="./controller/campuses.php"
var vaccessModule=new Array(false, false, false);
var vattachedFiles=[];
var vidEducationalLevels=0;
var vidCampusAssignments=0;
var vidEducationalOffer=0;

var vidUserType=0;
var vidCampus=0;
var vidCampusRecord=0;

$(document).ready(function() 
{
	//Purpose: It sets default options of the page WEB (campuses).
    //Limitations: The current page (view) must be fully loaded.  
    //Returns: None.
	
	menu(1, 3);    
	verifyAccessPrivileges();	
	disableMainButtonsStartAplication();
});

function verifyAccessPrivileges()
 {
	//Purpose: It verifies access privileges (end user) in order to set controls of the page WEB (campuses).
    //Limitations: The user access privileges data must exist in database server and brought by service rest (users/{idUser}/accessPrivileges).   
    //Returns: None.
    
    var verrorMessage="";
    $.ajax({
		type: "GET",
		url: vrootURLUser + "/users/0/accessPrivileges",
		dataType: "JSON",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: 	verrorMessage ="Ocurri� un error al tratar de obtener sus privilegios de acceso.<br />";
                            verrorMessage+="Intente de nuevo.";
                            openError(verrorMessage);
                            break;
                case 0:     verrorMessage ="No existen privilegios de acceso registrados.<br />";
                            verrorMessage+="Verifiquelo con el Administrador del Sistema.";
                            openError(verrorMessage);
                            break;
                case 1: 	if ( vresponse[1].backendUserAccessPrivileges.length > 0 ){
                                setControls(vresponse[1].backendUserAccessPrivileges);
							}
							else{
								verrorMessage ="No tiene privilegios de acceso registrados.<br />";
								verrorMessage+="Verifiquelo con el Administrador del Sistema.";
								openError(verrorMessage);
							}
                            break;
                default:    verrorMessage ="Ocurri� un error al tratar de obtener sus privilegios de acceso.";
                            openError(verrorMessage);
                            break;
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            verrorMessage=verrorThrown + "<br />";
            verrorMessage+="Verifiquelo con el Administrador del Sistema.";
            openError(verrorMessage);
		}
	});
 }

function setControls(vbackendUserAccessPrivileges)
 {
    //Purpose: It sets controls of the page WEB (campuses).
    //Limitations: The user access privileges data must exist in format JSON and the controls must exist in the page WEB (campuses).  
    //Returns: None.
	
	var vhtml="";
	var vhtmlLevel="";
	
	for(vaccessPrivilegeNumber=0; vaccessPrivilegeNumber<vbackendUserAccessPrivileges.length; vaccessPrivilegeNumber++){
		switch( vbackendUserAccessPrivileges[vaccessPrivilegeNumber].accessPrivilege.idAccessPrivilege ){		
			case 4: vaccessModule[0]=true;
					 vaccessModule[1]=true;
					 break;			
			case 5: vaccessModule[0]=true;
					 vaccessModule[2]=true;
					 break;	
			case 6: vaccessModule[0]=true;
					 vaccessModule[3]=true;
					 break;	
			case 7: vaccessModule[0]=true;
					 vaccessModule[4]=true;
					 break;
			case 8: vaccessModule[0]=true;
					 vaccessModule[5]=true;
					 break;
		}
	}
	vidUserType=vbackendUserAccessPrivileges[0].backendUser.userType.idUserType;
	vidCampus=vbackendUserAccessPrivileges[0].backendUser.campus.idCampus;
	vidCampusAssignments=vidCampus;	
	vidCampusRecord=vidCampus;
	
	if ( ! vaccessModule[0] ){
        verrorMessage ="Acceso Denegado.<br />";
        verrorMessage+="No tiene privilegios para acceder al m�dulo de campus.";
        openError(verrorMessage);
    }
	if ( vaccessModule[1] ){		
		$("#btnacceptFilter").attr("onclick", "showEducationalOfferList();");
		$("#btncancelFilter").attr("onclick", "cleanEmailsFormFilterFields();");
		showEducationalOfferList();
	}
	if ( vaccessModule[2] ){
		setAttachFile();
	}
		
	if ( vaccessModule[3] ){	
		$("#btnaddCampuses").prop("disabled", false);	
		$("#btnupdateCampuses").attr("onclick", "getEducationalOfferData();");
	}
	if ( vaccessModule[4] ){		
		$("#btndeleteCampuses").attr("onclick", "deleteAssignmentsAccessAddress();");
	}
	if ( vaccessModule[5] )  {		
		$("#btnrecordCampus").prop("disabled", false);
		$("#btnaddCampuses").attr("onclick", "clearTextField();");
		$("#btnrecordCampus").attr("onclick", "recordEducationalOfferData();");
	}
	
	vhtmlLevel+='	<label for="cmbeducationalLeval"><strong>Nivel Educativo:</strong></label>';
	vhtmlLevel+='	<select id="cmbeducationalLeval" name="cmbeducationalLeval" class="form-control form-white" tabindex="-1" style="display: none; width: 100%"></select>';
	$("#veducationalLevel").html(vhtmlLevel);
		
	if( vidUserType == 1) {		
		vhtml+='	<label for="cmbcampus"><strong>Campus:</strong></label>';
		vhtml+='	<select id="cmbcampus" name="cmbcampus" class="form-control form-white" tabindex="-1" style="display: none; width: 100%"></select>';
		$("#vcampus").html(vhtml);	

		$("#vcampus").addClass("col-md-6 form-group");
		$("#veducationalLevel").addClass("col-md-6 form-group");
		setCampusesList();
		setCampusesListRecord();
	}
	else {
		$("#veducationalLevel").addClass("col-md-12 form-group");	
	}	
	setEducationalLevelList();	
	setEducationalLevelListRecord();
 }

 function getEducationalOfferData()
 {
	//Purpose: It sets educational offer list of the page WEB (emails-record).
    //Limitations: The educational offer data must exist in database server and brought by service rest.
    //Returns: None.
	
	if( vidEducationalOffer != 0) {		
		var vtitle="Visualizaci�n de Configuraci�n de Campus."; 
		$.ajax({
			type: 'GET',
			url: vrootURL + "/campuses/" + vidCampus + "/educationalLevel/" + vidEducationalLevels + "/educationalOffer/" + vidEducationalOffer,
			dataType: "JSON",
			success: function(vresponse, vtextStatus, vjqXHR){
				switch(vresponse.messageNumber){
					case -100:  toastrAlert("Ocurri� un error al tratar de mostrar los datos de la configuraci�n del campus., intente de nuevo ", vtitle, 0);
							break;
					case 0:     toastrAlert("Los datos de la configuraci�n del campus no se encuentran registrados.", vtitle, 3);
								$("#cmbcampusRecord").val([0]).trigger("change");
								$("#cmbeducationalLevelRecord").val([0]).trigger("change");
								$("#cmbcampusRecord").prop("disabled", true);
								$("#cmbeducationalLevelRecord").prop("disabled", true);
								$("#txteducationalOffer").val('');
							break;
					case 1:		$("#cmbcampusRecord").val([0]).trigger("change");
								$("#cmbeducationalLevelRecord").val([0]).trigger("change");
								
								$("#cmbcampusRecord").prop("disabled", true);
								$("#cmbeducationalLevelRecord").prop("disabled", true);
								$("#txteducationalOffer").val('');
								$("#cmbcampusRecord").val([vresponse.educationalOffer.assignment.campus.idCampus]).trigger("change");
								$("#cmbeducationalLevelRecord").val([vresponse.educationalOffer.assignment.educationalLevel.idEducationalLevel]).trigger("change");
								$("#txteducationalOffer").val(vresponse.educationalOffer.educationalOffer);							
							break;
					default:   	toastrAlert("Ocurri� un error al tratar de mostrar los datos de la configuraci�n del campus.", vtitle, 0);
							break;
				}
			},
			error: function(vjqXHR, vtextStatus, verrorThrown){
				toastrAlert(verrorThrown, vtitle, 0);
			}
		});
	}
	else {
		clearTextField();		
	}	
 }
 
 function clearTextField()
 { 
	//Purpose: It cleans educational offer form filter fields.
    //Limitations: The educational offer form filter fields must exist.
    //Returns: None.
	
	if(vidCampusRecord != 0){
		$("#cmbeducationalLevelRecord").removeAttr("disabled"); 
	}
	else {
		$("#cmbeducationalLevelRecord").removeAttr("disabled"); 
		$("#cmbcampusRecord").removeAttr("disabled"); 
	}
	
	$("#cmbcampusRecord").val([0]).trigger("change");
	$("#cmbeducationalLevelRecord").val([0]).trigger("change");
	$("#txteducationalOffer").val('');
 }
 
 function recordEducationalOfferData()
 {
    //Purpose: It records (add/update) educational offer data.
    //Limitations: The page WEB controls must have data: Personal Data Section and Data Account Section.  
    //Returns: None.
	
    var vtitle="Registros Incompletos";
 
    if ( vidEducationalOffer == 0 ){
        if ( validateEducationalOfferFormFields() ){
            addEducationalOfferData(); 
        }
        else{
            toastrAlert("El registro de los datos de campus no puede ser realizado, no tiene los datos necesarios, complete los datos", vtitle, 3);     
        }        
    }
    else {
        if( validateEducationalOfferFormFields() ){
            updateEducationalOfferData();
        }
        else{
            toastrAlert("La actualizaci�n de los datos del campus no puede ser realizado, no tiene los datos necesarios, complete los datos", vtitle, 3);
        }
    }
 }

function validateEducationalOfferFormFields() 
 {
    //Purpose: It validates educational offer form fields.
    //Limitations: The educational offer form fields must exist.
    //Returns: None.
     
    var vstatus=true;       
    clearEducationalOfferFields();  
	  
	if (vidCampusRecord == 0){
		if(trim($('select[name=cmbcampusRecord]').val()) === "0"){
			$( "#vlistCampus" ).addClass("has-warning");
			if ( vstatus ){
				$( "#cmbcampusRecord" ).focus();
			}
			vstatus = false;
		}	
	}
    
    if( ($('select[name=cmbeducationalLevelRecord]').val()) === "0"){
        $( "#vlistEducationalLevel" ).addClass("has-warning");
        if ( vstatus ){
            $( "#cmbeducationalLevelRecord" ).focus();
        }
        vstatus = false;
    }    
    if(trim($( "#txteducationalOffer" ).val()) === ""){
        $( "#veducationalOffer" ).addClass("has-warning");
        if ( vstatus ){
            $( "#txteducationalOffer" ).focus();
        }        
        vstatus = false;
    }   
    return vstatus;
 } 

function clearEducationalOfferFields()
 {
    //Purpose: It clears educational offer form fields.
    //Limitations: The class "has-warning" must exist in educational offer form fields.
    //Returns: None.
    if (vidCampusRecord == 0){
		if( $("#vlistCampus").hasClass("has-warning") ){
			$("#vlistCampus").removeClass("has-warning");
		}
    }
    if( $("#vlistEducationalLevel").hasClass("has-warning") ){
        $("#vlistEducationalLevel").removeClass("has-warning");
    }
    if( $("#veducationalOffer").hasClass("has-warning") ){
        $("#veducationalOffer").removeClass("has-warning");
    }   
 }
 
function addEducationalOfferData()
 {
	//Purpose: It adds Educational Offer data.
    //Limitations: The Educational Offer data must not exist in database server and brought by service rest (campusEmailSettings).  
    //Returns: None.
	
    var vjson=$('#vdataEducationalOffer').serializeObject();   
	vproperty="idCampus";
    vjson[vproperty]=vidCampusRecord;
	
    var vtitle="Registro de Campus.";
	$.ajax({
		type: 'POST',
		url: vrootURL + "/educationalOffers",
		dataType: "JSON",
		data: JSON.stringify(vjson),
		success: function(vresponse, vtextStatus, vjqXHR){
			switch(vresponse.messageNumber){
				case -100:  toastrAlert("Ocurri� un error al tratar de registrar los datos de la configuraci�n del campus, intente de nuevo", vtitle, 0);
							break;     
				case -1:    toastrAlert("Imposible registrar la configuraci�n del campus., intente de nuevo (1)", vtitle, 3);
							break;
				case 0:     toastrAlert("Imposible registrar la configuraci�n del campus., intente de nuevo (2)", vtitle, 3);
							break;
				case 1:     toastrAlert("Los datos de la configuraci�n del campus han sido registrados correctamente", vtitle, 1);
							vidEducationalOffer=vresponse.idEducationalOffer;
							vidEducationalLevels=vresponse.idEducationalLevel;
							
							showEducationalOfferList();
							break;
				default:   	toastrAlert("Ocurri� un error al tratar de registrar los datos de la configuraci�n del campus.", vtitle, 0);
						break;
			}
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
			toastrAlert(verrorThrown, vtitle, 0);
		}
	});	      
 } 

function updateEducationalOfferData()
 {    
	//Purpose: It updates educational offer setting data.
    //Limitations: The educational offer setting data must be exist in database server and brought by service rest (campusEmailSetting/{idCampus}).  
    //Returns: None.
	
    var vjson=$('#vdataEducationalOffer').serializeObject(); 
	vproperty="idEducationalOffer";
    vjson[vproperty]=vidEducationalOffer;
	
	vproperty="vidEducationalLevel";
    vjson[vproperty]=vidEducationalLevels;
	
	vproperty="idCampus";
    vjson[vproperty]=vidCampusRecord;	
	
	vtitle="Actualizaci�n de Campus";
	
    $.ajax({
        type: 'PUT',
        url: vrootURL + "/educationalOffers",
        dataType: "JSON",
        data: JSON.stringify(vjson),
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100:  toastrAlert("Ocurri� un error al tratar de modificar los datos de la configuracion del campus, intente de nuevo.", vtitle, 0);
                            break;     
                case 0:     toastrAlert("Ning�n dato de la configuraci�n del campus ha sido modificado.", vtitle, 3);
                            break;
                case 1:     toastrAlert("Los datos de la configuraci�n del campus han sido modificados correctamente.", vtitle, 1);
							showEducationalOfferList();
                            break;
				default:   	toastrAlert("Ocurri� un error al tratar de modificar los datos de la configuraci�n del campus.", vtitle, 0);
						break;
            }
        },
        error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
        }
    });         
 }  
 
function setCampusesListRecord()
 {
    //Purpose: It sets campuses list of the page WEB (emails-record).
    //Limitations: The campuses data must exist in database server and brought by service rest (campuses).
    //Returns: None.
    
    $.ajax({
		type: "GET",
		url: vcampusesURL + "/campuses",
		dataType: "JSON",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurri� un error al tratar de mostrar los campus, intente de nuevo.", "Listado de Campus.", 0);
                           break;
                case 0:    toastrAlert("No existen campus registrados.", "Listado de Campus.", 3);
                           break;
                case 1:    var vcampuses = vresponse.campuses.campuses;
							var vhtml="";
							vhtml+='	<label for="cmbcampusRecord"><strong>Campus:</strong></label>';
							vhtml+='	<select id="cmbcampusRecord" name="cmbcampusRecord" class="form-control form-white" style="width: 100%;" disabled>';
							vhtml+='	<option value="0">--Seleccionar--</option>';						
							$.each(vcampuses, function(vi) {
								var vidCampus = vcampuses[vi].idCampus;
								var vcampus = vcampuses[vi].campus;
								vhtml+='<option value="' + vidCampus + '">' + vcampus + '</option>';	
							});
							vhtml+="</select>";
							$("#vlistCampus").html(vhtml);
							
                           break;
                default:   toastrAlert("Ocurri� un error al tratar de mostrar los campus.", "Listado de Campus.", 0);
                           break; 
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, "Listado de Campus.", 0);
		}
	});
 }

function setEducationalLevelListRecord()
 {
    //Purpose: It sets educational level list of the page WEB.
    //Limitations: The educational level data must exist in database server and brought by service rest (educationalLevels).
    //Returns: None.
      
    $.ajax({
		type: "GET",
		url: vrootURL + "/educationalLevels",
		dataType: "JSON",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurri� un error al tratar de mostrar los niveles de educaci�n, intente de nuevo.", "Listado de Niveles Educativos.", 0);
                           break;
                case 0:    toastrAlert("No existen niveles de educaci�n registrados.", "Listado de Niveles Educativos.", 3);
                           break;
                case 1:    
							var veducationalLevelsRecord = vresponse.educationalLevels.educationalLevels;
							var vhtml="";
								vhtml+='	<label for="cmbeducationalLevelRecord"><strong>Nivel Educativo:</strong></label>';
								vhtml+='	<select id="cmbeducationalLevelRecord" name="cmbeducationalLevelRecord" class="form-control form-white" style="width: 100%;" disabled>';
								vhtml+='	<option value="0">--Seleccionar--</option>';
							
								$.each(veducationalLevelsRecord, function(vi) {
									if(veducationalLevelsRecord[vi].idEducationalLevel != 0){
										var vidEducationalLevel = veducationalLevelsRecord[vi].idEducationalLevel;
										var veducationalLevel = veducationalLevelsRecord[vi].educationalLevel;
										vhtml+='<option value="' + vidEducationalLevel + '">' + veducationalLevel + '</option>';
									}										
								});
								vhtml+="</select>";
								$("#vlistEducationalLevel").html(vhtml);
								
                           break;
                default:   toastrAlert("Ocurri� un error al tratar de mostrar los niveles de educaci�n.", "Listado de Niveles Educativos.", 0);
                           break;  
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, "Listado de Niveles Educativos.", 0);
		}
	});
 }
 
function showEducationalOfferList() 
{	
	//Purpose: It sets assignments list of the page WEB.
    //Limitations: The assignments data must exist in database server and brought by service rest (assignments).
    //Returns: None.
	
	disableMainButtons();
    $.ajax({
        type: "GET",
        url: vrootURL + "/educationalOffers/" + vidCampusAssignments,
        dataType: "JSON",
        data: $('#vfilterData').serialize(),
        success: function(vresponse, vtextStatus, vjqXHR) {
            switch (vresponse.messageNumber) {
                case -100:	vmessageHTML  = '<p style="text-align:center" class="text-danger">';
							vmessageHTML += ' <strong>Ocurri� un error al tratar de mostrar los campus, intente de nuevo </strong>';
							vmessageHTML += '</p>';
							$("#vassignmentsList").html(vmessageHTML);
							break;
                case 0:		vmessageHTML  = '<p style="text-align:center" class="text-danger">';
							vmessageHTML += ' <strong>No existen campus registrados</strong>';
							vmessageHTML += '</p>';
							$("#vassignmentsList").html(vmessageHTML);
							break;
				case 1:		fillAssignmentsList( vresponse.assignments.educationalOffers );                   
                            break;					
                default:	verrorMessage ="Ocurri� un error al tratar de mostrar los campus.";
							$("#vassignmentsList").html(vmessageHTML);
							break;
            }
        },
        error: function(vjqXHR, vtextStatus, verrorThrown) {
            vmessageHTML = '<p style="text-align:center" class="text-danger">';
            vmessageHTML += ' <strong> ' + verrorThrown + ' </strong>';
            vmessageHTML += ' <strong>Ocurri� un error al tratar de mostrar los campus.</strong>';
            vmessageHTML += '</p>';
            $("#vassignmentsList").html(vmessageHTML);
        }
    });
}

function deleteAssignmentsAccessAddress() 
 {
	//Purpose: It deletes Campuses.
    //Limitations: The end Campuses must accept the deleting Campuses.
    //Returns: None.
	
    var vacceptFunctionName = "deleteAssignmentsData()";
    var vcancelFunctionName = "cancelAssignmentsAccessAddressRemoval()";
    var vmessage = "�Est� seguro que desea eliminar el campus?";
    var vtitle = "Eliminaci�n de Configuraci�n de Campus.";
    vconfirmDeleteAssignmentsAccessAddress = toastrConfirm(vacceptFunctionName, vcancelFunctionName, vmessage, vtitle);
 }

function cancelAssignmentsAccessAddressRemoval() 
 {
	//Purpose: It cancels deleting Campuses.
    //Limitations: None.
    //Returns: None.
	
    vconfirmDeleteAssignmentsAccessAddress.remove();
    toastr.clear(vconfirmDeleteAssignmentsAccessAddress, {
        force: true
    });
    toastrAlert("La eliminaci�n del campus ha sido cancelada.", "Eliminaci�n de Configuraci�n de Campus.", 2);
 }

function deleteAssignmentsData() 
 {
	//Purpose: It deletes Campuses data.
    //Limitations: The Campuses data must exist in database server and brought by service rest (emailMarketing/{idEmailMarketing}).
    //Returns: None.
	
    var vtitle = "Eliminaci�n de Campus.";
    vconfirmDeleteAssignmentsAccessAddress.remove();
    toastr.clear(vconfirmDeleteAssignmentsAccessAddress, {
        force: true
    });
	
    $.ajax({
        type: 'DELETE',
        url: vrootURL + "/campuses/" + vidCampus + "/educationalLevel/" + vidEducationalLevels + "/educationalOffers/" + vidEducationalOffer,
        dataType: "JSON",
        success: function(vresponse, vtextStatus, vjqXHR) {
            switch (vresponse.messageNumber) {
                case -100:
                    toastrAlert("Ocurri� un error al tratar de eliminar los datos del campus, intente de nuevo", vtitle, 0);
                    break;
                case -1:
                    toastrAlert("Imposible eliminar el campus, intente de nuevo.", vtitle, 3);
                    break;
                case 0:
                    toastrAlert("Imposible eliminar el campus, intente de nuevo." , vtitle, 3);
                    break;
                case 1:
					$("#vassignmentsListRow" + vidCampus + "-" + vidEducationalLevels + "-" + vidEducationalOffer).remove();
                    disableMainButtons();
                    toastrAlert("El campus ha sido eliminado correctamente.", vtitle, 1);
                    break;
                default:   
					toastrAlert("Ocurri� un error al tratar de eliminar los datos del campus.", vtitle, 0);
				    break; 
            }
        },
        error: function(vjqXHR, vtextStatus, verrorThrown) {
            toastrAlert(verrorThrown, vtitle, 0);
        }
    });
 } 

function fillAssignmentsList(veducationalOffers)
 {
    //Purpose: It fills assignments list.
    //Limitations: The assignments data must exist in format JSON.
    //Returns: None.

    var vhtml ='<div class="table-responsive">';
        vhtml+='	<table id="vgrdAssignmentsList" class="table table-hover">';
        vhtml+='		<thead>';
        vhtml+='			<tr>';
        vhtml+='				<th style="text-align:center;" class="no-sort">&nbsp;</th>';
		if(vidUserType == 1){
			vhtml+='			<th style="text-align:center;">Campus</th>';
		}
        vhtml+='				<th style="text-align:center;">Nivel Educativo</th>';
        vhtml+='				<th style="text-align:center;">Oferta Educativa</th>';
        vhtml+='				<th style="text-align:center;">Archivo</th>';
        vhtml+='			</tr>';
        vhtml+='		</thead>';
        vhtml+='		<tbody>';
    for(vi=0; vi<veducationalOffers.length; vi++){
		if(veducationalOffers[vi].idEducationalOffer != 0){			
			vhtml+='			<tr id="vassignmentsListRow' + veducationalOffers[vi].assignment.campus.idCampus + "-" + veducationalOffers[vi].assignment.educationalLevel.idEducationalLevel + "-" + veducationalOffers[vi].idEducationalOffer + '">';
			vhtml+='				<td style="text-align:center;">';
			vhtml+='					<label>';
			vhtml+='						<input type="radio" name="cmremailMarketing" value="' + veducationalOffers[vi].assignment.campus.idCampus + "-" +
																									veducationalOffers[vi].assignment.educationalLevel.idEducationalLevel + "-" +
																									veducationalOffers[vi].idEducationalOffer + '" title="Seleccionar campus" class="radio">';
			vhtml+='					</label>';
			vhtml+='				</td>';
			if(vidUserType == 1) {
				vhtml+='			<td style="text-align:center;">' + veducationalOffers[vi].assignment.campus.campus + '</td>';
			}
			vhtml+='				<td style="text-align:center;">' + veducationalOffers[vi].assignment.educationalLevel.educationalLevel + '</td>';
			vhtml+='				<td style="text-align:center;">' + veducationalOffers[vi].educationalOffer + '</td>';
			vhtml+='				<td style="text-align:center;">';
			if( (veducationalOffers[vi].fileName != "" ) && (veducationalOffers[vi].fileName != null )) {
				vhtml+='				<span>';       
				vhtml+='					<a href="./others/career-plan/'+ veducationalOffers[vi].fileName +'" target="_blank"><i class="fa fa-file-pdf-o m-r-xs"></i></a>';       
				vhtml+='				</span>'; 
			}			
			vhtml+='				</td>';       
			vhtml+='			</tr>';
		}
    }
        vhtml+='		</tbody>';
        vhtml+='	</table>';
        vhtml+='</div>';
    $("#vassignmentsList").html(vhtml);    
    setAssignmentsList();
    setRadioButtons();
 }
 
 function setAssignmentsList()
 {
    //Purpose: It sets assignments list.
    //Limitations: The assignments must exist in assignments list.  
    //Returns: None.
    
    $("#vgrdAssignmentsList").dataTable({
        "bLengthChange": false,
        "searching": false,
        "processing": true,
        "displayLength": 10,
        "columnDefs": [{
            "targets": 'no-sort',
            "orderable": false}],
        "language": {
            "sProcessing": "Procesando...",
            "sInfo": "_START_ - _END_ de _TOTAL_ ofertas educativas",
            "sInfoFiltered": "(filtrado de un total de _MAX_ ofertas educativas)",
            "sInfoPostFix":    "",
            "sUrl": "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "�ltimo",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
    $("#vgrdAssignmentsList").on("draw.dt", function(){
        setRadioButtons();  
    });
}

function setRadioButtons()
 {
    //Purpose: It sets radio buttons.
    //Limitations: The radio buttons must be created.  
    //Returns: None.
    
    $('input[type="checkbox"], input[type="radio"].radio').iCheck({
        checkboxClass:'icheckbox-primary',
        radioClass:'iradio_flat-blue'
    }).on('ifClicked', function(e){
        setAssignmentsIds(this.value);
    });
 }
 
function setAssignmentsIds(vvalue)
 {
    //Purpose: It sets assigments identifier.
    //Limitations: The assigments identifier must exist in users list.  
    //Returns: None.
	
	vidCampus = parseInt(getStringMatch("-", vvalue, 0, 1));
	vidEducationalLevels = parseInt(getStringMatch("-", vvalue, 1, 2));
	vidEducationalOffer = parseInt(getStringMatch("-", vvalue, 2, 3));

	if ( vaccessModule[2] ){
		$("#btnattachFile").prop( "disabled", false);
	}
	if ( vaccessModule[3] ){		
		$("#btnupdateCampuses").prop("disabled", false);
	}	
	if ( vaccessModule[4] ){
		$("#btndeleteCampuses").prop("disabled", false);
	}	
 }
 
function setEducationalLevelList()
 {
    //Purpose: It sets educational level list of the page WEB.
    //Limitations: The educational level data must exist in database server and brought by service rest (educationalLevels).
    //Returns: None.
    
    var vtitle="Listado de Niveles de Educaci�n.";
    
    $.ajax({
		type: "GET",
		url: vrootURL + "/educationalLevels",
		dataType: "JSON",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurri� un error al tratar de mostrar los niveles de educaci�n, intente de nuevo.", vtitle, 0);
                           break;
                case 0:    toastrAlert("No existen niveles de educaci�n registrados.", vtitle, 3);
                           break;
                case 1:    fillEducationalLevelsList(vresponse.educationalLevels.educationalLevels, 0);
                           break;
                default:   toastrAlert("Ocurri� un error al tratar de mostrar los niveles de educaci�n.", vtitle, 0);
                           break;  
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
 } 

function fillEducationalLevelsList(veducationalLevels, vid)
 {
    //Purpose: It fills educationals levels list.
    //Limitations: The educactionals levels status control must exist in the page WEB and the email marketing types (data) must exist in format JSON.  
    //Returns: None.
    
    var vstringJSON='[{"id":0, "text":"--Todos--"}';
    for(vi=0; vi<veducationalLevels.length; vi++){
		if(veducationalLevels[vi].idEducationalLevel != 0){
			vstringJSON+=', {"id":' + veducationalLevels[vi].idEducationalLevel;
			vstringJSON+=',  "text":"' + veducationalLevels[vi].educationalLevel + '"}';
		}
    }
    vstringJSON+="]";
    var vobjectJSON=jQuery.parseJSON(vstringJSON);
    
    vcmbEmailMarketingTypeList=$("#cmbeducationalLeval").select2({
        data:vobjectJSON,
        placeholder: {
            id: 0,
            text: "--Todos--"
        },
        language:{
            noResults: function() {
                return "Sin Niveles de educaci�n encontrados.";
            }
        }, 
    });
    if ( vid!=0 ){
        $("#cmbeducationalLeval").select2("val", vid);
    }
 }
 
function setCampusesList()
 {
    //Purpose: It sets campuses list of the page WEB (campuses).
    //Limitations: The campuses data must exist in database server and brought by service rest (campuses).
    //Returns: None.
    
    var vtitle="Listado de Campus.";
    
    $.ajax({
		type: "GET",
		url: vcampusesURL + "/campuses",
		dataType: "JSON",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurri� un error al tratar de mostrar los campus, intente de nuevo.", vtitle, 0);
                           break;
                case 0:    toastrAlert("No existen campus registrados.", vtitle, 3);
                           break;
                case 1:    fillCampusesList(vresponse.campuses.campuses, 0);							
                           break;
                default:   toastrAlert("Ocurri� un error al tratar de mostrar los campus.", vtitle, 0);
                           break; 
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
 } 

function fillCampusesList(vcampuses, vid)
 {
    //Purpose: It fills campuses list.
    //Limitations: The campus control must exist in the page WEB and the campuses (data) must exist in format JSON.  
    //Returns: None.
    
    var vstringJSON='[{"id":0, "text":"--Todos--"}';
    for(vi=0; vi<vcampuses.length; vi++){
        vstringJSON+=', {"id":' + vcampuses[vi].idCampus;
        vstringJSON+=',  "text":"' + vcampuses[vi].campus + '"}';
    }
    vstringJSON+="]";
    var vobjectJSON=jQuery.parseJSON(vstringJSON);
    
    vcmbCampusesList=$("#cmbcampus").select2({
        data:vobjectJSON,
        placeholder: {
            id: 0,
            text: "--Todos--"
        },
        language:{
            noResults: function() {
                return "Sin campus encontrados.";
            }
        }, 
    });
    if ( vid!=0 ){
        $("#cmbcampus").select2("val", vid);
    }  
 } 

function setAttachFile()
 {
	//Purpose: It sets attach file in server.
	//Limitations: none.
	//Returns: None.
	
    var vtitle="Adjuntar Archivo.";
	var vurl='./controller/upload-file.php';
	var vparams= new Array("/others/career-plan/");    
	var vextensionList = ["jpg", "png", "jpeg", "pdf"];
    $(document).ready(function(){		
        var vbutton = document.getElementById("btnattachFile").name;
        new AjaxUpload(vbutton, {
            action: vurl,
            data: {
                vuploadDirectory: vparams[0]
            },
            onSubmit: function(vfile , vext){                
				if ( ! isFileExtension(vextensionList, vext) ){
					toastrAlert("�nicamente puede subir archivos con extensiones \"." + (vextensionList.join(" .")) + "\"\nSeleccione un archivo v�lido.", vtitle, 3);
					return false;
				}
				else {
					$("#vattachStatus").html("<i class='fa fa-spinner faa-spin animated'></i>&nbsp;Cargando...</span>"); 
				}
            },
            onComplete: function(vfile, vresponse){
                $("#vattachStatus").html("");
                switch(vresponse){
                    case -100: toastrAlert("Ocurri� un error al tratar de cargar el archivo, intente de nuevo.", vtitle, 0);
                               break;
                    case 0:    toastrAlert("Ocurri� un error al tratar de renombrar el nombre del archivo, intente de nuevo.", vtitle, 3);
                               break;
                    default:   var vattachedFile=[];
                               vattachedFile[0]=vfile;
                               vattachedFile[1]=vresponse; 
                               vattachedFiles.push(vattachedFile); 
                               // showAttachedFilesList();
							   updateFileNameAssinmentData(vresponse);							   
                               toastrAlert("Se cargo correctamente el archivo.", vtitle, 1);
                               break;
                }
            },
            onError: function (){
                $("#vattachStatus").removeClass("fa fa-spinner faa-spin animated");
            }
        });
    });
 }

function updateFileNameAssinmentData(vfileName)
 {    
	//Purpose: It updates name file setting data.
    //Limitations: The assignments file name setting data must be exist in database server and brought by service rest.  
    //Returns: None.
	  
	vtitle="Actualizar nombre de archivo.";
	vidEmailMarketingStatuses = $("#cmbupdateEmailMarketingStatus").val();
	
	var vjson={
		idCampus: vidCampus,
		idEducationalLevel: vidEducationalLevels,
		idEducationalOffer: vidEducationalOffer,
		fileName: vfileName
	};
	
    $.ajax({
        type: 'PUT',
		url: vrootURL + "/educationalOffersFile",
        dataType: "JSON",
		data: JSON.stringify(vjson),
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100:  toastrAlert("Ocurri� un error al tratar de actualizar el nombre del archivo, intente de nuevo.", vtitle, 0);
                            break;     
                case 0:     toastrAlert("No se ha podido actualizar el nombre del archivo, intente de nuevo.", vtitle, 3);
                            break;
                case 1:     toastrAlert("El archivo a sido actualizado correctamente.", vtitle, 1);
                            showEducationalOfferList();
							break;
				default:   	toastrAlert("Ocurri� un error al tratar de actualizar el nombre del archivo.", vtitle, 0);
						break;
            }
        },
        error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
        }
    });         
 } 
 
function showAttachedFilesList()
 {
	//Purpose: It show attached file.
    //Limitations: The end user must accept the deleting attached file.
    //Returns: None.
	
    var vhtml="";
    for (var vi=0; vi<vattachedFiles.length; vi++){
        vhtml+='	<span class="label label-primary">' + vattachedFiles[vi][0] + '</span>';
        vhtml+='	<span style="cursor:pointer" onclick="deleteAttachedFile(\'' + vattachedFiles[vi][1] + '\');" class="label label-success"><strong>X</strong></span>';
    }
    vhtml+='<span id="vattachStatus">&nbsp;</span>';
    $("#vfileNames").html(vhtml);
 }

function deleteAttachedFile(vfileName)
 {
    //Purpose: It deletes attached file.
    //Limitations: The end user must accept the deleting attached file.
    //Returns: None.
    
    var vacceptFunctionName="deleteAttachedFileData('" + vfileName + "');";
    var vcancelFunctionName="cancelAttachedFileRemoval();";
    var vmessage="�Est� seguro que desea eliminar el archivo?.";
    var vtitle="Eliminaci�n de Archivo.";
    
    vconfirmDeleteAttachedFile=toastrConfirm(vacceptFunctionName, vcancelFunctionName, vmessage, vtitle);
 }
 
function cancelAttachedFileRemoval()
 {
    //Purpose: It cancels deleting attached file.
    //Limitations: None.
    //Returns: None.
    
    vconfirmDeleteAttachedFile.remove();
    toastr.clear(vconfirmDeleteAttachedFile, { force: true });
    toastrAlert("La eliminaci�n del ha sido cancelado.", "Eliminaci�n de Archivo.", 2);
 }
 
function deleteAttachedFileData(vfileName)
 {
    //Purpose: It deletes attached file data.
    //Limitations: The attached file data must exist in database server and brought by service rest (attachFiles/:vfileName).
    //Returns: None.
    
    var vtitle="Eliminaci�n de Archivo Adjuntado.";
    vconfirmDeleteAttachedFile.remove();
    toastr.clear(vconfirmDeleteAttachedFile, { force: true });
    
	$.ajax({
	   type: 'delete',
        url: vcampusesURL + "/attachFiles/" + vfileName,
		dataType: "json",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurri� un error al tratar de eliminar el archivo adjuntado, intente de nuevo.", vtitle, 0);
                           break;
                case -1:   toastrAlert("Imposible eliminar el archivo adjuntado, no existe.", vtitle, 3);
                           break;     
                case 0:    toastrAlert("Imposible eliminar el archivo, intente de nuevo.", vtitle, 3);
                           break;
                case 1:    var vattachedFileIndex=getAttachedFileIndexOnList(vfileName);
                           if ( vattachedFileIndex>=0 ){
                                vattachedFiles.splice(vattachedFileIndex, 1);
                           }
                           showAttachedFilesList(1);
                           toastrAlert("El archivo adjuntado ha sido eliminado correctamente.", vtitle, 1);
                           break;
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});    
 }

function getAttachedFileIndexOnList(vfileName)
 {
    //Purpose: It gets attached file index on attached files list.
    //Limitations: None.
    //Returns: Attached file index.
    
    var vattachedFileIndex=-1;
    for (var vi=0; vi<vattachedFiles.length; vi++){
        if ( vfileName==vattachedFiles[vi][1] ){
            vattachedFileIndex=vi;
            vi=vattachedFiles.length;
        }
    }
    
    return vattachedFileIndex;
 }
 
function cleanEmailsFormFilterFields()
 {
    //Purpose: It cleans assigments form filter fields.
    //Limitations: The assigments form filter fields must exist.
    //Returns: None.
    
    if ( vidUserType==1 ){
        $("#cmbcampus").select2("val", -1);
    }
    $("#cmbeducationalLeval").select2("val", -1);
	showEducationalOfferList();
 }
 
function enableMainButtons()
 {
    //Purpose: It enables main buttons: View assigments access privileges, update email marketing and delete email marketing.
    //Limitations: The main buttons must be disable.  
    //Returns: None.
    
    $("#btnattachFile").prop( "disabled", false);
 }

function disableMainButtons()
 {
    //Purpose: It disables main buttons: View assigments access privileges, update email marketing and delete email marketing.
    //Limitations: The main buttons must be enable.  
    //Returns: None.
    
    $("#btnattachFile").prop( "disabled", true);
    $("#btnupdateCampuses").prop( "disabled", true);
    $("#btndeleteCampuses").prop( "disabled", true);
 } 
 
 function disableMainButtonsStartAplication()
 {
    //Purpose: It disables main buttons: View email marketing access privileges, update email marketing and delete email marketing.
    //Limitations: The main buttons must be enable.  
    //Returns: None.
    
	$("#btnrecordCampus").prop( "disabled", true);
	$("#btnaddCampuses").prop( "disabled", true);
    $("#btnupdateCampuses").prop( "disabled", true);
    $("#btndeleteCampuses").prop( "disabled", true);
    
 }