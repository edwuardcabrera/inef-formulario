/********************************************************
Name: login.js
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodr�guez
Modification autor name: Edwuard H. Cabrera Rodr�guez
Creation date: 21/05/2017
Modification date: 30/05/2017
Description: JS file. It provides support between view and controller files, in order to login a user.
********************************************************/

var vrootURL="./controller/users.php";

$(document).ready(function(){
    //Purpose: It sets default options of the page WEB (login).
    //Limitations: The current page (view) must be fully loaded.  
    //Returns: None.
    
    showLoginFooter();
    setControls();
});

function setControls()
 {
    //Purpose: It sets controls of the page WEB (login).
    //Limitations: The controls must exist in the page WEB.  
    //Returns: None.
    
    $("#btnlogin").attr("onclick", "login();");
    $("#btnhome").attr("onclick", "location.href='./default.php';");
    $("#btnrestorePassword").attr("onclick", "restorePassword();");
    
    $.backstretch(["./tools/template-3.2/global/images/gallery/login5.jpg"], {
        fade: 600,
        duration: 4000
    });
 }

function login()
 {
    //Purpose: It logins user.
    //Limitations: The login data must exist in database server and brought by service rest (users/{emailAccount}/login).  
    //Returns: None.
    
    var vtitle="Login de Usuario.";
	if ( validateLoginFields() ){
		$.ajax({
            type: "get",
            url: vrootURL + "/users/" + trim($("#txtemailAccount").val()) + "/login",
            dataType: "json",
            data: "vpassword=" + trim($("#txtpassword").val()) + "&vtype=0",
            success: function(vresponse, vtextStatus, vjqXHR){
                switch(vresponse.messageNumber){
                    case -100: toastrAlert("Ocurri� un error al tratar de acceder al sistema, intente de nuevo.", vtitle, 0); 
                               break;
                    case -2:   toastrAlert("Ha superado el n�mero de intentos permitidos (3). <br />Su cuenta ha sido desactivada.<br />Reestablescala o p�ngase en contacto con el administrador del sistema.", vtitle, 3);
                               break;
                    case -1:   toastrAlert("Imposible acceder al sistema. <br />Su acceso se encuentra inactivo.<br />Reestablescala o p�ngase en contacto con el administrador del sistema.", vtitle, 3);
                               break;
                    case 0:    toastrAlert("Imposible acceder al sistema. <br />No existen usuarios registrados con el nombre y/o contrase�a proporcionado.", vtitle, 3);
                               break;
                    case 1:    if ( vresponse.user.userType.idUserType!=4 ){
                                    switch( vresponse.user.userType.idUserType ){
                                        case 1: location.href="./frmmy-profile.php";
                                                break;
                                        case 2: location.href="./frmmy-profile.php";
                                            break;
                                        case 3: location.href="./frmusers.php";   
                                            break;
                                        case 5: location.href="./frmapplicants.php";
                                                break;
                                    }
                               }
                               break;
                    default:   toastrAlert("Ocurri� un error al tratar de acceder al sistema.", vtitle, 0); 
                               break;
                }
            },
            error: function(vjqXHR, vtextStatus, verrorThrown){
                toastrAlert(verrorThrown, vtitle, 0);
            }
        });
	}
	else{
		toastrAlert("El acceso al sistema no puede ser realizado, no tiene los datos necesarios, complete los datos.", vtitle, 3);
	}
 }

function restorePassword()
 {
    //Purpose: It sends email to user in order to restore password.
    //Limitations: The user data must exist in database server and brought by service rest (users/{email}passwordReset).  
    //Returns: None.
    
    var vtitle="Restablecimiento de Contrase�a.";
    
    if ( validatePasswordResetFields() ){
		$.ajax({
            type: "post",
            url: vrootURL + "/users/" + trim($("#txtemail").val()) + "/passwordReset",
            dataType: "json",
            success: function(vresponse, vtextStatus, vjqXHR){
                switch(vresponse.messageNumber){
                    case -100: toastrAlert("Ocurri� un error al tratar de solicitar el restablecimiento de la contrase�a, intente de nuevo.", vtitle, 0);
                               break;
                    case -2:   toastrAlert("Imposible solicitar el restablecimiento de la contrase�a. <br />No se envi� la solicitud al correo electr�nico especificado.", vtitle, 3);
                               break; 
                    case -1:   toastrAlert("Imposible solicitar el restablecimiento de la contrase�a. intente de nuevo.", vtitle, 3);
                               break;
                    case 0:    toastrAlert("Imposible solicitar el restablecimiento de la contrase�a. <br />El correo electr�nico especificado no se encuentra registrado.", vtitle, 3);
                               break;
                    case 1:    toastrAlert("La solicitud de restablecimiento de contrase�a ha sido generada correctamente. <br />Verifique el correo especificado.", vtitle, 1);
                               break;
                    default:   toastrAlert("Ocurri� un error al tratar de solicitar el restablecimiento de la contrase�a.", vtitle, 0); 
                               break;
                }
            },
            error: function(vjqXHR, vtextStatus, verrorThrown){
                toastrAlert(verrorThrown, vtitle, 0);
            }
        });
	}
	else{
		toastrAlert("El restablecimiento de la contrase�a no puede ser realizada, no tiene los datos necesarios, complete los datos.", vtitle, 3);
	}
 }

function validateLoginFields()
 {
    //Purpose: It validates login fields.
    //Limitations: The login fields must exist.
    //Returns: None.
    
	vstatus=true; 
	
    if ( trim($("#txtemailAccount").val()) == "" ){
        $("#txtemailAccount").focus();
		vstatus=false;
	}
	else if ( ! isEmail(trim($("#txtemailAccount").val())) ){
		toastrAlert("Para acceder al sistema, el correo electr�nico debe tener un formato v�lido.", "Login de Usuario.", 3);
        $("#txtemailAccount").focus();
		vstatus=false;
	}
	if ( trim($("#txtpassword").val()) == "" ){
        if ( vstatus ){
		  $("#txtpassword").focus();
        }
		vstatus=false;
	}
		
	return vstatus;
 }

function validatePasswordResetFields()
 {
    //Purpose: It validates password reset fields.
    //Limitations: The password reset fields must exist.
    //Returns: None.
    
	vstatus=true;
	
    clearPasswordResetFields();
	if ( trim($("#txtemail").val()) == "" ){
        $("#vemail").addClass("has-warning");
		$("#txtemail").focus();
		vstatus=false;
	}
	else if ( ! isEmail(trim($("#txtemail").val())) ){
		toastrAlert("Para recuperar la contrase�a, el correo electr�nico debe tener un formato v�lido.", "Restablecimiento de Contrase�a.", 3);
        $("#vemail").addClass("has-warning");
		$("#txtemail").focus();
		vstatus=false;
	}
	
	return vstatus;
 }

function clearPasswordResetFields()
 {
    //Purpose: It clears password reset fields.
    //Limitations: The class "has-warning" must exist in password reset fields.
    //Returns: None.
    
    if ( $("#vemail").hasClass("has-warning") ){
        $("#vemail").removeClass("has-warning");
    }
 }