/********************************************************
Name: applicants.js
Version: 0.0.1
Autor name: Alejandro Hernández Guzmán
Modification autor name: 
Creation date: 02/06/2017
Modification date: 25/07/2017
Description: JS file. It provides support between view and controller files, in order to applicants
********************************************************/

var vusersURL = "./controller/users.php";
var vcampusesURL = "./controller/campuses.php";
var vdatetimeURL="./controller/datetime.php";
var vidUserType=0;
var vidCampus=0;
var vaccessModule=new Array(false, false);
var vcmbCampusesList;
var vidApplicant=0;
var vdatetime;
var vidCampusApplicant=0;
var  vcmbEducationalList;

$(document).ready(function () {
    //Purpose: It sets default options of the page WEB (userapplicants).
    //Limitations: The current page (view) must be fully loaded.  
    //Returns: None.
    
    configOthersControls();
    menu(9,0);
    verifyAccessPrivileges();
});

function verifyAccessPrivileges()
 {
    //Purpose: It verifies access privileges (end user) in order to set controls of the page WEB (emails-record).
    //Limitations: The user access privileges data must exist in database server and brought by service rest (users/{idUser}/accessPrivileges).   
    //Returns: None.
    
    var verrorMessage="";
    $.ajax({
		type: "get",
		url: vusersURL + "/users/0/accessPrivileges",
		dataType: "json",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: verrorMessage ="Ocurrió un error al tratar de obtener sus privilegios de acceso.<br />";
                           verrorMessage+="Intente de nuevo.";
                           openError(verrorMessage);
                           break;
                case 0:    verrorMessage ="No existen privilegios de acceso registrados.<br />";
                           verrorMessage+="Verifiquelo con el Administrador del Sistema.";
                           openError(verrorMessage);
                           break;
                case 1:    if ( vresponse[1].backendUserAccessPrivileges.length>0 ){
                                setControls(vresponse[1].backendUserAccessPrivileges);
                           }
                           else{
                                verrorMessage ="No tiene privilegios de acceso registrados.<br />";
                                verrorMessage+="Verifiquelo con el Administrador del Sistema.";
                                openError(verrorMessage);
                           }
                           break;
                default:   verrorMessage ="Ocurrió un error al tratar de obtener sus privilegios de acceso.";
                           openError(verrorMessage);
                           break;
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            verrorMessage=verrorThrown + "<br />";
            verrorMessage+="Verifiquelo con el Administrador del Sistema.";
            openError(verrorMessage);
		}
	});
 }

function setControls(vbackendUserAccessPrivileges)
 {
    //Purpose: It sets controls of the page WEB (applicants).
    //Limitations: The controls must exist in the page WEB (applicants).  
    //Returns: None.
	
    for(vaccessPrivilegeNumber=0; vaccessPrivilegeNumber<vbackendUserAccessPrivileges.length; vaccessPrivilegeNumber++){
        switch( vbackendUserAccessPrivileges[vaccessPrivilegeNumber].accessPrivilege.idAccessPrivilege ){
            case 10:  vaccessModule[0]=true;
                      vaccessModule[1]=true;
                      break;
            case 11:  vaccessModule[0]=true;
                      vaccessModule[2]=true;
                     break;
           
        }
    }
    vidUserType=vbackendUserAccessPrivileges[0].backendUser.userType.idUserType;
	vidCampus=vbackendUserAccessPrivileges[0].backendUser.campus.idCampus;
	vidCampusApplicant=vidCampus;	
    if ( ! vaccessModule[0] ){
        verrorMessage ="Acceso Denegado.<br />";
        verrorMessage+="No tiene privilegios para acceder al módulo de registro de correos electrónicos.";
        openError(verrorMessage);
    }
    getServerDateTime();
    if(vaccessModule[1] || vaccessModule[2]){
        $("#btnacceptFilter").attr("onclick", "showApplicantsList();");
        $("#btncancelFilter").attr("onclick", "cleanApplicantsFormFilterFields();");
        $("#txtinicial").attr("onkeypress", "return setEntriesType(event, 'date');");
        $("#txtinicial").val(vdatetime);
        $("#txtfinal").attr("onkeypress", "return setEntriesType(event, 'date');");
        $("#txtfinal").val(vdatetime);
        showApplicantsList();
    }
    if(vaccessModule[2]){
        $("#btnshowSetingsApplicants").prop( "disabled", true);
    }
    vidUserType=vbackendUserAccessPrivileges[0].backendUser.userType.idUserType;
    if (vidUserType==1){
        $("#vaddressee").html(getAddresseeControls(1));
        setCampusesList(0);
        
    }
    else{
        $("#vaddressee").html(getAddresseeControls(2));
        vidCampus=vbackendUserAccessPrivileges[0].backendUser.campus.idCampus;
        setEducationalLevelList(vidCampus, 0);
        
    }
 }

function setCampusesList(vid)
 {
    //Purpose: It sets campuses list of the page WEB (applicants).
    //Limitations: The campuses data must exist in database server and brought by service rest (campuses).
    //Returns: None.
    
    var vtitle="Listado de Campus.";
    
    $.ajax({
		type: "get",
		url: vcampusesURL + "/campuses",
		dataType: "json",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurrió un error al tratar de mostrar los campus, intente de nuevo.", vtitle, 0);
                           break;
                case 0:    toastrAlert("No existen campus registrados.", vtitle, 3);
                           break;
                case 1:    fillCampusesList(vresponse.campuses.campuses, vid);
                           break;
                default:   toastrAlert("Ocurrió un error al tratar de mostrar los campus.", vtitle, 0);
                           break; 
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
 }

function setEducationalLevelList(vidCampus, vid)
 {
    //Purpose: It sets educational levels list of the page WEB (applicants).
    //Limitations: The educational levels data must exist in database server and brought by service rest (campuses/{idCampus}/educationalLevels).
    //Returns: None.
    
    var vtitle="Listado de niveles educativos.";
    
    $.ajax({
		type: "get",
		url: vcampusesURL + "/campuses/" + vidCampus + "/educationalLevels",
		dataType: "json",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurrió un error al tratar de mostrar los niveles educativos, intente de nuevo.", vtitle, 0);
                           break;
                case 0:    toastrAlert("No existen niveles educativos registrados.", vtitle, 3);
                           break;
                case 1:    fillEducationalLevelList(vresponse.assignments.assignments, vid);
                           break;
                default:   toastrAlert("Ocurrió un error al tratar de mostrar los niveles educativos.", vtitle, 0);
                           break;
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
 }
 $('#vwndwshowApplicants').on('show.bs.modal', function (event) {
    //Purpose: It shows user access privileges windows modal and data
    //Limitations: By clicking on the detail button you can view the details of the selected applicant, this detail must exist in the database server and brought by the rest service (campus / idCampus / applicants / idApplicant)
    //Returns: None.

    var vrootURL = "./controller/applicants.php";
     
    $.ajax({
		type: "get",
		url: vrootURL + "/campus/" + vidCampus + "/applicants/"+  vidApplicant,
		dataType: "json",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: var vhtml ='<p style="text-align:center" class="text-danger">';
                               vhtml+='	<strong>Ocurrió un error al tratar de mostrar detalles de Aplicante.</strong>';
                               vhtml+='</p>';
                           $("#vsettingApplicant").html(vhtml);
                           break;
                case 0:    var vhtml ='<p style="text-align:center" class="text-warning">';
                               vhtml+='	<strong>No tiene privilegios de acceso registrados.</strong>';
                               vhtml+='</p>'; 
                           $("#vsettingApplicant").html(vhtml); 
                           break;
                case 1:    GetSettingApplicant(vresponse);
                           break;
                default:   var vhtml ='<p style="text-align:center" class="text-danger">';
                               vhtml+='	<strong>Ocurrió un error al tratar de mostrar.</strong>';
                               vhtml+='</p>';
                           $("#vsettingApplicant").html(vhtml);
                           break;            
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, "Visualización de solicitudes.", 0);
		}
	});
	
});

function GetSettingApplicant(applicant){
    //Purpose: It sets detail to applicant.
    //Limitations: The applicant data must exist in database server.
    //Returns: None.
 
    var vhtm='<div style=text-align:center class=col-md-6>';                                
        vhtm+=  '<span class="input input--hoshi" style="font-size:16px;">';
        vhtm+=          ' <label  for="lblfolio">';
        vhtm+=              ' <strong>Folio:</strong>';
        vhtm+=          '</label></br>';
        vhtm+=          '<label name="lblfolio" style="font-size:16px;font-weight:lighter;" id="lblfolio"> ' +" "+ applicant.applicants.idApplicant +'</label>';
        vhtm+=  '</span>';
        vhtm+= '</div>';
        vhtm+='<div style="text-align:center" class="col-md-6">';                                
        vhtm+=     '<span class="input input--hoshi" style="font-size:16px;">';
        vhtm+=           '<label  for="lblcampus">';
        vhtm+=                '<strong>Campus:</strong>';
        vhtm+=           '</label></br>';
        vhtm+=           '<label name="lblcampus" style="font-size:16px;font-weight:lighter;" id="lblcampus">'+" "+ applicant.applicants.campus.campus + '</label>';
        vhtm+=      '</span>';
        vhtm+= '</div>';
        vhtm+='<div style="text-align:center" class="col-md-6">' ;                               
        vhtm+=      '<span class="input input--hoshi" style="font-size:16px;">';
        vhtm+=          '<label  for="lblrequestdate">';
        vhtm+=            '<strong>Fecha de solicitud:</strong>';
        vhtm+=           '</label>';
        vhtm+=           '<label name="lblrequestdate" id="lblrequestdate" style="font-size:16px;font-weight:lighter;">'+applicant.applicants.requestDate+'</label>';
        vhtm+=       '</span>';
        vhtm+= '</div>';
        vhtm+= '<div style="text-align:center" class="col-md-6">';                                
        vhtm+=     '<span class="input input--hoshi" style="font-size:16px;">';
        vhtm+=          '<label  for="lbleducationalLevel">';
        vhtm+=             '<strong>Nivel:</strong>';
        vhtm+=           '</label></br>';
        vhtm+=           '<label name="lbleducationalLevel" id="lbleducationalLevel" style="font-size:16px;font-weight:lighter;">'+" "+applicant.applicants.educationalOffer.assignment.educationalLevel.educationalLevel+'</label>';
        vhtm+=     '</span>';
        vhtm+= '</div>';
        vhtm+= '<div style="text-align:center" class="col-md-6">';                                
        vhtm+=      '<span class="input input--hoshi" style="font-size:16px;">';
        vhtm+=         '<label  for="lbluser">';
        vhtm+=               '<strong>Solicitante:</strong>';
        vhtm+=         '</label></br>';
        vhtm+=          '<label name="lbluser" id="lbluser" style="font-size:16px;font-weight:lighter;">'+applicant.applicants.user.name +" "+applicant.applicants.user.firstName+" "+applicant.applicants.user.lastName+'</label>';
        vhtm+=       '</span>';
        vhtm+= '</div>';
        vhtm+= '<div style="text-align:center" class="col-md-6">';                                
        vhtm+=     '<span class="input input--hoshi" style="font-size:16px;">';
        vhtm+=          '<label  for="lbleducationalOffer">';
        vhtm+=             '<strong>Oferta Educativa:</strong>';
        vhtm+=           '</label>';
        vhtm+=           '<label name="lbleducationalOffer" id="lbleducationalOffer" style="font-size:14px;font-weight:lighter;">'+" "+applicant.applicants.educationalOffer.educationalOffer+'</label>';
        vhtm+=     '</span>';
        vhtm+= '</div>';
   
    
        $("#vapplicant").html(vhtm);
}

function fillCampusesList(vcampuses, vid)
 {
    //Purpose: It fills campuses list.
    //Limitations: The campus control must exist in the page WEB and the campuses (data) must exist in format JSON.  
    //Returns: None.
    
    var vstringJSON='[{"id":-1, "text":"--Todos--"}';
    for(vi=0; vi<vcampuses.length; vi++){
        vstringJSON+=', {"id":' + vcampuses[vi].idCampus;
        vstringJSON+=',  "text":"' + vcampuses[vi].campus + '"}';
    }
    vstringJSON+="]";
    var vobjectJSON=jQuery.parseJSON(vstringJSON);
    
    vcmbCampusesList=$("#cmbcampus").select2({
        data:vobjectJSON,
        placeholder: {
            id: -1,
            text: "--Todos--"
        },
        language:{
            noResults: function() {
                return "Sin campus encontrados.";
            }
        }, 
    });
    if ( vid!=0 ){
        $("#cmbcampus").select2("val", vid);
    }
    fillEducationalLevelList({}, 0);
    $("#cmbeducationalLevel").prop("disabled", false);
    vcmbCampusesList.on("select2:select", function (e){
        if ( e.params.data.element.value>=0 ){
            setEducationalLevelList(e.params.data.element.value, 0);
           
            $("#cmbeducationalLevel").prop("disabled", false);
        }
        
    });
 }

function fillEducationalLevelList(vassignments, vid)
 {
    //Purpose: It fills assignments list.
    //Limitations: The assigment control must exist in the page WEB and the assignments (data) must exist in format JSON.  
    //Returns: None.
    
    var vstringJSON='[{"id":-1, "text":"--Todos--"}';
    for(vi=0; vi<vassignments.length; vi++){
        vstringJSON+=', {"id":' + vassignments[vi].educationalLevel.idEducationalLevel;
        vstringJSON+=',  "text":"' + vassignments[vi].educationalLevel.educationalLevel + '"}';
    }
    vstringJSON+="]";
    var vobjectJSON=jQuery.parseJSON(vstringJSON);
    
    $("#cmbeducationalLevel").empty();
    vcmbEducationalList = $("#cmbeducationalLevel").select2({
        data: vobjectJSON,
        placeholder: {
            id: -1,
            text: "--Todos--"
        },
        language: {
            noResults: function () {
                return "Sin campus encontrados.";
            }
        },
    });
    if ( vid!=0 ){
        $("#cmbeducationalLevel").select2("val", vid);
    }
    
     fillEducationalOffer({},0);
    $("#cmbeducationalOffer").prop("disabled",false);
     vcmbEducationalList.on("select2:select",function(e){
        if( e.params.data.element.value >=0 ){
            var vidCampus_=vidCampus;
            if ( vidUserType==1 ){
                vidCampus_=$("#cmbcampus").val();
            }
            setEducationalOffer(vidCampus_,e.params.data.element.value,0);
            $("#cmbeducationalOffer").prop("disabled",false);
        } 
     });
 }
 
 function  setEducationalOffer(vidCampus,vidEducationalLevel,vid){
     //Purpose: It sets educationalOffer list of the page WEB (applicants).
    //Limitations: The educational Offer data must exist in database server and brought by service rest (campuses/{idCampus}/educationalLevels/{idEducationalLevel}/educationalOffers).
    //Returns: None.
    var vcampusesURL = "./controller/campuses.php";
   
  
    var vtitle="Listado de ofertas educativas.";
    
    $.ajax({
		type: "get",
		url: vcampusesURL + "/campuses/" + vidCampus + "/educationalLevels/"+vidEducationalLevel+"/educationalOffers",
		dataType: "json",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: toastrAlert("Ocurrió un error al tratar de mostrar los niveles educativos, intente de nuevo.", vtitle, 0);
                           break;
                case 0:    toastrAlert("No existen niveles educativos registrados.", vtitle, 3);
                           break;
                case 1:    fillEducationalOffer(vresponse.educationalOffers.educationalOffers, vid);
                           break;
                default:   toastrAlert("Ocurrió un error al tratar de mostrar los niveles educativos.", vtitle, 0);
                           break;
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
     
 }
 
 function fillEducationalOffer(veducationalOffers,vid){
    //Purpose: It fills educationalOffers list.
    //Limitations: The assigment control must exist in the page WEB and the assignments (data) must exist in format JSON.  
    //Returns: None.
    
    var vstringJSON='[{"id":-1, "text":"--Todos--"}';
    for(vi=0; vi<veducationalOffers.length; vi++){
        vstringJSON+=', {"id":' + veducationalOffers[vi].idEducationalOffer;
        vstringJSON+=',  "text":"' + veducationalOffers[vi].educationalOffer + '"}';
    }
    vstringJSON+="]";
    var vobjectJSON=jQuery.parseJSON(vstringJSON);
    
    $("#cmbeducationalOffer").empty();
    $("#cmbeducationalOffer").select2({
        data:vobjectJSON,
        placeholder: {
            id: -1,
            text: "--Todos--"
        },
        language:{
            noResults: function() {
                return "Sin offertas encontrados.";
            }
        }, 
    });
    if ( vid!=0 ){
        $("#cmbeducationalOffer").select2("val", vid);
    }
     
 }
 
function showApplicantsList()
 {
    //Purpose: It shows applicants list.
    //Limitations: The users data must exist in database server and brought by service rest (applicants).  
    //Returns: None.
    
    var campus=$('#cmbcampus').val();
    var educationalLevel=$('#cmbeducationalLevel').val();
    var educationalOffer=$('#cmbeducationalOffer').val();
    var vrootURL="./controller/applicants.php";
   
    $.ajax({
		type: "get",
		url: vrootURL + "/applicants/" + vidUserType+"/" + vidCampusApplicant ,
		dataType: "json",
        data: $('#vfilterData').serialize()+ "&vcampus=" + campus + "&veducationalLevel=" + educationalLevel + "&veducationalOffer=" + educationalOffer,
        success: function(vresponse, vtextStatus, vjqXHR){
             switch(vresponse.messageNumber){
                case -100: var vhtml ='<p style="text-align:center" class="text-danger">';
                               vhtml+='	<strong>Ocurrió un error al tratar de mostrar las solicitudes, intente de nuevo.</strong>';
                               vhtml+='</p>';
                           $("#vapplicantsList").html(vhtml);
                           break;
                case 0:    var vhtml ='<p style="text-align:center" class="text-warning">';
                               vhtml+='	<strong>No existen solicitudes registradas.</strong>';
                               vhtml+='</p>';
                           $("#vapplicantsList").html(vhtml);
                           break;
                case 1:    var vusers=null;
                           if (vresponse.applicants.applicants!=null ){
                                var vapplicants=vresponse.applicants.applicants;
                                   fillApplicantsList(vapplicants);
                           }
                           break;
                default:   var vhtml ='<p style="text-align:center" class="text-danger">';
                               vhtml+='	<strong>Ocurrió un error al tratar de mostrar los usuarios.</strong>';
                               vhtml+='</p>';
                           $("#vusersList").html(vhtml);
                           break;
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, "Visualización de solicitudes.", 0);
		}
	});
 }
 
function fillApplicantsList(vapplicants)
 {
    //Purpose: It fills users list.
    //Limitations: The users data must exist in format JSON.
    //Returns: None.
    
    var vhtml ='<div class="table-responsive">';
        vhtml+='	<table id="vgrdapplicantsList" class="table table-hover">';
        vhtml+='		<thead>';
        vhtml+='			<tr>';
        vhtml+='				<th style="text-align:center;" class="no-sort">&nbsp;</th>';
		vhtml+='				<th style="text-align:center;">Nombre</th>';
        vhtml+='				<th style="text-align:center;">Fecha</th>';
       if(vidUserType==1){
        vhtml+='				<th style="text-align:center;">Campus</th>';
        }
        vhtml+='				<th style="text-align:center;">Nivel de Estudio</th>';
        vhtml+='				<th style="text-align:center;">Oferta Educativa</th>';

        vhtml+='			</tr>';
        vhtml+='		</thead>';
        vhtml+='		<tbody>';
    for(vi=0; vi<vapplicants.length; vi++){
        vhtml+='			<tr id="vuserListRow' + vapplicants[vi].user.idUser + '">';
        vhtml+='				<td style="text-align:center;">';
        vhtml+='					<label>';                                                     
                                                                                     
        vhtml+='						<input type="radio" name="cmruser" value="' +vapplicants[vi].educationalOffer.assignment.campus.idCampus   + "-" + vapplicants[vi].idApplicant  +'" title="Seleccionar Usuario" class="radio">';
        vhtml+='					</label>';
        vhtml+='				</td>';
        vhtml+='				<td style="text-align:left;">' + vapplicants[vi].user.name + " "
                                                               + vapplicants[vi].user.firstName + " "
                                                               + vapplicants[vi].user.lastName;
        vhtml+='				</td>';
        vhtml+='				<td style="text-align:center;">' + vapplicants[vi].requestDate + '</td>';
       if(vidUserType==1){
        vhtml+='				<td style="text-align:center;">' + vapplicants[vi].educationalOffer.assignment.campus.campus + '</td>';
        }
        vhtml+='				<td style="text-align:center;">' + vapplicants[vi].educationalOffer.assignment.educationalLevel.educationalLevel + '</td>';
        vhtml+='				<td style="text-align:center;">' + vapplicants[vi].educationalOffer.educationalOffer + '</td>';

        vhtml+='					';
        vhtml+='				</td>';
        vhtml+='			</tr>';
    }
        vhtml+='		</tbody>';
        vhtml+='	</table>';
        vhtml+='</div>';
    $("#vapplicantsList").html(vhtml);
    
    setApplicantsList();
    setRadioButtons();
 }
 
function setApplicantsList()
 {
    //Purpose: It sets applicants list.
    //Limitations: The users must exist in users list.  
    //Returns: None.
    
    $("#vgrdapplicantsList").dataTable({
        "bLengthChange": false,
        "searching": false,
        "processing": true,
        "displayLength": 15,
        "columnDefs": [{
            "targets": 'no-sort',
            "orderable": false}],
        "language": {
            "sProcessing": "Procesando...",
            "sInfo": "_START_ - _END_ de _TOTAL_ solicitudes",
            "sInfoFiltered": "(filtrado de un total de _MAX_ solicitudes)",
            "sInfoPostFix":    "",
            "sUrl": "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "óltimo",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
    $("#vgrdapplicantsList").on("draw.dt", function(){
        setRadioButtons();  
    });
 }
 
function setRadioButtons()
 {
    //Purpose: It sets radio buttons.
    //Limitations: The radio buttons must be created.  
    //Returns: None.
    
    $('input[type="checkbox"], input[type="radio"].radio').iCheck({
        checkboxClass:'icheckbox-primary',
        radioClass:'iradio_flat-blue'
    }).on('ifClicked', function(e){
        setApplicantsIds(this.value);
         $("#btnshowSetingsApplicants").prop( "disabled", false);
    });
 }
 
function setApplicantsIds(vvalue)
 {
    //Purpose: It sets user identifier.
    //Limitations: The user identifier must exist in users list.  
    //Returns: None.
          
    vidCampus=parseInt(getStringMatch("-", vvalue, 0, 1));
    vidApplicant=getStringMatch("-", vvalue, 1, 2)+ "-" + getStringMatch("-", vvalue, 2, 3);
 }
 
function getAddresseeControls(vtype)
 {
    //Purpose: It gets addressee controls.
    //Limitations: None.
    //Returns: Addressee controls in HTML5 code.
    
    var vhtml="";
    if (vtype==1){
        vhtml+='<div id="vcampus" class="col-md-4 form-group">';
		vhtml+='	<label for="cmbcampus"><strong>Campus:</strong></label>';
		vhtml+='	<select id="cmbcampus" class="form-control form-white" tabindex="-1" style="display: none; width: 100%"></select>';
        vhtml+='</div>';
        vhtml+='<div id="veducationalLevel" class="col-md-4 form-group">';
		vhtml+='	<label for="cmbeducationalLevel"><strong>Nivel Educativo:</strong></label>';
		vhtml+='	<select id="cmbeducationalLevel" class="form-control form-white" tabindex="-1" style="display: none; width: 100%"></select>';
        vhtml+='</div>';
         vhtml+='<div id="veducationalOffer" class="col-md-4 form-group">';
		vhtml+='	<label for="cmbeducationalOffer"><strong>Oferta Educativa:</strong></label>';
		vhtml+='	<select id="cmbeducationalOffer" class="form-control form-white" tabindex="-1" style="display: none; width: 100%"></select>';
        vhtml+='</div>';
    }
    else if (vtype==2){
        vhtml+='<div id="veducationalLevel" class="col-md-6 form-group">';
		vhtml+='	<label for="cmbeducationalLevel"><strong>Nivel Educativo:</strong></label>';
		vhtml+='	<select id="cmbeducationalLevel" class="form-control form-white" tabindex="-1" style="display: none; width: 100%"></select>';
        vhtml+='</div>';
         vhtml+='<div id="veducationalOffer" class="col-md-6 form-group">';
		vhtml+='	<label for="cmbeducationalOffer"><strong>Oferta Educativa:</strong></label>';
		vhtml+='	<select id="cmbeducationalOffer" class="form-control form-white" tabindex="-1" style="display: none; width: 100%"></select>';
        vhtml+='</div>';
    }
        
    return vhtml;
 }

function configOthersControls()
 {
    //Purpose: It configs others controls.
    //Limitations: None.
    //Returns: None.
    
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '< Ant',
        nextText: 'Sig >',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miórcoles', 'Jueves', 'Viernes', 'Sóbado'],
        dayNamesShort: ['Dom','Lun','Mar','Mió','Juv','Vie','Sab'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sa'],
        weekHeader: 'Sm',
        dateFormat: 'mm/dd/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
    $('.datepicker').datepicker();
 }

function cleanApplicantsFormFilterFields()
 {
    //Purpose: It cleans applicant form filter fields.
    //Limitations: The applicants form filter fields must exist.
    //Returns: None.
    
    if ( vidUserType==1 ){
        $("#cmbcampus").select2("val", -1);
        $("#cmbeducationalLevel").prop("disabled", true);
         $("#cmbeducationalOffer").select2("val",-1);
    }
    $("#cmbeducationalLevel").select2("val",-1);
    $("#cmbeducationalOffer").select2("val",-1);
    $("#txtinicial").val(vdatetime);
    $("#txtfinal").val(vdatetime);
    showApplicantsList();
 }  

function getServerDateTime()
 {
    //Purpose: It gets server datetime.
    //Limitations: None.
    //Returns: None.
    
    var vtitle="Obtención de Fecha (Servidor)";
    
    $.ajax({
		type: "GET",
		url: vdatetimeURL,
		dataType: "json",
        async:false,
        success: function(vresponse, vtextStatus, vjqXHR){
            vdatetime=vresponse.substring(0, 10);
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
 }