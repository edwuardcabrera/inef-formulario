/********************************************************
Name: menu.js
Version: 0.0.1
Autor name: Edwuard H. Cabrera Rodr�guez
Modification autor name: Edwuard H. Cabrera Rodr�guez
Creation date: 21/05/2017
Modification date: 30/06/2017
Description: JS file. It provides support between view and controller files, in order to show the menu.
********************************************************/

var vusersURL="./controller/users.php";
var vsessionsURL="./controller/session-2.php";
var vconfirmProcessExit;

function menu(vidAccessPrivilegeGroup, vidAccessPrivilegeModule)
 {
    //Purpose: It shows main menu.
    //Limitations: The end user must have access privileges recorded in database server and brought by service rest (users/{idUser}/accessPrivileges).  
    //Returns: None.
    
    var verrorMessage="";
    $.ajax({
		type: "get",
		url: vusersURL + "/users/0/accessPrivileges",
		dataType: "json",
        success: function(vresponse, vtextStatus, vjqXHR){
            switch(vresponse.messageNumber){
                case -100: verrorMessage ="Ocurri� un error al tratar de obtener sus privilegios de acceso.<br />";
                           verrorMessage+="Intente de nuevo.";
                           //openError(verrorMessage);
                           break;
                case 0:    verrorMessage ="No existen privilegios de acceso registrados.<br />";
                           verrorMessage+="Verifiquelo con el Administrador del Sistema.";
                           openError(verrorMessage);
                           break;
                case 1:    if ( (vresponse[1].backendUserAccessPrivileges.length>0) || (vidAccessPrivilegeGroup==0) ){
                                setMenu(vresponse[1].backendUserAccessPrivileges, vresponse[0].accessPrivileges, vidAccessPrivilegeGroup, vidAccessPrivilegeModule);
                           }
                           else{
                                verrorMessage ="No tiene privilegios de acceso registrados.<br />";
                                verrorMessage+="Verifiquelo con el Administrador del Sistema.";
                                openError(verrorMessage);
                           }
                           break;
                default:   verrorMessage ="Ocurri� un error al tratar de obtener sus privilegios de acceso.";
                           openError(verrorMessage);
                           break;
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            verrorMessage=verrorThrown + "<br />";
            verrorMessage+="Verifiquelo con el Administrador del Sistema.";
            //openError(verrorMessage);
		}
	});
 }

function setMenu(vbackendUserAccessPrivileges, vaccessPrivileges, vidAccessPrivilegeGroup, vidAccessPrivilegeModule)
 {
    //Purpose: It sets menu. 
    //Limitations: The user access privileges data must exist in format JSON.  
    //Returns: None.
    
    var vmenuLogoHTML ='<div class="user-image">';
		vmenuLogoHTML+='	<img src="./tools/template-3.2/global/images/profil_page/logo-default.jpg" class="img-responsive img-circle" alt="Logo">';
		vmenuLogoHTML+='</div>';
		vmenuLogoHTML+='<h4>INEF/FLDCH</h4>';
    var vcampusHTML ='<strong>INEF/FLDCH</strong> - Dashboard';
    var vmenuRightHTML ="";
    var vsloganHTML ="<strong>INEF</strong> / <strong>FLDCH</strong>";
    var vaccessPrivilegesTotal=vbackendUserAccessPrivileges.length;
    if ( vaccessPrivilegesTotal>0 ){
        var vuserName =vbackendUserAccessPrivileges[0].backendUser.name + " ";
            vuserName+=vbackendUserAccessPrivileges[0].backendUser.firstName+ " ";
            vuserName+=vbackendUserAccessPrivileges[0].backendUser.lastName;
        vmenuRightHTML ='<!-- Begin User Dropdown -->';
		vmenuRightHTML+='<li class="dropdown" id="user-header">';
		vmenuRightHTML+='	<a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">';
        vmenuRightHTML+='		<img src="./tools/template-3.2/global/images/avatars/avatar0.jpg" alt="Imagen de Usuario" title="' + vuserName  + '">';
        vmenuRightHTML+='		<span id="vuserName" class="username">' + vuserName  + '</span>';
        vmenuRightHTML+='	</a>';
        vmenuRightHTML+='	<ul class="dropdown-menu">';
        vmenuRightHTML+='		<li>';
        vmenuRightHTML+='			<a href="./frmmy-profile.php">';
        vmenuRightHTML+='				<i class="icon-user"></i>';
        vmenuRightHTML+='				<span>Mi Perfil</span>';
        vmenuRightHTML+='			</a>';
        vmenuRightHTML+='		</li>';
        vmenuRightHTML+='		<li>';
        vmenuRightHTML+='			<a href="#" onclick="exit();">';
        vmenuRightHTML+='				<i class="icon-logout"></i>';
        vmenuRightHTML+='				<span>Salir</span>';
        vmenuRightHTML+='			</a>';
        vmenuRightHTML+='		</li>';
        vmenuRightHTML+='	</ul>';
        vmenuRightHTML+='</li>';
        vmenuRightHTML+='<!-- End User Dropdown -->';
        vmenuRightHTML+='<li id="language-header">';
        vmenuRightHTML+='	<a href="#" onclick="exit();">';
        vmenuRightHTML+='		<i class="icon-logout"></i>';
        vmenuRightHTML+='		<span>Salir</span>';
        vmenuRightHTML+='	</a>';
        vmenuRightHTML+='</li>';
        $("#vexit").attr("onclick", "exit();");
        
        if ( vbackendUserAccessPrivileges[0].backendUser.campus.idCampus!=0 ){
            vmenuLogoHTML ='<div class="user-image">';
            if ( vbackendUserAccessPrivileges[0].backendUser.campus.idCampus==1 ){
                vmenuLogoHTML+='	<img src="./tools/template-3.2/global/images/profil_page/logo-inef.png" class="img-responsive img-circle" alt="Logo ' + vbackendUserAccessPrivileges[0].backendUser.campus.campus + '">';
            }
            else if ( vbackendUserAccessPrivileges[0].backendUser.campus.idCampus==2 ){
                vmenuLogoHTML+='	<img src="./tools/template-3.2/global/images/profil_page/logo-fldch.png" class="img-responsive img-circle" alt="Logo ' + vbackendUserAccessPrivileges[0].backendUser.campus.campus + '">';
            }
            vmenuLogoHTML+='</div>';
            vmenuLogoHTML+='<h4>' + vbackendUserAccessPrivileges[0].backendUser.campus.campus +'</h4>';
            vcampusHTML ='<strong>' + vbackendUserAccessPrivileges[0].backendUser.campus.campus +'</strong> - Dashboard';
            vsloganHTML = vbackendUserAccessPrivileges[0].backendUser.campus.slogan;
        }
    }
    else{
        vmenuRightHTML ='<li id="language-header">';
        vmenuRightHTML+='	<a href="./">';
        vmenuRightHTML+='		<span><i class="fa fa-sign-in m-r-xs"></i>Ingresar</span>';
        vmenuRightHTML+='	</a>';
        vmenuRightHTML+='</li>';
    }
    $("#vmenuRight").html(vmenuRightHTML);
    $("#vlogo").html(vmenuLogoHTML);
    $("#vcampusName").html(vcampusHTML);
    
    var vmenuLeftHTML="";
    if ( vidAccessPrivilegeGroup!=0 ){
        vmenuLeftHTML ='<li>';
    }
    else{
        vmenuLeftHTML ='<li class="nav-active active">';
    }
    vmenuLeftHTML+='    <a href="./">';
	vmenuLeftHTML+='       <i class="icon-home"></i><span>Inicio</span>';
    vmenuLeftHTML+='    </a>';
    vmenuLeftHTML+='</li>';
    
    var vexistGroup=false;
    var vexistSubgroup=false;
    var vlevel1="";
    var vlevel2="";
    for(var vaccessPrivilegeNumber=0; vaccessPrivilegeNumber<vaccessPrivilegesTotal; vaccessPrivilegeNumber++){
        if (vbackendUserAccessPrivileges[vaccessPrivilegeNumber].accessPrivilege.level1!=vlevel1){
            if ( vexistSubgroup ){
                vmenuLeftHTML+='    </ul>';
            }
            if ( vexistGroup ){
                vmenuLeftHTML+='</li>';
            }
            vexistSubgroup=false;
            vexistGroup=false;
        }
        else if (vbackendUserAccessPrivileges[vaccessPrivilegeNumber].accessPrivilege.level2!=vlevel2){
            vexistSubgroup=false;
        }
        vlevel1=vbackendUserAccessPrivileges[vaccessPrivilegeNumber].accessPrivilege.level1;
        vlevel2=vbackendUserAccessPrivileges[vaccessPrivilegeNumber].accessPrivilege.level2;
        if( vbackendUserAccessPrivileges[vaccessPrivilegeNumber].accessPrivilege.level2=="00" ){
            vmenuLeftHTML+=createGroup(vbackendUserAccessPrivileges[vaccessPrivilegeNumber].accessPrivilege, vaccessPrivileges, vidAccessPrivilegeGroup);
            vexistGroup=true;
            vexistSubgroup=false;
        }
        else if( vbackendUserAccessPrivileges[vaccessPrivilegeNumber].accessPrivilege.level3=="00" ){
            if ( ! vexistGroup ){
                var vgroup=getGroup(vbackendUserAccessPrivileges[vaccessPrivilegeNumber].accessPrivilege, vaccessPrivileges);
                vmenuLeftHTML+=createGroup(vgroup, vaccessPrivileges, vidAccessPrivilegeGroup);
                vexistGroup=true;
            }
            vmenuLeftHTML+=createSubGroup(vbackendUserAccessPrivileges[vaccessPrivilegeNumber].accessPrivilege, vidAccessPrivilegeModule);
            vexistSubgroup=true;
        }
        else{
            if ( ! vexistGroup ){
                var vgroup=getGroup(vbackendUserAccessPrivileges[vaccessPrivilegeNumber].accessPrivilege, vaccessPrivileges);
                vmenuLeftHTML+=createGroup(vgroup, vaccessPrivileges, vidAccessPrivilegeGroup);
                vexistGroup=true;
            }
            if ( ! vexistSubgroup ){
                var vsubgroup=getSubgroup(vbackendUserAccessPrivileges[vaccessPrivilegeNumber].accessPrivilege, vaccessPrivileges);
                if ( vsubgroup!=null ){
                    vmenuLeftHTML+=createSubGroup(vsubgroup, vidAccessPrivilegeModule);
                    vexistSubgroup=true;
                }
            }
        }
        if( vaccessPrivilegeNumber==(vaccessPrivilegesTotal-1)){
            if ( vexistSubgroup ){
                vmenuLeftHTML+='    </ul>';
            }
            if ( vexistGroup ){
                vmenuLeftHTML+='</li>';
            }
        }
    }
    $("#vmenuLeft").html(vmenuLeftHTML);
    configureMenu();
    showGeneralFooter(vsloganHTML);
 }

function createGroup(vaccessPrivilege, vaccessPrivileges, vidAccessPrivilegeGroup)
 {
    //Purpose: It creates a group (menu). 
    //Limitations: The access privilege data must exist in format JSON.  
    //Returns: HTML5 code for a group (menu).
    
    var vhaveSubgroup=haveSubgroup(vaccessPrivilege, vaccessPrivileges);
    var vhtml='<li ';
    
    if ( vhaveSubgroup ){
        vhtml+=' class="nav-parent';
        if (vaccessPrivilege.idAccessPrivilege==vidAccessPrivilegeGroup){
            vhtml+=' nav-active active';
        }
        vhtml+='"';
    }
    else if (vaccessPrivilege.idAccessPrivilege==vidAccessPrivilegeGroup){
        vhtml+=' class="nav-active active"';
    }
    
    vhtml+='>';
    vhtml+='    <a href="';
    if ( vaccessPrivilege.url!="" ){
        vhtml+=vaccessPrivilege.url;
    }
    else{
        vhtml+='#';
    }
    vhtml+='">';
    vhtml+='        <i class="glyphicon ' + vaccessPrivilege.iconName + '"></i><span>' + vaccessPrivilege.accessPrivilege + '</span>';
    if ( vhaveSubgroup ){
        vhtml+='<span class="fa arrow"></span>';
    }
    vhtml+='    </a>';
    if ( vhaveSubgroup ){
        vhtml+='    <ul class="children collapse">';
    }
    
    return vhtml;
 }

function createSubGroup(vaccessPrivilege, vidAccessPrivilegeModule)
 {
    //Purpose: It creates a subgroup (menu). 
    //Limitations: The access privilege data must exist in format JSON.  
    //Returns: HTML5 code for a subgroup (menu).
    
    var vhtml='    <li';
    
    if ( vaccessPrivilege.idAccessPrivilege==vidAccessPrivilegeModule ){
        vhtml+=' class="active"';
    }
    
    vhtml+='><a href="' + vaccessPrivilege.url + '">' + vaccessPrivilege.accessPrivilege + '</a></li>';
    return vhtml;
 }

function haveSubgroup(vaccessPrivilege, vaccessPrivileges)
 {
    //Purpose: It verifies if a group has subgroup. 
    //Limitations: The access privilege data must exist in format JSON.  
    //Returns: Boolean value (true or false).
    
    var vhaveSubgroup=false;
    
    for(var vaccessPrivilegeNumber=0; vaccessPrivilegeNumber<vaccessPrivileges.length; vaccessPrivilegeNumber++){
        if ( (vaccessPrivileges[vaccessPrivilegeNumber].level1==vaccessPrivilege.level1) && 
             (vaccessPrivileges[vaccessPrivilegeNumber].accessPrivilegeType.idAccessPrivilegeType==2) ){
            vhaveSubgroup=true;
            vaccessPrivilegeNumber=vaccessPrivileges.length;
        }
    }
    
    return vhaveSubgroup;
 }
 
function getGroup(vaccessPrivilege, vaccessPrivileges)
 {
    //Purpose: It gets the group of a access privilege. 
    //Limitations: The access privilege data must exist in format JSON.  
    //Returns: The group of a access privilege (JSON) or null value.
    
    var vgetGroup=null;
    
    for(var vaccessPrivilegeNumber=0; vaccessPrivilegeNumber<vaccessPrivileges.length; vaccessPrivilegeNumber++){
        if ( (vaccessPrivileges[vaccessPrivilegeNumber].level1==vaccessPrivilege.level1) && 
             (vaccessPrivileges[vaccessPrivilegeNumber].accessPrivilegeType.idAccessPrivilegeType==1) ){
            vgetGroup=vaccessPrivileges[vaccessPrivilegeNumber];
            vaccessPrivilegeNumber=vaccessPrivileges.length;
        }
    }
    
    return vgetGroup;
 }
 
function getSubgroup(vaccessPrivilege, vaccessPrivileges)
 {
    //Purpose: It gets the subgroup of a access privilege. 
    //Limitations: The access privilege data must exist in format JSON.  
    //Returns: The subgroup of a access privilege (JSON) or null value.
    
    var vgetSubgroup=null;
    
    for(var vaccessPrivilegeNumber=0; vaccessPrivilegeNumber<vaccessPrivileges.length; vaccessPrivilegeNumber++){
        if ( (vaccessPrivileges[vaccessPrivilegeNumber].level1==vaccessPrivilege.level1) && 
             (vaccessPrivileges[vaccessPrivilegeNumber].level2==vaccessPrivilege.level2) && 
             (vaccessPrivileges[vaccessPrivilegeNumber].accessPrivilegeType.idAccessPrivilegeType==2) ){
            vgetSubgroup=vaccessPrivileges[vaccessPrivilegeNumber];
            vaccessPrivilegeNumber=vaccessPrivileges.length;
        }
    }
    
    return vgetSubgroup;
 }

function configureMenu()
 {
    //Purpose: It configures menu.
    //Limitations: The menu items must be created.  
    //Returns: None.
    
    createSideScroll();
    
    // Add class everytime a mouse pointer hover over it
    var hoverTimeout;
    $('.nav-sidebar > li').hover(function(){
        clearTimeout(hoverTimeout);
        $(this).siblings().removeClass('nav-hover');
        $(this).addClass('nav-hover');
    }, function(){
        var $self = $(this);
        hoverTimeout = setTimeout(function(){
            $self.removeClass('nav-hover');
        }, 200);
    });
    $('.nav-sidebar > li .children').hover(function(){
        clearTimeout(hoverTimeout);
        $(this).closest('.nav-parent').siblings().removeClass('nav-hover');
        $(this).closest('.nav-parent').addClass('nav-hover');
    }, function() {
        var $self = $(this);
        hoverTimeout = setTimeout(function(){
            $(this).closest('.nav-parent').removeClass('nav-hover');
        }, 200);
    });
 }

function openError(vmessage)
 {
    //Purpose: It opens error page WEB.
    //Limitations: The error page WEB must exist.
    //Returns: None.
    
    var vhtml ='<form id="vform" style="display:hidden" action="./frmerror.php" method="POST">';
        vhtml+='	<input type="hidden" id="verrorMessage" name="verrorMessage" value="' + vmessage + '" />';
		vhtml+='</form>';
	$('<div id="vformContent"></div>').appendTo('body');
	$("#vformContent").append(vhtml);
	$("#vform").submit();
 }

function exit()
 {
    //Purpose: It exit system.
    //Limitations: The end user must accept the exit system.
    //Returns: None.
    
    var vacceptFunctionName="processExit();";
    var vcancelFunctionName="cancelExit();";
    var vmessage="�Est� seguro que desea salir del sistema?.";
    var vtitle="Salida del sistema.";
    
    vconfirmProcessExit=toastrConfirm(vacceptFunctionName, vcancelFunctionName, vmessage, vtitle);
 }

function processExit()
 {
    //Purpose: It process exit system.
    //Limitations: None.  
    //Returns: None.
    
    var vtitle="Salida del sistema.";
    vconfirmProcessExit.remove();
    toastr.clear(vconfirmProcessExit, { force: true });
    
    $.ajax({
		type: "delete",
		url: vsessionsURL + "/sessions",
		dataType: "json",
        success: function(vresponse, vtextStatus, vjqXHR){
            if( vresponse.messageNumber==1 ){
                location.href="./";
            }
            else{
                toastrAlert("Ocurri� un error al tratar de salir del sistema, intente de nuevo.", vtitle, 0);
            }
		},
		error: function(vjqXHR, vtextStatus, verrorThrown){
            toastrAlert(verrorThrown, vtitle, 0);
		}
	});
 }

function cancelExit()
 {
    //Purpose: It cancels exit system.
    //Limitations: None.
    //Returns: None.
    
    vconfirmProcessExit.remove();
    toastr.clear(vconfirmProcessExit, { force: true });
 }

function showLoginFooter()
 {
    //Purpose: It shows login footer.
    //Limitations: None.
    //Returns: None.
    
    var vhtml ='<strong><span>Copyright <span class="copyright">�</span> 2017</span></strong> | '; 
        vhtml+='<span>Todos los Derechos Reservados.</span>';
    $("#vfooterText").html(vhtml);
 }

function showGeneralFooter(vsloganHTML)
 {
    //Purpose: It shows general footer.
    //Limitations: None.
    //Returns: None.
    
    var vhtml ='<div class="copyright">';
        vhtml+='	<p class="pull-left sm-pull-reset">';
		vhtml+='		<span>Copyright <span class="copyright">�</span> 2017</span> | '; 
		vhtml+='		<span>Todos los Derechos Reservados.</span>';
		vhtml+='	</p>';
		vhtml+='	<p class="pull-right sm-pull-reset">';
		vhtml+='		<span><a href="#" class="m-r-10">' + vsloganHTML + '</a> |&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Powered By<a href="http://www.bitzero.mx" target="_blank" class="m-l-5 m-r-5"><strong>Bitzero Technologies S.A de C.V.</strong></a></span>';
		vhtml+='	</p>';
		vhtml+='</div>';
    $("#vfooterText").html(vhtml);
 }

function showErrorFooter()
 {
    //Purpose: It shows login footer.
    //Limitations: None.
    //Returns: None.
    
    var vhtml ='<div class="copyright">';
        vhtml+='	<strong><span>Copyright <span class="copyright">�</span> 2017</span></strong> | '; 
        vhtml+='	<span>Todos los Derechos Reservados.</span></div>';
        vhtml+='</div>';
    $("#vfooterText").html(vhtml);
 }
 
$('.toggle_fullscreen').click(function() {
    toggleFullScreen();
});