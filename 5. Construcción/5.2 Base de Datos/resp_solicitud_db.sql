-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: localhost    Database: solicitud_db
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `c_accessprivilege`
--

DROP TABLE IF EXISTS `c_accessprivilege`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `c_accessprivilege` (
  `id_accessPrivilege` int(10) unsigned NOT NULL DEFAULT '0',
  `id_accessPrivilegeType` int(10) unsigned NOT NULL DEFAULT '0',
  `fldaccessPrivilege` varchar(50) NOT NULL,
  `fldlevel1` varchar(2) NOT NULL,
  `fldlevel2` varchar(2) NOT NULL,
  `fldlevel3` varchar(2) NOT NULL,
  `fldlevel4` varchar(2) NOT NULL,
  `fldurl` varchar(255) DEFAULT NULL,
  `fldiconName` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_accessPrivilege`),
  KEY `c_accessPrivilege_fki01` (`id_accessPrivilegeType`),
  CONSTRAINT `c_accessPrivilege_fk01` FOREIGN KEY (`id_accessPrivilegeType`) REFERENCES `c_accessprivilegetype` (`id_accessPrivilegeType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog. Access privileges.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `c_accessprivilege`
--

LOCK TABLES `c_accessprivilege` WRITE;
/*!40000 ALTER TABLE `c_accessprivilege` DISABLE KEYS */;
INSERT INTO `c_accessprivilege` VALUES (1,1,'Catálogos','01','00','00','00',NULL,'glyphicon-folder-close'),(2,2,'Usuarios','01','01','00','00','./frmusers.php',NULL),(3,2,'Ofertas Educativas','01','02','00','00','./frmcampuses.php',NULL),(4,3,'Listado de Ofertas Educativas','01','02','01','00',NULL,NULL),(5,4,'Registrar PDF de la Oferta Educativa','01','02','01','01',NULL,NULL),(6,4,'Modificar Oferta Educativa','01','02','01','02',NULL,NULL),(7,4,'Eliminar Oferta Educativa','01','02','01','03',NULL,NULL),(8,3,'Registrar Oferta Educativa','01','02','02','00',NULL,NULL),(9,1,'Solicitudes','02','00','00','00','./frmapplicants.php','glyphicon glyphicon-list'),(10,3,'Listado de Solicitudes','02','01','01','00',NULL,NULL),(11,4,'Ver Detalles de Solicitud','02','01','01','01',NULL,NULL),(12,1,'Emails','03','00','00','00','./frmemails.php','glyphicon glyphicon-envelope'),(13,3,'Listado de Emails','03','01','01','00',NULL,NULL),(14,4,'Cancelar Email','03','01','01','01',NULL,NULL),(15,4,'Enviar Email','03','01','01','02',NULL,NULL),(16,4,'Modificar Email','03','01','01','03',NULL,NULL),(17,4,'Eliminar Email','03','01','01','04',NULL,NULL),(18,3,'Configurar Email','03','01','02','00',NULL,NULL),(19,3,'Registrar Email','03','01','03','00',NULL,NULL);
/*!40000 ALTER TABLE `c_accessprivilege` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `c_accessprivilegetype`
--

DROP TABLE IF EXISTS `c_accessprivilegetype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `c_accessprivilegetype` (
  `id_accessPrivilegeType` int(10) unsigned NOT NULL DEFAULT '0',
  `fldaccessPrivilegeType` varchar(30) NOT NULL,
  PRIMARY KEY (`id_accessPrivilegeType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog. Access privilege types.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `c_accessprivilegetype`
--

LOCK TABLES `c_accessprivilegetype` WRITE;
/*!40000 ALTER TABLE `c_accessprivilegetype` DISABLE KEYS */;
INSERT INTO `c_accessprivilegetype` VALUES (1,'Grupo'),(2,'Módulo'),(3,'Sección'),(4,'Acción');
/*!40000 ALTER TABLE `c_accessprivilegetype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `c_assignment`
--

DROP TABLE IF EXISTS `c_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `c_assignment` (
  `id_campus` int(10) unsigned NOT NULL DEFAULT '0',
  `id_educationalLevel` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_campus`,`id_educationalLevel`),
  KEY `c_assignment_fki01` (`id_campus`),
  KEY `c_assignment_fki02` (`id_educationalLevel`),
  CONSTRAINT `c_assignment_fk01` FOREIGN KEY (`id_campus`) REFERENCES `c_campus` (`id_campus`),
  CONSTRAINT `c_assignment_fk02` FOREIGN KEY (`id_educationalLevel`) REFERENCES `c_educationallevel` (`id_educationalLevel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog. Assignments. Educational levels of faculties.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `c_assignment`
--

LOCK TABLES `c_assignment` WRITE;
/*!40000 ALTER TABLE `c_assignment` DISABLE KEYS */;
INSERT INTO `c_assignment` VALUES (1,0),(1,1),(1,2),(1,3),(2,0),(2,1),(2,2),(2,3);
/*!40000 ALTER TABLE `c_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `c_backenduser`
--

DROP TABLE IF EXISTS `c_backenduser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `c_backenduser` (
  `id_user` int(10) unsigned NOT NULL DEFAULT '0',
  `id_campus` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_user`),
  KEY `c_backendUser_fki01` (`id_campus`),
  CONSTRAINT `c_backendUser_fk01` FOREIGN KEY (`id_user`) REFERENCES `c_user` (`id_user`),
  CONSTRAINT `c_backendUser_fk02` FOREIGN KEY (`id_campus`) REFERENCES `c_campus` (`id_campus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog. Backend users.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `c_backenduser`
--

LOCK TABLES `c_backenduser` WRITE;
/*!40000 ALTER TABLE `c_backenduser` DISABLE KEYS */;
INSERT INTO `c_backenduser` VALUES (2,1),(3,1),(5,1);
/*!40000 ALTER TABLE `c_backenduser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `c_backenduseraccessprivilege`
--

DROP TABLE IF EXISTS `c_backenduseraccessprivilege`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `c_backenduseraccessprivilege` (
  `id_user` int(10) unsigned NOT NULL DEFAULT '0',
  `id_accessPrivilege` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_user`,`id_accessPrivilege`),
  KEY `c_backendUserAccessPrivilege_fki01` (`id_user`),
  KEY `c_backendUserAccessPrivilege_fki02` (`id_accessPrivilege`),
  CONSTRAINT `c_backendUserAccessPrivilege_fk01` FOREIGN KEY (`id_user`) REFERENCES `c_backenduser` (`id_user`),
  CONSTRAINT `c_backendUserAccessPrivilege_fk02` FOREIGN KEY (`id_accessPrivilege`) REFERENCES `c_accessprivilege` (`id_accessPrivilege`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog. Backend users access privileges.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `c_backenduseraccessprivilege`
--

LOCK TABLES `c_backenduseraccessprivilege` WRITE;
/*!40000 ALTER TABLE `c_backenduseraccessprivilege` DISABLE KEYS */;
/*!40000 ALTER TABLE `c_backenduseraccessprivilege` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `c_campus`
--

DROP TABLE IF EXISTS `c_campus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `c_campus` (
  `id_campus` int(10) unsigned NOT NULL DEFAULT '0',
  `fldcampus` varchar(30) NOT NULL,
  `fldslogan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_campus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog. Campuses.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `c_campus`
--

LOCK TABLES `c_campus` WRITE;
/*!40000 ALTER TABLE `c_campus` DISABLE KEYS */;
INSERT INTO `c_campus` VALUES (1,'INEF','Licenciaturas y Posgrados - INEF - Excelencia Académica y Humana'),(2,'FLDCH','Facultad Libre de Derecho de Chiapas 07PSU0128K');
/*!40000 ALTER TABLE `c_campus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `c_campusemailsetting`
--

DROP TABLE IF EXISTS `c_campusemailsetting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `c_campusemailsetting` (
  `id_campus` int(10) unsigned NOT NULL DEFAULT '0',
  `fldhostname` varchar(50) NOT NULL,
  `fldsmtpPort` int(11) NOT NULL DEFAULT '0',
  `fldsmtpAuth` tinyint(1) NOT NULL DEFAULT '1',
  `fldsmtpSecure` varchar(10) DEFAULT NULL,
  `flduserName` varchar(50) NOT NULL,
  `flduserPassword` varchar(20) NOT NULL,
  PRIMARY KEY (`id_campus`),
  CONSTRAINT `c_campusEmailSetting_fk01` FOREIGN KEY (`id_campus`) REFERENCES `c_campus` (`id_campus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog. Campus email settings.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `c_campusemailsetting`
--

LOCK TABLES `c_campusemailsetting` WRITE;
/*!40000 ALTER TABLE `c_campusemailsetting` DISABLE KEYS */;
INSERT INTO `c_campusemailsetting` VALUES (1,'gator4129.hostgator.com',465,1,'ssl','soporte@bitzero.mx','50p0rt3'),(2,'gator4129.hostgator.com',465,1,'ssl','soporte@bitzero.mx','50p0rt3');
/*!40000 ALTER TABLE `c_campusemailsetting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `c_educationallevel`
--

DROP TABLE IF EXISTS `c_educationallevel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `c_educationallevel` (
  `id_educationalLevel` int(10) unsigned NOT NULL DEFAULT '0',
  `fldeducationalLevel` varchar(30) NOT NULL,
  PRIMARY KEY (`id_educationalLevel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog. Educational levels.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `c_educationallevel`
--

LOCK TABLES `c_educationallevel` WRITE;
/*!40000 ALTER TABLE `c_educationallevel` DISABLE KEYS */;
INSERT INTO `c_educationallevel` VALUES (0,'S/N'),(1,'Licenciatura'),(2,'Maestría'),(3,'Doctorado');
/*!40000 ALTER TABLE `c_educationallevel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `c_educationaloffer`
--

DROP TABLE IF EXISTS `c_educationaloffer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `c_educationaloffer` (
  `id_campus` int(10) unsigned NOT NULL DEFAULT '0',
  `id_educationalLevel` int(10) unsigned NOT NULL DEFAULT '0',
  `id_educationalOffer` int(10) unsigned NOT NULL DEFAULT '0',
  `fldeducationalOffer` varchar(90) NOT NULL,
  `fldfileName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_campus`,`id_educationalLevel`,`id_educationalOffer`),
  KEY `c_educationalOffer_fki01` (`id_campus`,`id_educationalLevel`),
  CONSTRAINT `c_educationalOffer_fk01` FOREIGN KEY (`id_campus`, `id_educationalLevel`) REFERENCES `c_assignment` (`id_campus`, `id_educationalLevel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog. Educational Offers.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `c_educationaloffer`
--

LOCK TABLES `c_educationaloffer` WRITE;
/*!40000 ALTER TABLE `c_educationaloffer` DISABLE KEYS */;
INSERT INTO `c_educationaloffer` VALUES (1,1,0,'S/O',NULL),(1,1,1,'LIC. EN CONTADURIA PUBLICA (SEMESTRAL)','34c8b7ed08.pdf'),(1,1,2,'LIC. EN ADMINISTRACIÓN (SEMESTRAL)',''),(1,1,4,'LIC. EN CONTADURÍA PÚBLICA Y FINANZAS  (CUATRIMESTRAL)',NULL),(1,2,0,'S/O',NULL),(1,2,1,'MAESTRIA EN FINANZAS CORPORATIVAS',NULL),(1,2,2,'MAESTRIA EN DERECHO CORPORATIVO',NULL),(1,2,3,'MAESTRIA EN DERECHO FISCAL',NULL),(1,2,4,'MAESTRIA EN CALIDAD',NULL),(1,2,5,'MAESTRIA EN ADMINISTRACIÓN',NULL),(1,2,6,'MAESTRIA EN DESAROLLO ORGANIZACIONAL Y HUMANO',NULL),(1,2,7,'MAESTRIA EN ALTA DIRECCION',NULL),(1,3,0,'S/O',NULL),(1,3,1,'DOCTORADO EN ADMINISTRACIÓN',NULL),(2,1,0,'S/O',NULL),(2,1,1,'LIC. EN DERECHO (SEMESTRAL)',''),(2,1,2,'LIC. EN DERECHO (CUATRIMESTRAL)',NULL),(2,1,3,'LIC. EN CIENCIA POLÍTICA (SEMESTRAL)',NULL),(2,1,4,'LIC. EN CIENCIA POLÍTICA (CUATRIMESTRAL)','34c8b7ed08.pdf'),(2,1,5,'LIC. EN CRIMINOLOGÍA (SEMESTRAL)',NULL),(2,1,6,'LIC. EN CRIMINOLOGÍA (CUATRIMESTRAL)',NULL),(2,1,7,'LIC. EN SEGURIDAD CIUDADANA (SEMESTRAL)',NULL),(2,1,8,'LIC. EN SEGURIDAD CIUDADANA (CUATRIMESTRAL)',NULL),(2,2,0,'S/O',NULL),(2,2,1,'MAESTRÍA EN PROCESO PENAL ACUSATORIO',NULL),(2,2,2,'MAESTRÍA EN DERECHO CONSTITUCIONAL Y AMPARO',NULL),(2,2,3,'MAESTRÍA EN ESTUDIOS POLÍTICOS Y SOCIALES',NULL),(2,2,4,'MAESTRÍA EN PSICOLOGÍA FORENSE',NULL),(2,2,5,'MAESTRÍA EN CRIMINALÍSTICA',NULL),(2,2,6,'MAESTRÍA EN DERECHO CIVIL',NULL),(2,2,7,'MAESTRÍA EN CIENCIAS JURÍDICO PENALES Y CRIMINOLÓGICAS',NULL),(2,2,8,'MAESTRÍA EN EDUCACIÓN',NULL),(2,2,9,'MAESTRÍA EN DERECHO AMBIENTAL, ECOLÓGICO E INDÍGENA',NULL),(2,2,10,'MAESTRÍA EN MECANISMOS ALTERNATIVOS DE SOLUCIÓN DE CONTROVERSIAS',NULL),(2,2,11,'MAESTRÍA EN DERECHOS HUMANOS Y SUS GARANTÍAS',NULL),(2,2,12,'MAESTRÍA EN EQUIDAD DE GÉNERO',NULL),(2,3,0,'S/O',NULL),(2,3,1,'DOCTORADO EN DERECHO PROCESAL',NULL),(2,3,2,'DOCTORADO EN DERECHO PÚBLICO',NULL),(2,3,3,'DOCTORADO EN CIENCIAS POLÍTICAS Y SOCIALES',NULL),(2,3,4,'DOCTORADO EN EDUCACIÓN',NULL);
/*!40000 ALTER TABLE `c_educationaloffer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `c_emailmarketingstatus`
--

DROP TABLE IF EXISTS `c_emailmarketingstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `c_emailmarketingstatus` (
  `id_emailMarketingStatus` int(10) unsigned NOT NULL DEFAULT '0',
  `fldemailMarketingStatus` varchar(20) NOT NULL,
  PRIMARY KEY (`id_emailMarketingStatus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog. Email marketing statuses.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `c_emailmarketingstatus`
--

LOCK TABLES `c_emailmarketingstatus` WRITE;
/*!40000 ALTER TABLE `c_emailmarketingstatus` DISABLE KEYS */;
INSERT INTO `c_emailmarketingstatus` VALUES (1,'Enviado'),(2,'No Enviado'),(3,'Cancelado');
/*!40000 ALTER TABLE `c_emailmarketingstatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `c_emailmarketingtype`
--

DROP TABLE IF EXISTS `c_emailmarketingtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `c_emailmarketingtype` (
  `id_emailMarketingType` int(10) unsigned NOT NULL DEFAULT '0',
  `fldemailMarketingType` varchar(20) NOT NULL,
  PRIMARY KEY (`id_emailMarketingType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog. Email marketing types.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `c_emailmarketingtype`
--

LOCK TABLES `c_emailmarketingtype` WRITE;
/*!40000 ALTER TABLE `c_emailmarketingtype` DISABLE KEYS */;
INSERT INTO `c_emailmarketingtype` VALUES (1,'Normal'),(2,'Programado');
/*!40000 ALTER TABLE `c_emailmarketingtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `c_passwordresetstatus`
--

DROP TABLE IF EXISTS `c_passwordresetstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `c_passwordresetstatus` (
  `id_passwordResetStatus` int(10) unsigned NOT NULL DEFAULT '0',
  `fldpasswordResetStatus` varchar(20) NOT NULL,
  PRIMARY KEY (`id_passwordResetStatus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog. Password reset statuses.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `c_passwordresetstatus`
--

LOCK TABLES `c_passwordresetstatus` WRITE;
/*!40000 ALTER TABLE `c_passwordresetstatus` DISABLE KEYS */;
INSERT INTO `c_passwordresetstatus` VALUES (0,'Pendiente'),(1,'Reestablecido'),(2,'Expirado');
/*!40000 ALTER TABLE `c_passwordresetstatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `c_user`
--

DROP TABLE IF EXISTS `c_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `c_user` (
  `id_user` int(10) unsigned NOT NULL DEFAULT '0',
  `id_userType` int(10) unsigned NOT NULL DEFAULT '0',
  `id_userStatus` int(10) unsigned NOT NULL DEFAULT '0',
  `fldfirstName` varchar(30) NOT NULL,
  `fldlastName` varchar(30) NOT NULL,
  `fldname` varchar(30) NOT NULL,
  `fldpassword` varchar(32) NOT NULL,
  `fldemailAccount` varchar(50) NOT NULL,
  PRIMARY KEY (`id_user`),
  KEY `c_user_fki01` (`id_userType`),
  KEY `c_user_fki02` (`id_userStatus`),
  CONSTRAINT `c_user_fk01` FOREIGN KEY (`id_userType`) REFERENCES `c_usertype` (`id_userType`),
  CONSTRAINT `c_user_fk02` FOREIGN KEY (`id_userStatus`) REFERENCES `c_userstatus` (`id_userStatus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog. Users.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `c_user`
--

LOCK TABLES `c_user` WRITE;
/*!40000 ALTER TABLE `c_user` DISABLE KEYS */;
INSERT INTO `c_user` VALUES (1,1,1,'Cabrera','Rodríguez','Edwuard Hernán','5e12d50f5b4796a72fbe79a65bbecb49','edwuardc1@gmail.com'),(2,2,1,'Cabrera','Rodríguez','Edwuard Hernán','5e12d50f5b4796a72fbe79a65bbecb49','edwuardcabrera1@hotmail.com'),(3,1,2,'.','.','soporte','855fa866d6d3f72f6a50bc213244e36d','soporte@bitzero.mx'),(4,4,2,'ca','ro','ed','44d53a20aa284dd42124e5ea31b6913d','edwuardcabrera2@hotmail.com'),(5,5,2,'sad','asdasd','asd','6f01d388837c54407f53c1dea4ec1f82','asdasdasd@asdadasd.com'),(6,4,2,'ca','ro','ed','6feaf592b8b755e56a02750a25bdd988','edwuardcabrera@hotmail.com');
/*!40000 ALTER TABLE `c_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `c_userapplicant`
--

DROP TABLE IF EXISTS `c_userapplicant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `c_userapplicant` (
  `id_user` int(10) unsigned NOT NULL DEFAULT '0',
  `fldmovilNumber` varchar(13) NOT NULL,
  PRIMARY KEY (`id_user`),
  CONSTRAINT `c_userApplicant_fk01` FOREIGN KEY (`id_user`) REFERENCES `c_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog. User applicants.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `c_userapplicant`
--

LOCK TABLES `c_userapplicant` WRITE;
/*!40000 ALTER TABLE `c_userapplicant` DISABLE KEYS */;
INSERT INTO `c_userapplicant` VALUES (4,'9613263015'),(6,'9613263015');
/*!40000 ALTER TABLE `c_userapplicant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `c_userstatus`
--

DROP TABLE IF EXISTS `c_userstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `c_userstatus` (
  `id_userStatus` int(10) unsigned NOT NULL DEFAULT '0',
  `flduserStatus` varchar(30) NOT NULL,
  PRIMARY KEY (`id_userStatus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog. User statuses.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `c_userstatus`
--

LOCK TABLES `c_userstatus` WRITE;
/*!40000 ALTER TABLE `c_userstatus` DISABLE KEYS */;
INSERT INTO `c_userstatus` VALUES (0,'Inactivo'),(1,'Activo'),(2,'Pendiente');
/*!40000 ALTER TABLE `c_userstatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `c_usertype`
--

DROP TABLE IF EXISTS `c_usertype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `c_usertype` (
  `id_userType` int(10) unsigned NOT NULL DEFAULT '0',
  `flduserType` varchar(30) NOT NULL,
  PRIMARY KEY (`id_userType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog. User types.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `c_usertype`
--

LOCK TABLES `c_usertype` WRITE;
/*!40000 ALTER TABLE `c_usertype` DISABLE KEYS */;
INSERT INTO `c_usertype` VALUES (1,'Superadministrador'),(2,'Administrador General'),(3,'Administrador de Campus'),(4,'Solicitante'),(5,'Backend');
/*!40000 ALTER TABLE `c_usertype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_applicant`
--

DROP TABLE IF EXISTS `p_applicant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_applicant` (
  `id_campus` int(10) unsigned NOT NULL DEFAULT '0',
  `id_applicant` varchar(9) NOT NULL DEFAULT '000000-00',
  `id_user` int(10) unsigned NOT NULL DEFAULT '0',
  `id_educationalLevel` int(10) unsigned NOT NULL DEFAULT '0',
  `id_educationalOffer` int(10) unsigned NOT NULL DEFAULT '0',
  `fldrequestDate` datetime NOT NULL,
  PRIMARY KEY (`id_campus`,`id_applicant`),
  KEY `p_applicant_fki01` (`id_campus`),
  KEY `p_applicant_fki02` (`id_user`),
  KEY `p_applicant_fki03` (`id_campus`,`id_educationalLevel`,`id_educationalOffer`),
  CONSTRAINT `p_applicant_fk01` FOREIGN KEY (`id_campus`) REFERENCES `c_campus` (`id_campus`),
  CONSTRAINT `p_applicant_fk02` FOREIGN KEY (`id_user`) REFERENCES `c_userapplicant` (`id_user`),
  CONSTRAINT `p_applicant_fk03` FOREIGN KEY (`id_campus`, `id_educationalLevel`, `id_educationalOffer`) REFERENCES `c_educationaloffer` (`id_campus`, `id_educationalLevel`, `id_educationalOffer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Principal. Applicants.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_applicant`
--

LOCK TABLES `p_applicant` WRITE;
/*!40000 ALTER TABLE `p_applicant` DISABLE KEYS */;
INSERT INTO `p_applicant` VALUES (1,'000001-17',4,1,2,'2017-07-21 12:45:59'),(1,'000002-17',4,1,1,'2017-07-21 12:46:44'),(1,'000003-17',4,1,2,'2017-07-21 12:49:34'),(1,'000004-17',4,1,0,'2017-07-21 12:50:05'),(1,'000005-17',4,1,1,'2017-07-21 12:50:22'),(1,'000006-17',4,1,4,'2017-07-21 15:04:34'),(1,'000007-17',4,1,4,'2017-07-21 15:05:23'),(1,'000008-17',4,1,4,'2017-07-21 15:06:25'),(1,'000009-17',6,1,1,'2017-07-25 10:05:49'),(1,'000010-17',6,1,4,'2017-07-25 10:06:10'),(1,'000011-17',6,1,2,'2017-07-25 10:06:20'),(1,'000012-17',6,1,1,'2017-07-25 10:06:26'),(1,'000013-17',6,1,4,'2017-07-25 10:08:13'),(1,'000014-17',6,2,7,'2017-07-25 10:11:28'),(2,'000001-17',4,1,4,'2017-07-21 12:48:02'),(2,'000002-17',4,1,4,'2017-07-21 12:48:49'),(2,'000003-17',4,1,4,'2017-07-21 12:50:59'),(2,'000004-17',6,1,4,'2017-07-25 10:06:45'),(2,'000005-17',6,1,6,'2017-07-25 10:07:03');
/*!40000 ALTER TABLE `p_applicant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_emailmarketing`
--

DROP TABLE IF EXISTS `p_emailmarketing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_emailmarketing` (
  `id_campus` int(10) unsigned NOT NULL DEFAULT '0',
  `id_emailMarketing` varchar(9) NOT NULL DEFAULT '000000-00',
  `id_emailMarketingType` int(10) unsigned NOT NULL DEFAULT '0',
  `id_emailMarketingStatus` int(10) unsigned NOT NULL DEFAULT '0',
  `id_userRecord` int(10) unsigned NOT NULL DEFAULT '0',
  `id_educationalLevel` int(10) unsigned NOT NULL DEFAULT '0',
  `id_educationalOffer` int(10) unsigned NOT NULL DEFAULT '0',
  `fldsubject` varchar(255) NOT NULL,
  `fldmessage` longtext NOT NULL,
  `fldrecordDate` datetime NOT NULL,
  PRIMARY KEY (`id_campus`,`id_emailMarketing`),
  KEY `p_emailMarketing_fki01` (`id_campus`),
  KEY `p_emailMarketing_fki02` (`id_emailMarketingType`),
  KEY `p_emailMarketing_fki03` (`id_emailMarketingStatus`),
  KEY `p_emailMarketing_fki04` (`id_userRecord`),
  KEY `p_emailMarketing_fki05` (`id_campus`,`id_educationalLevel`,`id_educationalOffer`),
  CONSTRAINT `p_emailMarketing_fk01` FOREIGN KEY (`id_campus`) REFERENCES `c_campus` (`id_campus`),
  CONSTRAINT `p_emailMarketing_fk02` FOREIGN KEY (`id_emailMarketingType`) REFERENCES `c_emailmarketingtype` (`id_emailMarketingType`),
  CONSTRAINT `p_emailMarketing_fk03` FOREIGN KEY (`id_emailMarketingStatus`) REFERENCES `c_emailmarketingstatus` (`id_emailMarketingStatus`),
  CONSTRAINT `p_emailMarketing_fk04` FOREIGN KEY (`id_userRecord`) REFERENCES `c_backenduser` (`id_user`),
  CONSTRAINT `p_emailMarketing_fk05` FOREIGN KEY (`id_campus`, `id_educationalLevel`, `id_educationalOffer`) REFERENCES `c_educationaloffer` (`id_campus`, `id_educationalLevel`, `id_educationalOffer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Principal. Email marketings.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_emailmarketing`
--

LOCK TABLES `p_emailmarketing` WRITE;
/*!40000 ALTER TABLE `p_emailmarketing` DISABLE KEYS */;
INSERT INTO `p_emailmarketing` VALUES (1,'000001-17',1,1,2,1,2,'Prueba 1','<p>Correo de prueba 11</p>','2017-07-11 15:49:00'),(1,'000002-17',2,2,2,1,4,'Prueba 2','<p>Correo de prueba 21</p>','2017-07-12 10:47:34'),(1,'000003-17',1,1,2,1,0,'prueba 3','<p>prueba 3</p>','2017-07-12 14:21:27'),(1,'000004-17',1,2,2,1,4,'otra prueba','<p>adeweqweqw</p>','2017-07-12 14:37:13'),(1,'000005-17',2,2,2,1,2,'das','<p>asdsadasdasd</p>','2017-08-05 14:23:40');
/*!40000 ALTER TABLE `p_emailmarketing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_emailmarketingfile`
--

DROP TABLE IF EXISTS `p_emailmarketingfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_emailmarketingfile` (
  `id_campus` int(10) unsigned NOT NULL DEFAULT '0',
  `id_emailMarketing` varchar(9) NOT NULL DEFAULT '000000-00',
  `id_emailMarketingFile` int(10) unsigned NOT NULL DEFAULT '0',
  `fldrealName` varchar(255) NOT NULL,
  `fldfisicName` varchar(255) NOT NULL,
  PRIMARY KEY (`id_campus`,`id_emailMarketing`,`id_emailMarketingFile`),
  KEY `p_emailMarketingFile_fki01` (`id_campus`,`id_emailMarketing`),
  CONSTRAINT `p_emailMarketingFile_fk01` FOREIGN KEY (`id_campus`, `id_emailMarketing`) REFERENCES `p_emailmarketing` (`id_campus`, `id_emailMarketing`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Principal. Email marketing files.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_emailmarketingfile`
--

LOCK TABLES `p_emailmarketingfile` WRITE;
/*!40000 ALTER TABLE `p_emailmarketingfile` DISABLE KEYS */;
INSERT INTO `p_emailmarketingfile` VALUES (1,'000001-17',1,'plantillas.txt','8780cc1c74.'),(1,'000002-17',1,'plantillas.txt','197e664c1e.'),(1,'000003-17',1,'plantillas.txt','93b340c368.'),(1,'000004-17',1,'plantillas.txt','0d33a1ae9f.');
/*!40000 ALTER TABLE `p_emailmarketingfile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_emailmarketingscheduled`
--

DROP TABLE IF EXISTS `p_emailmarketingscheduled`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_emailmarketingscheduled` (
  `id_campus` int(10) unsigned NOT NULL DEFAULT '0',
  `id_emailMarketing` varchar(9) NOT NULL DEFAULT '000000-00',
  `fldscheduledDate` datetime NOT NULL,
  PRIMARY KEY (`id_campus`,`id_emailMarketing`),
  CONSTRAINT `p_emailMarketingScheduled_fk01` FOREIGN KEY (`id_campus`, `id_emailMarketing`) REFERENCES `p_emailmarketing` (`id_campus`, `id_emailMarketing`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Principal. Email marketing scheduled.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_emailmarketingscheduled`
--

LOCK TABLES `p_emailmarketingscheduled` WRITE;
/*!40000 ALTER TABLE `p_emailmarketingscheduled` DISABLE KEYS */;
INSERT INTO `p_emailmarketingscheduled` VALUES (1,'000002-17','2017-07-12 10:45:00'),(1,'000005-17','2017-08-05 14:23:00');
/*!40000 ALTER TABLE `p_emailmarketingscheduled` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_emailmarketingsend`
--

DROP TABLE IF EXISTS `p_emailmarketingsend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_emailmarketingsend` (
  `id_campus` int(10) unsigned NOT NULL DEFAULT '0',
  `id_emailMarketing` varchar(9) NOT NULL DEFAULT '000000-00',
  `id_userMailing` int(10) unsigned NOT NULL DEFAULT '0',
  `fldmailingDate` datetime NOT NULL,
  PRIMARY KEY (`id_campus`,`id_emailMarketing`),
  KEY `p_emailMarketingSend_fki01` (`id_userMailing`),
  CONSTRAINT `p_emailMarketingSend_fk01` FOREIGN KEY (`id_campus`, `id_emailMarketing`) REFERENCES `p_emailmarketing` (`id_campus`, `id_emailMarketing`),
  CONSTRAINT `p_emailMarketingSend_fk02` FOREIGN KEY (`id_userMailing`) REFERENCES `c_backenduser` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Principal. Email marketing send.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_emailmarketingsend`
--

LOCK TABLES `p_emailmarketingsend` WRITE;
/*!40000 ALTER TABLE `p_emailmarketingsend` DISABLE KEYS */;
INSERT INTO `p_emailmarketingsend` VALUES (1,'000001-17',2,'2017-07-12 12:53:23'),(1,'000003-17',2,'2017-07-12 14:21:45');
/*!40000 ALTER TABLE `p_emailmarketingsend` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_passwordreset`
--

DROP TABLE IF EXISTS `p_passwordreset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_passwordreset` (
  `id_passwordReset` varchar(32) NOT NULL,
  `id_passwordResetStatus` int(10) unsigned NOT NULL DEFAULT '0',
  `id_user` int(10) unsigned NOT NULL DEFAULT '0',
  `fldeffectiveStartDate` datetime NOT NULL,
  `fldeffectiveEndDate` datetime NOT NULL,
  PRIMARY KEY (`id_passwordReset`),
  KEY `p_passwordReset_fki01` (`id_passwordResetStatus`),
  KEY `p_passwordReset_fki02` (`id_user`),
  CONSTRAINT `p_passwordReset_fk01` FOREIGN KEY (`id_passwordResetStatus`) REFERENCES `c_passwordresetstatus` (`id_passwordResetStatus`),
  CONSTRAINT `p_passwordReset_fk02` FOREIGN KEY (`id_user`) REFERENCES `c_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Principal. Password reset.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_passwordreset`
--

LOCK TABLES `p_passwordreset` WRITE;
/*!40000 ALTER TABLE `p_passwordreset` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_passwordreset` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-21 11:49:38
